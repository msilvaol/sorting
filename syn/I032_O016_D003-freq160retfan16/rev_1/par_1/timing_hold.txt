Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Sun Feb 10 01:32:20 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.010        0.000                      0                14792        2.552        0.000                       0                 16156  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 3.125}        6.250           160.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.010        0.000                      0                14792        2.552        0.000                       0                 15301  
clk_wrapper                                                                             498.562        0.000                       0                   855  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.010ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        2.552ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.010ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_14[415]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_15[415]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.224ns  (logic 0.072ns (32.143%)  route 0.152ns (67.857%))
  Logic Levels:           0  
  Clock Path Skew:        0.159ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.694ns
    Source Clock Delay      (SCD):    4.029ns
    Clock Pessimism Removal (CPR):    0.506ns
  Clock Net Delay (Source):      3.199ns (routing 1.770ns, distribution 1.429ns)
  Clock Net Delay (Destination): 3.553ns (routing 1.937ns, distribution 1.616ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=15300, routed)       3.199     4.029    shift_reg_tap_i/clk_c
    SLICE_X78Y480        FDRE                                         r  shift_reg_tap_i/sr_p.sr_14[415]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X78Y480        FDRE (Prop_FFF2_SLICEM_C_Q)
                                                      0.072     4.101 r  shift_reg_tap_i/sr_p.sr_14[415]/Q
                         net (fo=1, routed)           0.152     4.253    shift_reg_tap_i/sr_14[415]
    SLICE_X79Y478        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[415]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=15300, routed)       3.553     4.694    shift_reg_tap_i/clk_c
    SLICE_X79Y478        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[415]/C
                         clock pessimism             -0.506     4.188    
    SLICE_X79Y478        FDRE (Hold_GFF2_SLICEL_C_D)
                                                      0.055     4.243    shift_reg_tap_i/sr_p.sr_15[415]
  -------------------------------------------------------------------
                         required time                         -4.243    
                         arrival time                           4.253    
  -------------------------------------------------------------------
                         slack                                  0.010    

Slack (MET) :             0.010ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_14[415]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_15[415]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.224ns  (logic 0.072ns (32.143%)  route 0.152ns (67.857%))
  Logic Levels:           0  
  Clock Path Skew:        0.159ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.694ns
    Source Clock Delay      (SCD):    4.029ns
    Clock Pessimism Removal (CPR):    0.506ns
  Clock Net Delay (Source):      3.199ns (routing 1.770ns, distribution 1.429ns)
  Clock Net Delay (Destination): 3.553ns (routing 1.937ns, distribution 1.616ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=15300, routed)       3.199     4.029    shift_reg_tap_i/clk_c
    SLICE_X78Y480        FDRE                                         r  shift_reg_tap_i/sr_p.sr_14[415]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X78Y480        FDRE (Prop_FFF2_SLICEM_C_Q)
                                                      0.072     4.101 f  shift_reg_tap_i/sr_p.sr_14[415]/Q
                         net (fo=1, routed)           0.152     4.253    shift_reg_tap_i/sr_14[415]
    SLICE_X79Y478        FDRE                                         f  shift_reg_tap_i/sr_p.sr_15[415]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=15300, routed)       3.553     4.694    shift_reg_tap_i/clk_c
    SLICE_X79Y478        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[415]/C
                         clock pessimism             -0.506     4.188    
    SLICE_X79Y478        FDRE (Hold_GFF2_SLICEL_C_D)
                                                      0.055     4.243    shift_reg_tap_i/sr_p.sr_15[415]
  -------------------------------------------------------------------
                         required time                         -4.243    
                         arrival time                           4.253    
  -------------------------------------------------------------------
                         slack                                  0.010    

Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_3[463]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_4[463]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.199ns  (logic 0.072ns (36.181%)  route 0.127ns (63.819%))
  Logic Levels:           0  
  Clock Path Skew:        0.132ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.696ns
    Source Clock Delay      (SCD):    4.039ns
    Clock Pessimism Removal (CPR):    0.525ns
  Clock Net Delay (Source):      3.209ns (routing 1.770ns, distribution 1.439ns)
  Clock Net Delay (Destination): 3.555ns (routing 1.937ns, distribution 1.618ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=15300, routed)       3.209     4.039    shift_reg_tap_i/clk_c
    SLICE_X69Y544        FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[463]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X69Y544        FDRE (Prop_EFF2_SLICEL_C_Q)
                                                      0.072     4.111 r  shift_reg_tap_i/sr_p.sr_3[463]/Q
                         net (fo=1, routed)           0.127     4.238    shift_reg_tap_i/sr_3[463]
    SLICE_X69Y538        FDRE                                         r  shift_reg_tap_i/sr_p.sr_4[463]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=15300, routed)       3.555     4.696    shift_reg_tap_i/clk_c
    SLICE_X69Y538        FDRE                                         r  shift_reg_tap_i/sr_p.sr_4[463]/C
                         clock pessimism             -0.525     4.171    
    SLICE_X69Y538        FDRE (Hold_CFF2_SLICEL_C_D)
                                                      0.055     4.226    shift_reg_tap_i/sr_p.sr_4[463]
  -------------------------------------------------------------------
                         required time                         -4.226    
                         arrival time                           4.238    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_3[463]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_4[463]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.199ns  (logic 0.072ns (36.181%)  route 0.127ns (63.819%))
  Logic Levels:           0  
  Clock Path Skew:        0.132ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.696ns
    Source Clock Delay      (SCD):    4.039ns
    Clock Pessimism Removal (CPR):    0.525ns
  Clock Net Delay (Source):      3.209ns (routing 1.770ns, distribution 1.439ns)
  Clock Net Delay (Destination): 3.555ns (routing 1.937ns, distribution 1.618ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=15300, routed)       3.209     4.039    shift_reg_tap_i/clk_c
    SLICE_X69Y544        FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[463]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X69Y544        FDRE (Prop_EFF2_SLICEL_C_Q)
                                                      0.072     4.111 f  shift_reg_tap_i/sr_p.sr_3[463]/Q
                         net (fo=1, routed)           0.127     4.238    shift_reg_tap_i/sr_3[463]
    SLICE_X69Y538        FDRE                                         f  shift_reg_tap_i/sr_p.sr_4[463]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=15300, routed)       3.555     4.696    shift_reg_tap_i/clk_c
    SLICE_X69Y538        FDRE                                         r  shift_reg_tap_i/sr_p.sr_4[463]/C
                         clock pessimism             -0.525     4.171    
    SLICE_X69Y538        FDRE (Hold_CFF2_SLICEL_C_D)
                                                      0.055     4.226    shift_reg_tap_i/sr_p.sr_4[463]
  -------------------------------------------------------------------
                         required time                         -4.226    
                         arrival time                           4.238    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.013ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_14[339]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_15[339]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.217ns  (logic 0.072ns (33.180%)  route 0.145ns (66.820%))
  Logic Levels:           0  
  Clock Path Skew:        0.149ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.689ns
    Source Clock Delay      (SCD):    4.034ns
    Clock Pessimism Removal (CPR):    0.506ns
  Clock Net Delay (Source):      3.204ns (routing 1.770ns, distribution 1.434ns)
  Clock Net Delay (Destination): 3.548ns (routing 1.937ns, distribution 1.611ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=15300, routed)       3.204     4.034    shift_reg_tap_i/clk_c
    SLICE_X79Y483        FDRE                                         r  shift_reg_tap_i/sr_p.sr_14[339]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X79Y483        FDRE (Prop_GFF2_SLICEL_C_Q)
                                                      0.072     4.106 r  shift_reg_tap_i/sr_p.sr_14[339]/Q
                         net (fo=1, routed)           0.145     4.251    shift_reg_tap_i/sr_14[339]
    SLICE_X79Y478        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[339]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=15300, routed)       3.548     4.689    shift_reg_tap_i/clk_c
    SLICE_X79Y478        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[339]/C
                         clock pessimism             -0.506     4.183    
    SLICE_X79Y478        FDRE (Hold_AFF2_SLICEL_C_D)
                                                      0.055     4.238    shift_reg_tap_i/sr_p.sr_15[339]
  -------------------------------------------------------------------
                         required time                         -4.238    
                         arrival time                           4.251    
  -------------------------------------------------------------------
                         slack                                  0.013    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 3.125 }
Period(ns):         6.250
Sources:            { clk }

Check Type        Corner  Lib Pin     Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I    n/a            1.499         6.250       4.751      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     SRL16E/CLK  n/a            1.146         6.250       5.104      SLICE_X99Y390   muon_sorter_1/sr_3_5.roi_5_sr_3_0.pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         6.250       5.104      SLICE_X99Y390   muon_sorter_1/sr_3_5.roi_6_sr_3_0.pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         6.250       5.104      SLICE_X97Y380   muon_sorter_1/sr_3_5.roi_sr_3_0.pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         6.250       5.104      SLICE_X102Y406  muon_sorter_1/sr_3_5.sector_0_sr_3_0.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X102Y402  muon_sorter_1/sr_3_5.sector_sr_3_0.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X95Y409   muon_sorter_1/sr_3_6.roi_1_sr_3_0.pt_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X97Y369   muon_sorter_1/sr_3_6.roi_2_sr_3_0.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X95Y409   muon_sorter_1/sr_3_6.roi_3_sr_3_0.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X95Y409   muon_sorter_1/sr_3_6.roi_4_sr_3_0.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X56Y332   muon_sorter_1/sr_3_1.sector_1_sr_3_0.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X56Y332   muon_sorter_1/sr_3_1.sector_2_sr_3_0.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X56Y332   muon_sorter_1/sr_3_1.sector_sr_3_0.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X56Y332   muon_sorter_1/sr_3_3.sector_0_sr_3_0.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X102Y423  muon_sorter_1/sr_3_4.roi_0_sr_3_0.pt_1/CLK



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X77Y543          lsfr_1/output_vector_1[511]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X80Y543          lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X54Y310          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X80Y543          lsfr_1/shiftreg_vector[0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X58Y310          lsfr_1/shiftreg_vector[107]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X59Y310          lsfr_1/shiftreg_vector[108]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X77Y543          lsfr_1/output_vector_1[511]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X58Y310          lsfr_1/shiftreg_vector[107]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X59Y310          lsfr_1/shiftreg_vector[108]/C



