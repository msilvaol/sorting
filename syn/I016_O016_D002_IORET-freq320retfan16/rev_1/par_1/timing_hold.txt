Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Wed Feb 13 22:34:00 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There are 0 input ports with no input delay specified.

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.000        0.000                      0                 4759        1.287        0.000                       0                  5092  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.563}        3.125           320.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.000        0.000                      0                 4759        1.287        0.000                       0                  4750  
clk_wrapper                                                                             498.562        0.000                       0                   342  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.000ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.287ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.000ns  (arrival time - required time)
  Source:                 muon_sorter_1/sr_p.sr_1_13.pt_ret_11/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            muon_sorter_1/sr_p.sr_2_12.pt_1[0]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.333ns  (logic 0.170ns (51.051%)  route 0.163ns (48.949%))
  Logic Levels:           1  (LUT6=1)
  Clock Path Skew:        0.280ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.461ns
    Source Clock Delay      (SCD):    2.763ns
    Clock Pessimism Removal (CPR):    0.418ns
  Clock Net Delay (Source):      1.933ns (routing 1.130ns, distribution 0.803ns)
  Clock Net Delay (Destination): 2.320ns (routing 1.237ns, distribution 1.083ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=4749, routed)        1.933     2.763    muon_sorter_1/clk_c
    SLICE_X100Y479       FDRE                                         r  muon_sorter_1/sr_p.sr_1_13.pt_ret_11/C
  -------------------------------------------------------------------    -------------------
    SLICE_X100Y479       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.069     2.832 r  muon_sorter_1/sr_p.sr_1_13.pt_ret_11/Q
                         net (fo=10, routed)          0.129     2.961    muon_sorter_1/sr_1[212]
    SLICE_X102Y481       LUT6 (Prop_H6LUT_SLICEM_I3_O)
                                                      0.101     3.062 r  muon_sorter_1/sr_p.sr_2_12.pt_1_RNO[0]/O
                         net (fo=1, routed)           0.034     3.096    muon_sorter_1/top_candoi_0_0[203]
    SLICE_X102Y481       FDRE                                         r  muon_sorter_1/sr_p.sr_2_12.pt_1[0]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=4749, routed)        2.320     3.461    muon_sorter_1/clk_c
    SLICE_X102Y481       FDRE                                         r  muon_sorter_1/sr_p.sr_2_12.pt_1[0]/C
                         clock pessimism             -0.418     3.043    
    SLICE_X102Y481       FDRE (Hold_HFF_SLICEM_C_D)
                                                      0.053     3.096    muon_sorter_1/sr_p.sr_2_12.pt_1[0]
  -------------------------------------------------------------------
                         required time                         -3.096    
                         arrival time                           3.096    
  -------------------------------------------------------------------
                         slack                                  0.000    

Slack (MET) :             0.000ns  (arrival time - required time)
  Source:                 muon_sorter_1/sr_p.sr_1_13.pt_ret_11/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            muon_sorter_1/sr_p.sr_2_12.pt_1[0]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.333ns  (logic 0.170ns (51.051%)  route 0.163ns (48.949%))
  Logic Levels:           1  (LUT6=1)
  Clock Path Skew:        0.280ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.461ns
    Source Clock Delay      (SCD):    2.763ns
    Clock Pessimism Removal (CPR):    0.418ns
  Clock Net Delay (Source):      1.933ns (routing 1.130ns, distribution 0.803ns)
  Clock Net Delay (Destination): 2.320ns (routing 1.237ns, distribution 1.083ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=4749, routed)        1.933     2.763    muon_sorter_1/clk_c
    SLICE_X100Y479       FDRE                                         r  muon_sorter_1/sr_p.sr_1_13.pt_ret_11/C
  -------------------------------------------------------------------    -------------------
    SLICE_X100Y479       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.069     2.832 f  muon_sorter_1/sr_p.sr_1_13.pt_ret_11/Q
                         net (fo=10, routed)          0.129     2.961    muon_sorter_1/sr_1[212]
    SLICE_X102Y481       LUT6 (Prop_H6LUT_SLICEM_I3_O)
                                                      0.101     3.062 f  muon_sorter_1/sr_p.sr_2_12.pt_1_RNO[0]/O
                         net (fo=1, routed)           0.034     3.096    muon_sorter_1/top_candoi_0_0[203]
    SLICE_X102Y481       FDRE                                         f  muon_sorter_1/sr_p.sr_2_12.pt_1[0]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=4749, routed)        2.320     3.461    muon_sorter_1/clk_c
    SLICE_X102Y481       FDRE                                         r  muon_sorter_1/sr_p.sr_2_12.pt_1[0]/C
                         clock pessimism             -0.418     3.043    
    SLICE_X102Y481       FDRE (Hold_HFF_SLICEM_C_D)
                                                      0.053     3.096    muon_sorter_1/sr_p.sr_2_12.pt_1[0]
  -------------------------------------------------------------------
                         required time                         -3.096    
                         arrival time                           3.096    
  -------------------------------------------------------------------
                         slack                                  0.000    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_13[162]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_o/sr_p.sr_14[162]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.036ns
    Source Clock Delay      (SCD):    1.668ns
    Clock Pessimism Removal (CPR):    0.362ns
  Clock Net Delay (Source):      1.122ns (routing 0.638ns, distribution 0.484ns)
  Clock Net Delay (Destination): 1.264ns (routing 0.707ns, distribution 0.557ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=4749, routed)        1.122     1.668    shift_reg_tap_o/clk_c
    SLICE_X109Y476       FDRE                                         r  shift_reg_tap_o/sr_p.sr_13[162]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X109Y476       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.707 r  shift_reg_tap_o/sr_p.sr_13[162]/Q
                         net (fo=1, routed)           0.033     1.740    shift_reg_tap_o/sr_13[162]
    SLICE_X109Y476       FDRE                                         r  shift_reg_tap_o/sr_p.sr_14[162]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=4749, routed)        1.264     2.036    shift_reg_tap_o/clk_c
    SLICE_X109Y476       FDRE                                         r  shift_reg_tap_o/sr_p.sr_14[162]/C
                         clock pessimism             -0.362     1.674    
    SLICE_X109Y476       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.721    shift_reg_tap_o/sr_p.sr_14[162]
  -------------------------------------------------------------------
                         required time                         -1.721    
                         arrival time                           1.740    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_13[162]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_o/sr_p.sr_14[162]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.036ns
    Source Clock Delay      (SCD):    1.668ns
    Clock Pessimism Removal (CPR):    0.362ns
  Clock Net Delay (Source):      1.122ns (routing 0.638ns, distribution 0.484ns)
  Clock Net Delay (Destination): 1.264ns (routing 0.707ns, distribution 0.557ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=4749, routed)        1.122     1.668    shift_reg_tap_o/clk_c
    SLICE_X109Y476       FDRE                                         r  shift_reg_tap_o/sr_p.sr_13[162]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X109Y476       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.707 f  shift_reg_tap_o/sr_p.sr_13[162]/Q
                         net (fo=1, routed)           0.033     1.740    shift_reg_tap_o/sr_13[162]
    SLICE_X109Y476       FDRE                                         f  shift_reg_tap_o/sr_p.sr_14[162]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=4749, routed)        1.264     2.036    shift_reg_tap_o/clk_c
    SLICE_X109Y476       FDRE                                         r  shift_reg_tap_o/sr_p.sr_14[162]/C
                         clock pessimism             -0.362     1.674    
    SLICE_X109Y476       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.721    shift_reg_tap_o/sr_p.sr_14[162]
  -------------------------------------------------------------------
                         required time                         -1.721    
                         arrival time                           1.740    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_8[109]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_o/sr_p.sr_9[109]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.011ns
    Source Clock Delay      (SCD):    1.644ns
    Clock Pessimism Removal (CPR):    0.361ns
  Clock Net Delay (Source):      1.098ns (routing 0.638ns, distribution 0.460ns)
  Clock Net Delay (Destination): 1.239ns (routing 0.707ns, distribution 0.532ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=4749, routed)        1.098     1.644    shift_reg_tap_o/clk_c
    SLICE_X89Y464        FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[109]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X89Y464        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.683 r  shift_reg_tap_o/sr_p.sr_8[109]/Q
                         net (fo=1, routed)           0.033     1.716    shift_reg_tap_o/sr_8[109]
    SLICE_X89Y464        FDRE                                         r  shift_reg_tap_o/sr_p.sr_9[109]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=4749, routed)        1.239     2.011    shift_reg_tap_o/clk_c
    SLICE_X89Y464        FDRE                                         r  shift_reg_tap_o/sr_p.sr_9[109]/C
                         clock pessimism             -0.361     1.650    
    SLICE_X89Y464        FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.697    shift_reg_tap_o/sr_p.sr_9[109]
  -------------------------------------------------------------------
                         required time                         -1.697    
                         arrival time                           1.716    
  -------------------------------------------------------------------
                         slack                                  0.019    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.563 }
Period(ns):         3.125
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         3.125       1.626      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X99Y445   muon_sorter_1/sr_p.sr_1_0.pt_1[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X99Y447   muon_sorter_1/sr_p.sr_1_0.pt_1[1]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X99Y446   muon_sorter_1/sr_p.sr_1_0.pt_1[2]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X101Y445  muon_sorter_1/sr_p.sr_1_0.pt_1[3]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X99Y446   muon_sorter_1/sr_p.sr_1_0.pt_1[2]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X92Y451   muon_sorter_1/sr_p.sr_1_13.sector_ret_77/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X93Y446   muon_sorter_1/sr_p.sr_2_5.pt_1[3]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X90Y449   muon_sorter_1/sr_p.sr_2_5.roi_1[6]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X108Y511  shift_reg_tap_o/sr_p.sr_11[33]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X91Y470   muon_sorter_1/sr_p.sr_1_13.sector_ret_80/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X90Y479   muon_sorter_1/sr_p.sr_1_13.sector_ret_81/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X88Y452   muon_sorter_1/sr_p.sr_1_13.sector_ret_82/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X94Y481   muon_sorter_1/sr_p.sr_1_13.sector_ret_84/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X103Y480  muon_sorter_1/sr_p.sr_1_13.sector_ret_98/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X104Y526         reducer_1/delay_block[0][224]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X104Y526         reducer_1/delay_block[0][225]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X104Y526         reducer_1/delay_block[0][226]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X92Y515          reducer_1/delay_block[0][240]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X92Y515          reducer_1/delay_block[0][241]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X92Y515          reducer_1/delay_block[0][242]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y526         reducer_1/delay_block[0][224]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y526         reducer_1/delay_block[0][225]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y526         reducer_1/delay_block[0][226]/C



