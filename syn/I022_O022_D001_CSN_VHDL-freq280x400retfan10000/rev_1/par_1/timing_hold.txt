Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Mon Mar 11 23:19:39 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.029        0.000                      0                  778        1.511        0.000                       0                  1737  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.786}        3.572           279.955         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.029        0.000                      0                  778        1.511        0.000                       0                  1065  
clk_wrapper                                                                             498.562        0.000                       0                   672  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.029ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.511ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.029ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[238]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_17.idx_ret_14[0]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.081ns  (logic 0.039ns (48.148%)  route 0.042ns (51.852%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.101ns
    Source Clock Delay      (SCD):    1.732ns
    Clock Pessimism Removal (CPR):    0.363ns
  Clock Net Delay (Source):      1.186ns (routing 0.652ns, distribution 0.534ns)
  Clock Net Delay (Destination): 1.329ns (routing 0.718ns, distribution 0.611ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=1064, routed)        1.186     1.732    shift_reg_tap_i/clk_c
    SLICE_X96Y515        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[238]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y515        FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.039     1.771 r  shift_reg_tap_i/sr_p.sr_1[238]/Q
                         net (fo=1, routed)           0.042     1.813    dut_inst/input_slr[238]
    SLICE_X96Y515        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_14[0]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=1064, routed)        1.329     2.101    dut_inst/clk_c
    SLICE_X96Y515        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_14[0]/C
                         clock pessimism             -0.363     1.738    
    SLICE_X96Y515        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.046     1.784    dut_inst/ret_array_1_17.idx_ret_14[0]
  -------------------------------------------------------------------
                         required time                         -1.784    
                         arrival time                           1.813    
  -------------------------------------------------------------------
                         slack                                  0.029    

Slack (MET) :             0.029ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[238]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_17.idx_ret_14[0]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.081ns  (logic 0.039ns (48.148%)  route 0.042ns (51.852%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.101ns
    Source Clock Delay      (SCD):    1.732ns
    Clock Pessimism Removal (CPR):    0.363ns
  Clock Net Delay (Source):      1.186ns (routing 0.652ns, distribution 0.534ns)
  Clock Net Delay (Destination): 1.329ns (routing 0.718ns, distribution 0.611ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=1064, routed)        1.186     1.732    shift_reg_tap_i/clk_c
    SLICE_X96Y515        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[238]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y515        FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.039     1.771 f  shift_reg_tap_i/sr_p.sr_1[238]/Q
                         net (fo=1, routed)           0.042     1.813    dut_inst/input_slr[238]
    SLICE_X96Y515        FDRE                                         f  dut_inst/ret_array_1_17.idx_ret_14[0]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=1064, routed)        1.329     2.101    dut_inst/clk_c
    SLICE_X96Y515        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_14[0]/C
                         clock pessimism             -0.363     1.738    
    SLICE_X96Y515        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.046     1.784    dut_inst/ret_array_1_17.idx_ret_14[0]
  -------------------------------------------------------------------
                         required time                         -1.784    
                         arrival time                           1.813    
  -------------------------------------------------------------------
                         slack                                  0.029    

Slack (MET) :             0.029ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[134]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_17.idx_ret_18[0]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.081ns  (logic 0.039ns (48.148%)  route 0.042ns (51.852%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.084ns
    Source Clock Delay      (SCD):    1.715ns
    Clock Pessimism Removal (CPR):    0.363ns
  Clock Net Delay (Source):      1.169ns (routing 0.652ns, distribution 0.517ns)
  Clock Net Delay (Destination): 1.312ns (routing 0.718ns, distribution 0.594ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=1064, routed)        1.169     1.715    shift_reg_tap_i/clk_c
    SLICE_X91Y509        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[134]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X91Y509        FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.039     1.754 r  shift_reg_tap_i/sr_p.sr_1[134]/Q
                         net (fo=1, routed)           0.042     1.796    dut_inst/input_slr[134]
    SLICE_X91Y509        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_18[0]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=1064, routed)        1.312     2.084    dut_inst/clk_c
    SLICE_X91Y509        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_18[0]/C
                         clock pessimism             -0.363     1.721    
    SLICE_X91Y509        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.046     1.767    dut_inst/ret_array_1_17.idx_ret_18[0]
  -------------------------------------------------------------------
                         required time                         -1.767    
                         arrival time                           1.796    
  -------------------------------------------------------------------
                         slack                                  0.029    

Slack (MET) :             0.029ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[134]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_17.idx_ret_18[0]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.081ns  (logic 0.039ns (48.148%)  route 0.042ns (51.852%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.084ns
    Source Clock Delay      (SCD):    1.715ns
    Clock Pessimism Removal (CPR):    0.363ns
  Clock Net Delay (Source):      1.169ns (routing 0.652ns, distribution 0.517ns)
  Clock Net Delay (Destination): 1.312ns (routing 0.718ns, distribution 0.594ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=1064, routed)        1.169     1.715    shift_reg_tap_i/clk_c
    SLICE_X91Y509        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[134]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X91Y509        FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.039     1.754 f  shift_reg_tap_i/sr_p.sr_1[134]/Q
                         net (fo=1, routed)           0.042     1.796    dut_inst/input_slr[134]
    SLICE_X91Y509        FDRE                                         f  dut_inst/ret_array_1_17.idx_ret_18[0]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=1064, routed)        1.312     2.084    dut_inst/clk_c
    SLICE_X91Y509        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_18[0]/C
                         clock pessimism             -0.363     1.721    
    SLICE_X91Y509        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.046     1.767    dut_inst/ret_array_1_17.idx_ret_18[0]
  -------------------------------------------------------------------
                         required time                         -1.767    
                         arrival time                           1.796    
  -------------------------------------------------------------------
                         slack                                  0.029    

Slack (MET) :             0.030ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[256]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_17.idx_ret_14[14]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.082ns  (logic 0.039ns (47.561%)  route 0.043ns (52.439%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.100ns
    Source Clock Delay      (SCD):    1.730ns
    Clock Pessimism Removal (CPR):    0.364ns
  Clock Net Delay (Source):      1.184ns (routing 0.652ns, distribution 0.532ns)
  Clock Net Delay (Destination): 1.328ns (routing 0.718ns, distribution 0.610ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=1064, routed)        1.184     1.730    shift_reg_tap_i/clk_c
    SLICE_X96Y508        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[256]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y508        FDRE (Prop_GFF_SLICEL_C_Q)
                                                      0.039     1.769 r  shift_reg_tap_i/sr_p.sr_1[256]/Q
                         net (fo=1, routed)           0.043     1.812    dut_inst/input_slr[256]
    SLICE_X96Y508        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_14[14]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=1064, routed)        1.328     2.100    dut_inst/clk_c
    SLICE_X96Y508        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_14[14]/C
                         clock pessimism             -0.364     1.736    
    SLICE_X96Y508        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.046     1.782    dut_inst/ret_array_1_17.idx_ret_14[14]
  -------------------------------------------------------------------
                         required time                         -1.782    
                         arrival time                           1.812    
  -------------------------------------------------------------------
                         slack                                  0.030    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.786 }
Period(ns):         3.572
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location       Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         3.572       2.073      BUFGCE_X1Y224  clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X89Y531  dut_inst/ret_array_1_0.pt_ret[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X89Y530  dut_inst/ret_array_1_0.pt_ret[1]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X92Y530  dut_inst/ret_array_1_0.pt_ret[2]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X89Y532  dut_inst/ret_array_1_0.pt_ret[3]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X89Y530  dut_inst/ret_array_1_0.pt_ret[1]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X92Y530  dut_inst/ret_array_1_0.pt_ret_3[0]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X92Y532  dut_inst/ret_array_1_0.pt_ret_3[2]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X93Y515  dut_inst/ret_array_1_17.idx_ret_19[2]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X94Y515  dut_inst/ret_array_1_17.idx_ret_20[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X92Y530  dut_inst/ret_array_1_0.pt_ret[2]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X92Y530  dut_inst/ret_array_1_0.pt_ret_3[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X88Y532  dut_inst/ret_array_1_0.pt_ret_3[3]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X90Y526  dut_inst/ret_array_1_0.pt_ret_7_rep_30/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X93Y533  dut_inst/ret_array_1_11.idx_ret_1/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X98Y511          lsfr_1/shiftreg_vector[278]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X98Y511          lsfr_1/shiftreg_vector[279]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X96Y514          lsfr_1/shiftreg_vector[27]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X89Y531          lsfr_1/shiftreg_vector[2]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X87Y535          reducer_1/delay_block[0][228]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X87Y535          reducer_1/delay_block[0][229]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X98Y511          lsfr_1/shiftreg_vector[278]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X98Y511          lsfr_1/shiftreg_vector[279]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X98Y511          lsfr_1/shiftreg_vector[280]/C



