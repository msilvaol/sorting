Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Tue Feb  5 22:41:27 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.004        0.000                      0                 8608        5.975        0.000                       0                  9450  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 6.250}        12.500          80.000          
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.004        0.000                      0                 8608        5.975        0.000                       0                  8851  
clk_wrapper                                                                             498.562        0.000                       0                   599  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.004ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        5.975ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.004ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_6[122]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_7[122]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.170ns  (logic 0.071ns (41.765%)  route 0.099ns (58.235%))
  Logic Levels:           0  
  Clock Path Skew:        0.111ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.348ns
    Source Clock Delay      (SCD):    2.778ns
    Clock Pessimism Removal (CPR):    0.459ns
  Clock Net Delay (Source):      1.948ns (routing 1.158ns, distribution 0.790ns)
  Clock Net Delay (Destination): 2.207ns (routing 1.268ns, distribution 0.939ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=8850, routed)        1.948     2.778    shift_reg_tap_o/clk_c
    SLICE_X83Y510        FDRE                                         r  shift_reg_tap_o/sr_p.sr_6[122]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X83Y510        FDRE (Prop_BFF2_SLICEL_C_Q)
                                                      0.071     2.849 r  shift_reg_tap_o/sr_p.sr_6[122]/Q
                         net (fo=1, routed)           0.099     2.948    shift_reg_tap_o/sr_6[122]
    SLICE_X82Y510        FDRE                                         r  shift_reg_tap_o/sr_p.sr_7[122]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=8850, routed)        2.207     3.348    shift_reg_tap_o/clk_c
    SLICE_X82Y510        FDRE                                         r  shift_reg_tap_o/sr_p.sr_7[122]/C
                         clock pessimism             -0.459     2.889    
    SLICE_X82Y510        FDRE (Hold_GFF2_SLICEL_C_D)
                                                      0.055     2.944    shift_reg_tap_o/sr_p.sr_7[122]
  -------------------------------------------------------------------
                         required time                         -2.944    
                         arrival time                           2.948    
  -------------------------------------------------------------------
                         slack                                  0.004    

Slack (MET) :             0.004ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_6[122]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_7[122]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.170ns  (logic 0.071ns (41.765%)  route 0.099ns (58.235%))
  Logic Levels:           0  
  Clock Path Skew:        0.111ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.348ns
    Source Clock Delay      (SCD):    2.778ns
    Clock Pessimism Removal (CPR):    0.459ns
  Clock Net Delay (Source):      1.948ns (routing 1.158ns, distribution 0.790ns)
  Clock Net Delay (Destination): 2.207ns (routing 1.268ns, distribution 0.939ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=8850, routed)        1.948     2.778    shift_reg_tap_o/clk_c
    SLICE_X83Y510        FDRE                                         r  shift_reg_tap_o/sr_p.sr_6[122]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X83Y510        FDRE (Prop_BFF2_SLICEL_C_Q)
                                                      0.071     2.849 f  shift_reg_tap_o/sr_p.sr_6[122]/Q
                         net (fo=1, routed)           0.099     2.948    shift_reg_tap_o/sr_6[122]
    SLICE_X82Y510        FDRE                                         f  shift_reg_tap_o/sr_p.sr_7[122]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=8850, routed)        2.207     3.348    shift_reg_tap_o/clk_c
    SLICE_X82Y510        FDRE                                         r  shift_reg_tap_o/sr_p.sr_7[122]/C
                         clock pessimism             -0.459     2.889    
    SLICE_X82Y510        FDRE (Hold_GFF2_SLICEL_C_D)
                                                      0.055     2.944    shift_reg_tap_o/sr_p.sr_7[122]
  -------------------------------------------------------------------
                         required time                         -2.944    
                         arrival time                           2.948    
  -------------------------------------------------------------------
                         slack                                  0.004    

Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_5[226]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_6[226]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.173ns  (logic 0.070ns (40.462%)  route 0.103ns (59.538%))
  Logic Levels:           0  
  Clock Path Skew:        0.106ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.353ns
    Source Clock Delay      (SCD):    2.788ns
    Clock Pessimism Removal (CPR):    0.459ns
  Clock Net Delay (Source):      1.958ns (routing 1.158ns, distribution 0.800ns)
  Clock Net Delay (Destination): 2.212ns (routing 1.268ns, distribution 0.944ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=8850, routed)        1.958     2.788    shift_reg_tap_o/clk_c
    SLICE_X83Y517        FDRE                                         r  shift_reg_tap_o/sr_p.sr_5[226]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X83Y517        FDRE (Prop_DFF_SLICEL_C_Q)
                                                      0.070     2.858 r  shift_reg_tap_o/sr_p.sr_5[226]/Q
                         net (fo=1, routed)           0.103     2.961    shift_reg_tap_o/sr_5[226]
    SLICE_X82Y517        FDRE                                         r  shift_reg_tap_o/sr_p.sr_6[226]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=8850, routed)        2.212     3.353    shift_reg_tap_o/clk_c
    SLICE_X82Y517        FDRE                                         r  shift_reg_tap_o/sr_p.sr_6[226]/C
                         clock pessimism             -0.459     2.894    
    SLICE_X82Y517        FDRE (Hold_CFF2_SLICEL_C_D)
                                                      0.055     2.949    shift_reg_tap_o/sr_p.sr_6[226]
  -------------------------------------------------------------------
                         required time                         -2.949    
                         arrival time                           2.961    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_5[226]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_6[226]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.173ns  (logic 0.070ns (40.462%)  route 0.103ns (59.538%))
  Logic Levels:           0  
  Clock Path Skew:        0.106ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.353ns
    Source Clock Delay      (SCD):    2.788ns
    Clock Pessimism Removal (CPR):    0.459ns
  Clock Net Delay (Source):      1.958ns (routing 1.158ns, distribution 0.800ns)
  Clock Net Delay (Destination): 2.212ns (routing 1.268ns, distribution 0.944ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=8850, routed)        1.958     2.788    shift_reg_tap_o/clk_c
    SLICE_X83Y517        FDRE                                         r  shift_reg_tap_o/sr_p.sr_5[226]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X83Y517        FDRE (Prop_DFF_SLICEL_C_Q)
                                                      0.070     2.858 f  shift_reg_tap_o/sr_p.sr_5[226]/Q
                         net (fo=1, routed)           0.103     2.961    shift_reg_tap_o/sr_5[226]
    SLICE_X82Y517        FDRE                                         f  shift_reg_tap_o/sr_p.sr_6[226]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=8850, routed)        2.212     3.353    shift_reg_tap_o/clk_c
    SLICE_X82Y517        FDRE                                         r  shift_reg_tap_o/sr_p.sr_6[226]/C
                         clock pessimism             -0.459     2.894    
    SLICE_X82Y517        FDRE (Hold_CFF2_SLICEL_C_D)
                                                      0.055     2.949    shift_reg_tap_o/sr_p.sr_6[226]
  -------------------------------------------------------------------
                         required time                         -2.949    
                         arrival time                           2.961    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.014ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_14[209]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_15[209]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.170ns  (logic 0.069ns (40.588%)  route 0.101ns (59.412%))
  Logic Levels:           0  
  Clock Path Skew:        0.101ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.362ns
    Source Clock Delay      (SCD):    2.803ns
    Clock Pessimism Removal (CPR):    0.459ns
  Clock Net Delay (Source):      1.973ns (routing 1.158ns, distribution 0.815ns)
  Clock Net Delay (Destination): 2.221ns (routing 1.268ns, distribution 0.953ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=8850, routed)        1.973     2.803    shift_reg_tap_i/clk_c
    SLICE_X85Y501        FDRE                                         r  shift_reg_tap_i/sr_p.sr_14[209]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X85Y501        FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     2.872 r  shift_reg_tap_i/sr_p.sr_14[209]/Q
                         net (fo=1, routed)           0.101     2.973    shift_reg_tap_i/sr_14[209]
    SLICE_X86Y501        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[209]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=8850, routed)        2.221     3.362    shift_reg_tap_i/clk_c
    SLICE_X86Y501        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[209]/C
                         clock pessimism             -0.459     2.903    
    SLICE_X86Y501        FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.055     2.958    shift_reg_tap_i/sr_p.sr_15[209]
  -------------------------------------------------------------------
                         required time                         -2.958    
                         arrival time                           2.973    
  -------------------------------------------------------------------
                         slack                                  0.014    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 6.250 }
Period(ns):         12.500
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         12.500      11.001     BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X102Y513  muon_sorter_1/sr_p.sr_1_11.pt_ret_218/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X102Y512  muon_sorter_1/sr_p.sr_1_11.pt_ret_223/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X97Y511   muon_sorter_1/sr_p.sr_1_12.sector_ret_513/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X90Y520   muon_sorter_1/sr_p.sr_1_9.roi_ret_180/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X86Y520   muon_sorter_1/sr_p.sr_1_9.roi_ret_188/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X86Y520   muon_sorter_1/sr_p.sr_1_9.roi_ret_208/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X101Y532  shift_reg_tap_i/sr_p.sr_10[114]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X102Y531  shift_reg_tap_i/sr_p.sr_10[115]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X107Y500  shift_reg_tap_i/sr_p.sr_10[116]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X100Y534  shift_reg_tap_i/sr_p.sr_10[111]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X79Y510   shift_reg_tap_i/sr_p.sr_13[190]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X82Y501   shift_reg_tap_i/sr_p.sr_13[195]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X103Y534  shift_reg_tap_i/sr_p.sr_5[113]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X104Y534  shift_reg_tap_i/sr_p.sr_5[114]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X110Y532         lsfr_1/output_vector_1[255]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X110Y533         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X104Y528         lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X105Y534         lsfr_1/shiftreg_vector[116]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X105Y534         lsfr_1/shiftreg_vector[117]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X109Y533         lsfr_1/shiftreg_vector[125]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X110Y532         lsfr_1/output_vector_1[255]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X105Y532         lsfr_1/shiftreg_vector[104]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y535         lsfr_1/shiftreg_vector[105]/C



