Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Sat Mar  2 21:44:31 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_utilization -hierarchical -file hier_utilization.txt
| Design       : bitonic_sorter_16_top
| Device       : xcvu9pflgc2104-1
| Design State : Routed
------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+------------------------------------------+-------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
|                 Instance                 |                            Module                           | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+------------------------------------------+-------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
| bitonic_sorter_16_top                    |                                                       (top) |       1274 |       1266 |       0 |    8 | 1631 |      0 |      0 |    0 |            0 |
|   (bitonic_sorter_16_top)                |                                                       (top) |          0 |          0 |       0 |    0 |  416 |      0 |      0 |    0 |            0 |
|   dut_inst                               | muon_sorter_I016_O016_D003_BITONIC-freq160retfan10000_rev_1 |       1204 |       1196 |       0 |    8 |  728 |      0 |      0 |    0 |            0 |
|     (dut_inst)                           | muon_sorter_I016_O016_D003_BITONIC-freq160retfan10000_rev_1 |          8 |          0 |       0 |    8 |  728 |      0 |      0 |    0 |            0 |
|     sorter_inst                          |                                      bitonic_sort_16s_1s_8s |       1196 |       1196 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|       genblk1.bitonic_merge_i            |                                     bitonic_merge_16s_1s_8s |        462 |        462 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_merge_high       |                                    bitonic_merge_8s_1s_4s_1 |        165 |        165 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_high     |                                    bitonic_merge_4s_1s_2s_6 |         55 |         55 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                   bitonic_merge_2s_1s_1s_18 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_54 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                   bitonic_merge_2s_1s_1s_17 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_53 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_52 |          6 |          6 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_51 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_low      |                                    bitonic_merge_4s_1s_2s_5 |         55 |         55 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                   bitonic_merge_2s_1s_1s_16 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_50 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                   bitonic_merge_2s_1s_1s_15 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_49 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_48 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_47 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[0].compare_i     |                                               compare_1s_45 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[1].compare_i     |                                               compare_1s_46 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[2].compare_i     |                                               compare_1s_43 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[3].compare_i     |                                               compare_1s_44 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_merge_low        |                                    bitonic_merge_8s_1s_4s_0 |        174 |        174 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_high     |                                    bitonic_merge_4s_1s_2s_4 |         61 |         61 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                   bitonic_merge_2s_1s_1s_14 |         24 |         24 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_42 |         24 |         24 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                   bitonic_merge_2s_1s_1s_13 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_41 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_40 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_39 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_low      |                                    bitonic_merge_4s_1s_2s_3 |         63 |         63 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                   bitonic_merge_2s_1s_1s_12 |         25 |         25 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_38 |         25 |         25 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                   bitonic_merge_2s_1s_1s_11 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_37 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_36 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_35 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[0].compare_i     |                                               compare_1s_33 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[1].compare_i     |                                               compare_1s_34 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[2].compare_i     |                                               compare_1s_31 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[3].compare_i     |                                               compare_1s_32 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[0].compare_i       |                                               compare_1s_29 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[1].compare_i       |                                               compare_1s_30 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[2].compare_i       |                                               compare_1s_27 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[3].compare_i       |                                               compare_1s_28 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[4].compare_i       |                                               compare_1s_25 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[5].compare_i       |                                               compare_1s_26 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[6].compare_i       |                                               compare_1s_23 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[7].compare_i       |                                               compare_1s_24 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|       genblk1.bitonic_sort_high          |                                       bitonic_sort_8s_0s_4s |        366 |        366 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_merge_i          |                                      bitonic_merge_8s_0s_4s |        163 |        163 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_high     |                                    bitonic_merge_4s_0s_2s_2 |         57 |         57 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                   bitonic_merge_2s_0s_1s_10 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_0s_22 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_0s_1s_9 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_0s_21 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_0s_20 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_0s_19 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_low      |                                    bitonic_merge_4s_0s_2s_1 |         59 |         59 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_0s_1s_8 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_0s_18 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_0s_1s_7 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_0s_17 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_0s_16 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_0s_15 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[0].compare_i     |                                               compare_0s_13 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[1].compare_i     |                                               compare_0s_14 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[2].compare_i     |                                               compare_0s_11 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[3].compare_i     |                                               compare_0s_12 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_sort_high        |                                     bitonic_sort_4s_0s_2s_0 |        101 |        101 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_i        |                                    bitonic_merge_4s_0s_2s_0 |         57 |         57 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_0s_1s_6 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_0s_10 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_0s_1s_5 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_9 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                                compare_0s_8 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                                compare_0s_7 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_high      |                                     bitonic_sort_2s_0s_1s_2 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                    bitonic_merge_2s_0s_1s_4 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_6 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_low       |                                     bitonic_sort_2s_1s_1s_2 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                   bitonic_merge_2s_1s_1s_10 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_22 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_sort_low         |                                     bitonic_sort_4s_1s_2s_0 |        102 |        102 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_i        |                                    bitonic_merge_4s_1s_2s_2 |         58 |         58 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_1s_1s_9 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_21 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_1s_1s_8 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_20 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_19 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_18 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_high      |                                     bitonic_sort_2s_0s_1s_1 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                    bitonic_merge_2s_0s_1s_3 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_5 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_low       |                                     bitonic_sort_2s_1s_1s_1 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                    bitonic_merge_2s_1s_1s_7 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_17 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|       genblk1.bitonic_sort_low           |                                       bitonic_sort_8s_1s_4s |        368 |        368 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_merge_i          |                                      bitonic_merge_8s_1s_4s |        167 |        167 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_high     |                                    bitonic_merge_4s_1s_2s_1 |         57 |         57 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_1s_1s_6 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_16 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_1s_1s_5 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_15 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_14 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_13 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_low      |                                    bitonic_merge_4s_1s_2s_0 |         52 |         52 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_1s_1s_4 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_12 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_1s_1s_3 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_11 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_10 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                                compare_1s_9 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[0].compare_i     |                                                compare_1s_7 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[1].compare_i     |                                                compare_1s_8 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[2].compare_i     |                                                compare_1s_5 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[3].compare_i     |                                                compare_1s_6 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_sort_high        |                                       bitonic_sort_4s_0s_2s |        101 |        101 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_i        |                                      bitonic_merge_4s_0s_2s |         57 |         57 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_0s_1s_2 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_4 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_0s_1s_1 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_3 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                                compare_0s_2 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                                compare_0s_1 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_high      |                                     bitonic_sort_2s_0s_1s_0 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                    bitonic_merge_2s_0s_1s_0 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_0 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_low       |                                     bitonic_sort_2s_1s_1s_0 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                    bitonic_merge_2s_1s_1s_2 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_1s_4 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_sort_low         |                                       bitonic_sort_4s_1s_2s |        100 |        100 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_i        |                                      bitonic_merge_4s_1s_2s |         55 |         55 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_1s_1s_1 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_1s_3 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_1s_1s_0 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_1s_2 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                                compare_1s_1 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                                compare_1s_0 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_high      |                                       bitonic_sort_2s_0s_1s |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                      bitonic_merge_2s_0s_1s |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                  compare_0s |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_low       |                                       bitonic_sort_2s_1s_1s |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                      bitonic_merge_2s_1s_1s |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                  compare_1s |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   input_lfsr                             |                                                   lfsr_208s |          1 |          1 |       0 |    0 |  209 |      0 |      0 |    0 |            0 |
|   output_reducer                         |                                                  reducer_4s |         69 |         69 |       0 |    0 |  278 |      0 |      0 |    0 |            0 |
+------------------------------------------+-------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
* Note: The sum of lower-level cells may be larger than their parent cells total, due to cross-hierarchy LUT combining


