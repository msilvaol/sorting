Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Tue Mar  5 22:29:20 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper_csn
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+------+--------+
|  Fanout | Nets |      % |
+---------+------+--------+
|       1 |  828 |  32.84 |
|       2 |  630 |  24.99 |
|       3 |  448 |  17.77 |
|       4 |  222 |   8.80 |
|    5-10 |  295 |  11.70 |
|   11-50 |   98 |   3.88 |
|  51-100 |    0 |   0.00 |
| 101-500 |    0 |   0.00 |
|    >500 |    0 |   0.00 |
+---------+------+--------+
|     ALL | 2521 | 100.00 |
+---------+------+--------+


