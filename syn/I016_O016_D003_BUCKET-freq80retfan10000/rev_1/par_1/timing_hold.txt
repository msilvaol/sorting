Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Fri Feb 22 00:25:51 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_bucket
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 250 combinational loops in the design. (HIGH)


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.002        0.000                      0                 1893        5.677        0.000                       0                  2749  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 6.250}        12.500          80.000          
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.002        0.000                      0                 1893        5.677        0.000                       0                  2150  
clk_wrapper                                                                             498.562        0.000                       0                   599  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.002ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        5.677ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.002ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_1[61]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_2[61]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.318ns  (logic 0.070ns (22.013%)  route 0.248ns (77.987%))
  Logic Levels:           0  
  Clock Path Skew:        0.263ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.381ns
    Source Clock Delay      (SCD):    2.717ns
    Clock Pessimism Removal (CPR):    0.401ns
  Clock Net Delay (Source):      1.887ns (routing 0.932ns, distribution 0.955ns)
  Clock Net Delay (Destination): 2.240ns (routing 1.022ns, distribution 1.218ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X5Y8 (CLOCK_ROOT)    net (fo=2149, routed)        1.887     2.717    shift_reg_tap_o/clk_c
    SLICE_X156Y538       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[61]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X156Y538       FDRE (Prop_DFF_SLICEM_C_Q)
                                                      0.070     2.787 r  shift_reg_tap_o/sr_p.sr_1[61]/Q
                         net (fo=1, routed)           0.248     3.035    shift_reg_tap_o/sr_1[61]
    SLICE_X152Y542       FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[61]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X5Y8 (CLOCK_ROOT)    net (fo=2149, routed)        2.240     3.381    shift_reg_tap_o/clk_c
    SLICE_X152Y542       FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[61]/C
                         clock pessimism             -0.401     2.980    
    SLICE_X152Y542       FDRE (Hold_EFF_SLICEM_C_D)
                                                      0.053     3.033    shift_reg_tap_o/sr_p.sr_2[61]
  -------------------------------------------------------------------
                         required time                         -3.033    
                         arrival time                           3.035    
  -------------------------------------------------------------------
                         slack                                  0.002    

Slack (MET) :             0.002ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_1[61]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_2[61]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.318ns  (logic 0.070ns (22.013%)  route 0.248ns (77.987%))
  Logic Levels:           0  
  Clock Path Skew:        0.263ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.381ns
    Source Clock Delay      (SCD):    2.717ns
    Clock Pessimism Removal (CPR):    0.401ns
  Clock Net Delay (Source):      1.887ns (routing 0.932ns, distribution 0.955ns)
  Clock Net Delay (Destination): 2.240ns (routing 1.022ns, distribution 1.218ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X5Y8 (CLOCK_ROOT)    net (fo=2149, routed)        1.887     2.717    shift_reg_tap_o/clk_c
    SLICE_X156Y538       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[61]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X156Y538       FDRE (Prop_DFF_SLICEM_C_Q)
                                                      0.070     2.787 f  shift_reg_tap_o/sr_p.sr_1[61]/Q
                         net (fo=1, routed)           0.248     3.035    shift_reg_tap_o/sr_1[61]
    SLICE_X152Y542       FDRE                                         f  shift_reg_tap_o/sr_p.sr_2[61]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X5Y8 (CLOCK_ROOT)    net (fo=2149, routed)        2.240     3.381    shift_reg_tap_o/clk_c
    SLICE_X152Y542       FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[61]/C
                         clock pessimism             -0.401     2.980    
    SLICE_X152Y542       FDRE (Hold_EFF_SLICEM_C_D)
                                                      0.053     3.033    shift_reg_tap_o/sr_p.sr_2[61]
  -------------------------------------------------------------------
                         required time                         -3.033    
                         arrival time                           3.035    
  -------------------------------------------------------------------
                         slack                                  0.002    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_3[148]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            muon_cand_9.pt_rep_27[0]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.998ns
    Source Clock Delay      (SCD):    1.633ns
    Clock Pessimism Removal (CPR):    0.359ns
  Clock Net Delay (Source):      1.087ns (routing 0.523ns, distribution 0.564ns)
  Clock Net Delay (Destination): 1.226ns (routing 0.583ns, distribution 0.643ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X5Y8 (CLOCK_ROOT)    net (fo=2149, routed)        1.087     1.633    shift_reg_tap_i/clk_c
    SLICE_X157Y495       FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[148]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X157Y495       FDRE (Prop_DFF_SLICEL_C_Q)
                                                      0.039     1.672 r  shift_reg_tap_i/sr_p.sr_3[148]/Q
                         net (fo=2, routed)           0.033     1.705    input_slr[148]
    SLICE_X157Y495       FDRE                                         r  muon_cand_9.pt_rep_27[0]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X5Y8 (CLOCK_ROOT)    net (fo=2149, routed)        1.226     1.998    clk_c
    SLICE_X157Y495       FDRE                                         r  muon_cand_9.pt_rep_27[0]/C
                         clock pessimism             -0.359     1.639    
    SLICE_X157Y495       FDRE (Hold_CFF2_SLICEL_C_D)
                                                      0.047     1.686    muon_cand_9.pt_rep_27[0]
  -------------------------------------------------------------------
                         required time                         -1.686    
                         arrival time                           1.705    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_3[148]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            muon_cand_9.pt_rep_27[0]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.998ns
    Source Clock Delay      (SCD):    1.633ns
    Clock Pessimism Removal (CPR):    0.359ns
  Clock Net Delay (Source):      1.087ns (routing 0.523ns, distribution 0.564ns)
  Clock Net Delay (Destination): 1.226ns (routing 0.583ns, distribution 0.643ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X5Y8 (CLOCK_ROOT)    net (fo=2149, routed)        1.087     1.633    shift_reg_tap_i/clk_c
    SLICE_X157Y495       FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[148]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X157Y495       FDRE (Prop_DFF_SLICEL_C_Q)
                                                      0.039     1.672 f  shift_reg_tap_i/sr_p.sr_3[148]/Q
                         net (fo=2, routed)           0.033     1.705    input_slr[148]
    SLICE_X157Y495       FDRE                                         f  muon_cand_9.pt_rep_27[0]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X5Y8 (CLOCK_ROOT)    net (fo=2149, routed)        1.226     1.998    clk_c
    SLICE_X157Y495       FDRE                                         r  muon_cand_9.pt_rep_27[0]/C
                         clock pessimism             -0.359     1.639    
    SLICE_X157Y495       FDRE (Hold_CFF2_SLICEL_C_D)
                                                      0.047     1.686    muon_cand_9.pt_rep_27[0]
  -------------------------------------------------------------------
                         required time                         -1.686    
                         arrival time                           1.705    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_2[219]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_3[219]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.069ns
    Source Clock Delay      (SCD):    1.695ns
    Clock Pessimism Removal (CPR):    0.368ns
  Clock Net Delay (Source):      1.149ns (routing 0.523ns, distribution 0.626ns)
  Clock Net Delay (Destination): 1.297ns (routing 0.583ns, distribution 0.714ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X5Y8 (CLOCK_ROOT)    net (fo=2149, routed)        1.149     1.695    shift_reg_tap_i/clk_c
    SLICE_X136Y427       FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[219]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X136Y427       FDRE (Prop_BFF_SLICEL_C_Q)
                                                      0.039     1.734 r  shift_reg_tap_i/sr_p.sr_2[219]/Q
                         net (fo=1, routed)           0.033     1.767    shift_reg_tap_i/sr_2[219]
    SLICE_X136Y427       FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[219]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X5Y8 (CLOCK_ROOT)    net (fo=2149, routed)        1.297     2.069    shift_reg_tap_i/clk_c
    SLICE_X136Y427       FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[219]/C
                         clock pessimism             -0.368     1.701    
    SLICE_X136Y427       FDRE (Hold_CFF2_SLICEL_C_D)
                                                      0.047     1.748    shift_reg_tap_i/sr_p.sr_3[219]
  -------------------------------------------------------------------
                         required time                         -1.748    
                         arrival time                           1.767    
  -------------------------------------------------------------------
                         slack                                  0.019    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 6.250 }
Period(ns):         12.500
Sources:            { clk }

Check Type        Corner  Lib Pin     Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I    n/a            1.499         12.500      11.001     BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X152Y527  muon_sorter_1/sr_3_2.pt_0_sr_3_1.pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X152Y527  muon_sorter_1/sr_3_2.pt_1_sr_3_1.pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X152Y527  muon_sorter_1/sr_3_2.pt_2_sr_3_1.pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X153Y532  muon_sorter_1/sr_3_2.pt_sr_3_1.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X161Y531  muon_sorter_1/sr_3_5.roi_0_sr_3_1.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X161Y531  muon_sorter_1/sr_3_5.roi_1_sr_3_1.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X161Y531  muon_sorter_1/sr_3_5.roi_3_sr_3_1.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X161Y531  muon_sorter_1/sr_3_6.roi_1_sr_3_1.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X156Y540  muon_sorter_1/sr_3_6.roi_4_sr_3_1.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X153Y545  muon_sorter_1/sr_3_3.pt_1_sr_3_1.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X156Y538  muon_sorter_1/sr_3_3.roi_0_sr_3_1.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X156Y538  muon_sorter_1/sr_3_3.roi_1_sr_3_1.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X156Y538  muon_sorter_1/sr_3_3.roi_3_sr_3_1.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X156Y538  muon_sorter_1/sr_3_3.roi_4_sr_3_1.pt_1/CLK



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X162Y564         lsfr_1/output_vector_1[255]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X159Y561         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X113Y492         lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X159Y561         lsfr_1/shiftreg_vector[0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X123Y426         lsfr_1/shiftreg_vector[107]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X123Y426         lsfr_1/shiftreg_vector[108]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X113Y492         lsfr_1/shiftreg_vector[100]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X114Y493         lsfr_1/shiftreg_vector[101]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X115Y493         lsfr_1/shiftreg_vector[102]/C



