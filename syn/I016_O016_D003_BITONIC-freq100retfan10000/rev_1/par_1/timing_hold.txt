Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Sat Mar  2 21:59:32 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : bitonic_sorter_16_top
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.019        0.000                      0                  624        4.427        0.000                       0                  1323  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 5.000}        10.000          100.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.019        0.000                      0                  624        4.427        0.000                       0                   833  
clk_wrapper                                                                             498.562        0.000                       0                   490  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.019ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        4.427ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 dut_inst/qdelay_3__12_.idx_3__0_DOUT[0]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Destination:            sorted_muons[12].idx[3]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.185ns  (logic 0.069ns (37.297%)  route 0.116ns (62.703%))
  Logic Levels:           0  
  Clock Path Skew:        0.111ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.733ns
    Source Clock Delay      (SCD):    2.220ns
    Clock Pessimism Removal (CPR):    0.402ns
  Clock Net Delay (Source):      1.371ns (routing 0.568ns, distribution 0.803ns)
  Clock Net Delay (Destination): 1.573ns (routing 0.621ns, distribution 0.952ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.529     0.529 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.529    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.529 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.825    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.849 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=832, routed)         1.371     2.220    dut_inst/clk_c
    SLICE_X102Y566       FDRE                                         r  dut_inst/qdelay_3__12_.idx_3__0_DOUT[0]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X102Y566       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.069     2.289 r  dut_inst/qdelay_3__12_.idx_3__0_DOUT[0]/Q
                         net (fo=1, routed)           0.116     2.405    qdelay_3__12_.idx_3__0_DOUT[0]
    SLICE_X101Y565       FDRE                                         r  sorted_muons[12].idx[3]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.798     0.798 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.798    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.798 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.132    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.160 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=832, routed)         1.573     2.733    clk_c
    SLICE_X101Y565       FDRE                                         r  sorted_muons[12].idx[3]/C
                         clock pessimism             -0.402     2.331    
    SLICE_X101Y565       FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.055     2.386    sorted_muons[12].idx[3]
  -------------------------------------------------------------------
                         required time                         -2.386    
                         arrival time                           2.405    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 dut_inst/qdelay_3__12_.idx_3__0_DOUT[0]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Destination:            sorted_muons[12].idx[3]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.185ns  (logic 0.069ns (37.297%)  route 0.116ns (62.703%))
  Logic Levels:           0  
  Clock Path Skew:        0.111ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.733ns
    Source Clock Delay      (SCD):    2.220ns
    Clock Pessimism Removal (CPR):    0.402ns
  Clock Net Delay (Source):      1.371ns (routing 0.568ns, distribution 0.803ns)
  Clock Net Delay (Destination): 1.573ns (routing 0.621ns, distribution 0.952ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.529     0.529 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.529    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.529 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.825    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.849 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=832, routed)         1.371     2.220    dut_inst/clk_c
    SLICE_X102Y566       FDRE                                         r  dut_inst/qdelay_3__12_.idx_3__0_DOUT[0]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X102Y566       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.069     2.289 f  dut_inst/qdelay_3__12_.idx_3__0_DOUT[0]/Q
                         net (fo=1, routed)           0.116     2.405    qdelay_3__12_.idx_3__0_DOUT[0]
    SLICE_X101Y565       FDRE                                         f  sorted_muons[12].idx[3]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.798     0.798 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.798    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.798 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.132    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.160 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=832, routed)         1.573     2.733    clk_c
    SLICE_X101Y565       FDRE                                         r  sorted_muons[12].idx[3]/C
                         clock pessimism             -0.402     2.331    
    SLICE_X101Y565       FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.055     2.386    sorted_muons[12].idx[3]
  -------------------------------------------------------------------
                         required time                         -2.386    
                         arrival time                           2.405    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.029ns  (arrival time - required time)
  Source:                 dut_inst/qdelay_3__12_.idx_3__0_DOUT[0]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Destination:            sorted_muons[12].idx[3]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.126ns  (logic 0.038ns (30.159%)  route 0.088ns (69.841%))
  Logic Levels:           0  
  Clock Path Skew:        0.050ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.690ns
    Source Clock Delay      (SCD):    1.361ns
    Clock Pessimism Removal (CPR):    0.279ns
  Clock Net Delay (Source):      0.796ns (routing 0.319ns, distribution 0.477ns)
  Clock Net Delay (Destination): 0.899ns (routing 0.351ns, distribution 0.548ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.404     0.404 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.404    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.404 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.548    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.565 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=832, routed)         0.796     1.361    dut_inst/clk_c
    SLICE_X102Y566       FDRE                                         r  dut_inst/qdelay_3__12_.idx_3__0_DOUT[0]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X102Y566       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.038     1.399 r  dut_inst/qdelay_3__12_.idx_3__0_DOUT[0]/Q
                         net (fo=1, routed)           0.088     1.487    qdelay_3__12_.idx_3__0_DOUT[0]
    SLICE_X101Y565       FDRE                                         r  sorted_muons[12].idx[3]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.591     0.591 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.591    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.591 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.772    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.791 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=832, routed)         0.899     1.690    clk_c
    SLICE_X101Y565       FDRE                                         r  sorted_muons[12].idx[3]/C
                         clock pessimism             -0.279     1.411    
    SLICE_X101Y565       FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.047     1.458    sorted_muons[12].idx[3]
  -------------------------------------------------------------------
                         required time                         -1.458    
                         arrival time                           1.487    
  -------------------------------------------------------------------
                         slack                                  0.029    

Slack (MET) :             0.029ns  (arrival time - required time)
  Source:                 dut_inst/qdelay_3__12_.idx_3__0_DOUT[0]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Destination:            sorted_muons[12].idx[3]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.126ns  (logic 0.038ns (30.159%)  route 0.088ns (69.841%))
  Logic Levels:           0  
  Clock Path Skew:        0.050ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.690ns
    Source Clock Delay      (SCD):    1.361ns
    Clock Pessimism Removal (CPR):    0.279ns
  Clock Net Delay (Source):      0.796ns (routing 0.319ns, distribution 0.477ns)
  Clock Net Delay (Destination): 0.899ns (routing 0.351ns, distribution 0.548ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.404     0.404 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.404    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.404 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.548    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.565 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=832, routed)         0.796     1.361    dut_inst/clk_c
    SLICE_X102Y566       FDRE                                         r  dut_inst/qdelay_3__12_.idx_3__0_DOUT[0]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X102Y566       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.038     1.399 f  dut_inst/qdelay_3__12_.idx_3__0_DOUT[0]/Q
                         net (fo=1, routed)           0.088     1.487    qdelay_3__12_.idx_3__0_DOUT[0]
    SLICE_X101Y565       FDRE                                         f  sorted_muons[12].idx[3]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.591     0.591 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.591    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.591 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.772    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.791 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=832, routed)         0.899     1.690    clk_c
    SLICE_X101Y565       FDRE                                         r  sorted_muons[12].idx[3]/C
                         clock pessimism             -0.279     1.411    
    SLICE_X101Y565       FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.047     1.458    sorted_muons[12].idx[3]
  -------------------------------------------------------------------
                         required time                         -1.458    
                         arrival time                           1.487    
  -------------------------------------------------------------------
                         slack                                  0.029    

Slack (MET) :             0.030ns  (arrival time - required time)
  Source:                 dut_inst/qdelay_3__10_.idx_2__0_DOUT[0]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Destination:            sorted_muons[10].idx[2]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.194ns  (logic 0.069ns (35.567%)  route 0.125ns (64.433%))
  Logic Levels:           0  
  Clock Path Skew:        0.111ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.733ns
    Source Clock Delay      (SCD):    2.220ns
    Clock Pessimism Removal (CPR):    0.402ns
  Clock Net Delay (Source):      1.371ns (routing 0.568ns, distribution 0.803ns)
  Clock Net Delay (Destination): 1.573ns (routing 0.621ns, distribution 0.952ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.529     0.529 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.529    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.529 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.825    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.849 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=832, routed)         1.371     2.220    dut_inst/clk_c
    SLICE_X102Y566       FDRE                                         r  dut_inst/qdelay_3__10_.idx_2__0_DOUT[0]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X102Y566       FDRE (Prop_AFF_SLICEM_C_Q)
                                                      0.069     2.289 r  dut_inst/qdelay_3__10_.idx_2__0_DOUT[0]/Q
                         net (fo=1, routed)           0.125     2.414    qdelay_3__10_.idx_2__0_DOUT[0]
    SLICE_X101Y565       FDRE                                         r  sorted_muons[10].idx[2]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.798     0.798 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.798    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.798 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.132    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.160 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=832, routed)         1.573     2.733    clk_c
    SLICE_X101Y565       FDRE                                         r  sorted_muons[10].idx[2]/C
                         clock pessimism             -0.402     2.331    
    SLICE_X101Y565       FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.053     2.384    sorted_muons[10].idx[2]
  -------------------------------------------------------------------
                         required time                         -2.384    
                         arrival time                           2.414    
  -------------------------------------------------------------------
                         slack                                  0.030    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 5.000 }
Period(ns):         10.000
Sources:            { clk }

Check Type        Corner  Lib Pin     Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location       Pin
Min Period        n/a     BUFGCE/I    n/a            1.499         10.000      8.501      BUFGCE_X1Y218  clk_ibuf/I
Min Period        n/a     SRL16E/CLK  n/a            1.146         10.000      8.854      SLICE_X95Y588  dut_inst/qdelay_3__0_.idx_0__0_pt_3__1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         10.000      8.854      SLICE_X95Y585  dut_inst/qdelay_3__0_.idx_1__0_pt_3__1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         10.000      8.854      SLICE_X99Y588  dut_inst/qdelay_3__0_.idx_2__0_pt_3__1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         10.000      8.854      SLICE_X99Y572  dut_inst/qdelay_3__14_.idx_5__0_pt_3__1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         5.000       4.427      SLICE_X99Y593  dut_inst/qdelay_3__5_.idx_8__0_pt_3__1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         5.000       4.427      SLICE_X99Y568  dut_inst/qdelay_3__10_.pt_2__0_pt_3__1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         5.000       4.427      SLICE_X99Y568  dut_inst/qdelay_3__11_.idx_1__0_pt_3__1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         5.000       4.427      SLICE_X99Y568  dut_inst/qdelay_3__8_.idx_0__0_pt_3__1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         5.000       4.427      SLICE_X99Y568  dut_inst/qdelay_3__11_.pt_1__0_pt_3__1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         5.000       4.427      SLICE_X95Y585  dut_inst/qdelay_3__0_.idx_1__0_pt_3__1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         5.000       4.427      SLICE_X99Y583  dut_inst/qdelay_3__5_.idx_2__0_pt_3__1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         5.000       4.427      SLICE_X95Y585  dut_inst/qdelay_3__0_.idx_4__0_pt_3__1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         5.000       4.427      SLICE_X95Y585  dut_inst/qdelay_3__6_.idx_1__0_pt_3__1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         5.000       4.427      SLICE_X99Y583  dut_inst/qdelay_3__6_.idx_2__0_pt_3__1/CLK



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y518  dout/C
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y517  lfsr_din/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y224          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X97Y568          input_lfsr/shiftreg_vector[181]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X93Y570          input_lfsr/shiftreg_vector[182]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  dout/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  dout/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y517  lfsr_din/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y517  lfsr_din/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X95Y570          input_lfsr/shiftreg_vector[188]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  dout/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  dout/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y517  lfsr_din/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y517  lfsr_din/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X97Y568          input_lfsr/shiftreg_vector[181]/C



