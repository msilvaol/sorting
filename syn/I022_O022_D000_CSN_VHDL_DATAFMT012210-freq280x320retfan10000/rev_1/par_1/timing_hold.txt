Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Thu Mar  7 02:28:18 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.001        0.000                      0                 1109        1.511        0.000                       0                  2122  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.786}        3.572           279.955         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.001        0.000                      0                 1109        1.511        0.000                       0                  1450  
clk_wrapper                                                                             498.562        0.000                       0                   672  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.001ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.511ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.001ns  (arrival time - required time)
  Source:                 dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/ret_array_2_12.idx_ret_15/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[206]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.269ns  (logic 0.097ns (36.059%)  route 0.172ns (63.941%))
  Logic Levels:           1  (LUT5=1)
  Clock Path Skew:        0.215ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.753ns
    Source Clock Delay      (SCD):    3.059ns
    Clock Pessimism Removal (CPR):    0.479ns
  Clock Net Delay (Source):      2.229ns (routing 1.334ns, distribution 0.895ns)
  Clock Net Delay (Destination): 2.612ns (routing 1.460ns, distribution 1.152ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1449, routed)        2.229     3.059    dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/clk_c
    SLICE_X101Y435       FDRE                                         r  dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/ret_array_2_12.idx_ret_15/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y435       FDRE (Prop_CFF_SLICEL_C_Q)
                                                      0.070     3.129 r  dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/ret_array_2_12.idx_ret_15/Q
                         net (fo=17, routed)          0.138     3.267    dut_inst/stage_g.11.pair_g.1.csn_cmp_inst/un1_b_i_reto_2
    SLICE_X102Y438       LUT5 (Prop_B6LUT_SLICEM_I1_O)
                                                      0.027     3.294 r  dut_inst/stage_g.11.pair_g.1.csn_cmp_inst/ret_array_2_14.idx_ret_31_RNIBQKOB/O
                         net (fo=1, routed)           0.034     3.328    shift_reg_tap_o/idx_ret_31_RNIBQKOB
    SLICE_X102Y438       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[206]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1449, routed)        2.612     3.753    shift_reg_tap_o/clk_c
    SLICE_X102Y438       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[206]/C
                         clock pessimism             -0.479     3.274    
    SLICE_X102Y438       FDRE (Hold_BFF_SLICEM_C_D)
                                                      0.053     3.327    shift_reg_tap_o/sr_p.sr_1[206]
  -------------------------------------------------------------------
                         required time                         -3.327    
                         arrival time                           3.328    
  -------------------------------------------------------------------
                         slack                                  0.001    

Slack (MET) :             0.001ns  (arrival time - required time)
  Source:                 dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/ret_array_2_12.idx_ret_15/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[206]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.269ns  (logic 0.097ns (36.059%)  route 0.172ns (63.941%))
  Logic Levels:           1  (LUT5=1)
  Clock Path Skew:        0.215ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.753ns
    Source Clock Delay      (SCD):    3.059ns
    Clock Pessimism Removal (CPR):    0.479ns
  Clock Net Delay (Source):      2.229ns (routing 1.334ns, distribution 0.895ns)
  Clock Net Delay (Destination): 2.612ns (routing 1.460ns, distribution 1.152ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1449, routed)        2.229     3.059    dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/clk_c
    SLICE_X101Y435       FDRE                                         r  dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/ret_array_2_12.idx_ret_15/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y435       FDRE (Prop_CFF_SLICEL_C_Q)
                                                      0.070     3.129 f  dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/ret_array_2_12.idx_ret_15/Q
                         net (fo=17, routed)          0.138     3.267    dut_inst/stage_g.11.pair_g.1.csn_cmp_inst/un1_b_i_reto_2
    SLICE_X102Y438       LUT5 (Prop_B6LUT_SLICEM_I1_O)
                                                      0.027     3.294 r  dut_inst/stage_g.11.pair_g.1.csn_cmp_inst/ret_array_2_14.idx_ret_31_RNIBQKOB/O
                         net (fo=1, routed)           0.034     3.328    shift_reg_tap_o/idx_ret_31_RNIBQKOB
    SLICE_X102Y438       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[206]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1449, routed)        2.612     3.753    shift_reg_tap_o/clk_c
    SLICE_X102Y438       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[206]/C
                         clock pessimism             -0.479     3.274    
    SLICE_X102Y438       FDRE (Hold_BFF_SLICEM_C_D)
                                                      0.053     3.327    shift_reg_tap_o/sr_p.sr_1[206]
  -------------------------------------------------------------------
                         required time                         -3.327    
                         arrival time                           3.328    
  -------------------------------------------------------------------
                         slack                                  0.001    

Slack (MET) :             0.001ns  (arrival time - required time)
  Source:                 dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/ret_array_2_12.idx_ret_15/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[206]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.269ns  (logic 0.097ns (36.059%)  route 0.172ns (63.941%))
  Logic Levels:           1  (LUT5=1)
  Clock Path Skew:        0.215ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.753ns
    Source Clock Delay      (SCD):    3.059ns
    Clock Pessimism Removal (CPR):    0.479ns
  Clock Net Delay (Source):      2.229ns (routing 1.334ns, distribution 0.895ns)
  Clock Net Delay (Destination): 2.612ns (routing 1.460ns, distribution 1.152ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1449, routed)        2.229     3.059    dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/clk_c
    SLICE_X101Y435       FDRE                                         r  dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/ret_array_2_12.idx_ret_15/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y435       FDRE (Prop_CFF_SLICEL_C_Q)
                                                      0.070     3.129 r  dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/ret_array_2_12.idx_ret_15/Q
                         net (fo=17, routed)          0.138     3.267    dut_inst/stage_g.11.pair_g.1.csn_cmp_inst/un1_b_i_reto_2
    SLICE_X102Y438       LUT5 (Prop_B6LUT_SLICEM_I1_O)
                                                      0.027     3.294 f  dut_inst/stage_g.11.pair_g.1.csn_cmp_inst/ret_array_2_14.idx_ret_31_RNIBQKOB/O
                         net (fo=1, routed)           0.034     3.328    shift_reg_tap_o/idx_ret_31_RNIBQKOB
    SLICE_X102Y438       FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[206]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1449, routed)        2.612     3.753    shift_reg_tap_o/clk_c
    SLICE_X102Y438       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[206]/C
                         clock pessimism             -0.479     3.274    
    SLICE_X102Y438       FDRE (Hold_BFF_SLICEM_C_D)
                                                      0.053     3.327    shift_reg_tap_o/sr_p.sr_1[206]
  -------------------------------------------------------------------
                         required time                         -3.327    
                         arrival time                           3.328    
  -------------------------------------------------------------------
                         slack                                  0.001    

Slack (MET) :             0.001ns  (arrival time - required time)
  Source:                 dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/ret_array_2_12.idx_ret_15/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[206]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.269ns  (logic 0.097ns (36.059%)  route 0.172ns (63.941%))
  Logic Levels:           1  (LUT5=1)
  Clock Path Skew:        0.215ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.753ns
    Source Clock Delay      (SCD):    3.059ns
    Clock Pessimism Removal (CPR):    0.479ns
  Clock Net Delay (Source):      2.229ns (routing 1.334ns, distribution 0.895ns)
  Clock Net Delay (Destination): 2.612ns (routing 1.460ns, distribution 1.152ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1449, routed)        2.229     3.059    dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/clk_c
    SLICE_X101Y435       FDRE                                         r  dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/ret_array_2_12.idx_ret_15/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y435       FDRE (Prop_CFF_SLICEL_C_Q)
                                                      0.070     3.129 f  dut_inst/stage_g.10.pair_g.2.csn_cmp_inst/ret_array_2_12.idx_ret_15/Q
                         net (fo=17, routed)          0.138     3.267    dut_inst/stage_g.11.pair_g.1.csn_cmp_inst/un1_b_i_reto_2
    SLICE_X102Y438       LUT5 (Prop_B6LUT_SLICEM_I1_O)
                                                      0.027     3.294 f  dut_inst/stage_g.11.pair_g.1.csn_cmp_inst/ret_array_2_14.idx_ret_31_RNIBQKOB/O
                         net (fo=1, routed)           0.034     3.328    shift_reg_tap_o/idx_ret_31_RNIBQKOB
    SLICE_X102Y438       FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[206]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1449, routed)        2.612     3.753    shift_reg_tap_o/clk_c
    SLICE_X102Y438       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[206]/C
                         clock pessimism             -0.479     3.274    
    SLICE_X102Y438       FDRE (Hold_BFF_SLICEM_C_D)
                                                      0.053     3.327    shift_reg_tap_o/sr_p.sr_1[206]
  -------------------------------------------------------------------
                         required time                         -3.327    
                         arrival time                           3.328    
  -------------------------------------------------------------------
                         slack                                  0.001    

Slack (MET) :             0.002ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_2_18.idx[6]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[244]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.294ns  (logic 0.070ns (23.810%)  route 0.224ns (76.190%))
  Logic Levels:           0  
  Clock Path Skew:        0.239ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.735ns
    Source Clock Delay      (SCD):    3.017ns
    Clock Pessimism Removal (CPR):    0.479ns
  Clock Net Delay (Source):      2.187ns (routing 1.334ns, distribution 0.853ns)
  Clock Net Delay (Destination): 2.594ns (routing 1.460ns, distribution 1.134ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1449, routed)        2.187     3.017    dut_inst/clk_c
    SLICE_X104Y431       FDRE                                         r  dut_inst/ret_array_2_18.idx[6]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X104Y431       FDRE (Prop_DFF_SLICEM_C_Q)
                                                      0.070     3.087 r  dut_inst/ret_array_2_18.idx[6]/Q
                         net (fo=1, routed)           0.224     3.311    shift_reg_tap_o/idx_2[6]
    SLICE_X102Y443       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[244]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1449, routed)        2.594     3.735    shift_reg_tap_o/clk_c
    SLICE_X102Y443       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[244]/C
                         clock pessimism             -0.479     3.256    
    SLICE_X102Y443       FDRE (Hold_FFF_SLICEM_C_D)
                                                      0.053     3.309    shift_reg_tap_o/sr_p.sr_1[244]
  -------------------------------------------------------------------
                         required time                         -3.309    
                         arrival time                           3.311    
  -------------------------------------------------------------------
                         slack                                  0.002    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.786 }
Period(ns):         3.572
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         3.572       2.073      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X104Y420  dut_inst/ret_array_1_11.idx_ret_19[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X103Y420  dut_inst/ret_array_1_11.idx_ret_19[1]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X103Y420  dut_inst/ret_array_1_11.idx_ret_19[2]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X104Y420  dut_inst/ret_array_1_11.idx_ret_19[3]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X104Y420  dut_inst/ret_array_1_11.idx_ret_19[0]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X103Y420  dut_inst/ret_array_1_11.idx_ret_19[1]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X103Y420  dut_inst/ret_array_1_11.idx_ret_19[2]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X104Y420  dut_inst/ret_array_1_11.idx_ret_19[3]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X103Y420  dut_inst/ret_array_1_11.idx_ret_19[4]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X104Y420  dut_inst/ret_array_1_11.idx_ret_19[0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X104Y420  dut_inst/ret_array_1_11.idx_ret_19[3]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X103Y420  dut_inst/ret_array_1_11.idx_ret_19[5]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X103Y420  dut_inst/ret_array_1_11.idx_ret_19[5]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X103Y420  dut_inst/ret_array_1_11.idx_ret_19[6]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X104Y411         lsfr_1/shiftreg_vector[125]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X104Y411         lsfr_1/shiftreg_vector[126]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X104Y411         lsfr_1/shiftreg_vector[127]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X94Y440          reducer_1/delay_block[0][44]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X101Y411         lsfr_1/shiftreg_vector[145]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X101Y411         lsfr_1/shiftreg_vector[146]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y411         lsfr_1/shiftreg_vector[125]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y411         lsfr_1/shiftreg_vector[126]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y411         lsfr_1/shiftreg_vector[127]/C



