Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Tue Mar 12 00:27:53 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_utilization -hierarchical -file hier_utilization.txt
| Design       : wrapper_csn
| Device       : xcvu9pflgc2104-1
| Design State : Routed
------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+--------------------------------------+------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
|               Instance               |                              Module                              | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+--------------------------------------+------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
| wrapper_csn                          |                                                            (top) |       4297 |       4297 |       0 |    0 | 2190 |      0 |      0 |    0 |            0 |
|   (wrapper_csn)                      |                                                            (top) |          0 |          0 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   dut_inst                           | muon_sorter_I022_O022_D002_CSN_VHDL-freq280x400retfan10000_rev_1 |       4199 |       4199 |       0 |    0 |  945 |      0 |      0 |    0 |            0 |
|     (dut_inst)                       | muon_sorter_I022_O022_D002_CSN_VHDL-freq280x400retfan10000_rev_1 |          9 |          9 |       0 |    0 |  613 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.0.csn_cmp_inst  |                                    csn_cmp_false_false_false_113 |          5 |          5 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_27 |         14 |         14 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.10.csn_cmp_inst |                                     csn_cmp_false_false_false_54 |         20 |         20 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.2.csn_cmp_inst  |                                     csn_cmp_false_false_false_24 |         15 |         15 |       0 |    0 |    7 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.3.csn_cmp_inst  |                                    csn_cmp_false_false_false_114 |         11 |         11 |       0 |    0 |    8 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_37 |         18 |         18 |       0 |    0 |    9 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.5.csn_cmp_inst  |                                     csn_cmp_false_false_false_40 |         11 |         11 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.6.csn_cmp_inst  |                                     csn_cmp_false_false_false_43 |         18 |         18 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_45 |         12 |         12 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.8.csn_cmp_inst  |                                     csn_cmp_false_false_false_48 |         16 |         16 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.9.csn_cmp_inst  |                                     csn_cmp_false_false_false_51 |         17 |         17 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.0.csn_cmp_inst  |                                     csn_cmp_false_false_false_57 |         22 |         22 |       0 |    0 |   13 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_60 |         26 |         26 |       0 |    0 |   15 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.10.csn_cmp_inst |                                     csn_cmp_false_false_false_83 |         23 |         23 |       0 |    0 |   13 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.2.csn_cmp_inst  |                                     csn_cmp_false_false_false_63 |         31 |         31 |       0 |    0 |   12 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_65 |         23 |         23 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_68 |         29 |         29 |       0 |    0 |   14 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.5.csn_cmp_inst  |                                     csn_cmp_false_false_false_70 |         20 |         20 |       0 |    0 |    9 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.6.csn_cmp_inst  |                                     csn_cmp_false_false_false_72 |         25 |         25 |       0 |    0 |   15 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_74 |         25 |         25 |       0 |    0 |   13 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.8.csn_cmp_inst  |                                     csn_cmp_false_false_false_77 |         27 |         27 |       0 |    0 |   15 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.9.csn_cmp_inst  |                                     csn_cmp_false_false_false_80 |         24 |         24 |       0 |    0 |   13 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.0.csn_cmp_inst |                                    csn_cmp_false_false_false_104 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.1.csn_cmp_inst |                                     csn_cmp_false_false_false_15 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.2.csn_cmp_inst |                                      csn_cmp_false_false_false_4 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.3.csn_cmp_inst |                                     csn_cmp_false_false_false_19 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.4.csn_cmp_inst |                                     csn_cmp_false_false_false_26 |         24 |         24 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.5.csn_cmp_inst |                                     csn_cmp_false_false_false_12 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.6.csn_cmp_inst |                                      csn_cmp_false_false_false_0 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.7.csn_cmp_inst |                                    csn_cmp_false_false_false_109 |         27 |         27 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.0.csn_cmp_inst |                                     csn_cmp_false_false_false_29 |         29 |         29 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.1.csn_cmp_inst |                                     csn_cmp_false_false_false_34 |         25 |         25 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.2.csn_cmp_inst |                                     csn_cmp_false_false_false_33 |         26 |         26 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.3.csn_cmp_inst |                                     csn_cmp_false_false_false_32 |         27 |         27 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.4.csn_cmp_inst |                                     csn_cmp_false_false_false_35 |         26 |         26 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.5.csn_cmp_inst |                                     csn_cmp_false_false_false_31 |         25 |         25 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.6.csn_cmp_inst |                                     csn_cmp_false_false_false_30 |         32 |         32 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.0.csn_cmp_inst  |                                     csn_cmp_false_false_false_86 |         52 |         52 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_89 |         23 |         23 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.10.csn_cmp_inst |                                     csn_cmp_false_false_false_18 |         24 |         24 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.2.csn_cmp_inst  |                                     csn_cmp_false_false_false_92 |         27 |         27 |       0 |    0 |    9 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_94 |         24 |         24 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_97 |         30 |         30 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.5.csn_cmp_inst  |                                    csn_cmp_false_false_false_100 |         28 |         28 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.6.csn_cmp_inst  |                                    csn_cmp_false_false_false_102 |         23 |         23 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.7.csn_cmp_inst  |                                    csn_cmp_false_false_false_108 |         30 |         30 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.8.csn_cmp_inst  |                                     csn_cmp_false_false_false_28 |         28 |         28 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.9.csn_cmp_inst  |                                    csn_cmp_false_false_false_105 |         26 |         26 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.0.csn_cmp_inst  |                                    csn_cmp_false_false_false_103 |         46 |         46 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_11 |         40 |         40 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.10.csn_cmp_inst |                                      csn_cmp_false_false_false_3 |         30 |         30 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.2.csn_cmp_inst  |                                     csn_cmp_false_false_false_14 |         36 |         36 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.3.csn_cmp_inst  |                                      csn_cmp_false_false_false_6 |         41 |         41 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.4.csn_cmp_inst  |                                      csn_cmp_false_false_false_5 |         40 |         40 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.5.csn_cmp_inst  |                                     csn_cmp_false_false_false_25 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.6.csn_cmp_inst  |                                     csn_cmp_false_false_false_16 |         31 |         31 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.7.csn_cmp_inst  |                                    csn_cmp_false_false_false_107 |         32 |         32 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.8.csn_cmp_inst  |                                      csn_cmp_false_false_false_2 |         36 |         36 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.9.csn_cmp_inst  |                                    csn_cmp_false_false_false_111 |         29 |         29 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.0.csn_cmp_inst  |                                     csn_cmp_false_false_false_36 |         23 |         23 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_39 |         18 |         18 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.2.csn_cmp_inst  |                                     csn_cmp_false_false_false_42 |         11 |         11 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_44 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_47 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.5.csn_cmp_inst  |                                     csn_cmp_false_false_false_50 |         12 |         12 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.6.csn_cmp_inst  |                                     csn_cmp_false_false_false_53 |         13 |         13 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_56 |         12 |         12 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.8.csn_cmp_inst  |                                     csn_cmp_false_false_false_59 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.9.csn_cmp_inst  |                                     csn_cmp_false_false_false_62 |         13 |         13 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.0.csn_cmp_inst  |                                     csn_cmp_false_false_false_67 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_69 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.2.csn_cmp_inst  |                                     csn_cmp_false_false_false_71 |          6 |          6 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_73 |         10 |         10 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_76 |         10 |         10 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.5.csn_cmp_inst  |                                     csn_cmp_false_false_false_79 |          9 |          9 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.6.csn_cmp_inst  |                                     csn_cmp_false_false_false_82 |         19 |         19 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_85 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.8.csn_cmp_inst  |                                     csn_cmp_false_false_false_88 |         10 |         10 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.9.csn_cmp_inst  |                                     csn_cmp_false_false_false_91 |         13 |         13 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.0.csn_cmp_inst  |                                     csn_cmp_false_false_false_96 |         35 |         35 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_99 |         28 |         28 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.2.csn_cmp_inst  |                                    csn_cmp_false_false_false_101 |         95 |         95 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_21 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.4.csn_cmp_inst  |                                        csn_cmp_false_false_false |         16 |         16 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.5.csn_cmp_inst  |                                    csn_cmp_false_false_false_106 |         33 |         33 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.6.csn_cmp_inst  |                                     csn_cmp_false_false_false_17 |          9 |          9 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_20 |         84 |         84 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.8.csn_cmp_inst  |                                     csn_cmp_false_false_false_10 |         30 |         30 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.9.csn_cmp_inst  |                                     csn_cmp_false_false_false_13 |         34 |         34 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.0.csn_cmp_inst  |                                      csn_cmp_false_false_false_7 |        104 |        104 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.1.csn_cmp_inst  |                                    csn_cmp_false_false_false_110 |        163 |        163 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.2.csn_cmp_inst  |                                      csn_cmp_false_false_false_9 |        184 |        184 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_23 |        200 |        200 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.4.csn_cmp_inst  |                                    csn_cmp_false_false_false_112 |        195 |        195 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.5.csn_cmp_inst  |                                      csn_cmp_false_false_false_1 |        192 |        192 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.6.csn_cmp_inst  |                                      csn_cmp_false_false_false_8 |        210 |        210 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_22 |        163 |        163 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.8.csn_cmp_inst  |                                     csn_cmp_false_false_false_38 |        167 |        167 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.9.csn_cmp_inst  |                                     csn_cmp_false_false_false_41 |         92 |         92 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.0.csn_cmp_inst  |                                     csn_cmp_false_false_false_46 |         17 |         17 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_49 |         24 |         24 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.2.csn_cmp_inst  |                                     csn_cmp_false_false_false_52 |         27 |         27 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_55 |         24 |         24 |       0 |    0 |    7 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_58 |         27 |         27 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.5.csn_cmp_inst  |                                     csn_cmp_false_false_false_61 |         25 |         25 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.6.csn_cmp_inst  |                                     csn_cmp_false_false_false_64 |         22 |         22 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_66 |         18 |         18 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.0.csn_cmp_inst  |                                     csn_cmp_false_false_false_75 |         28 |         28 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_78 |         25 |         25 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.2.csn_cmp_inst  |                                     csn_cmp_false_false_false_81 |         20 |         20 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_84 |         28 |         28 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_87 |         24 |         24 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.5.csn_cmp_inst  |                                     csn_cmp_false_false_false_90 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.6.csn_cmp_inst  |                                     csn_cmp_false_false_false_93 |         27 |         27 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_95 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.8.csn_cmp_inst  |                                     csn_cmp_false_false_false_98 |         27 |         27 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   lsfr_1                             |                                                             lfsr |          1 |          1 |       0 |    0 |  287 |      0 |      0 |    0 |            0 |
|   reducer_1                          |                                                          reducer |         97 |         97 |       0 |    0 |  383 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_i                    |                                              shift_reg_tap_286_1 |          0 |          0 |       0 |    0 |  289 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_o                    |                                             shift_reg_tap_1024_1 |          0 |          0 |       0 |    0 |  286 |      0 |      0 |    0 |            0 |
+--------------------------------------+------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
* Note: The sum of lower-level cells may be larger than their parent cells total, due to cross-hierarchy LUT combining


