Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Tue Mar 12 00:28:01 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.046        0.000                      0                 1231        1.511        0.000                       0                  2193  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.786}        3.572           279.955         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.046        0.000                      0                 1231        1.511        0.000                       0                  1521  
clk_wrapper                                                                             498.562        0.000                       0                   672  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.046ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.511ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.046ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[153]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_5.idx_ret_9[6]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.110ns  (logic 0.039ns (35.455%)  route 0.071ns (64.545%))
  Logic Levels:           0  
  Clock Path Skew:        0.017ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.008ns
    Source Clock Delay      (SCD):    1.644ns
    Clock Pessimism Removal (CPR):    0.347ns
  Clock Net Delay (Source):      1.098ns (routing 0.638ns, distribution 0.460ns)
  Clock Net Delay (Destination): 1.236ns (routing 0.707ns, distribution 0.529ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=1520, routed)        1.098     1.644    shift_reg_tap_i/clk_c
    SLICE_X99Y442        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[153]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X99Y442        FDRE (Prop_HFF_SLICEM_C_Q)
                                                      0.039     1.683 r  shift_reg_tap_i/sr_p.sr_1[153]/Q
                         net (fo=1, routed)           0.071     1.754    dut_inst/input_slr[153]
    SLICE_X99Y444        FDRE                                         r  dut_inst/ret_array_1_5.idx_ret_9[6]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=1520, routed)        1.236     2.008    dut_inst/clk_c
    SLICE_X99Y444        FDRE                                         r  dut_inst/ret_array_1_5.idx_ret_9[6]/C
                         clock pessimism             -0.347     1.661    
    SLICE_X99Y444        FDRE (Hold_FFF2_SLICEM_C_D)
                                                      0.047     1.708    dut_inst/ret_array_1_5.idx_ret_9[6]
  -------------------------------------------------------------------
                         required time                         -1.708    
                         arrival time                           1.754    
  -------------------------------------------------------------------
                         slack                                  0.046    

Slack (MET) :             0.046ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[153]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_5.idx_ret_9[6]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.110ns  (logic 0.039ns (35.455%)  route 0.071ns (64.545%))
  Logic Levels:           0  
  Clock Path Skew:        0.017ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.008ns
    Source Clock Delay      (SCD):    1.644ns
    Clock Pessimism Removal (CPR):    0.347ns
  Clock Net Delay (Source):      1.098ns (routing 0.638ns, distribution 0.460ns)
  Clock Net Delay (Destination): 1.236ns (routing 0.707ns, distribution 0.529ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=1520, routed)        1.098     1.644    shift_reg_tap_i/clk_c
    SLICE_X99Y442        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[153]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X99Y442        FDRE (Prop_HFF_SLICEM_C_Q)
                                                      0.039     1.683 f  shift_reg_tap_i/sr_p.sr_1[153]/Q
                         net (fo=1, routed)           0.071     1.754    dut_inst/input_slr[153]
    SLICE_X99Y444        FDRE                                         f  dut_inst/ret_array_1_5.idx_ret_9[6]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=1520, routed)        1.236     2.008    dut_inst/clk_c
    SLICE_X99Y444        FDRE                                         r  dut_inst/ret_array_1_5.idx_ret_9[6]/C
                         clock pessimism             -0.347     1.661    
    SLICE_X99Y444        FDRE (Hold_FFF2_SLICEM_C_D)
                                                      0.047     1.708    dut_inst/ret_array_1_5.idx_ret_9[6]
  -------------------------------------------------------------------
                         required time                         -1.708    
                         arrival time                           1.754    
  -------------------------------------------------------------------
                         slack                                  0.046    

Slack (MET) :             0.048ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[282]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_5.idx_ret_19[5]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.115ns  (logic 0.039ns (33.913%)  route 0.076ns (66.087%))
  Logic Levels:           0  
  Clock Path Skew:        0.021ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.034ns
    Source Clock Delay      (SCD):    1.664ns
    Clock Pessimism Removal (CPR):    0.349ns
  Clock Net Delay (Source):      1.118ns (routing 0.638ns, distribution 0.480ns)
  Clock Net Delay (Destination): 1.262ns (routing 0.707ns, distribution 0.555ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=1520, routed)        1.118     1.664    shift_reg_tap_i/clk_c
    SLICE_X110Y468       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[282]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X110Y468       FDRE (Prop_DFF_SLICEM_C_Q)
                                                      0.039     1.703 r  shift_reg_tap_i/sr_p.sr_1[282]/Q
                         net (fo=1, routed)           0.076     1.779    dut_inst/input_slr[282]
    SLICE_X110Y467       FDRE                                         r  dut_inst/ret_array_1_5.idx_ret_19[5]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=1520, routed)        1.262     2.034    dut_inst/clk_c
    SLICE_X110Y467       FDRE                                         r  dut_inst/ret_array_1_5.idx_ret_19[5]/C
                         clock pessimism             -0.349     1.685    
    SLICE_X110Y467       FDRE (Hold_EFF_SLICEM_C_D)
                                                      0.046     1.731    dut_inst/ret_array_1_5.idx_ret_19[5]
  -------------------------------------------------------------------
                         required time                         -1.731    
                         arrival time                           1.779    
  -------------------------------------------------------------------
                         slack                                  0.048    

Slack (MET) :             0.048ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[282]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_5.idx_ret_19[5]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.115ns  (logic 0.039ns (33.913%)  route 0.076ns (66.087%))
  Logic Levels:           0  
  Clock Path Skew:        0.021ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.034ns
    Source Clock Delay      (SCD):    1.664ns
    Clock Pessimism Removal (CPR):    0.349ns
  Clock Net Delay (Source):      1.118ns (routing 0.638ns, distribution 0.480ns)
  Clock Net Delay (Destination): 1.262ns (routing 0.707ns, distribution 0.555ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=1520, routed)        1.118     1.664    shift_reg_tap_i/clk_c
    SLICE_X110Y468       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[282]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X110Y468       FDRE (Prop_DFF_SLICEM_C_Q)
                                                      0.039     1.703 f  shift_reg_tap_i/sr_p.sr_1[282]/Q
                         net (fo=1, routed)           0.076     1.779    dut_inst/input_slr[282]
    SLICE_X110Y467       FDRE                                         f  dut_inst/ret_array_1_5.idx_ret_19[5]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=1520, routed)        1.262     2.034    dut_inst/clk_c
    SLICE_X110Y467       FDRE                                         r  dut_inst/ret_array_1_5.idx_ret_19[5]/C
                         clock pessimism             -0.349     1.685    
    SLICE_X110Y467       FDRE (Hold_EFF_SLICEM_C_D)
                                                      0.046     1.731    dut_inst/ret_array_1_5.idx_ret_19[5]
  -------------------------------------------------------------------
                         required time                         -1.731    
                         arrival time                           1.779    
  -------------------------------------------------------------------
                         slack                                  0.048    

Slack (MET) :             0.050ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_1_5.idx_ret_15[2]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/stage_g.1.pair_g.8.csn_cmp_inst/ret_array_2_5.idx_ret_1108_ret/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.144ns  (logic 0.054ns (37.500%)  route 0.090ns (62.500%))
  Logic Levels:           1  (LUT5=1)
  Clock Path Skew:        0.048ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.031ns
    Source Clock Delay      (SCD):    1.665ns
    Clock Pessimism Removal (CPR):    0.318ns
  Clock Net Delay (Source):      1.119ns (routing 0.638ns, distribution 0.481ns)
  Clock Net Delay (Destination): 1.259ns (routing 0.707ns, distribution 0.552ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=1520, routed)        1.119     1.665    dut_inst/clk_c
    SLICE_X111Y470       FDRE                                         r  dut_inst/ret_array_1_5.idx_ret_15[2]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X111Y470       FDRE (Prop_EFF_SLICEL_C_Q)
                                                      0.039     1.704 r  dut_inst/ret_array_1_5.idx_ret_15[2]/Q
                         net (fo=3, routed)           0.073     1.777    dut_inst/stage_g.1.pair_g.8.csn_cmp_inst/muon_i_o_44
    SLICE_X109Y470       LUT5 (Prop_H6LUT_SLICEL_I1_O)
                                                      0.015     1.792 r  dut_inst/stage_g.1.pair_g.8.csn_cmp_inst/ret_array_2_5.idx_ret_1108_ret_RNO/O
                         net (fo=1, routed)           0.017     1.809    dut_inst/stage_g.1.pair_g.8.csn_cmp_inst/idx_reti_0[2]
    SLICE_X109Y470       FDRE                                         r  dut_inst/stage_g.1.pair_g.8.csn_cmp_inst/ret_array_2_5.idx_ret_1108_ret/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=1520, routed)        1.259     2.031    dut_inst/stage_g.1.pair_g.8.csn_cmp_inst/clk_c
    SLICE_X109Y470       FDRE                                         r  dut_inst/stage_g.1.pair_g.8.csn_cmp_inst/ret_array_2_5.idx_ret_1108_ret/C
                         clock pessimism             -0.318     1.713    
    SLICE_X109Y470       FDRE (Hold_HFF_SLICEL_C_D)
                                                      0.046     1.759    dut_inst/stage_g.1.pair_g.8.csn_cmp_inst/ret_array_2_5.idx_ret_1108_ret
  -------------------------------------------------------------------
                         required time                         -1.759    
                         arrival time                           1.809    
  -------------------------------------------------------------------
                         slack                                  0.050    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.786 }
Period(ns):         3.572
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         3.572       2.073      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X96Y471   dut_inst/ret_array_1_11.pt_ret_43[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X94Y467   dut_inst/ret_array_1_11.pt_ret_43[1]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X93Y467   dut_inst/ret_array_1_11.pt_ret_43[3]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X95Y468   dut_inst/ret_array_1_11.pt_ret_44[0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X107Y450  dut_inst/stage_g.0.pair_g.3.csn_cmp_inst/ret_array_2_5.idx_ret_532_ret_2/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X110Y447  dut_inst/stage_g.0.pair_g.3.csn_cmp_inst/ret_array_2_5.idx_ret_964_ret_15/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X109Y449  dut_inst/stage_g.0.pair_g.3.csn_cmp_inst/ret_array_2_5.idx_ret_964_ret_9/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X102Y454  dut_inst/ret_array_2_15.idx_ret_38_rep_105/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X104Y449  dut_inst/stage_g.0.pair_g.4.csn_cmp_inst/ret_array_2_5.idx_ret_1000_ret_13/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X94Y467   dut_inst/ret_array_1_11.pt_ret_43[1]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X93Y467   dut_inst/ret_array_1_11.pt_ret_43[3]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X94Y467   dut_inst/ret_array_1_11.pt_ret_44[1]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X93Y467   dut_inst/ret_array_1_11.pt_ret_44[3]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X98Y466   dut_inst/ret_array_1_11.pt_ret_45[3]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X92Y472          reducer_1/delay_block[1][0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X91Y458          reducer_1/delay_block[1][10]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X91Y458          reducer_1/delay_block[1][11]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X95Y445          reducer_1/delay_block[1][29]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X95Y445          reducer_1/delay_block[1][33]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X93Y442          reducer_1/delay_block[1][51]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X91Y458          reducer_1/delay_block[1][11]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X92Y449          reducer_1/delay_block[1][13]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X92Y449          reducer_1/delay_block[1][15]/C



