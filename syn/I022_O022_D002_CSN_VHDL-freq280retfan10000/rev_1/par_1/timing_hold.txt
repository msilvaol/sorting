Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Tue Mar 12 00:07:09 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.010        0.000                      0                 1141        1.511        0.000                       0                  2100  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.786}        3.572           279.955         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.010        0.000                      0                 1141        1.511        0.000                       0                  1428  
clk_wrapper                                                                             498.562        0.000                       0                   672  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.010ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.511ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.010ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_1_5.idx_ret_10[4]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/ret_array_2_5.idx_ret_496/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.293ns  (logic 0.139ns (47.440%)  route 0.154ns (52.560%))
  Logic Levels:           1  (LUT5=1)
  Clock Path Skew:        0.230ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.860ns
    Source Clock Delay      (SCD):    3.174ns
    Clock Pessimism Removal (CPR):    0.456ns
  Clock Net Delay (Source):      2.344ns (routing 1.538ns, distribution 0.806ns)
  Clock Net Delay (Destination): 2.719ns (routing 1.683ns, distribution 1.036ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=1427, routed)        2.344     3.174    dut_inst/clk_c
    SLICE_X107Y358       FDRE                                         r  dut_inst/ret_array_1_5.idx_ret_10[4]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X107Y358       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.069     3.243 r  dut_inst/ret_array_1_5.idx_ret_10[4]/Q
                         net (fo=4, routed)           0.120     3.363    dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/muon_i_o_1
    SLICE_X108Y361       LUT5 (Prop_B6LUT_SLICEM_I0_O)
                                                      0.070     3.433 r  dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/ret_array_2_5.idx_ret_496_RNO/O
                         net (fo=1, routed)           0.034     3.467    dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/idx_reti_0[4]
    SLICE_X108Y361       FDRE                                         r  dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/ret_array_2_5.idx_ret_496/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=1427, routed)        2.719     3.860    dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/clk_c
    SLICE_X108Y361       FDRE                                         r  dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/ret_array_2_5.idx_ret_496/C
                         clock pessimism             -0.456     3.404    
    SLICE_X108Y361       FDRE (Hold_BFF_SLICEM_C_D)
                                                      0.053     3.457    dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/ret_array_2_5.idx_ret_496
  -------------------------------------------------------------------
                         required time                         -3.457    
                         arrival time                           3.467    
  -------------------------------------------------------------------
                         slack                                  0.010    

Slack (MET) :             0.010ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_1_5.idx_ret_10[4]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/ret_array_2_5.idx_ret_496/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.293ns  (logic 0.139ns (47.440%)  route 0.154ns (52.560%))
  Logic Levels:           1  (LUT5=1)
  Clock Path Skew:        0.230ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.860ns
    Source Clock Delay      (SCD):    3.174ns
    Clock Pessimism Removal (CPR):    0.456ns
  Clock Net Delay (Source):      2.344ns (routing 1.538ns, distribution 0.806ns)
  Clock Net Delay (Destination): 2.719ns (routing 1.683ns, distribution 1.036ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=1427, routed)        2.344     3.174    dut_inst/clk_c
    SLICE_X107Y358       FDRE                                         r  dut_inst/ret_array_1_5.idx_ret_10[4]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X107Y358       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.069     3.243 f  dut_inst/ret_array_1_5.idx_ret_10[4]/Q
                         net (fo=4, routed)           0.120     3.363    dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/muon_i_o_1
    SLICE_X108Y361       LUT5 (Prop_B6LUT_SLICEM_I0_O)
                                                      0.070     3.433 f  dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/ret_array_2_5.idx_ret_496_RNO/O
                         net (fo=1, routed)           0.034     3.467    dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/idx_reti_0[4]
    SLICE_X108Y361       FDRE                                         f  dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/ret_array_2_5.idx_ret_496/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=1427, routed)        2.719     3.860    dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/clk_c
    SLICE_X108Y361       FDRE                                         r  dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/ret_array_2_5.idx_ret_496/C
                         clock pessimism             -0.456     3.404    
    SLICE_X108Y361       FDRE (Hold_BFF_SLICEM_C_D)
                                                      0.053     3.457    dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/ret_array_2_5.idx_ret_496
  -------------------------------------------------------------------
                         required time                         -3.457    
                         arrival time                           3.467    
  -------------------------------------------------------------------
                         slack                                  0.010    

Slack (MET) :             0.015ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_1_5.idx_ret_15[5]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_2_5.idx_ret_15[5]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.172ns  (logic 0.069ns (40.116%)  route 0.103ns (59.884%))
  Logic Levels:           0  
  Clock Path Skew:        0.102ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.824ns
    Source Clock Delay      (SCD):    3.224ns
    Clock Pessimism Removal (CPR):    0.499ns
  Clock Net Delay (Source):      2.394ns (routing 1.538ns, distribution 0.856ns)
  Clock Net Delay (Destination): 2.683ns (routing 1.683ns, distribution 1.000ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=1427, routed)        2.394     3.224    dut_inst/clk_c
    SLICE_X105Y364       FDRE                                         r  dut_inst/ret_array_1_5.idx_ret_15[5]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X105Y364       FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     3.293 r  dut_inst/ret_array_1_5.idx_ret_15[5]/Q
                         net (fo=2, routed)           0.103     3.396    dut_inst/muon_i_o[228]
    SLICE_X106Y364       FDRE                                         r  dut_inst/ret_array_2_5.idx_ret_15[5]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=1427, routed)        2.683     3.824    dut_inst/clk_c
    SLICE_X106Y364       FDRE                                         r  dut_inst/ret_array_2_5.idx_ret_15[5]/C
                         clock pessimism             -0.499     3.325    
    SLICE_X106Y364       FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.055     3.380    dut_inst/ret_array_2_5.idx_ret_15[5]
  -------------------------------------------------------------------
                         required time                         -3.380    
                         arrival time                           3.396    
  -------------------------------------------------------------------
                         slack                                  0.015    

Slack (MET) :             0.015ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_1_5.idx_ret_15[5]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_2_5.idx_ret_15[5]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.172ns  (logic 0.069ns (40.116%)  route 0.103ns (59.884%))
  Logic Levels:           0  
  Clock Path Skew:        0.102ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.824ns
    Source Clock Delay      (SCD):    3.224ns
    Clock Pessimism Removal (CPR):    0.499ns
  Clock Net Delay (Source):      2.394ns (routing 1.538ns, distribution 0.856ns)
  Clock Net Delay (Destination): 2.683ns (routing 1.683ns, distribution 1.000ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=1427, routed)        2.394     3.224    dut_inst/clk_c
    SLICE_X105Y364       FDRE                                         r  dut_inst/ret_array_1_5.idx_ret_15[5]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X105Y364       FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     3.293 f  dut_inst/ret_array_1_5.idx_ret_15[5]/Q
                         net (fo=2, routed)           0.103     3.396    dut_inst/muon_i_o[228]
    SLICE_X106Y364       FDRE                                         f  dut_inst/ret_array_2_5.idx_ret_15[5]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=1427, routed)        2.683     3.824    dut_inst/clk_c
    SLICE_X106Y364       FDRE                                         r  dut_inst/ret_array_2_5.idx_ret_15[5]/C
                         clock pessimism             -0.499     3.325    
    SLICE_X106Y364       FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.055     3.380    dut_inst/ret_array_2_5.idx_ret_15[5]
  -------------------------------------------------------------------
                         required time                         -3.380    
                         arrival time                           3.396    
  -------------------------------------------------------------------
                         slack                                  0.015    

Slack (MET) :             0.016ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_1_5.idx_ret_10[4]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/ret_array_2_5.idx_ret_496/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.184ns  (logic 0.073ns (39.674%)  route 0.111ns (60.326%))
  Logic Levels:           1  (LUT5=1)
  Clock Path Skew:        0.122ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.333ns
    Source Clock Delay      (SCD):    1.890ns
    Clock Pessimism Removal (CPR):    0.321ns
  Clock Net Delay (Source):      1.344ns (routing 0.870ns, distribution 0.474ns)
  Clock Net Delay (Destination): 1.561ns (routing 0.965ns, distribution 0.596ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=1427, routed)        1.344     1.890    dut_inst/clk_c
    SLICE_X107Y358       FDRE                                         r  dut_inst/ret_array_1_5.idx_ret_10[4]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X107Y358       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.038     1.928 r  dut_inst/ret_array_1_5.idx_ret_10[4]/Q
                         net (fo=4, routed)           0.090     2.018    dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/muon_i_o_1
    SLICE_X108Y361       LUT5 (Prop_B6LUT_SLICEM_I0_O)
                                                      0.035     2.053 r  dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/ret_array_2_5.idx_ret_496_RNO/O
                         net (fo=1, routed)           0.021     2.074    dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/idx_reti_0[4]
    SLICE_X108Y361       FDRE                                         r  dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/ret_array_2_5.idx_ret_496/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=1427, routed)        1.561     2.333    dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/clk_c
    SLICE_X108Y361       FDRE                                         r  dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/ret_array_2_5.idx_ret_496/C
                         clock pessimism             -0.321     2.012    
    SLICE_X108Y361       FDRE (Hold_BFF_SLICEM_C_D)
                                                      0.046     2.058    dut_inst/stage_g.1.pair_g.6.csn_cmp_inst/ret_array_2_5.idx_ret_496
  -------------------------------------------------------------------
                         required time                         -2.058    
                         arrival time                           2.074    
  -------------------------------------------------------------------
                         slack                                  0.016    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.786 }
Period(ns):         3.572
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         3.572       2.073      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X101Y362  dut_inst/ret_array_1_11.pt_ret_43[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X101Y362  dut_inst/ret_array_1_11.pt_ret_43[1]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X100Y363  dut_inst/ret_array_1_11.pt_ret_43[2]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X100Y367  dut_inst/ret_array_1_11.pt_ret_43[3]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X98Y364   dut_inst/ret_array_1_11.pt_ret_45[2]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X98Y363   dut_inst/ret_array_1_5.idx_ret_532/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X106Y365  dut_inst/ret_array_2_5.idx_ret_208/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X104Y380  shift_reg_tap_o/sr_p.sr_1[144]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X106Y382  shift_reg_tap_o/sr_p.sr_1[152]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X101Y362  dut_inst/ret_array_1_11.pt_ret_43[0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X101Y362  dut_inst/ret_array_1_11.pt_ret_43[1]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X100Y363  dut_inst/ret_array_1_11.pt_ret_43[2]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X100Y367  dut_inst/ret_array_1_11.pt_ret_43[3]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X101Y366  dut_inst/ret_array_1_11.pt_ret_44[0]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X107Y358         lsfr_1/shiftreg_vector[140]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X107Y358         lsfr_1/shiftreg_vector[141]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X107Y358         lsfr_1/shiftreg_vector[142]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X107Y358         lsfr_1/shiftreg_vector[140]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X107Y358         lsfr_1/shiftreg_vector[141]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X107Y358         lsfr_1/shiftreg_vector[142]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X101Y359         lsfr_1/shiftreg_vector[144]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X101Y359         lsfr_1/shiftreg_vector[145]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X101Y359         lsfr_1/shiftreg_vector[146]/C



