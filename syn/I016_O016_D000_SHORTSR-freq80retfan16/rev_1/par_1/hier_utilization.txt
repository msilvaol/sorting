Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Thu Feb 14 13:16:43 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_utilization -hierarchical -file hier_utilization.txt
| Design       : wrapper
| Device       : xcvu9pflgc2104-1
| Design State : Routed
------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+-------------------+---------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
|      Instance     |                          Module                         | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+-------------------+---------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
| wrapper           |                                                   (top) |       6598 |       6598 |       0 |    0 | 2389 |      0 |      0 |    0 |            0 |
|   (wrapper)       |                                                   (top) |          0 |          0 |       0 |    0 |  256 |      0 |      0 |    0 |            0 |
|   lsfr_1          |                                                    lfsr |          1 |          1 |       0 |    0 |  257 |      0 |      0 |    0 |            0 |
|   dut_inst        | muon_sorter_I016_O016_D000_SHORTSR-freq80retfan16_rev_1 |       6512 |       6512 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   reducer_1       |                                                 reducer |         85 |         85 |       0 |    0 |  340 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_i |                                     shift_reg_tap_256_2 |          0 |          0 |       0 |    0 |  768 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_o |                                   shift_reg_tap_256_2_0 |          0 |          0 |       0 |    0 |  768 |      0 |      0 |    0 |            0 |
+-------------------+---------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
* Note: The sum of lower-level cells may be larger than their parent cells total, due to cross-hierarchy LUT combining


