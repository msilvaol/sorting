Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Tue Mar 19 15:01:08 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+-------+--------+
|  Fanout |  Nets |      % |
+---------+-------+--------+
|       1 | 13210 |  15.73 |
|       2 | 30224 |  36.00 |
|       3 | 19182 |  22.85 |
|       4 |  9949 |  11.85 |
|    5-10 |  2881 |   3.43 |
|   11-50 |  8345 |   9.94 |
|  51-100 |   146 |   0.17 |
| 101-500 |     0 |   0.00 |
|    >500 |     0 |   0.00 |
+---------+-------+--------+
|     ALL | 83937 | 100.00 |
+---------+-------+--------+


