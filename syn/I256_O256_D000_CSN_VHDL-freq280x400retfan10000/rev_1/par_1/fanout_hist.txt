Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Thu Apr  4 22:19:05 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper_csn
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+--------+--------+
|  Fanout |   Nets |      % |
+---------+--------+--------+
|       1 |  18165 |  17.18 |
|       2 |  29638 |  28.03 |
|       3 |  23092 |  21.84 |
|       4 |  14763 |  13.96 |
|    5-10 |  11870 |  11.22 |
|   11-50 |   8148 |   7.70 |
|  51-100 |     30 |   0.02 |
| 101-500 |      0 |   0.00 |
|    >500 |      0 |   0.00 |
+---------+--------+--------+
|     ALL | 105706 | 100.00 |
+---------+--------+--------+


