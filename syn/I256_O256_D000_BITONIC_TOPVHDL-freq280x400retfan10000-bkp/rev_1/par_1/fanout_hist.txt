Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Wed Mar 13 11:23:26 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+--------+--------+
|  Fanout |   Nets |      % |
+---------+--------+--------+
|       1 |  14456 |  11.63 |
|       2 |  40575 |  32.65 |
|       3 |  35980 |  28.95 |
|       4 |  14641 |  11.78 |
|    5-10 |   7051 |   5.67 |
|   11-50 |  11490 |   9.24 |
|  51-100 |     76 |   0.06 |
| 101-500 |      0 |   0.00 |
|    >500 |      0 |   0.00 |
+---------+--------+--------+
|     ALL | 124269 | 100.00 |
+---------+--------+--------+


