Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Tue Mar  5 23:43:01 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.017        0.000                      0                 1025        1.213        0.000                       0                  1722  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.786}        3.572           279.955         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.017        0.000                      0                 1025        1.213        0.000                       0                  1234  
clk_wrapper                                                                             498.562        0.000                       0                   488  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.017ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.213ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.017ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[137]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret_7_1_ret_array_1_12.idx_ret_8_1/D
                            (rising edge-triggered cell SRL16E clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.159ns  (logic 0.072ns (45.283%)  route 0.087ns (54.717%))
  Logic Levels:           0  
  Clock Path Skew:        0.130ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.707ns
    Source Clock Delay      (SCD):    2.173ns
    Clock Pessimism Removal (CPR):    0.404ns
  Clock Net Delay (Source):      1.343ns (routing 0.562ns, distribution 0.781ns)
  Clock Net Delay (Destination): 1.566ns (routing 0.616ns, distribution 0.950ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1233, routed)        1.343     2.173    shift_reg_tap_i/clk_c
    SLICE_X100Y574       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[137]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X100Y574       FDRE (Prop_EFF2_SLICEM_C_Q)
                                                      0.072     2.245 r  shift_reg_tap_i/sr_p.sr_1[137]/Q
                         net (fo=1, routed)           0.087     2.332    dut_inst/input_slr[137]
    SLICE_X102Y574       SRL16E                                       r  dut_inst/ret_array_1_12.idx_ret_7_1_ret_array_1_12.idx_ret_8_1/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1233, routed)        1.566     2.707    dut_inst/clk_c
    SLICE_X102Y574       SRL16E                                       r  dut_inst/ret_array_1_12.idx_ret_7_1_ret_array_1_12.idx_ret_8_1/CLK
                         clock pessimism             -0.404     2.303    
    SLICE_X102Y574       SRL16E (Hold_E6LUT_SLICEM_CLK_D)
                                                      0.012     2.315    dut_inst/ret_array_1_12.idx_ret_7_1_ret_array_1_12.idx_ret_8_1
  -------------------------------------------------------------------
                         required time                         -2.315    
                         arrival time                           2.332    
  -------------------------------------------------------------------
                         slack                                  0.017    

Slack (MET) :             0.017ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[137]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret_7_1_ret_array_1_12.idx_ret_8_1/D
                            (rising edge-triggered cell SRL16E clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.159ns  (logic 0.072ns (45.283%)  route 0.087ns (54.717%))
  Logic Levels:           0  
  Clock Path Skew:        0.130ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.707ns
    Source Clock Delay      (SCD):    2.173ns
    Clock Pessimism Removal (CPR):    0.404ns
  Clock Net Delay (Source):      1.343ns (routing 0.562ns, distribution 0.781ns)
  Clock Net Delay (Destination): 1.566ns (routing 0.616ns, distribution 0.950ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1233, routed)        1.343     2.173    shift_reg_tap_i/clk_c
    SLICE_X100Y574       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[137]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X100Y574       FDRE (Prop_EFF2_SLICEM_C_Q)
                                                      0.072     2.245 f  shift_reg_tap_i/sr_p.sr_1[137]/Q
                         net (fo=1, routed)           0.087     2.332    dut_inst/input_slr[137]
    SLICE_X102Y574       SRL16E                                       f  dut_inst/ret_array_1_12.idx_ret_7_1_ret_array_1_12.idx_ret_8_1/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1233, routed)        1.566     2.707    dut_inst/clk_c
    SLICE_X102Y574       SRL16E                                       r  dut_inst/ret_array_1_12.idx_ret_7_1_ret_array_1_12.idx_ret_8_1/CLK
                         clock pessimism             -0.404     2.303    
    SLICE_X102Y574       SRL16E (Hold_E6LUT_SLICEM_CLK_D)
                                                      0.012     2.315    dut_inst/ret_array_1_12.idx_ret_7_1_ret_array_1_12.idx_ret_8_1
  -------------------------------------------------------------------
                         required time                         -2.315    
                         arrival time                           2.332    
  -------------------------------------------------------------------
                         slack                                  0.017    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[179]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret_0_4_ret_array_1_12.idx_ret_8_1/D
                            (rising edge-triggered cell SRL16E clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.081ns  (logic 0.038ns (46.914%)  route 0.043ns (53.086%))
  Logic Levels:           0  
  Clock Path Skew:        0.045ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.683ns
    Source Clock Delay      (SCD):    1.326ns
    Clock Pessimism Removal (CPR):    0.312ns
  Clock Net Delay (Source):      0.780ns (routing 0.316ns, distribution 0.464ns)
  Clock Net Delay (Destination): 0.911ns (routing 0.348ns, distribution 0.563ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1233, routed)        0.780     1.326    shift_reg_tap_i/clk_c
    SLICE_X107Y573       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[179]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X107Y573       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.038     1.364 r  shift_reg_tap_i/sr_p.sr_1[179]/Q
                         net (fo=1, routed)           0.043     1.407    dut_inst/input_slr[179]
    SLICE_X107Y574       SRL16E                                       r  dut_inst/ret_array_1_12.idx_ret_0_4_ret_array_1_12.idx_ret_8_1/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1233, routed)        0.911     1.683    dut_inst/clk_c
    SLICE_X107Y574       SRL16E                                       r  dut_inst/ret_array_1_12.idx_ret_0_4_ret_array_1_12.idx_ret_8_1/CLK
                         clock pessimism             -0.312     1.371    
    SLICE_X107Y574       SRL16E (Hold_C6LUT_SLICEM_CLK_D)
                                                      0.016     1.387    dut_inst/ret_array_1_12.idx_ret_0_4_ret_array_1_12.idx_ret_8_1
  -------------------------------------------------------------------
                         required time                         -1.387    
                         arrival time                           1.407    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[179]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret_0_4_ret_array_1_12.idx_ret_8_1/D
                            (rising edge-triggered cell SRL16E clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.081ns  (logic 0.038ns (46.914%)  route 0.043ns (53.086%))
  Logic Levels:           0  
  Clock Path Skew:        0.045ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.683ns
    Source Clock Delay      (SCD):    1.326ns
    Clock Pessimism Removal (CPR):    0.312ns
  Clock Net Delay (Source):      0.780ns (routing 0.316ns, distribution 0.464ns)
  Clock Net Delay (Destination): 0.911ns (routing 0.348ns, distribution 0.563ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1233, routed)        0.780     1.326    shift_reg_tap_i/clk_c
    SLICE_X107Y573       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[179]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X107Y573       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.038     1.364 f  shift_reg_tap_i/sr_p.sr_1[179]/Q
                         net (fo=1, routed)           0.043     1.407    dut_inst/input_slr[179]
    SLICE_X107Y574       SRL16E                                       f  dut_inst/ret_array_1_12.idx_ret_0_4_ret_array_1_12.idx_ret_8_1/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1233, routed)        0.911     1.683    dut_inst/clk_c
    SLICE_X107Y574       SRL16E                                       r  dut_inst/ret_array_1_12.idx_ret_0_4_ret_array_1_12.idx_ret_8_1/CLK
                         clock pessimism             -0.312     1.371    
    SLICE_X107Y574       SRL16E (Hold_C6LUT_SLICEM_CLK_D)
                                                      0.016     1.387    dut_inst/ret_array_1_12.idx_ret_0_4_ret_array_1_12.idx_ret_8_1
  -------------------------------------------------------------------
                         required time                         -1.387    
                         arrival time                           1.407    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.024ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_3_0.pt_ret[1]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_4_0.pt_ret[1]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.077ns  (logic 0.038ns (49.351%)  route 0.039ns (50.649%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.671ns
    Source Clock Delay      (SCD):    1.339ns
    Clock Pessimism Removal (CPR):    0.326ns
  Clock Net Delay (Source):      0.793ns (routing 0.316ns, distribution 0.477ns)
  Clock Net Delay (Destination): 0.899ns (routing 0.348ns, distribution 0.551ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1233, routed)        0.793     1.339    dut_inst/clk_c
    SLICE_X100Y595       FDRE                                         r  dut_inst/ret_array_3_0.pt_ret[1]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X100Y595       FDRE (Prop_AFF_SLICEM_C_Q)
                                                      0.038     1.377 r  dut_inst/ret_array_3_0.pt_ret[1]/Q
                         net (fo=1, routed)           0.039     1.416    dut_inst/pt_o_o_o_1[1]
    SLICE_X100Y595       FDRE                                         r  dut_inst/ret_array_4_0.pt_ret[1]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1233, routed)        0.899     1.671    dut_inst/clk_c
    SLICE_X100Y595       FDRE                                         r  dut_inst/ret_array_4_0.pt_ret[1]/C
                         clock pessimism             -0.326     1.345    
    SLICE_X100Y595       FDRE (Hold_BFF2_SLICEM_C_D)
                                                      0.047     1.392    dut_inst/ret_array_4_0.pt_ret[1]
  -------------------------------------------------------------------
                         required time                         -1.392    
                         arrival time                           1.416    
  -------------------------------------------------------------------
                         slack                                  0.024    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.786 }
Period(ns):         3.572
Sources:            { clk }

Check Type        Corner  Lib Pin     Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I    n/a            1.499         3.572       2.073      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.572       2.426      SLICE_X108Y573  dut_inst/ret_array_1_12.idx_ret_0_0_ret_array_1_12.idx_ret_8_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.572       2.426      SLICE_X108Y573  dut_inst/ret_array_1_12.idx_ret_0_10_ret_array_1_12.idx_ret_8_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.572       2.426      SLICE_X108Y573  dut_inst/ret_array_1_12.idx_ret_0_11_ret_array_1_12.idx_ret_8_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.572       2.426      SLICE_X107Y574  dut_inst/ret_array_1_12.idx_ret_0_12_ret_array_1_12.idx_ret_8_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X108Y573  dut_inst/ret_array_1_12.idx_ret_0_0_ret_array_1_12.idx_ret_8_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X108Y573  dut_inst/ret_array_1_12.idx_ret_0_10_ret_array_1_12.idx_ret_8_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X108Y573  dut_inst/ret_array_1_12.idx_ret_0_11_ret_array_1_12.idx_ret_8_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X107Y574  dut_inst/ret_array_1_12.idx_ret_0_12_ret_array_1_12.idx_ret_8_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X97Y573   dut_inst/ret_array_1_12.idx_ret_14_14_ret_array_1_12.idx_ret_8_1/CLK
High Pulse Width  Fast    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X108Y573  dut_inst/ret_array_1_12.idx_ret_0_0_ret_array_1_12.idx_ret_8_1/CLK
High Pulse Width  Fast    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X108Y573  dut_inst/ret_array_1_12.idx_ret_0_10_ret_array_1_12.idx_ret_8_1/CLK
High Pulse Width  Fast    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X108Y573  dut_inst/ret_array_1_12.idx_ret_0_11_ret_array_1_12.idx_ret_8_1/CLK
High Pulse Width  Fast    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X95Y579   dut_inst/ret_array_1_12.idx_ret_14_13_ret_array_1_12.idx_ret_8_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X97Y573   dut_inst/ret_array_1_12.idx_ret_14_14_ret_array_1_12.idx_ret_8_1/CLK



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X99Y572          lsfr_1/shiftreg_vector[19]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X98Y585          lsfr_1/shiftreg_vector[1]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X105Y593         lsfr_1/shiftreg_vector[200]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X105Y593         lsfr_1/shiftreg_vector[200]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X100Y577         lsfr_1/shiftreg_vector[207]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X99Y575          lsfr_1/shiftreg_vector[26]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X99Y572          lsfr_1/shiftreg_vector[19]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X98Y585          lsfr_1/shiftreg_vector[1]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X99Y572          lsfr_1/shiftreg_vector[20]/C



