Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Wed Mar  6 17:03:53 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_utilization -hierarchical -file hier_utilization.txt
| Design       : wrapper_csn
| Device       : xcvu9pflgc2104-1
| Design State : Routed
------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+-------------------------------------+------------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
|               Instance              |                                 Module                                 | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+-------------------------------------+------------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
| wrapper_csn                         |                                                                  (top) |       1340 |       1278 |       0 |   62 | 1373 |      0 |      0 |    0 |            0 |
|   (wrapper_csn)                     |                                                                  (top) |          0 |          0 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   dut_inst                          | muon_sorter_I016_O016_D000_CSN_VHDL_DATAFMT14-freq280retfan10000_rev_1 |       1270 |       1208 |       0 |   62 |  435 |      0 |      0 |    0 |            0 |
|     (dut_inst)                      | muon_sorter_I016_O016_D000_CSN_VHDL_DATAFMT14-freq280retfan10000_rev_1 |         65 |          3 |       0 |   62 |  185 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.0.csn_cmp_inst |                                            csn_cmp_false_false_false_6 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.1.csn_cmp_inst |                                           csn_cmp_false_false_false_40 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.2.csn_cmp_inst |                                            csn_cmp_false_false_false_4 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.3.csn_cmp_inst |                                           csn_cmp_false_false_false_30 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.4.csn_cmp_inst |                                           csn_cmp_false_false_false_35 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.5.csn_cmp_inst |                                           csn_cmp_false_false_false_45 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.6.csn_cmp_inst |                                           csn_cmp_false_false_false_22 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.7.csn_cmp_inst |                                           csn_cmp_false_false_false_57 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.0.csn_cmp_inst |                                           csn_cmp_false_false_false_27 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.1.csn_cmp_inst |                                           csn_cmp_false_false_false_13 |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.2.csn_cmp_inst |                                           csn_cmp_false_false_false_44 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.3.csn_cmp_inst |                                           csn_cmp_false_false_false_28 |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.4.csn_cmp_inst |                                           csn_cmp_false_false_false_26 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.5.csn_cmp_inst |                                           csn_cmp_false_false_false_11 |         24 |         24 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.6.csn_cmp_inst |                                           csn_cmp_false_false_false_39 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.7.csn_cmp_inst |                                           csn_cmp_false_false_false_10 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.0.csn_cmp_inst |                                           csn_cmp_false_false_false_32 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.1.csn_cmp_inst |                                           csn_cmp_false_false_false_15 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.2.csn_cmp_inst |                                           csn_cmp_false_false_false_34 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.3.csn_cmp_inst |                                           csn_cmp_false_false_false_21 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.4.csn_cmp_inst |                                           csn_cmp_false_false_false_16 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.5.csn_cmp_inst |                                           csn_cmp_false_false_false_59 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.6.csn_cmp_inst |                                           csn_cmp_false_false_false_51 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.7.csn_cmp_inst |                                           csn_cmp_false_false_false_12 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.0.csn_cmp_inst |                                           csn_cmp_false_false_false_17 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.1.csn_cmp_inst |                                           csn_cmp_false_false_false_25 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.2.csn_cmp_inst |                                           csn_cmp_false_false_false_43 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.3.csn_cmp_inst |                                            csn_cmp_false_false_false_7 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.4.csn_cmp_inst |                                            csn_cmp_false_false_false_9 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.5.csn_cmp_inst |                                            csn_cmp_false_false_false_5 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.6.csn_cmp_inst |                                           csn_cmp_false_false_false_31 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.7.csn_cmp_inst |                                              csn_cmp_false_false_false |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.0.csn_cmp_inst |                                           csn_cmp_false_false_false_20 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.1.csn_cmp_inst |                                           csn_cmp_false_false_false_47 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.2.csn_cmp_inst |                                           csn_cmp_false_false_false_58 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.3.csn_cmp_inst |                                           csn_cmp_false_false_false_50 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.4.csn_cmp_inst |                                           csn_cmp_false_false_false_14 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.5.csn_cmp_inst |                                           csn_cmp_false_false_false_33 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.6.csn_cmp_inst |                                           csn_cmp_false_false_false_24 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.0.csn_cmp_inst |                                           csn_cmp_false_false_false_41 |         22 |         22 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.1.csn_cmp_inst |                                            csn_cmp_false_false_false_1 |         18 |         18 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.2.csn_cmp_inst |                                            csn_cmp_false_false_false_8 |         24 |         24 |       0 |    0 |    9 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.3.csn_cmp_inst |                                           csn_cmp_false_false_false_37 |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.4.csn_cmp_inst |                                            csn_cmp_false_false_false_0 |         24 |         24 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.5.csn_cmp_inst |                                           csn_cmp_false_false_false_53 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.6.csn_cmp_inst |                                           csn_cmp_false_false_false_55 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.0.csn_cmp_inst |                                            csn_cmp_false_false_false_3 |         25 |         25 |       0 |    0 |    9 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.1.csn_cmp_inst |                                           csn_cmp_false_false_false_18 |         29 |         29 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.2.csn_cmp_inst |                                           csn_cmp_false_false_false_49 |         30 |         30 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.3.csn_cmp_inst |                                           csn_cmp_false_false_false_48 |         18 |         18 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.4.csn_cmp_inst |                                           csn_cmp_false_false_false_19 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.5.csn_cmp_inst |                                           csn_cmp_false_false_false_29 |         29 |         29 |       0 |    0 |   17 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.0.csn_cmp_inst |                                           csn_cmp_false_false_false_36 |         21 |         21 |       0 |    0 |   23 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.1.csn_cmp_inst |                                           csn_cmp_false_false_false_38 |         20 |         20 |       0 |    0 |   54 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.2.csn_cmp_inst |                                           csn_cmp_false_false_false_52 |         19 |         19 |       0 |    0 |   16 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.3.csn_cmp_inst |                                           csn_cmp_false_false_false_54 |         16 |         16 |       0 |    0 |   70 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.0.csn_cmp_inst |                                           csn_cmp_false_false_false_56 |         30 |         30 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.1.csn_cmp_inst |                                            csn_cmp_false_false_false_2 |         28 |         28 |       0 |    0 |   10 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.2.csn_cmp_inst |                                           csn_cmp_false_false_false_23 |         28 |         28 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.3.csn_cmp_inst |                                           csn_cmp_false_false_false_42 |         27 |         27 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.4.csn_cmp_inst |                                           csn_cmp_false_false_false_46 |         29 |         29 |       0 |    0 |   20 |      0 |      0 |    0 |            0 |
|   lsfr_1                            |                                                                   lfsr |          1 |          1 |       0 |    0 |  209 |      0 |      0 |    0 |            0 |
|   reducer_1                         |                                                                reducer |         69 |         69 |       0 |    0 |  277 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_i                   |                                                    shift_reg_tap_208_1 |          0 |          0 |       0 |    0 |  244 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_o                   |                                                    shift_reg_tap_256_1 |          0 |          0 |       0 |    0 |  208 |      0 |      0 |    0 |            0 |
+-------------------------------------+------------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
* Note: The sum of lower-level cells may be larger than their parent cells total, due to cross-hierarchy LUT combining


