#--  Synopsys, Inc.
#--  Version O-2018.09
#--  Project file D:\mygitlab\sorting\syn\I016_O016_D002_IORET-freq320retfan10000\rev_1\run_options.txt
#--  Written on Wed Feb 13 21:50:51 2019


#project files
add_file -vhdl -lib work "D:/mygitlab/sorting/src/rtl/MuctpiDataTypes.vhd"
add_file -vhdl -lib work "D:/mygitlab/sorting/src/rtl/MuctpiFunctions.vhd"
add_file -vhdl -lib work "D:/mygitlab/sorting/src/rtl/muon_sorter.vhd"
add_file -constraint "D:/mygitlab/sorting/src/xdc/clock_320.sdc"
add_file -constraint "D:/mygitlab/sorting/src/xdc/muon_sorter.sdc"



#implementation: "rev_1"
impl -add rev_1 -type fpga

#
#implementation attributes

set_option -vlog_std sysv
set_option -project_relative_includes 1

#
#implementation parameter settings
set_option -hdl_param -set I 16
set_option -hdl_param -set O 16
set_option -hdl_param -set delay 2
set_option -hdl_param -set num_in 16
set_option -hdl_param -set num_out 16
set_option -include_path {../}
set_option -library_path {../}

#pr_1 attributes
set_option -job pr_1 -add par

#device options
set_option -technology VIRTEX-ULTRASCALEPLUS-FPGAS
set_option -part XCVU9P
set_option -package FLGC2104
set_option -speed_grade -1-e
set_option -part_companion ""

#compilation/mapping options
set_option -use_fsm_explorer 0
set_option -top_module "work.muon_sorter"

# hdl_compiler_options
set_option -distributed_compile 1

# mapper_without_write_options
set_option -frequency 320
set_option -srs_instrumentation 1

# mapper_options
set_option -write_verilog 1
set_option -write_vhdl 0

# xilinx_options
set_option -rw_check_on_ram 1
set_option -optimize_ngc 1

# Xilinx Virtex2
set_option -run_prop_extract 1
set_option -maxfan 10000
set_option -disable_io_insertion 1
set_option -pipe 1
set_option -update_models_cp 0
set_option -retiming 1
set_option -no_sequential_opt 0
set_option -fix_gated_and_generated_clocks 1

# Xilinx Virtex UltraScale+ FPGAs
set_option -enable_prepacking 1
set_option -use_vivado 1

# sequential_optimization_options
set_option -symbolic_fsm_compiler 1

# Compiler Options
set_option -compiler_compatible 0
set_option -resource_sharing 1
set_option -multi_file_compilation_unit 1

# Compiler Options
set_option -auto_infer_blackbox 0

# Compiler Options
set_option -vhdl2008 1

#automatic place and route (vendor) options
set_option -write_apr_constraint 1

#set result format/file last
project -result_file "../rev_1/muon_sorter.edf"

#design plan options
impl -active "rev_1"
