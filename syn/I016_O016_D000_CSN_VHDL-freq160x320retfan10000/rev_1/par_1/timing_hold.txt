Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Tue Mar  5 22:36:25 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.184        0.000                      0                  208        2.850        0.000                       0                   905  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 3.125}        6.250           160.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.184        0.000                      0                  208        2.850        0.000                       0                   417  
clk_wrapper                                                                             498.562        0.000                       0                   488  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.184ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        2.850ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.184ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1_fast[133]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[16]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.270ns  (logic 0.093ns (34.444%)  route 0.177ns (65.556%))
  Logic Levels:           2  (LUT6=2)
  Clock Path Skew:        0.040ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.626ns
    Source Clock Delay      (SCD):    1.305ns
    Clock Pessimism Removal (CPR):    0.282ns
  Clock Net Delay (Source):      0.759ns (routing 0.316ns, distribution 0.443ns)
  Clock Net Delay (Destination): 0.854ns (routing 0.348ns, distribution 0.506ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=416, routed)         0.759     1.305    shift_reg_tap_i/clk_c
    SLICE_X95Y567        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1_fast[133]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X95Y567        FDRE (Prop_HFF_SLICEM_C_Q)
                                                      0.039     1.344 r  shift_reg_tap_i/sr_p.sr_1_fast[133]/Q
                         net (fo=29, routed)          0.088     1.432    dut_inst/stage_g.2.pair_g.4.csn_cmp_inst/input_slr_26
    SLICE_X93Y568        LUT6 (Prop_C6LUT_SLICEM_I2_O)
                                                      0.040     1.472 r  dut_inst/stage_g.2.pair_g.4.csn_cmp_inst/b_o_comb.pt[3]/O
                         net (fo=10, routed)          0.072     1.544    dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/pt_37_0
    SLICE_X94Y569        LUT6 (Prop_C6LUT_SLICEL_I3_O)
                                                      0.014     1.558 r  dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/a_o_comb.pt_0[3]/O
                         net (fo=1, routed)           0.017     1.575    shift_reg_tap_o/un1_dut_inst_0_13
    SLICE_X94Y569        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[16]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=416, routed)         0.854     1.626    shift_reg_tap_o/clk_c
    SLICE_X94Y569        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[16]/C
                         clock pessimism             -0.282     1.345    
    SLICE_X94Y569        FDRE (Hold_CFF_SLICEL_C_D)
                                                      0.046     1.391    shift_reg_tap_o/sr_p.sr_1[16]
  -------------------------------------------------------------------
                         required time                         -1.391    
                         arrival time                           1.575    
  -------------------------------------------------------------------
                         slack                                  0.184    

Slack (MET) :             0.184ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1_fast[133]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[16]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.270ns  (logic 0.093ns (34.444%)  route 0.177ns (65.556%))
  Logic Levels:           2  (LUT6=2)
  Clock Path Skew:        0.040ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.626ns
    Source Clock Delay      (SCD):    1.305ns
    Clock Pessimism Removal (CPR):    0.282ns
  Clock Net Delay (Source):      0.759ns (routing 0.316ns, distribution 0.443ns)
  Clock Net Delay (Destination): 0.854ns (routing 0.348ns, distribution 0.506ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=416, routed)         0.759     1.305    shift_reg_tap_i/clk_c
    SLICE_X95Y567        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1_fast[133]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X95Y567        FDRE (Prop_HFF_SLICEM_C_Q)
                                                      0.039     1.344 f  shift_reg_tap_i/sr_p.sr_1_fast[133]/Q
                         net (fo=29, routed)          0.088     1.432    dut_inst/stage_g.2.pair_g.4.csn_cmp_inst/input_slr_26
    SLICE_X93Y568        LUT6 (Prop_C6LUT_SLICEM_I2_O)
                                                      0.040     1.472 f  dut_inst/stage_g.2.pair_g.4.csn_cmp_inst/b_o_comb.pt[3]/O
                         net (fo=10, routed)          0.072     1.544    dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/pt_37_0
    SLICE_X94Y569        LUT6 (Prop_C6LUT_SLICEL_I3_O)
                                                      0.014     1.558 f  dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/a_o_comb.pt_0[3]/O
                         net (fo=1, routed)           0.017     1.575    shift_reg_tap_o/un1_dut_inst_0_13
    SLICE_X94Y569        FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[16]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=416, routed)         0.854     1.626    shift_reg_tap_o/clk_c
    SLICE_X94Y569        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[16]/C
                         clock pessimism             -0.282     1.345    
    SLICE_X94Y569        FDRE (Hold_CFF_SLICEL_C_D)
                                                      0.046     1.391    shift_reg_tap_o/sr_p.sr_1[16]
  -------------------------------------------------------------------
                         required time                         -1.391    
                         arrival time                           1.575    
  -------------------------------------------------------------------
                         slack                                  0.184    

Slack (MET) :             0.222ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1_fast[146]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[16]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.308ns  (logic 0.112ns (36.364%)  route 0.196ns (63.636%))
  Logic Levels:           2  (LUT6=2)
  Clock Path Skew:        0.040ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.626ns
    Source Clock Delay      (SCD):    1.305ns
    Clock Pessimism Removal (CPR):    0.282ns
  Clock Net Delay (Source):      0.759ns (routing 0.316ns, distribution 0.443ns)
  Clock Net Delay (Destination): 0.854ns (routing 0.348ns, distribution 0.506ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=416, routed)         0.759     1.305    shift_reg_tap_i/clk_c
    SLICE_X95Y567        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1_fast[146]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X95Y567        FDRE (Prop_HFF2_SLICEM_C_Q)
                                                      0.039     1.344 r  shift_reg_tap_i/sr_p.sr_1_fast[146]/Q
                         net (fo=29, routed)          0.107     1.451    dut_inst/stage_g.2.pair_g.4.csn_cmp_inst/input_slr_39
    SLICE_X93Y568        LUT6 (Prop_C6LUT_SLICEM_I3_O)
                                                      0.059     1.510 r  dut_inst/stage_g.2.pair_g.4.csn_cmp_inst/b_o_comb.pt[3]/O
                         net (fo=10, routed)          0.072     1.582    dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/pt_37_0
    SLICE_X94Y569        LUT6 (Prop_C6LUT_SLICEL_I3_O)
                                                      0.014     1.596 r  dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/a_o_comb.pt_0[3]/O
                         net (fo=1, routed)           0.017     1.613    shift_reg_tap_o/un1_dut_inst_0_13
    SLICE_X94Y569        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[16]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=416, routed)         0.854     1.626    shift_reg_tap_o/clk_c
    SLICE_X94Y569        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[16]/C
                         clock pessimism             -0.282     1.345    
    SLICE_X94Y569        FDRE (Hold_CFF_SLICEL_C_D)
                                                      0.046     1.391    shift_reg_tap_o/sr_p.sr_1[16]
  -------------------------------------------------------------------
                         required time                         -1.391    
                         arrival time                           1.613    
  -------------------------------------------------------------------
                         slack                                  0.222    

Slack (MET) :             0.222ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1_fast[146]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[16]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.308ns  (logic 0.112ns (36.364%)  route 0.196ns (63.636%))
  Logic Levels:           2  (LUT6=2)
  Clock Path Skew:        0.040ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.626ns
    Source Clock Delay      (SCD):    1.305ns
    Clock Pessimism Removal (CPR):    0.282ns
  Clock Net Delay (Source):      0.759ns (routing 0.316ns, distribution 0.443ns)
  Clock Net Delay (Destination): 0.854ns (routing 0.348ns, distribution 0.506ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=416, routed)         0.759     1.305    shift_reg_tap_i/clk_c
    SLICE_X95Y567        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1_fast[146]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X95Y567        FDRE (Prop_HFF2_SLICEM_C_Q)
                                                      0.039     1.344 f  shift_reg_tap_i/sr_p.sr_1_fast[146]/Q
                         net (fo=29, routed)          0.107     1.451    dut_inst/stage_g.2.pair_g.4.csn_cmp_inst/input_slr_39
    SLICE_X93Y568        LUT6 (Prop_C6LUT_SLICEM_I3_O)
                                                      0.059     1.510 f  dut_inst/stage_g.2.pair_g.4.csn_cmp_inst/b_o_comb.pt[3]/O
                         net (fo=10, routed)          0.072     1.582    dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/pt_37_0
    SLICE_X94Y569        LUT6 (Prop_C6LUT_SLICEL_I3_O)
                                                      0.014     1.596 f  dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/a_o_comb.pt_0[3]/O
                         net (fo=1, routed)           0.017     1.613    shift_reg_tap_o/un1_dut_inst_0_13
    SLICE_X94Y569        FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[16]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=416, routed)         0.854     1.626    shift_reg_tap_o/clk_c
    SLICE_X94Y569        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[16]/C
                         clock pessimism             -0.282     1.345    
    SLICE_X94Y569        FDRE (Hold_CFF_SLICEL_C_D)
                                                      0.046     1.391    shift_reg_tap_o/sr_p.sr_1[16]
  -------------------------------------------------------------------
                         required time                         -1.391    
                         arrival time                           1.613    
  -------------------------------------------------------------------
                         slack                                  0.222    

Slack (MET) :             0.233ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[3]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[3]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.297ns  (logic 0.096ns (32.323%)  route 0.201ns (67.677%))
  Logic Levels:           2  (LUT4=2)
  Clock Path Skew:        0.018ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.628ns
    Source Clock Delay      (SCD):    1.300ns
    Clock Pessimism Removal (CPR):    0.311ns
  Clock Net Delay (Source):      0.754ns (routing 0.316ns, distribution 0.438ns)
  Clock Net Delay (Destination): 0.856ns (routing 0.348ns, distribution 0.508ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=416, routed)         0.754     1.300    shift_reg_tap_i/clk_c
    SLICE_X95Y571        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[3]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X95Y571        FDRE (Prop_HFF2_SLICEM_C_Q)
                                                      0.039     1.339 r  shift_reg_tap_i/sr_p.sr_1[3]/Q
                         net (fo=21, routed)          0.067     1.406    dut_inst/stage_g.1.pair_g.0.csn_cmp_inst/input_slr_2
    SLICE_X95Y572        LUT4 (Prop_C6LUT_SLICEM_I0_O)
                                                      0.022     1.428 r  dut_inst/stage_g.1.pair_g.0.csn_cmp_inst/a_o_comb.pt[3]/O
                         net (fo=9, routed)           0.113     1.541    dut_inst/stage_g.3.pair_g.0.csn_cmp_inst/pt_72_0
    SLICE_X95Y572        LUT4 (Prop_H6LUT_SLICEM_I1_O)
                                                      0.035     1.576 r  dut_inst/stage_g.3.pair_g.0.csn_cmp_inst/a_o_comb.pt_0[3]/O
                         net (fo=1, routed)           0.021     1.597    shift_reg_tap_o/un1_dut_inst_0_0
    SLICE_X95Y572        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[3]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=416, routed)         0.856     1.628    shift_reg_tap_o/clk_c
    SLICE_X95Y572        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[3]/C
                         clock pessimism             -0.311     1.318    
    SLICE_X95Y572        FDRE (Hold_HFF_SLICEM_C_D)
                                                      0.046     1.364    shift_reg_tap_o/sr_p.sr_1[3]
  -------------------------------------------------------------------
                         required time                         -1.364    
                         arrival time                           1.597    
  -------------------------------------------------------------------
                         slack                                  0.233    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 3.125 }
Period(ns):         6.250
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location       Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         6.250       4.751      BUFGCE_X1Y224  clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X95Y567  shift_reg_tap_i/sr_p.sr_1[143]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X95Y567  shift_reg_tap_i/sr_p.sr_1[144]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X95Y567  shift_reg_tap_i/sr_p.sr_1[145]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X94Y560  shift_reg_tap_i/sr_p.sr_1[147]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X94Y565  shift_reg_tap_i/sr_p.sr_1[158]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X94Y565  shift_reg_tap_i/sr_p.sr_1[159]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X92Y563  shift_reg_tap_i/sr_p.sr_1[165]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X92Y563  shift_reg_tap_i/sr_p.sr_1[167]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X94Y565  shift_reg_tap_i/sr_p.sr_1[171]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X93Y559  shift_reg_tap_i/sr_p.sr_1[150]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X93Y559  shift_reg_tap_i/sr_p.sr_1[151]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X93Y559  shift_reg_tap_i/sr_p.sr_1[152]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X94Y562  shift_reg_tap_i/sr_p.sr_1[160]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X94Y574  shift_reg_tap_i/sr_p.sr_1[71]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X93Y562          lsfr_1/shiftreg_vector[160]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X93Y562          lsfr_1/shiftreg_vector[161]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X93Y562          lsfr_1/shiftreg_vector[162]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X93Y562          lsfr_1/shiftreg_vector[160]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X93Y562          lsfr_1/shiftreg_vector[161]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X93Y562          lsfr_1/shiftreg_vector[162]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X93Y562          lsfr_1/shiftreg_vector[160]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X93Y562          lsfr_1/shiftreg_vector[161]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X93Y562          lsfr_1/shiftreg_vector[162]/C



