Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Sat Feb  9 21:26:29 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+-------+--------+
|  Fanout |  Nets |      % |
+---------+-------+--------+
|       1 | 22499 |  58.95 |
|       2 |  3626 |   9.50 |
|       3 |  2295 |   6.01 |
|       4 |  1598 |   4.18 |
|    5-10 |  4480 |  11.73 |
|   11-50 |  3615 |   9.47 |
|  51-100 |    50 |   0.13 |
| 101-500 |     0 |   0.00 |
|    >500 |     0 |   0.00 |
+---------+-------+--------+
|     ALL | 38163 | 100.00 |
+---------+-------+--------+


