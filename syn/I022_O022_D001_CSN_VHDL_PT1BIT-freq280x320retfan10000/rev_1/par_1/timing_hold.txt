Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Mon Mar 11 18:08:06 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.036        0.000                      0                 1016        1.511        0.000                       0                  1595  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.786}        3.572           279.955         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.036        0.000                      0                 1016        1.511        0.000                       0                  1079  
clk_wrapper                                                                             498.562        0.000                       0                   516  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.036ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.511ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.036ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[74]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_17.idx_ret_2213[3]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.111ns  (logic 0.040ns (36.036%)  route 0.071ns (63.964%))
  Logic Levels:           0  
  Clock Path Skew:        0.029ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.875ns
    Source Clock Delay      (SCD):    1.512ns
    Clock Pessimism Removal (CPR):    0.334ns
  Clock Net Delay (Source):      0.966ns (routing 0.522ns, distribution 0.444ns)
  Clock Net Delay (Destination): 1.103ns (routing 0.578ns, distribution 0.525ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1078, routed)        0.966     1.512    shift_reg_tap_i/clk_c
    SLICE_X89Y515        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[74]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X89Y515        FDRE (Prop_EFF2_SLICEL_C_Q)
                                                      0.040     1.552 r  shift_reg_tap_i/sr_p.sr_1[74]/Q
                         net (fo=4, routed)           0.071     1.623    dut_inst/input_slr[74]
    SLICE_X89Y514        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_2213[3]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1078, routed)        1.103     1.875    dut_inst/clk_c
    SLICE_X89Y514        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_2213[3]/C
                         clock pessimism             -0.334     1.541    
    SLICE_X89Y514        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.046     1.587    dut_inst/ret_array_1_17.idx_ret_2213[3]
  -------------------------------------------------------------------
                         required time                         -1.587    
                         arrival time                           1.623    
  -------------------------------------------------------------------
                         slack                                  0.036    

Slack (MET) :             0.036ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[74]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_17.idx_ret_2213[3]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.111ns  (logic 0.040ns (36.036%)  route 0.071ns (63.964%))
  Logic Levels:           0  
  Clock Path Skew:        0.029ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.875ns
    Source Clock Delay      (SCD):    1.512ns
    Clock Pessimism Removal (CPR):    0.334ns
  Clock Net Delay (Source):      0.966ns (routing 0.522ns, distribution 0.444ns)
  Clock Net Delay (Destination): 1.103ns (routing 0.578ns, distribution 0.525ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1078, routed)        0.966     1.512    shift_reg_tap_i/clk_c
    SLICE_X89Y515        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[74]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X89Y515        FDRE (Prop_EFF2_SLICEL_C_Q)
                                                      0.040     1.552 f  shift_reg_tap_i/sr_p.sr_1[74]/Q
                         net (fo=4, routed)           0.071     1.623    dut_inst/input_slr[74]
    SLICE_X89Y514        FDRE                                         f  dut_inst/ret_array_1_17.idx_ret_2213[3]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1078, routed)        1.103     1.875    dut_inst/clk_c
    SLICE_X89Y514        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_2213[3]/C
                         clock pessimism             -0.334     1.541    
    SLICE_X89Y514        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.046     1.587    dut_inst/ret_array_1_17.idx_ret_2213[3]
  -------------------------------------------------------------------
                         required time                         -1.587    
                         arrival time                           1.623    
  -------------------------------------------------------------------
                         slack                                  0.036    

Slack (MET) :             0.037ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[100]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_7/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.110ns  (logic 0.052ns (47.273%)  route 0.058ns (52.727%))
  Logic Levels:           1  (LUT6=1)
  Clock Path Skew:        0.027ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.887ns
    Source Clock Delay      (SCD):    1.527ns
    Clock Pessimism Removal (CPR):    0.334ns
  Clock Net Delay (Source):      0.981ns (routing 0.522ns, distribution 0.459ns)
  Clock Net Delay (Destination): 1.115ns (routing 0.578ns, distribution 0.537ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1078, routed)        0.981     1.527    shift_reg_tap_i/clk_c
    SLICE_X90Y495        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[100]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X90Y495        FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.038     1.565 r  shift_reg_tap_i/sr_p.sr_1[100]/Q
                         net (fo=34, routed)          0.040     1.605    dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/input_slr_0
    SLICE_X90Y496        LUT6 (Prop_A6LUT_SLICEM_I0_O)
                                                      0.014     1.619 r  dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_7_RNO/O
                         net (fo=1, routed)           0.018     1.637    dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/idx_reti[8]
    SLICE_X90Y496        FDRE                                         r  dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_7/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1078, routed)        1.115     1.887    dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/clk_c
    SLICE_X90Y496        FDRE                                         r  dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_7/C
                         clock pessimism             -0.334     1.553    
    SLICE_X90Y496        FDRE (Hold_AFF_SLICEM_C_D)
                                                      0.046     1.599    dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_7
  -------------------------------------------------------------------
                         required time                         -1.599    
                         arrival time                           1.637    
  -------------------------------------------------------------------
                         slack                                  0.037    

Slack (MET) :             0.037ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[100]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_7/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.110ns  (logic 0.052ns (47.273%)  route 0.058ns (52.727%))
  Logic Levels:           1  (LUT6=1)
  Clock Path Skew:        0.027ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.887ns
    Source Clock Delay      (SCD):    1.527ns
    Clock Pessimism Removal (CPR):    0.334ns
  Clock Net Delay (Source):      0.981ns (routing 0.522ns, distribution 0.459ns)
  Clock Net Delay (Destination): 1.115ns (routing 0.578ns, distribution 0.537ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1078, routed)        0.981     1.527    shift_reg_tap_i/clk_c
    SLICE_X90Y495        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[100]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X90Y495        FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.038     1.565 f  shift_reg_tap_i/sr_p.sr_1[100]/Q
                         net (fo=34, routed)          0.040     1.605    dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/input_slr_0
    SLICE_X90Y496        LUT6 (Prop_A6LUT_SLICEM_I0_O)
                                                      0.014     1.619 r  dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_7_RNO/O
                         net (fo=1, routed)           0.018     1.637    dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/idx_reti[8]
    SLICE_X90Y496        FDRE                                         r  dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_7/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1078, routed)        1.115     1.887    dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/clk_c
    SLICE_X90Y496        FDRE                                         r  dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_7/C
                         clock pessimism             -0.334     1.553    
    SLICE_X90Y496        FDRE (Hold_AFF_SLICEM_C_D)
                                                      0.046     1.599    dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_7
  -------------------------------------------------------------------
                         required time                         -1.599    
                         arrival time                           1.637    
  -------------------------------------------------------------------
                         slack                                  0.037    

Slack (MET) :             0.037ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[100]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_7/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.110ns  (logic 0.052ns (47.273%)  route 0.058ns (52.727%))
  Logic Levels:           1  (LUT6=1)
  Clock Path Skew:        0.027ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.887ns
    Source Clock Delay      (SCD):    1.527ns
    Clock Pessimism Removal (CPR):    0.334ns
  Clock Net Delay (Source):      0.981ns (routing 0.522ns, distribution 0.459ns)
  Clock Net Delay (Destination): 1.115ns (routing 0.578ns, distribution 0.537ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1078, routed)        0.981     1.527    shift_reg_tap_i/clk_c
    SLICE_X90Y495        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[100]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X90Y495        FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.038     1.565 r  shift_reg_tap_i/sr_p.sr_1[100]/Q
                         net (fo=34, routed)          0.040     1.605    dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/input_slr_0
    SLICE_X90Y496        LUT6 (Prop_A6LUT_SLICEM_I0_O)
                                                      0.014     1.619 f  dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_7_RNO/O
                         net (fo=1, routed)           0.018     1.637    dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/idx_reti[8]
    SLICE_X90Y496        FDRE                                         f  dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_7/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1078, routed)        1.115     1.887    dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/clk_c
    SLICE_X90Y496        FDRE                                         r  dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_7/C
                         clock pessimism             -0.334     1.553    
    SLICE_X90Y496        FDRE (Hold_AFF_SLICEM_C_D)
                                                      0.046     1.599    dut_inst/stage_g.1.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_7
  -------------------------------------------------------------------
                         required time                         -1.599    
                         arrival time                           1.637    
  -------------------------------------------------------------------
                         slack                                  0.037    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.786 }
Period(ns):         3.572
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         3.572       2.073      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X93Y515   dut_inst/ret_array_1_17.idx_ret_2251[4]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X90Y514   dut_inst/ret_array_1_17.idx_ret_2251[6]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X94Y514   dut_inst/ret_array_1_17.idx_ret_2251[7]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X95Y515   dut_inst/ret_array_1_17.idx_ret_2251[8]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X89Y501   dut_inst/ret_array_1_17.idx_ret_2327[2]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X89Y501   dut_inst/stage_g.0.pair_g.9.csn_cmp_inst/ret_array_1_17.idx_ret_2117_ret_6/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X97Y493   dut_inst/stage_g.2.pair_g.5.csn_cmp_inst/ret_array_1_17.idx_ret_1300_ret_2/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X93Y493   dut_inst/stage_g.2.pair_g.6.csn_cmp_inst/ret_array_1_11.idx_ret_43_ret_0/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X104Y508  shift_reg_tap_o/sr_p.sr_1[120]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X93Y515   dut_inst/ret_array_1_17.idx_ret_2251[4]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X90Y514   dut_inst/ret_array_1_17.idx_ret_2251[6]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X94Y514   dut_inst/ret_array_1_17.idx_ret_2251[7]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X95Y515   dut_inst/ret_array_1_17.idx_ret_2251[8]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X91Y513   dut_inst/ret_array_1_17.idx_ret_2251[9]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X88Y498          lsfr_1/shiftreg_vector[151]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X88Y500          lsfr_1/shiftreg_vector[152]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X88Y500          lsfr_1/shiftreg_vector[153]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X88Y497          lsfr_1/shiftreg_vector[162]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X88Y497          lsfr_1/shiftreg_vector[163]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X88Y497          lsfr_1/shiftreg_vector[164]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X88Y498          lsfr_1/shiftreg_vector[151]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X88Y500          lsfr_1/shiftreg_vector[152]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X88Y500          lsfr_1/shiftreg_vector[153]/C



