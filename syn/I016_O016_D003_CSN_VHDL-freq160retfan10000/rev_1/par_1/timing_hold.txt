Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Tue Mar  5 23:30:27 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.038        0.000                      0                  803        2.552        0.000                       0                  1499  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 3.125}        6.250           160.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.038        0.000                      0                  803        2.552        0.000                       0                  1011  
clk_wrapper                                                                             498.562        0.000                       0                   488  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.038ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        2.552ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.038ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[176]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret_0_14_ret_array_1_12.idx_ret_1/D
                            (rising edge-triggered cell SRL16E clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.190ns  (logic 0.070ns (36.842%)  route 0.120ns (63.158%))
  Logic Levels:           0  
  Clock Path Skew:        0.137ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.700ns
    Source Clock Delay      (SCD):    2.159ns
    Clock Pessimism Removal (CPR):    0.404ns
  Clock Net Delay (Source):      1.329ns (routing 0.562ns, distribution 0.767ns)
  Clock Net Delay (Destination): 1.559ns (routing 0.616ns, distribution 0.943ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1010, routed)        1.329     2.159    shift_reg_tap_i/clk_c
    SLICE_X98Y576        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[176]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y576        FDRE (Prop_EFF_SLICEL_C_Q)
                                                      0.070     2.229 r  shift_reg_tap_i/sr_p.sr_1[176]/Q
                         net (fo=1, routed)           0.120     2.349    dut_inst/input_slr[176]
    SLICE_X99Y576        SRL16E                                       r  dut_inst/ret_array_1_12.idx_ret_0_14_ret_array_1_12.idx_ret_1/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1010, routed)        1.559     2.700    dut_inst/clk_c
    SLICE_X99Y576        SRL16E                                       r  dut_inst/ret_array_1_12.idx_ret_0_14_ret_array_1_12.idx_ret_1/CLK
                         clock pessimism             -0.404     2.296    
    SLICE_X99Y576        SRL16E (Hold_A6LUT_SLICEM_CLK_D)
                                                      0.015     2.311    dut_inst/ret_array_1_12.idx_ret_0_14_ret_array_1_12.idx_ret_1
  -------------------------------------------------------------------
                         required time                         -2.311    
                         arrival time                           2.349    
  -------------------------------------------------------------------
                         slack                                  0.038    

Slack (MET) :             0.038ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[176]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret_0_14_ret_array_1_12.idx_ret_1/D
                            (rising edge-triggered cell SRL16E clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.190ns  (logic 0.070ns (36.842%)  route 0.120ns (63.158%))
  Logic Levels:           0  
  Clock Path Skew:        0.137ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.700ns
    Source Clock Delay      (SCD):    2.159ns
    Clock Pessimism Removal (CPR):    0.404ns
  Clock Net Delay (Source):      1.329ns (routing 0.562ns, distribution 0.767ns)
  Clock Net Delay (Destination): 1.559ns (routing 0.616ns, distribution 0.943ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1010, routed)        1.329     2.159    shift_reg_tap_i/clk_c
    SLICE_X98Y576        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[176]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y576        FDRE (Prop_EFF_SLICEL_C_Q)
                                                      0.070     2.229 f  shift_reg_tap_i/sr_p.sr_1[176]/Q
                         net (fo=1, routed)           0.120     2.349    dut_inst/input_slr[176]
    SLICE_X99Y576        SRL16E                                       f  dut_inst/ret_array_1_12.idx_ret_0_14_ret_array_1_12.idx_ret_1/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1010, routed)        1.559     2.700    dut_inst/clk_c
    SLICE_X99Y576        SRL16E                                       r  dut_inst/ret_array_1_12.idx_ret_0_14_ret_array_1_12.idx_ret_1/CLK
                         clock pessimism             -0.404     2.296    
    SLICE_X99Y576        SRL16E (Hold_A6LUT_SLICEM_CLK_D)
                                                      0.015     2.311    dut_inst/ret_array_1_12.idx_ret_0_14_ret_array_1_12.idx_ret_1
  -------------------------------------------------------------------
                         required time                         -2.311    
                         arrival time                           2.349    
  -------------------------------------------------------------------
                         slack                                  0.038    

Slack (MET) :             0.040ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[193]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret_7_ret_array_1_12.idx_ret_1/D
                            (rising edge-triggered cell SRL16E clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.099ns  (logic 0.041ns (41.414%)  route 0.058ns (58.586%))
  Logic Levels:           0  
  Clock Path Skew:        0.041ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.682ns
    Source Clock Delay      (SCD):    1.330ns
    Clock Pessimism Removal (CPR):    0.311ns
  Clock Net Delay (Source):      0.784ns (routing 0.316ns, distribution 0.468ns)
  Clock Net Delay (Destination): 0.910ns (routing 0.348ns, distribution 0.562ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1010, routed)        0.784     1.330    shift_reg_tap_i/clk_c
    SLICE_X102Y583       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[193]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X102Y583       FDRE (Prop_DFF2_SLICEM_C_Q)
                                                      0.041     1.371 r  shift_reg_tap_i/sr_p.sr_1[193]/Q
                         net (fo=1, routed)           0.058     1.429    dut_inst/input_slr[193]
    SLICE_X102Y584       SRL16E                                       r  dut_inst/ret_array_1_12.idx_ret_7_ret_array_1_12.idx_ret_1/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1010, routed)        0.910     1.682    dut_inst/clk_c
    SLICE_X102Y584       SRL16E                                       r  dut_inst/ret_array_1_12.idx_ret_7_ret_array_1_12.idx_ret_1/CLK
                         clock pessimism             -0.311     1.371    
    SLICE_X102Y584       SRL16E (Hold_H6LUT_SLICEM_CLK_D)
                                                      0.018     1.389    dut_inst/ret_array_1_12.idx_ret_7_ret_array_1_12.idx_ret_1
  -------------------------------------------------------------------
                         required time                         -1.389    
                         arrival time                           1.429    
  -------------------------------------------------------------------
                         slack                                  0.040    

Slack (MET) :             0.040ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[193]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret_7_ret_array_1_12.idx_ret_1/D
                            (rising edge-triggered cell SRL16E clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.099ns  (logic 0.041ns (41.414%)  route 0.058ns (58.586%))
  Logic Levels:           0  
  Clock Path Skew:        0.041ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.682ns
    Source Clock Delay      (SCD):    1.330ns
    Clock Pessimism Removal (CPR):    0.311ns
  Clock Net Delay (Source):      0.784ns (routing 0.316ns, distribution 0.468ns)
  Clock Net Delay (Destination): 0.910ns (routing 0.348ns, distribution 0.562ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1010, routed)        0.784     1.330    shift_reg_tap_i/clk_c
    SLICE_X102Y583       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[193]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X102Y583       FDRE (Prop_DFF2_SLICEM_C_Q)
                                                      0.041     1.371 f  shift_reg_tap_i/sr_p.sr_1[193]/Q
                         net (fo=1, routed)           0.058     1.429    dut_inst/input_slr[193]
    SLICE_X102Y584       SRL16E                                       f  dut_inst/ret_array_1_12.idx_ret_7_ret_array_1_12.idx_ret_1/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1010, routed)        0.910     1.682    dut_inst/clk_c
    SLICE_X102Y584       SRL16E                                       r  dut_inst/ret_array_1_12.idx_ret_7_ret_array_1_12.idx_ret_1/CLK
                         clock pessimism             -0.311     1.371    
    SLICE_X102Y584       SRL16E (Hold_H6LUT_SLICEM_CLK_D)
                                                      0.018     1.389    dut_inst/ret_array_1_12.idx_ret_7_ret_array_1_12.idx_ret_1
  -------------------------------------------------------------------
                         required time                         -1.389    
                         arrival time                           1.429    
  -------------------------------------------------------------------
                         slack                                  0.040    

Slack (MET) :             0.041ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[194]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret_8_ret_array_1_12.idx_ret_1/D
                            (rising edge-triggered cell SRL16E clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.092ns  (logic 0.039ns (42.391%)  route 0.053ns (57.609%))
  Logic Levels:           0  
  Clock Path Skew:        0.033ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.678ns
    Source Clock Delay      (SCD):    1.334ns
    Clock Pessimism Removal (CPR):    0.311ns
  Clock Net Delay (Source):      0.788ns (routing 0.316ns, distribution 0.472ns)
  Clock Net Delay (Destination): 0.906ns (routing 0.348ns, distribution 0.558ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1010, routed)        0.788     1.334    shift_reg_tap_i/clk_c
    SLICE_X101Y581       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[194]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y581       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.373 r  shift_reg_tap_i/sr_p.sr_1[194]/Q
                         net (fo=1, routed)           0.053     1.426    dut_inst/input_slr[194]
    SLICE_X100Y581       SRL16E                                       r  dut_inst/ret_array_1_12.idx_ret_8_ret_array_1_12.idx_ret_1/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1010, routed)        0.906     1.678    dut_inst/clk_c
    SLICE_X100Y581       SRL16E                                       r  dut_inst/ret_array_1_12.idx_ret_8_ret_array_1_12.idx_ret_1/CLK
                         clock pessimism             -0.311     1.367    
    SLICE_X100Y581       SRL16E (Hold_H6LUT_SLICEM_CLK_D)
                                                      0.018     1.385    dut_inst/ret_array_1_12.idx_ret_8_ret_array_1_12.idx_ret_1
  -------------------------------------------------------------------
                         required time                         -1.385    
                         arrival time                           1.426    
  -------------------------------------------------------------------
                         slack                                  0.041    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 3.125 }
Period(ns):         6.250
Sources:            { clk }

Check Type        Corner  Lib Pin     Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I    n/a            1.499         6.250       4.751      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     SRL16E/CLK  n/a            1.146         6.250       5.104      SLICE_X102Y581  dut_inst/ret_array_1_12.idx_ret_0_0_ret_array_1_12.idx_ret_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         6.250       5.104      SLICE_X102Y581  dut_inst/ret_array_1_12.idx_ret_0_10_ret_array_1_12.idx_ret_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         6.250       5.104      SLICE_X100Y578  dut_inst/ret_array_1_12.idx_ret_0_11_ret_array_1_12.idx_ret_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         6.250       5.104      SLICE_X100Y575  dut_inst/ret_array_1_12.idx_ret_0_12_ret_array_1_12.idx_ret_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X93Y583   dut_inst/ret_array_1_12.idx_ret_13_16_ret_array_1_12.idx_ret_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X93Y580   dut_inst/ret_array_1_12.idx_ret_13_5_ret_array_1_12.idx_ret_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X102Y586  dut_inst/ret_array_1_12.idx_ret_2_2_ret_array_1_12.idx_ret_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X102Y584  dut_inst/ret_array_1_12.idx_ret_2_4_ret_array_1_12.idx_ret_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X102Y584  dut_inst/ret_array_1_12.idx_ret_2_4_ret_array_1_12.idx_ret_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X107Y586  dut_inst/ret_array_1_12.idx_ret_2_3_ret_array_1_12.idx_ret_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X95Y579   dut_inst/ret_array_1_12.idx_ret_14_12_ret_array_1_12.idx_ret_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X95Y579   dut_inst/ret_array_1_12.idx_ret_14_15_ret_array_1_12.idx_ret_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X95Y579   dut_inst/ret_array_1_12.idx_ret_11_10_ret_array_1_12.idx_ret_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X95Y579   dut_inst/ret_array_1_12.idx_ret_11_11_ret_array_1_12.idx_ret_1/CLK



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X103Y585         lsfr_1/shiftreg_vector[116]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X103Y585         lsfr_1/shiftreg_vector[117]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X103Y585         lsfr_1/shiftreg_vector[118]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y585         lsfr_1/shiftreg_vector[116]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y585         lsfr_1/shiftreg_vector[117]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y585         lsfr_1/shiftreg_vector[118]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X97Y582          lsfr_1/shiftreg_vector[11]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X105Y586         lsfr_1/shiftreg_vector[124]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X105Y586         lsfr_1/shiftreg_vector[125]/C



