Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Wed Mar  6 18:42:21 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.031        0.000                      0                  753        1.511        0.000                       0                  1450  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.786}        3.572           279.955         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.031        0.000                      0                  753        1.511        0.000                       0                   962  
clk_wrapper                                                                             498.562        0.000                       0                   488  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.031ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.511ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.031ns  (arrival time - required time)
  Source:                 dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_10.idx_ret_71/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[154]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.133ns  (logic 0.052ns (39.098%)  route 0.081ns (60.902%))
  Logic Levels:           1  (LUT6=1)
  Clock Path Skew:        0.056ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.025ns
    Source Clock Delay      (SCD):    1.651ns
    Clock Pessimism Removal (CPR):    0.318ns
  Clock Net Delay (Source):      1.105ns (routing 0.638ns, distribution 0.467ns)
  Clock Net Delay (Destination): 1.253ns (routing 0.707ns, distribution 0.546ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=961, routed)         1.105     1.651    dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/clk_c
    SLICE_X99Y479        FDRE                                         r  dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_10.idx_ret_71/C
  -------------------------------------------------------------------    -------------------
    SLICE_X99Y479        FDRE (Prop_CFF_SLICEM_C_Q)
                                                      0.038     1.689 f  dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_10.idx_ret_71/Q
                         net (fo=3, routed)           0.060     1.749    dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/idx_ret_71
    SLICE_X100Y479       LUT6 (Prop_B6LUT_SLICEM_I0_O)
                                                      0.014     1.763 r  dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_12.idx_ret_42_RNI1NUS4_0/O
                         net (fo=1, routed)           0.021     1.784    shift_reg_tap_o/idx_ret_42_RNI1NUS4_0
    SLICE_X100Y479       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[154]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=961, routed)         1.253     2.025    shift_reg_tap_o/clk_c
    SLICE_X100Y479       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[154]/C
                         clock pessimism             -0.318     1.707    
    SLICE_X100Y479       FDRE (Hold_BFF_SLICEM_C_D)
                                                      0.046     1.753    shift_reg_tap_o/sr_p.sr_1[154]
  -------------------------------------------------------------------
                         required time                         -1.753    
                         arrival time                           1.784    
  -------------------------------------------------------------------
                         slack                                  0.031    

Slack (MET) :             0.031ns  (arrival time - required time)
  Source:                 dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_10.idx_ret_71/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[154]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.133ns  (logic 0.052ns (39.098%)  route 0.081ns (60.902%))
  Logic Levels:           1  (LUT6=1)
  Clock Path Skew:        0.056ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.025ns
    Source Clock Delay      (SCD):    1.651ns
    Clock Pessimism Removal (CPR):    0.318ns
  Clock Net Delay (Source):      1.105ns (routing 0.638ns, distribution 0.467ns)
  Clock Net Delay (Destination): 1.253ns (routing 0.707ns, distribution 0.546ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=961, routed)         1.105     1.651    dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/clk_c
    SLICE_X99Y479        FDRE                                         r  dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_10.idx_ret_71/C
  -------------------------------------------------------------------    -------------------
    SLICE_X99Y479        FDRE (Prop_CFF_SLICEM_C_Q)
                                                      0.038     1.689 r  dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_10.idx_ret_71/Q
                         net (fo=3, routed)           0.060     1.749    dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/idx_ret_71
    SLICE_X100Y479       LUT6 (Prop_B6LUT_SLICEM_I0_O)
                                                      0.014     1.763 f  dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_12.idx_ret_42_RNI1NUS4_0/O
                         net (fo=1, routed)           0.021     1.784    shift_reg_tap_o/idx_ret_42_RNI1NUS4_0
    SLICE_X100Y479       FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[154]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=961, routed)         1.253     2.025    shift_reg_tap_o/clk_c
    SLICE_X100Y479       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[154]/C
                         clock pessimism             -0.318     1.707    
    SLICE_X100Y479       FDRE (Hold_BFF_SLICEM_C_D)
                                                      0.046     1.753    shift_reg_tap_o/sr_p.sr_1[154]
  -------------------------------------------------------------------
                         required time                         -1.753    
                         arrival time                           1.784    
  -------------------------------------------------------------------
                         slack                                  0.031    

Slack (MET) :             0.038ns  (arrival time - required time)
  Source:                 dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_10.idx_ret_71/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[154]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.208ns  (logic 0.096ns (46.154%)  route 0.112ns (53.846%))
  Logic Levels:           1  (LUT6=1)
  Clock Path Skew:        0.117ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.321ns
    Source Clock Delay      (SCD):    2.747ns
    Clock Pessimism Removal (CPR):    0.457ns
  Clock Net Delay (Source):      1.917ns (routing 1.130ns, distribution 0.787ns)
  Clock Net Delay (Destination): 2.180ns (routing 1.237ns, distribution 0.943ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=961, routed)         1.917     2.747    dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/clk_c
    SLICE_X99Y479        FDRE                                         r  dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_10.idx_ret_71/C
  -------------------------------------------------------------------    -------------------
    SLICE_X99Y479        FDRE (Prop_CFF_SLICEM_C_Q)
                                                      0.069     2.816 f  dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_10.idx_ret_71/Q
                         net (fo=3, routed)           0.078     2.894    dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/idx_ret_71
    SLICE_X100Y479       LUT6 (Prop_B6LUT_SLICEM_I0_O)
                                                      0.027     2.921 r  dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_12.idx_ret_42_RNI1NUS4_0/O
                         net (fo=1, routed)           0.034     2.955    shift_reg_tap_o/idx_ret_42_RNI1NUS4_0
    SLICE_X100Y479       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[154]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=961, routed)         2.180     3.321    shift_reg_tap_o/clk_c
    SLICE_X100Y479       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[154]/C
                         clock pessimism             -0.457     2.864    
    SLICE_X100Y479       FDRE (Hold_BFF_SLICEM_C_D)
                                                      0.053     2.917    shift_reg_tap_o/sr_p.sr_1[154]
  -------------------------------------------------------------------
                         required time                         -2.917    
                         arrival time                           2.955    
  -------------------------------------------------------------------
                         slack                                  0.038    

Slack (MET) :             0.038ns  (arrival time - required time)
  Source:                 dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_10.idx_ret_71/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[154]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.208ns  (logic 0.096ns (46.154%)  route 0.112ns (53.846%))
  Logic Levels:           1  (LUT6=1)
  Clock Path Skew:        0.117ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.321ns
    Source Clock Delay      (SCD):    2.747ns
    Clock Pessimism Removal (CPR):    0.457ns
  Clock Net Delay (Source):      1.917ns (routing 1.130ns, distribution 0.787ns)
  Clock Net Delay (Destination): 2.180ns (routing 1.237ns, distribution 0.943ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=961, routed)         1.917     2.747    dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/clk_c
    SLICE_X99Y479        FDRE                                         r  dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_10.idx_ret_71/C
  -------------------------------------------------------------------    -------------------
    SLICE_X99Y479        FDRE (Prop_CFF_SLICEM_C_Q)
                                                      0.069     2.816 r  dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_10.idx_ret_71/Q
                         net (fo=3, routed)           0.078     2.894    dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/idx_ret_71
    SLICE_X100Y479       LUT6 (Prop_B6LUT_SLICEM_I0_O)
                                                      0.027     2.921 f  dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_12.idx_ret_42_RNI1NUS4_0/O
                         net (fo=1, routed)           0.034     2.955    shift_reg_tap_o/idx_ret_42_RNI1NUS4_0
    SLICE_X100Y479       FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[154]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=961, routed)         2.180     3.321    shift_reg_tap_o/clk_c
    SLICE_X100Y479       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[154]/C
                         clock pessimism             -0.457     2.864    
    SLICE_X100Y479       FDRE (Hold_BFF_SLICEM_C_D)
                                                      0.053     2.917    shift_reg_tap_o/sr_p.sr_1[154]
  -------------------------------------------------------------------
                         required time                         -2.917    
                         arrival time                           2.955    
  -------------------------------------------------------------------
                         slack                                  0.038    

Slack (MET) :             0.041ns  (arrival time - required time)
  Source:                 dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_11.pt_ret_8/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[145]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.093ns  (logic 0.053ns (56.989%)  route 0.040ns (43.011%))
  Logic Levels:           1  (LUT5=1)
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.057ns
    Source Clock Delay      (SCD):    1.687ns
    Clock Pessimism Removal (CPR):    0.364ns
  Clock Net Delay (Source):      1.141ns (routing 0.638ns, distribution 0.503ns)
  Clock Net Delay (Destination): 1.285ns (routing 0.707ns, distribution 0.578ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=961, routed)         1.141     1.687    dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/clk_c
    SLICE_X103Y480       FDRE                                         r  dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_11.pt_ret_8/C
  -------------------------------------------------------------------    -------------------
    SLICE_X103Y480       FDRE (Prop_CFF_SLICEL_C_Q)
                                                      0.039     1.726 f  dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_11.pt_ret_8/Q
                         net (fo=1, routed)           0.024     1.750    dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/pt_ret_8
    SLICE_X103Y480       LUT5 (Prop_D6LUT_SLICEL_I0_O)
                                                      0.014     1.764 r  dut_inst/stage_g.8.pair_g.4.csn_cmp_inst/ret_array_2_11.pt_ret_8_RNI5TM04/O
                         net (fo=1, routed)           0.016     1.780    shift_reg_tap_o/pt_ret_8_RNI5TM04
    SLICE_X103Y480       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[145]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=961, routed)         1.285     2.057    shift_reg_tap_o/clk_c
    SLICE_X103Y480       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[145]/C
                         clock pessimism             -0.364     1.693    
    SLICE_X103Y480       FDRE (Hold_DFF_SLICEL_C_D)
                                                      0.046     1.739    shift_reg_tap_o/sr_p.sr_1[145]
  -------------------------------------------------------------------
                         required time                         -1.739    
                         arrival time                           1.780    
  -------------------------------------------------------------------
                         slack                                  0.041    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.786 }
Period(ns):         3.572
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         3.572       2.073      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X100Y486  dut_inst/ret_array_1_12.idx_ret_11[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X100Y486  dut_inst/ret_array_1_12.idx_ret_11[1]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X101Y485  dut_inst/ret_array_1_12.idx_ret_11[2]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X102Y487  dut_inst/ret_array_1_12.idx_ret_11[3]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X103Y487  dut_inst/ret_array_1_12.idx_ret_11[8]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X98Y483   dut_inst/ret_array_1_4.idx_ret_46[0]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X98Y483   dut_inst/ret_array_1_4.idx_ret_46[4]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X101Y484  dut_inst/ret_array_1_4.idx_ret_46[6]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X96Y488   shift_reg_tap_i/sr_p.sr_1[197]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X101Y485  dut_inst/ret_array_1_12.idx_ret_11[2]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X103Y488  dut_inst/ret_array_1_12.idx_ret_11[4]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X103Y488  dut_inst/ret_array_1_12.idx_ret_11[5]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X100Y485  dut_inst/ret_array_1_12.idx_ret_11[6]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X100Y485  dut_inst/ret_array_1_12.idx_ret_11[6]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X103Y478         lsfr_1/output_vector_1[207]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X104Y488         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X101Y494         lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y488         lsfr_1/shiftreg_vector[0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X95Y490          lsfr_1/shiftreg_vector[107]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X97Y493          reducer_1/delay_block[0][15]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X100Y494         lsfr_1/shiftreg_vector[104]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y492          lsfr_1/shiftreg_vector[105]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y492          lsfr_1/shiftreg_vector[106]/C



