Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Fri Feb  8 18:40:08 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.012        0.000                      0                14008        5.975        0.000                       0                 15344  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 6.250}        12.500          80.000          
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.012        0.000                      0                14008        5.975        0.000                       0                 14489  
clk_wrapper                                                                             498.562        0.000                       0                   855  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.012ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        5.975ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_5[42]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_6[42]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.207ns  (logic 0.070ns (33.816%)  route 0.137ns (66.184%))
  Logic Levels:           0  
  Clock Path Skew:        0.140ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.559ns
    Source Clock Delay      (SCD):    2.996ns
    Clock Pessimism Removal (CPR):    0.423ns
  Clock Net Delay (Source):      2.166ns (routing 0.926ns, distribution 1.240ns)
  Clock Net Delay (Destination): 2.418ns (routing 1.014ns, distribution 1.404ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=14488, routed)       2.166     2.996    shift_reg_tap_i/clk_c
    SLICE_X86Y417        FDRE                                         r  shift_reg_tap_i/sr_p.sr_5[42]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X86Y417        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.070     3.066 r  shift_reg_tap_i/sr_p.sr_5[42]/Q
                         net (fo=1, routed)           0.137     3.203    shift_reg_tap_i/sr_5[42]
    SLICE_X86Y421        FDRE                                         r  shift_reg_tap_i/sr_p.sr_6[42]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=14488, routed)       2.418     3.559    shift_reg_tap_i/clk_c
    SLICE_X86Y421        FDRE                                         r  shift_reg_tap_i/sr_p.sr_6[42]/C
                         clock pessimism             -0.423     3.136    
    SLICE_X86Y421        FDRE (Hold_BFF2_SLICEL_C_D)
                                                      0.055     3.191    shift_reg_tap_i/sr_p.sr_6[42]
  -------------------------------------------------------------------
                         required time                         -3.191    
                         arrival time                           3.203    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_5[42]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_6[42]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.207ns  (logic 0.070ns (33.816%)  route 0.137ns (66.184%))
  Logic Levels:           0  
  Clock Path Skew:        0.140ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.559ns
    Source Clock Delay      (SCD):    2.996ns
    Clock Pessimism Removal (CPR):    0.423ns
  Clock Net Delay (Source):      2.166ns (routing 0.926ns, distribution 1.240ns)
  Clock Net Delay (Destination): 2.418ns (routing 1.014ns, distribution 1.404ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=14488, routed)       2.166     2.996    shift_reg_tap_i/clk_c
    SLICE_X86Y417        FDRE                                         r  shift_reg_tap_i/sr_p.sr_5[42]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X86Y417        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.070     3.066 f  shift_reg_tap_i/sr_p.sr_5[42]/Q
                         net (fo=1, routed)           0.137     3.203    shift_reg_tap_i/sr_5[42]
    SLICE_X86Y421        FDRE                                         f  shift_reg_tap_i/sr_p.sr_6[42]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=14488, routed)       2.418     3.559    shift_reg_tap_i/clk_c
    SLICE_X86Y421        FDRE                                         r  shift_reg_tap_i/sr_p.sr_6[42]/C
                         clock pessimism             -0.423     3.136    
    SLICE_X86Y421        FDRE (Hold_BFF2_SLICEL_C_D)
                                                      0.055     3.191    shift_reg_tap_i/sr_p.sr_6[42]
  -------------------------------------------------------------------
                         required time                         -3.191    
                         arrival time                           3.203    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.013ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_14[215]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_15[215]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.214ns  (logic 0.070ns (32.710%)  route 0.144ns (67.290%))
  Logic Levels:           0  
  Clock Path Skew:        0.146ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.446ns
    Source Clock Delay      (SCD):    2.896ns
    Clock Pessimism Removal (CPR):    0.404ns
  Clock Net Delay (Source):      2.066ns (routing 0.926ns, distribution 1.140ns)
  Clock Net Delay (Destination): 2.305ns (routing 1.014ns, distribution 1.291ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=14488, routed)       2.066     2.896    shift_reg_tap_i/clk_c
    SLICE_X93Y479        FDRE                                         r  shift_reg_tap_i/sr_p.sr_14[215]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X93Y479        FDRE (Prop_GFF_SLICEM_C_Q)
                                                      0.070     2.966 r  shift_reg_tap_i/sr_p.sr_14[215]/Q
                         net (fo=1, routed)           0.144     3.110    shift_reg_tap_i/sr_14[215]
    SLICE_X95Y480        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[215]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=14488, routed)       2.305     3.446    shift_reg_tap_i/clk_c
    SLICE_X95Y480        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[215]/C
                         clock pessimism             -0.404     3.042    
    SLICE_X95Y480        FDRE (Hold_GFF2_SLICEM_C_D)
                                                      0.055     3.097    shift_reg_tap_i/sr_p.sr_15[215]
  -------------------------------------------------------------------
                         required time                         -3.097    
                         arrival time                           3.110    
  -------------------------------------------------------------------
                         slack                                  0.013    

Slack (MET) :             0.013ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_14[215]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_15[215]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.214ns  (logic 0.070ns (32.710%)  route 0.144ns (67.290%))
  Logic Levels:           0  
  Clock Path Skew:        0.146ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.446ns
    Source Clock Delay      (SCD):    2.896ns
    Clock Pessimism Removal (CPR):    0.404ns
  Clock Net Delay (Source):      2.066ns (routing 0.926ns, distribution 1.140ns)
  Clock Net Delay (Destination): 2.305ns (routing 1.014ns, distribution 1.291ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=14488, routed)       2.066     2.896    shift_reg_tap_i/clk_c
    SLICE_X93Y479        FDRE                                         r  shift_reg_tap_i/sr_p.sr_14[215]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X93Y479        FDRE (Prop_GFF_SLICEM_C_Q)
                                                      0.070     2.966 f  shift_reg_tap_i/sr_p.sr_14[215]/Q
                         net (fo=1, routed)           0.144     3.110    shift_reg_tap_i/sr_14[215]
    SLICE_X95Y480        FDRE                                         f  shift_reg_tap_i/sr_p.sr_15[215]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=14488, routed)       2.305     3.446    shift_reg_tap_i/clk_c
    SLICE_X95Y480        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[215]/C
                         clock pessimism             -0.404     3.042    
    SLICE_X95Y480        FDRE (Hold_GFF2_SLICEM_C_D)
                                                      0.055     3.097    shift_reg_tap_i/sr_p.sr_15[215]
  -------------------------------------------------------------------
                         required time                         -3.097    
                         arrival time                           3.110    
  -------------------------------------------------------------------
                         slack                                  0.013    

Slack (MET) :             0.015ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_14[292]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_15[292]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.240ns  (logic 0.070ns (29.167%)  route 0.170ns (70.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.170ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.465ns
    Source Clock Delay      (SCD):    2.891ns
    Clock Pessimism Removal (CPR):    0.404ns
  Clock Net Delay (Source):      2.061ns (routing 0.926ns, distribution 1.135ns)
  Clock Net Delay (Destination): 2.324ns (routing 1.014ns, distribution 1.310ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=14488, routed)       2.061     2.891    shift_reg_tap_i/clk_c
    SLICE_X91Y473        FDRE                                         r  shift_reg_tap_i/sr_p.sr_14[292]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X91Y473        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.070     2.961 r  shift_reg_tap_i/sr_p.sr_14[292]/Q
                         net (fo=1, routed)           0.170     3.131    shift_reg_tap_i/sr_14[292]
    SLICE_X91Y481        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[292]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=14488, routed)       2.324     3.465    shift_reg_tap_i/clk_c
    SLICE_X91Y481        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[292]/C
                         clock pessimism             -0.404     3.061    
    SLICE_X91Y481        FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.055     3.116    shift_reg_tap_i/sr_p.sr_15[292]
  -------------------------------------------------------------------
                         required time                         -3.116    
                         arrival time                           3.131    
  -------------------------------------------------------------------
                         slack                                  0.015    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 6.250 }
Period(ns):         12.500
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         12.500      11.001     BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X81Y539   muon_sorter_1/sr_p.sr_1_12.roi_ret_2770/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X75Y546   muon_sorter_1/sr_p.sr_1_12.roi_ret_2773/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X70Y538   muon_sorter_1/sr_p.sr_1_12.roi_ret_2776/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X86Y551   muon_sorter_1/sr_p.sr_1_15.roi_ret_23530/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X70Y546   muon_sorter_1/sr_p.sr_1_15.roi_ret_23531/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X96Y470   shift_reg_tap_i/sr_p.sr_13[69]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X100Y544  shift_reg_tap_i/sr_p.sr_2[395]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X100Y545  shift_reg_tap_i/sr_p.sr_2[397]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X69Y457   shift_reg_tap_o/sr_p.sr_10[146]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X75Y546   muon_sorter_1/sr_p.sr_1_12.roi_ret_2773/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X69Y540   muon_sorter_1/sr_p.sr_1_15.roi_ret_25780/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X76Y479   muon_sorter_1/sr_p.sr_1_4.roi[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X69Y493   muon_sorter_1/sr_p.sr_1_8.roi_ret_73/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X77Y450   shift_reg_tap_i/sr_p.sr_10[193]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X59Y567          lsfr_1/output_vector_1[511]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X100Y565         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X88Y393          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X94Y403          lsfr_1/shiftreg_vector[124]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X94Y403          lsfr_1/shiftreg_vector[125]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X101Y408         lsfr_1/shiftreg_vector[129]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X87Y390          lsfr_1/shiftreg_vector[102]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X87Y390          lsfr_1/shiftreg_vector[103]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X91Y391          lsfr_1/shiftreg_vector[109]/C



