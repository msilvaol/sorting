Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Fri Feb  8 18:43:28 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+-------+--------+
|  Fanout |  Nets |      % |
+---------+-------+--------+
|       1 | 21683 |  61.05 |
|       2 |  4218 |  11.87 |
|       3 |  2701 |   7.60 |
|       4 |  1683 |   4.73 |
|    5-10 |  3142 |   8.84 |
|   11-50 |  1966 |   5.53 |
|  51-100 |   123 |   0.34 |
| 101-500 |     0 |   0.00 |
|    >500 |     0 |   0.00 |
+---------+-------+--------+
|     ALL | 35516 | 100.00 |
+---------+-------+--------+


