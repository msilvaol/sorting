Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Thu Feb 14 16:54:42 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.019        0.000                      0                 2048       12.225        0.000                       0                  2904  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 12.500}       25.000          40.000          
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.019        0.000                      0                 2048       12.225        0.000                       0                  2305  
clk_wrapper                                                                             498.562        0.000                       0                   599  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.019ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack       12.225ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_2[231]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_i/sr_p.sr_3[231]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.145ns
    Source Clock Delay      (SCD):    1.765ns
    Clock Pessimism Removal (CPR):    0.374ns
  Clock Net Delay (Source):      1.219ns (routing 0.754ns, distribution 0.465ns)
  Clock Net Delay (Destination): 1.373ns (routing 0.836ns, distribution 0.537ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=2304, routed)        1.219     1.765    shift_reg_tap_i/clk_c
    SLICE_X106Y387       FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[231]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X106Y387       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.804 r  shift_reg_tap_i/sr_p.sr_2[231]/Q
                         net (fo=1, routed)           0.033     1.837    shift_reg_tap_i/sr_2[231]
    SLICE_X106Y387       FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[231]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=2304, routed)        1.373     2.145    shift_reg_tap_i/clk_c
    SLICE_X106Y387       FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[231]/C
                         clock pessimism             -0.374     1.771    
    SLICE_X106Y387       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.818    shift_reg_tap_i/sr_p.sr_3[231]
  -------------------------------------------------------------------
                         required time                         -1.818    
                         arrival time                           1.837    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_2[231]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_i/sr_p.sr_3[231]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.145ns
    Source Clock Delay      (SCD):    1.765ns
    Clock Pessimism Removal (CPR):    0.374ns
  Clock Net Delay (Source):      1.219ns (routing 0.754ns, distribution 0.465ns)
  Clock Net Delay (Destination): 1.373ns (routing 0.836ns, distribution 0.537ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=2304, routed)        1.219     1.765    shift_reg_tap_i/clk_c
    SLICE_X106Y387       FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[231]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X106Y387       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.804 f  shift_reg_tap_i/sr_p.sr_2[231]/Q
                         net (fo=1, routed)           0.033     1.837    shift_reg_tap_i/sr_2[231]
    SLICE_X106Y387       FDRE                                         f  shift_reg_tap_i/sr_p.sr_3[231]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=2304, routed)        1.373     2.145    shift_reg_tap_i/clk_c
    SLICE_X106Y387       FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[231]/C
                         clock pessimism             -0.374     1.771    
    SLICE_X106Y387       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.818    shift_reg_tap_i/sr_p.sr_3[231]
  -------------------------------------------------------------------
                         required time                         -1.818    
                         arrival time                           1.837    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_2[137]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_o/sr_p.sr_3[137]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.153ns
    Source Clock Delay      (SCD):    1.773ns
    Clock Pessimism Removal (CPR):    0.374ns
  Clock Net Delay (Source):      1.227ns (routing 0.754ns, distribution 0.473ns)
  Clock Net Delay (Destination): 1.381ns (routing 0.836ns, distribution 0.545ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=2304, routed)        1.227     1.773    shift_reg_tap_o/clk_c
    SLICE_X98Y367        FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[137]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y367        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.812 r  shift_reg_tap_o/sr_p.sr_2[137]/Q
                         net (fo=1, routed)           0.033     1.845    shift_reg_tap_o/sr_2[137]
    SLICE_X98Y367        FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[137]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=2304, routed)        1.381     2.153    shift_reg_tap_o/clk_c
    SLICE_X98Y367        FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[137]/C
                         clock pessimism             -0.374     1.779    
    SLICE_X98Y367        FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.826    shift_reg_tap_o/sr_p.sr_3[137]
  -------------------------------------------------------------------
                         required time                         -1.826    
                         arrival time                           1.845    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_2[137]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_o/sr_p.sr_3[137]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.153ns
    Source Clock Delay      (SCD):    1.773ns
    Clock Pessimism Removal (CPR):    0.374ns
  Clock Net Delay (Source):      1.227ns (routing 0.754ns, distribution 0.473ns)
  Clock Net Delay (Destination): 1.381ns (routing 0.836ns, distribution 0.545ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=2304, routed)        1.227     1.773    shift_reg_tap_o/clk_c
    SLICE_X98Y367        FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[137]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y367        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.812 f  shift_reg_tap_o/sr_p.sr_2[137]/Q
                         net (fo=1, routed)           0.033     1.845    shift_reg_tap_o/sr_2[137]
    SLICE_X98Y367        FDRE                                         f  shift_reg_tap_o/sr_p.sr_3[137]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=2304, routed)        1.381     2.153    shift_reg_tap_o/clk_c
    SLICE_X98Y367        FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[137]/C
                         clock pessimism             -0.374     1.779    
    SLICE_X98Y367        FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.826    shift_reg_tap_o/sr_p.sr_3[137]
  -------------------------------------------------------------------
                         required time                         -1.826    
                         arrival time                           1.845    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_2[181]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_o/sr_p.sr_3[181]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.148ns
    Source Clock Delay      (SCD):    1.768ns
    Clock Pessimism Removal (CPR):    0.374ns
  Clock Net Delay (Source):      1.222ns (routing 0.754ns, distribution 0.468ns)
  Clock Net Delay (Destination): 1.376ns (routing 0.836ns, distribution 0.540ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=2304, routed)        1.222     1.768    shift_reg_tap_o/clk_c
    SLICE_X101Y381       FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[181]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y381       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.807 r  shift_reg_tap_o/sr_p.sr_2[181]/Q
                         net (fo=1, routed)           0.033     1.840    shift_reg_tap_o/sr_2[181]
    SLICE_X101Y381       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[181]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=2304, routed)        1.376     2.148    shift_reg_tap_o/clk_c
    SLICE_X101Y381       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[181]/C
                         clock pessimism             -0.374     1.774    
    SLICE_X101Y381       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.821    shift_reg_tap_o/sr_p.sr_3[181]
  -------------------------------------------------------------------
                         required time                         -1.821    
                         arrival time                           1.840    
  -------------------------------------------------------------------
                         slack                                  0.019    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 12.500 }
Period(ns):         25.000
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         25.000      23.501     BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X101Y366  muon_cand_2.roi[2]/C
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X99Y366   muon_cand_2.roi[3]/C
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X100Y370  muon_cand_2.roi[4]/C
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X101Y369  muon_cand_2.roi[5]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X94Y394   shift_reg_tap_o/sr_p.sr_2[210]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X93Y372   shift_reg_tap_o/sr_p.sr_2[226]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X102Y379  muon_cand_3.sector[3]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X90Y374   shift_reg_tap_i/sr_p.sr_2[214]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X90Y374   shift_reg_tap_i/sr_p.sr_2[215]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X100Y370  muon_cand_2.roi[4]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X101Y369  muon_cand_2.roi[5]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X98Y371   muon_cand_2.roi[6]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X101Y371  muon_cand_2.sector[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X102Y371  muon_cand_2.sector[1]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X103Y384         lsfr_1/output_vector_1[255]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X103Y384         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X105Y384         lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y385         lsfr_1/shiftreg_vector[107]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y385         lsfr_1/shiftreg_vector[108]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y371          reducer_1/delay_block[1][38]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X110Y401         lsfr_1/shiftreg_vector[101]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X110Y401         lsfr_1/shiftreg_vector[102]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X110Y401         lsfr_1/shiftreg_vector[103]/C



