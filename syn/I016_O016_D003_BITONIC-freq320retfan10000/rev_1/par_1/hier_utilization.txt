Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Sat Mar  2 21:28:58 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_utilization -hierarchical -file hier_utilization.txt
| Design       : bitonic_sorter_16_top
| Device       : xcvu9pflgc2104-1
| Design State : Routed
------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+------------------------------------------+-------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
|                 Instance                 |                            Module                           | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+------------------------------------------+-------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
| bitonic_sorter_16_top                    |                                                       (top) |       1410 |       1402 |       0 |    8 | 1646 |      0 |      0 |    0 |            0 |
|   (bitonic_sorter_16_top)                |                                                       (top) |          0 |          0 |       0 |    0 |  416 |      0 |      0 |    0 |            0 |
|   dut_inst                               | muon_sorter_I016_O016_D003_BITONIC-freq320retfan10000_rev_1 |       1340 |       1332 |       0 |    8 |  743 |      0 |      0 |    0 |            0 |
|     (dut_inst)                           | muon_sorter_I016_O016_D003_BITONIC-freq320retfan10000_rev_1 |          8 |          0 |       0 |    8 |  724 |      0 |      0 |    0 |            0 |
|     sorter_inst                          |                                      bitonic_sort_16s_1s_8s |       1332 |       1332 |       0 |    0 |   19 |      0 |      0 |    0 |            0 |
|       genblk1.bitonic_merge_i            |                                     bitonic_merge_16s_1s_8s |        593 |        593 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_merge_high       |                                    bitonic_merge_8s_1s_4s_1 |        214 |        214 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_high     |                                    bitonic_merge_4s_1s_2s_6 |         85 |         85 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                   bitonic_merge_2s_1s_1s_18 |         30 |         30 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_54 |         30 |         30 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                   bitonic_merge_2s_1s_1s_17 |         31 |         31 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_53 |         31 |         31 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_52 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_51 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_low      |                                    bitonic_merge_4s_1s_2s_5 |         73 |         73 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                   bitonic_merge_2s_1s_1s_16 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_50 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                   bitonic_merge_2s_1s_1s_15 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_49 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_48 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_47 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[0].compare_i     |                                               compare_1s_45 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[1].compare_i     |                                               compare_1s_46 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[2].compare_i     |                                               compare_1s_43 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[3].compare_i     |                                               compare_1s_44 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_merge_low        |                                    bitonic_merge_8s_1s_4s_0 |        246 |        246 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_high     |                                    bitonic_merge_4s_1s_2s_4 |        103 |        103 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                   bitonic_merge_2s_1s_1s_14 |         33 |         33 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_42 |         33 |         33 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                   bitonic_merge_2s_1s_1s_13 |         27 |         27 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_41 |         27 |         27 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_40 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_39 |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_low      |                                    bitonic_merge_4s_1s_2s_3 |        102 |        102 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                   bitonic_merge_2s_1s_1s_12 |         33 |         33 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_38 |         33 |         33 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                   bitonic_merge_2s_1s_1s_11 |         29 |         29 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_37 |         29 |         29 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_36 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_35 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[0].compare_i     |                                               compare_1s_33 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[1].compare_i     |                                               compare_1s_34 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[2].compare_i     |                                               compare_1s_31 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[3].compare_i     |                                               compare_1s_32 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[0].compare_i       |                                               compare_1s_29 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[1].compare_i       |                                               compare_1s_30 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[2].compare_i       |                                               compare_1s_27 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[3].compare_i       |                                               compare_1s_28 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[4].compare_i       |                                               compare_1s_25 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[5].compare_i       |                                               compare_1s_26 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[6].compare_i       |                                               compare_1s_23 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[7].compare_i       |                                               compare_1s_24 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|       genblk1.bitonic_sort_high          |                                       bitonic_sort_8s_0s_4s |        370 |        370 |       0 |    0 |    7 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_merge_i          |                                      bitonic_merge_8s_0s_4s |        169 |        169 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_high     |                                    bitonic_merge_4s_0s_2s_2 |         60 |         60 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                   bitonic_merge_2s_0s_1s_10 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_0s_22 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_0s_1s_9 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_0s_21 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_0s_20 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_0s_19 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_low      |                                    bitonic_merge_4s_0s_2s_1 |         60 |         60 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_0s_1s_8 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_0s_18 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_0s_1s_7 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_0s_17 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_0s_16 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_0s_15 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[0].compare_i     |                                               compare_0s_13 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[1].compare_i     |                                               compare_0s_14 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[2].compare_i     |                                               compare_0s_11 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[3].compare_i     |                                               compare_0s_12 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_sort_high        |                                     bitonic_sort_4s_0s_2s_0 |        100 |        100 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_i        |                                    bitonic_merge_4s_0s_2s_0 |         59 |         59 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_0s_1s_6 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_0s_10 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_0s_1s_5 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_9 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                                compare_0s_8 |         16 |         16 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                                compare_0s_7 |         21 |         21 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_high      |                                     bitonic_sort_2s_0s_1s_2 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                    bitonic_merge_2s_0s_1s_4 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_6 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_low       |                                     bitonic_sort_2s_1s_1s_2 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                   bitonic_merge_2s_1s_1s_10 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_22 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_sort_low         |                                     bitonic_sort_4s_1s_2s_0 |        101 |        101 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_i        |                                    bitonic_merge_4s_1s_2s_2 |         61 |         61 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_1s_1s_9 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_21 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_1s_1s_8 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_20 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_19 |         20 |         20 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_18 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_high      |                                     bitonic_sort_2s_0s_1s_1 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                    bitonic_merge_2s_0s_1s_3 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_5 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_low       |                                     bitonic_sort_2s_1s_1s_1 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                    bitonic_merge_2s_1s_1s_7 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_17 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|       genblk1.bitonic_sort_low           |                                       bitonic_sort_8s_1s_4s |        369 |        369 |       0 |    0 |   12 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_merge_i          |                                      bitonic_merge_8s_1s_4s |        172 |        172 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_high     |                                    bitonic_merge_4s_1s_2s_1 |         58 |         58 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_1s_1s_6 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_16 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_1s_1s_5 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_15 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_14 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_13 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_low      |                                    bitonic_merge_4s_1s_2s_0 |         60 |         60 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_1s_1s_4 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_12 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_1s_1s_3 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_11 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_10 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                                compare_1s_9 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[0].compare_i     |                                                compare_1s_7 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[1].compare_i     |                                                compare_1s_8 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[2].compare_i     |                                                compare_1s_5 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[3].compare_i     |                                                compare_1s_6 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_sort_high        |                                       bitonic_sort_4s_0s_2s |        100 |        100 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_i        |                                      bitonic_merge_4s_0s_2s |         60 |         60 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_0s_1s_2 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_4 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_0s_1s_1 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_3 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                                compare_0s_2 |         19 |         19 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                                compare_0s_1 |         19 |         19 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_high      |                                     bitonic_sort_2s_0s_1s_0 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                    bitonic_merge_2s_0s_1s_0 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_0 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_low       |                                     bitonic_sort_2s_1s_1s_0 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                    bitonic_merge_2s_1s_1s_2 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_1s_4 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_sort_low         |                                       bitonic_sort_4s_1s_2s |         97 |         97 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_i        |                                      bitonic_merge_4s_1s_2s |         58 |         58 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_1s_1s_1 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_1s_3 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_1s_1s_0 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_1s_2 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                                compare_1s_1 |         15 |         15 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                                compare_1s_0 |         21 |         21 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_high      |                                       bitonic_sort_2s_0s_1s |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                      bitonic_merge_2s_0s_1s |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                  compare_0s |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_low       |                                       bitonic_sort_2s_1s_1s |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                      bitonic_merge_2s_1s_1s |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                  compare_1s |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   input_lfsr                             |                                                   lfsr_208s |          1 |          1 |       0 |    0 |  209 |      0 |      0 |    0 |            0 |
|   output_reducer                         |                                                  reducer_4s |         69 |         69 |       0 |    0 |  278 |      0 |      0 |    0 |            0 |
+------------------------------------------+-------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
* Note: The sum of lower-level cells may be larger than their parent cells total, due to cross-hierarchy LUT combining


