Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Thu Feb 14 16:10:38 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.014        0.000                      0                 2994        2.850        0.000                       0                  3829  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 3.125}        6.250           160.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.014        0.000                      0                 2994        2.850        0.000                       0                  3230  
clk_wrapper                                                                             498.562        0.000                       0                   599  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.014ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        2.850ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.014ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_2[36]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_3[36]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.181ns  (logic 0.069ns (38.122%)  route 0.112ns (61.878%))
  Logic Levels:           0  
  Clock Path Skew:        0.112ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.100ns
    Source Clock Delay      (SCD):    2.550ns
    Clock Pessimism Removal (CPR):    0.438ns
  Clock Net Delay (Source):      1.720ns (routing 0.926ns, distribution 0.794ns)
  Clock Net Delay (Destination): 1.959ns (routing 1.014ns, distribution 0.945ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=3229, routed)        1.720     2.550    shift_reg_tap_i/clk_c
    SLICE_X101Y522       FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[36]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y522       FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     2.619 r  shift_reg_tap_i/sr_p.sr_2[36]/Q
                         net (fo=1, routed)           0.112     2.731    shift_reg_tap_i/sr_2[36]
    SLICE_X103Y520       FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[36]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=3229, routed)        1.959     3.100    shift_reg_tap_i/clk_c
    SLICE_X103Y520       FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[36]/C
                         clock pessimism             -0.438     2.662    
    SLICE_X103Y520       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.055     2.717    shift_reg_tap_i/sr_p.sr_3[36]
  -------------------------------------------------------------------
                         required time                         -2.717    
                         arrival time                           2.731    
  -------------------------------------------------------------------
                         slack                                  0.014    

Slack (MET) :             0.014ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_2[36]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_3[36]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.181ns  (logic 0.069ns (38.122%)  route 0.112ns (61.878%))
  Logic Levels:           0  
  Clock Path Skew:        0.112ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.100ns
    Source Clock Delay      (SCD):    2.550ns
    Clock Pessimism Removal (CPR):    0.438ns
  Clock Net Delay (Source):      1.720ns (routing 0.926ns, distribution 0.794ns)
  Clock Net Delay (Destination): 1.959ns (routing 1.014ns, distribution 0.945ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=3229, routed)        1.720     2.550    shift_reg_tap_i/clk_c
    SLICE_X101Y522       FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[36]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y522       FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     2.619 f  shift_reg_tap_i/sr_p.sr_2[36]/Q
                         net (fo=1, routed)           0.112     2.731    shift_reg_tap_i/sr_2[36]
    SLICE_X103Y520       FDRE                                         f  shift_reg_tap_i/sr_p.sr_3[36]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=3229, routed)        1.959     3.100    shift_reg_tap_i/clk_c
    SLICE_X103Y520       FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[36]/C
                         clock pessimism             -0.438     2.662    
    SLICE_X103Y520       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.055     2.717    shift_reg_tap_i/sr_p.sr_3[36]
  -------------------------------------------------------------------
                         required time                         -2.717    
                         arrival time                           2.731    
  -------------------------------------------------------------------
                         slack                                  0.014    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_2[128]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_3[128]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.891ns
    Source Clock Delay      (SCD):    1.538ns
    Clock Pessimism Removal (CPR):    0.347ns
  Clock Net Delay (Source):      0.992ns (routing 0.522ns, distribution 0.470ns)
  Clock Net Delay (Destination): 1.119ns (routing 0.578ns, distribution 0.541ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=3229, routed)        0.992     1.538    shift_reg_tap_i/clk_c
    SLICE_X98Y486        FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[128]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y486        FDRE (Prop_BFF_SLICEL_C_Q)
                                                      0.039     1.577 r  shift_reg_tap_i/sr_p.sr_2[128]/Q
                         net (fo=1, routed)           0.033     1.610    shift_reg_tap_i/sr_2[128]
    SLICE_X98Y486        FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[128]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=3229, routed)        1.119     1.891    shift_reg_tap_i/clk_c
    SLICE_X98Y486        FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[128]/C
                         clock pessimism             -0.347     1.544    
    SLICE_X98Y486        FDRE (Hold_CFF2_SLICEL_C_D)
                                                      0.047     1.591    shift_reg_tap_i/sr_p.sr_3[128]
  -------------------------------------------------------------------
                         required time                         -1.591    
                         arrival time                           1.610    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_2[128]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_3[128]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.891ns
    Source Clock Delay      (SCD):    1.538ns
    Clock Pessimism Removal (CPR):    0.347ns
  Clock Net Delay (Source):      0.992ns (routing 0.522ns, distribution 0.470ns)
  Clock Net Delay (Destination): 1.119ns (routing 0.578ns, distribution 0.541ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=3229, routed)        0.992     1.538    shift_reg_tap_i/clk_c
    SLICE_X98Y486        FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[128]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y486        FDRE (Prop_BFF_SLICEL_C_Q)
                                                      0.039     1.577 f  shift_reg_tap_i/sr_p.sr_2[128]/Q
                         net (fo=1, routed)           0.033     1.610    shift_reg_tap_i/sr_2[128]
    SLICE_X98Y486        FDRE                                         f  shift_reg_tap_i/sr_p.sr_3[128]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=3229, routed)        1.119     1.891    shift_reg_tap_i/clk_c
    SLICE_X98Y486        FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[128]/C
                         clock pessimism             -0.347     1.544    
    SLICE_X98Y486        FDRE (Hold_CFF2_SLICEL_C_D)
                                                      0.047     1.591    shift_reg_tap_i/sr_p.sr_3[128]
  -------------------------------------------------------------------
                         required time                         -1.591    
                         arrival time                           1.610    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_2[39]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_3[39]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.872ns
    Source Clock Delay      (SCD):    1.520ns
    Clock Pessimism Removal (CPR):    0.346ns
  Clock Net Delay (Source):      0.974ns (routing 0.522ns, distribution 0.452ns)
  Clock Net Delay (Destination): 1.100ns (routing 0.578ns, distribution 0.522ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=3229, routed)        0.974     1.520    shift_reg_tap_o/clk_c
    SLICE_X94Y525        FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[39]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X94Y525        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.559 r  shift_reg_tap_o/sr_p.sr_2[39]/Q
                         net (fo=1, routed)           0.033     1.592    shift_reg_tap_o/sr_2[39]
    SLICE_X94Y525        FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[39]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=3229, routed)        1.100     1.872    shift_reg_tap_o/clk_c
    SLICE_X94Y525        FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[39]/C
                         clock pessimism             -0.346     1.526    
    SLICE_X94Y525        FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.573    shift_reg_tap_o/sr_p.sr_3[39]
  -------------------------------------------------------------------
                         required time                         -1.573    
                         arrival time                           1.592    
  -------------------------------------------------------------------
                         slack                                  0.019    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 3.125 }
Period(ns):         6.250
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         6.250       4.751      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X106Y516  muon_cand_4.pt_fast[2]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X105Y519  muon_cand_4.pt_fast[3]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X96Y517   muon_cand_4.roi[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X97Y515   muon_cand_4.roi[1]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X97Y515   muon_cand_5.pt[0]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X89Y493   muon_sorter_1/sr_p.sr_2_15.roi_ret_54/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X89Y493   muon_sorter_1/sr_p.sr_2_15.roi_ret_56/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X97Y522   muon_cand_5.sector[2]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X97Y522   muon_sorter_1/sr_p.sr_2_1.sector[2]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X97Y520   muon_cand_4.roi[5]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X110Y517  muon_cand_5.pt[1]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X105Y518  muon_cand_5.pt_0_rep1/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X106Y517  muon_cand_5.pt_1_rep1/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X106Y516  muon_cand_5.pt_2_rep1/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X100Y524         lsfr_1/output_vector_1[255]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X101Y526         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X92Y527          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X100Y524         lsfr_1/output_vector_1[255]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X95Y526          lsfr_1/shiftreg_vector[101]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X99Y492          lsfr_1/shiftreg_vector[105]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X92Y527          lsfr_1/shiftreg_vector[100]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X95Y526          lsfr_1/shiftreg_vector[101]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X98Y524          lsfr_1/shiftreg_vector[102]/C



