Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Thu Feb 14 15:13:17 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.007        0.000                      0                 1792       12.225        0.000                       0                  2648  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 12.500}       25.000          40.000          
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.007        0.000                      0                 1792       12.225        0.000                       0                  2049  
clk_wrapper                                                                             498.562        0.000                       0                   599  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.007ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack       12.225ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.007ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_2[246]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_o/sr_p.sr_3[246]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.171ns  (logic 0.069ns (40.351%)  route 0.102ns (59.649%))
  Logic Levels:           0  
  Clock Path Skew:        0.109ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.018ns
    Source Clock Delay      (SCD):    3.397ns
    Clock Pessimism Removal (CPR):    0.513ns
  Clock Net Delay (Source):      2.567ns (routing 1.566ns, distribution 1.001ns)
  Clock Net Delay (Destination): 2.877ns (routing 1.714ns, distribution 1.163ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=2048, routed)        2.567     3.397    shift_reg_tap_o/clk_c
    SLICE_X106Y432       FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[246]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X106Y432       FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     3.466 r  shift_reg_tap_o/sr_p.sr_2[246]/Q
                         net (fo=1, routed)           0.102     3.568    shift_reg_tap_o/sr_2[246]
    SLICE_X105Y432       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[246]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=2048, routed)        2.877     4.018    shift_reg_tap_o/clk_c
    SLICE_X105Y432       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[246]/C
                         clock pessimism             -0.513     3.506    
    SLICE_X105Y432       FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.055     3.561    shift_reg_tap_o/sr_p.sr_3[246]
  -------------------------------------------------------------------
                         required time                         -3.561    
                         arrival time                           3.568    
  -------------------------------------------------------------------
                         slack                                  0.007    

Slack (MET) :             0.007ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_2[246]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_o/sr_p.sr_3[246]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.171ns  (logic 0.069ns (40.351%)  route 0.102ns (59.649%))
  Logic Levels:           0  
  Clock Path Skew:        0.109ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.018ns
    Source Clock Delay      (SCD):    3.397ns
    Clock Pessimism Removal (CPR):    0.513ns
  Clock Net Delay (Source):      2.567ns (routing 1.566ns, distribution 1.001ns)
  Clock Net Delay (Destination): 2.877ns (routing 1.714ns, distribution 1.163ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=2048, routed)        2.567     3.397    shift_reg_tap_o/clk_c
    SLICE_X106Y432       FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[246]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X106Y432       FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     3.466 f  shift_reg_tap_o/sr_p.sr_2[246]/Q
                         net (fo=1, routed)           0.102     3.568    shift_reg_tap_o/sr_2[246]
    SLICE_X105Y432       FDRE                                         f  shift_reg_tap_o/sr_p.sr_3[246]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=2048, routed)        2.877     4.018    shift_reg_tap_o/clk_c
    SLICE_X105Y432       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[246]/C
                         clock pessimism             -0.513     3.506    
    SLICE_X105Y432       FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.055     3.561    shift_reg_tap_o/sr_p.sr_3[246]
  -------------------------------------------------------------------
                         required time                         -3.561    
                         arrival time                           3.568    
  -------------------------------------------------------------------
                         slack                                  0.007    

Slack (MET) :             0.011ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[26]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_i/sr_p.sr_2[26]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.168ns  (logic 0.069ns (41.071%)  route 0.099ns (58.929%))
  Logic Levels:           0  
  Clock Path Skew:        0.102ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.956ns
    Source Clock Delay      (SCD):    3.346ns
    Clock Pessimism Removal (CPR):    0.509ns
  Clock Net Delay (Source):      2.516ns (routing 1.566ns, distribution 0.950ns)
  Clock Net Delay (Destination): 2.815ns (routing 1.714ns, distribution 1.101ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=2048, routed)        2.516     3.346    shift_reg_tap_i/clk_c
    SLICE_X106Y411       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[26]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X106Y411       FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     3.415 r  shift_reg_tap_i/sr_p.sr_1[26]/Q
                         net (fo=1, routed)           0.099     3.514    shift_reg_tap_i/sr_1[26]
    SLICE_X105Y411       FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[26]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=2048, routed)        2.815     3.956    shift_reg_tap_i/clk_c
    SLICE_X105Y411       FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[26]/C
                         clock pessimism             -0.509     3.448    
    SLICE_X105Y411       FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.055     3.503    shift_reg_tap_i/sr_p.sr_2[26]
  -------------------------------------------------------------------
                         required time                         -3.503    
                         arrival time                           3.514    
  -------------------------------------------------------------------
                         slack                                  0.011    

Slack (MET) :             0.011ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[26]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_i/sr_p.sr_2[26]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.168ns  (logic 0.069ns (41.071%)  route 0.099ns (58.929%))
  Logic Levels:           0  
  Clock Path Skew:        0.102ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.956ns
    Source Clock Delay      (SCD):    3.346ns
    Clock Pessimism Removal (CPR):    0.509ns
  Clock Net Delay (Source):      2.516ns (routing 1.566ns, distribution 0.950ns)
  Clock Net Delay (Destination): 2.815ns (routing 1.714ns, distribution 1.101ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=2048, routed)        2.516     3.346    shift_reg_tap_i/clk_c
    SLICE_X106Y411       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[26]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X106Y411       FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     3.415 f  shift_reg_tap_i/sr_p.sr_1[26]/Q
                         net (fo=1, routed)           0.099     3.514    shift_reg_tap_i/sr_1[26]
    SLICE_X105Y411       FDRE                                         f  shift_reg_tap_i/sr_p.sr_2[26]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=2048, routed)        2.815     3.956    shift_reg_tap_i/clk_c
    SLICE_X105Y411       FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[26]/C
                         clock pessimism             -0.509     3.448    
    SLICE_X105Y411       FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.055     3.503    shift_reg_tap_i/sr_p.sr_2[26]
  -------------------------------------------------------------------
                         required time                         -3.503    
                         arrival time                           3.514    
  -------------------------------------------------------------------
                         slack                                  0.011    

Slack (MET) :             0.014ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[176]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_i/sr_p.sr_2[176]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.170ns  (logic 0.069ns (40.588%)  route 0.101ns (59.412%))
  Logic Levels:           0  
  Clock Path Skew:        0.101ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.954ns
    Source Clock Delay      (SCD):    3.345ns
    Clock Pessimism Removal (CPR):    0.509ns
  Clock Net Delay (Source):      2.515ns (routing 1.566ns, distribution 0.949ns)
  Clock Net Delay (Destination): 2.813ns (routing 1.714ns, distribution 1.099ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=2048, routed)        2.515     3.345    shift_reg_tap_i/clk_c
    SLICE_X106Y408       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[176]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X106Y408       FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     3.414 r  shift_reg_tap_i/sr_p.sr_1[176]/Q
                         net (fo=1, routed)           0.101     3.515    shift_reg_tap_i/sr_1[176]
    SLICE_X105Y408       FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[176]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=2048, routed)        2.813     3.954    shift_reg_tap_i/clk_c
    SLICE_X105Y408       FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[176]/C
                         clock pessimism             -0.509     3.446    
    SLICE_X105Y408       FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.055     3.501    shift_reg_tap_i/sr_p.sr_2[176]
  -------------------------------------------------------------------
                         required time                         -3.501    
                         arrival time                           3.515    
  -------------------------------------------------------------------
                         slack                                  0.014    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 12.500 }
Period(ns):         25.000
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         25.000      23.501     BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X93Y436   muon_cand_15.roi[2]/C
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X92Y437   muon_cand_15.roi[3]/C
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X93Y437   muon_cand_15.roi[4]/C
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X94Y433   muon_cand_15.roi[5]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X97Y434   muon_sorter_1/sr_p.sr_1_1.roi[1]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X98Y431   muon_sorter_1/sr_p.sr_1_1.roi[4]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X94Y438   muon_sorter_1/sr_p.sr_1_2.sector[0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X92Y416   shift_reg_tap_i/sr_p.sr_1[138]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X90Y408   muon_cand_4.pt[1]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X92Y437   muon_cand_15.roi[3]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X94Y433   muon_cand_15.roi[5]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X93Y436   muon_cand_15.roi[6]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X95Y437   muon_cand_15.roi[7]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X107Y428  muon_cand_15.sector[2]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X95Y437          lsfr_1/output_vector_1[255]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X108Y439         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X86Y416          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y407          lsfr_1/shiftreg_vector[10]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y407          lsfr_1/shiftreg_vector[11]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X87Y414          lsfr_1/shiftreg_vector[123]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X95Y437          lsfr_1/output_vector_1[255]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X108Y439         lsfr_1/shiftreg_vector[0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X87Y415          lsfr_1/shiftreg_vector[104]/C



