Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Fri Feb  8 08:15:14 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+-------+--------+
|  Fanout |  Nets |      % |
+---------+-------+--------+
|       1 | 21622 |  58.98 |
|       2 |  3396 |   9.26 |
|       3 |  1789 |   4.88 |
|       4 |  1170 |   3.19 |
|    5-10 |  3996 |  10.90 |
|   11-50 |  4591 |  12.52 |
|  51-100 |    92 |   0.25 |
| 101-500 |     3 |   0.01 |
|    >500 |     0 |   0.00 |
+---------+-------+--------+
|     ALL | 36659 | 100.00 |
+---------+-------+--------+


