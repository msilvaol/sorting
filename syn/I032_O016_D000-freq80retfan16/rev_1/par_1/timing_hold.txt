Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Fri Feb  8 08:10:45 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.011        0.000                      0                11529        5.975        0.000                       0                 12897  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 6.250}        12.500          80.000          
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.011        0.000                      0                11529        5.975        0.000                       0                 12042  
clk_wrapper                                                                             498.562        0.000                       0                   855  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.011ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        5.975ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.011ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_12[90]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_13[90]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.270ns  (logic 0.072ns (26.667%)  route 0.198ns (73.333%))
  Logic Levels:           0  
  Clock Path Skew:        0.206ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.871ns
    Source Clock Delay      (SCD):    3.239ns
    Clock Pessimism Removal (CPR):    0.426ns
  Clock Net Delay (Source):      2.409ns (routing 1.128ns, distribution 1.281ns)
  Clock Net Delay (Destination): 2.730ns (routing 1.234ns, distribution 1.496ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=12041, routed)       2.409     3.239    shift_reg_tap_o/clk_c
    SLICE_X150Y479       FDRE                                         r  shift_reg_tap_o/sr_p.sr_12[90]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X150Y479       FDRE (Prop_GFF2_SLICEM_C_Q)
                                                      0.072     3.311 r  shift_reg_tap_o/sr_p.sr_12[90]/Q
                         net (fo=1, routed)           0.198     3.509    shift_reg_tap_o/sr_12[90]
    SLICE_X151Y493       FDRE                                         r  shift_reg_tap_o/sr_p.sr_13[90]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=12041, routed)       2.730     3.871    shift_reg_tap_o/clk_c
    SLICE_X151Y493       FDRE                                         r  shift_reg_tap_o/sr_p.sr_13[90]/C
                         clock pessimism             -0.426     3.445    
    SLICE_X151Y493       FDRE (Hold_DFF_SLICEL_C_D)
                                                      0.053     3.498    shift_reg_tap_o/sr_p.sr_13[90]
  -------------------------------------------------------------------
                         required time                         -3.498    
                         arrival time                           3.509    
  -------------------------------------------------------------------
                         slack                                  0.011    

Slack (MET) :             0.011ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_12[90]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_13[90]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.270ns  (logic 0.072ns (26.667%)  route 0.198ns (73.333%))
  Logic Levels:           0  
  Clock Path Skew:        0.206ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.871ns
    Source Clock Delay      (SCD):    3.239ns
    Clock Pessimism Removal (CPR):    0.426ns
  Clock Net Delay (Source):      2.409ns (routing 1.128ns, distribution 1.281ns)
  Clock Net Delay (Destination): 2.730ns (routing 1.234ns, distribution 1.496ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=12041, routed)       2.409     3.239    shift_reg_tap_o/clk_c
    SLICE_X150Y479       FDRE                                         r  shift_reg_tap_o/sr_p.sr_12[90]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X150Y479       FDRE (Prop_GFF2_SLICEM_C_Q)
                                                      0.072     3.311 f  shift_reg_tap_o/sr_p.sr_12[90]/Q
                         net (fo=1, routed)           0.198     3.509    shift_reg_tap_o/sr_12[90]
    SLICE_X151Y493       FDRE                                         f  shift_reg_tap_o/sr_p.sr_13[90]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=12041, routed)       2.730     3.871    shift_reg_tap_o/clk_c
    SLICE_X151Y493       FDRE                                         r  shift_reg_tap_o/sr_p.sr_13[90]/C
                         clock pessimism             -0.426     3.445    
    SLICE_X151Y493       FDRE (Hold_DFF_SLICEL_C_D)
                                                      0.053     3.498    shift_reg_tap_o/sr_p.sr_13[90]
  -------------------------------------------------------------------
                         required time                         -3.498    
                         arrival time                           3.509    
  -------------------------------------------------------------------
                         slack                                  0.011    

Slack (MET) :             0.011ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_12[250]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_13[250]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.250ns  (logic 0.070ns (28.000%)  route 0.180ns (72.000%))
  Logic Levels:           0  
  Clock Path Skew:        0.185ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.863ns
    Source Clock Delay      (SCD):    3.183ns
    Clock Pessimism Removal (CPR):    0.496ns
  Clock Net Delay (Source):      2.353ns (routing 1.128ns, distribution 1.225ns)
  Clock Net Delay (Destination): 2.722ns (routing 1.234ns, distribution 1.488ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=12041, routed)       2.353     3.183    shift_reg_tap_o/clk_c
    SLICE_X141Y490       FDRE                                         r  shift_reg_tap_o/sr_p.sr_12[250]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X141Y490       FDRE (Prop_GFF_SLICEM_C_Q)
                                                      0.070     3.253 r  shift_reg_tap_o/sr_p.sr_12[250]/Q
                         net (fo=1, routed)           0.180     3.433    shift_reg_tap_o/sr_12[250]
    SLICE_X142Y492       FDRE                                         r  shift_reg_tap_o/sr_p.sr_13[250]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=12041, routed)       2.722     3.863    shift_reg_tap_o/clk_c
    SLICE_X142Y492       FDRE                                         r  shift_reg_tap_o/sr_p.sr_13[250]/C
                         clock pessimism             -0.496     3.367    
    SLICE_X142Y492       FDRE (Hold_CFF2_SLICEM_C_D)
                                                      0.054     3.421    shift_reg_tap_o/sr_p.sr_13[250]
  -------------------------------------------------------------------
                         required time                         -3.421    
                         arrival time                           3.433    
  -------------------------------------------------------------------
                         slack                                  0.011    

Slack (MET) :             0.011ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_12[250]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_13[250]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.250ns  (logic 0.070ns (28.000%)  route 0.180ns (72.000%))
  Logic Levels:           0  
  Clock Path Skew:        0.185ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.863ns
    Source Clock Delay      (SCD):    3.183ns
    Clock Pessimism Removal (CPR):    0.496ns
  Clock Net Delay (Source):      2.353ns (routing 1.128ns, distribution 1.225ns)
  Clock Net Delay (Destination): 2.722ns (routing 1.234ns, distribution 1.488ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=12041, routed)       2.353     3.183    shift_reg_tap_o/clk_c
    SLICE_X141Y490       FDRE                                         r  shift_reg_tap_o/sr_p.sr_12[250]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X141Y490       FDRE (Prop_GFF_SLICEM_C_Q)
                                                      0.070     3.253 f  shift_reg_tap_o/sr_p.sr_12[250]/Q
                         net (fo=1, routed)           0.180     3.433    shift_reg_tap_o/sr_12[250]
    SLICE_X142Y492       FDRE                                         f  shift_reg_tap_o/sr_p.sr_13[250]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=12041, routed)       2.722     3.863    shift_reg_tap_o/clk_c
    SLICE_X142Y492       FDRE                                         r  shift_reg_tap_o/sr_p.sr_13[250]/C
                         clock pessimism             -0.496     3.367    
    SLICE_X142Y492       FDRE (Hold_CFF2_SLICEM_C_D)
                                                      0.054     3.421    shift_reg_tap_o/sr_p.sr_13[250]
  -------------------------------------------------------------------
                         required time                         -3.421    
                         arrival time                           3.433    
  -------------------------------------------------------------------
                         slack                                  0.011    

Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_11[330]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_12[330]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.206ns  (logic 0.070ns (33.981%)  route 0.136ns (66.019%))
  Logic Levels:           0  
  Clock Path Skew:        0.141ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.753ns
    Source Clock Delay      (SCD):    3.186ns
    Clock Pessimism Removal (CPR):    0.426ns
  Clock Net Delay (Source):      2.356ns (routing 1.128ns, distribution 1.228ns)
  Clock Net Delay (Destination): 2.612ns (routing 1.234ns, distribution 1.378ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=12041, routed)       2.356     3.186    shift_reg_tap_i/clk_c
    SLICE_X114Y484       FDRE                                         r  shift_reg_tap_i/sr_p.sr_11[330]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X114Y484       FDRE (Prop_BFF_SLICEL_C_Q)
                                                      0.070     3.256 r  shift_reg_tap_i/sr_p.sr_11[330]/Q
                         net (fo=1, routed)           0.136     3.392    shift_reg_tap_i/sr_11[330]
    SLICE_X114Y478       FDRE                                         r  shift_reg_tap_i/sr_p.sr_12[330]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=12041, routed)       2.612     3.753    shift_reg_tap_i/clk_c
    SLICE_X114Y478       FDRE                                         r  shift_reg_tap_i/sr_p.sr_12[330]/C
                         clock pessimism             -0.426     3.327    
    SLICE_X114Y478       FDRE (Hold_BFF_SLICEL_C_D)
                                                      0.053     3.380    shift_reg_tap_i/sr_p.sr_12[330]
  -------------------------------------------------------------------
                         required time                         -3.380    
                         arrival time                           3.392    
  -------------------------------------------------------------------
                         slack                                  0.012    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 6.250 }
Period(ns):         12.500
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         12.500      11.001     BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X112Y326  shift_reg_tap_i/sr_p.sr_13[381]/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X112Y325  shift_reg_tap_i/sr_p.sr_13[382]/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X111Y327  shift_reg_tap_i/sr_p.sr_13[383]/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X112Y328  shift_reg_tap_i/sr_p.sr_13[384]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X121Y412  shift_reg_tap_i/sr_p.sr_13[390]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X136Y469  shift_reg_tap_i/sr_p.sr_13[408]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X140Y471  shift_reg_tap_i/sr_p.sr_13[411]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X106Y378  shift_reg_tap_i/sr_p.sr_2[300]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X106Y378  shift_reg_tap_i/sr_p.sr_2[302]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X112Y328  shift_reg_tap_i/sr_p.sr_13[384]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X112Y328  shift_reg_tap_i/sr_p.sr_13[385]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X155Y539  shift_reg_tap_i/sr_p.sr_2[28]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X136Y350  shift_reg_tap_i/sr_p.sr_9[7]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X138Y496  shift_reg_tap_i/sr_p.sr_9[89]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X145Y563         reducer_1/delay_block[0][98]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X145Y563         reducer_1/delay_block[0][99]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X145Y519         reducer_1/delay_block[0][9]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X145Y563         reducer_1/delay_block[0][98]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X145Y563         reducer_1/delay_block[0][99]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X148Y543         reducer_1/delay_block[1][10]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X153Y554         reducer_1/delay_block[1][11]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X140Y563         reducer_1/delay_block[1][16]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X143Y563         reducer_1/delay_block[1][19]/C



