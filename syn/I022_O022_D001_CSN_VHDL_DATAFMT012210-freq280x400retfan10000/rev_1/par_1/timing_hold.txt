Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Thu Mar  7 03:09:35 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.020        0.000                      0                 1484        1.511        0.000                       0                  2444  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.786}        3.572           279.955         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.020        0.000                      0                 1484        1.511        0.000                       0                  1772  
clk_wrapper                                                                             498.562        0.000                       0                   672  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.020ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.511ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[259]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_17.idx_ret_14[17]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.871ns
    Source Clock Delay      (SCD):    1.518ns
    Clock Pessimism Removal (CPR):    0.347ns
  Clock Net Delay (Source):      0.972ns (routing 0.522ns, distribution 0.450ns)
  Clock Net Delay (Destination): 1.099ns (routing 0.578ns, distribution 0.521ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1771, routed)        0.972     1.518    shift_reg_tap_i/clk_c
    SLICE_X94Y533        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[259]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X94Y533        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.557 r  shift_reg_tap_i/sr_p.sr_1[259]/Q
                         net (fo=1, routed)           0.034     1.591    dut_inst/input_slr[259]
    SLICE_X94Y533        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_14[17]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1771, routed)        1.099     1.871    dut_inst/clk_c
    SLICE_X94Y533        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_14[17]/C
                         clock pessimism             -0.347     1.524    
    SLICE_X94Y533        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.571    dut_inst/ret_array_1_17.idx_ret_14[17]
  -------------------------------------------------------------------
                         required time                         -1.571    
                         arrival time                           1.591    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[259]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_17.idx_ret_14[17]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.871ns
    Source Clock Delay      (SCD):    1.518ns
    Clock Pessimism Removal (CPR):    0.347ns
  Clock Net Delay (Source):      0.972ns (routing 0.522ns, distribution 0.450ns)
  Clock Net Delay (Destination): 1.099ns (routing 0.578ns, distribution 0.521ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1771, routed)        0.972     1.518    shift_reg_tap_i/clk_c
    SLICE_X94Y533        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[259]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X94Y533        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.557 f  shift_reg_tap_i/sr_p.sr_1[259]/Q
                         net (fo=1, routed)           0.034     1.591    dut_inst/input_slr[259]
    SLICE_X94Y533        FDRE                                         f  dut_inst/ret_array_1_17.idx_ret_14[17]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1771, routed)        1.099     1.871    dut_inst/clk_c
    SLICE_X94Y533        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_14[17]/C
                         clock pessimism             -0.347     1.524    
    SLICE_X94Y533        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.571    dut_inst/ret_array_1_17.idx_ret_14[17]
  -------------------------------------------------------------------
                         required time                         -1.571    
                         arrival time                           1.591    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[149]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_17.idx_ret_18[11]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.871ns
    Source Clock Delay      (SCD):    1.519ns
    Clock Pessimism Removal (CPR):    0.346ns
  Clock Net Delay (Source):      0.973ns (routing 0.522ns, distribution 0.451ns)
  Clock Net Delay (Destination): 1.099ns (routing 0.578ns, distribution 0.521ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1771, routed)        0.973     1.519    shift_reg_tap_i/clk_c
    SLICE_X89Y529        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[149]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X89Y529        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.558 r  shift_reg_tap_i/sr_p.sr_1[149]/Q
                         net (fo=1, routed)           0.034     1.592    dut_inst/input_slr[149]
    SLICE_X89Y529        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_18[11]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1771, routed)        1.099     1.871    dut_inst/clk_c
    SLICE_X89Y529        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_18[11]/C
                         clock pessimism             -0.346     1.525    
    SLICE_X89Y529        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.572    dut_inst/ret_array_1_17.idx_ret_18[11]
  -------------------------------------------------------------------
                         required time                         -1.572    
                         arrival time                           1.592    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[149]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_17.idx_ret_18[11]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.871ns
    Source Clock Delay      (SCD):    1.519ns
    Clock Pessimism Removal (CPR):    0.346ns
  Clock Net Delay (Source):      0.973ns (routing 0.522ns, distribution 0.451ns)
  Clock Net Delay (Destination): 1.099ns (routing 0.578ns, distribution 0.521ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1771, routed)        0.973     1.519    shift_reg_tap_i/clk_c
    SLICE_X89Y529        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[149]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X89Y529        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.558 f  shift_reg_tap_i/sr_p.sr_1[149]/Q
                         net (fo=1, routed)           0.034     1.592    dut_inst/input_slr[149]
    SLICE_X89Y529        FDRE                                         f  dut_inst/ret_array_1_17.idx_ret_18[11]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1771, routed)        1.099     1.871    dut_inst/clk_c
    SLICE_X89Y529        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_18[11]/C
                         clock pessimism             -0.346     1.525    
    SLICE_X89Y529        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.572    dut_inst/ret_array_1_17.idx_ret_18[11]
  -------------------------------------------------------------------
                         required time                         -1.572    
                         arrival time                           1.592    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[123]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_1_17.idx_ret_19[11]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.889ns
    Source Clock Delay      (SCD):    1.536ns
    Clock Pessimism Removal (CPR):    0.347ns
  Clock Net Delay (Source):      0.990ns (routing 0.522ns, distribution 0.468ns)
  Clock Net Delay (Destination): 1.117ns (routing 0.578ns, distribution 0.539ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1771, routed)        0.990     1.536    shift_reg_tap_i/clk_c
    SLICE_X98Y534        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[123]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y534        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.575 r  shift_reg_tap_i/sr_p.sr_1[123]/Q
                         net (fo=1, routed)           0.034     1.609    dut_inst/input_slr[123]
    SLICE_X98Y534        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_19[11]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1771, routed)        1.117     1.889    dut_inst/clk_c
    SLICE_X98Y534        FDRE                                         r  dut_inst/ret_array_1_17.idx_ret_19[11]/C
                         clock pessimism             -0.347     1.542    
    SLICE_X98Y534        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.589    dut_inst/ret_array_1_17.idx_ret_19[11]
  -------------------------------------------------------------------
                         required time                         -1.589    
                         arrival time                           1.609    
  -------------------------------------------------------------------
                         slack                                  0.020    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.786 }
Period(ns):         3.572
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         3.572       2.073      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X104Y531  dut_inst/ret_array_1_11.idx_ret_31[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X104Y531  dut_inst/ret_array_1_11.idx_ret_31[1]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X97Y530   dut_inst/ret_array_1_11.idx_ret_31[2]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X100Y528  dut_inst/ret_array_1_11.idx_ret_31[3]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X97Y530   dut_inst/ret_array_1_11.idx_ret_31[2]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X101Y533  dut_inst/ret_array_1_11.idx_ret_32[0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X101Y533  dut_inst/ret_array_1_11.idx_ret_32[1]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X100Y530  shift_reg_tap_i/sr_p.sr_1[144]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X98Y513   shift_reg_tap_o/sr_p.sr_1[133]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X101Y533  dut_inst/ret_array_1_11.idx_ret_32[0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X101Y533  dut_inst/ret_array_1_11.idx_ret_32[1]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X101Y525  dut_inst/ret_array_1_13.idx_ret_49[3]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X96Y533   dut_inst/ret_array_1_17.idx_ret_19[6]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X96Y533   dut_inst/ret_array_1_17.idx_ret_19[7]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X90Y532          lsfr_1/shiftreg_vector[283]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X90Y532          lsfr_1/shiftreg_vector[284]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X91Y531          lsfr_1/shiftreg_vector[285]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X91Y531          lsfr_1/shiftreg_vector[285]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y526         lsfr_1/shiftreg_vector[31]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y526         lsfr_1/shiftreg_vector[32]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[5][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y527         lsfr_1/shiftreg_vector[28]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y527         lsfr_1/shiftreg_vector[29]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y527         lsfr_1/shiftreg_vector[30]/C



