Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Thu Mar  7 03:12:33 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper_csn
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+------+--------+
|  Fanout | Nets |      % |
+---------+------+--------+
|       1 | 2167 |  40.50 |
|       2 | 1466 |  27.40 |
|       3 |  683 |  12.76 |
|       4 |  457 |   8.54 |
|    5-10 |  341 |   6.37 |
|   11-50 |  236 |   4.41 |
|  51-100 |    0 |   0.00 |
| 101-500 |    0 |   0.00 |
|    >500 |    0 |   0.00 |
+---------+------+--------+
|     ALL | 5350 | 100.00 |
+---------+------+--------+


