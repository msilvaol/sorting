Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Sat Feb  9 06:40:22 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+-------+--------+
|  Fanout |  Nets |      % |
+---------+-------+--------+
|       1 | 22165 |  58.17 |
|       2 |  4038 |  10.59 |
|       3 |  2424 |   6.36 |
|       4 |  1353 |   3.55 |
|    5-10 |  4489 |  11.78 |
|   11-50 |  3578 |   9.39 |
|  51-100 |    52 |   0.13 |
| 101-500 |     0 |   0.00 |
|    >500 |     0 |   0.00 |
+---------+-------+--------+
|     ALL | 38099 | 100.00 |
+---------+-------+--------+


