Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Mon Mar 11 17:35:39 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_utilization -hierarchical -file hier_utilization.txt
| Design       : wrapper_csn
| Device       : xcvu9pflgc2104-1
| Design State : Routed
------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+--------------------------------------+---------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
|               Instance               |                                Module                               | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+--------------------------------------+---------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
| wrapper_csn                          |                                                               (top) |       1465 |       1465 |       0 |    0 | 1336 |      0 |      0 |    0 |            0 |
|   (wrapper_csn)                      |                                                               (top) |          2 |          2 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   dut_inst                           | muon_sorter_I022_O022_D001_CSN_VHDL_PT1BIT-freq280retfan10000_rev_1 |       1388 |       1388 |       0 |    0 |  382 |      0 |      0 |    0 |            0 |
|     (dut_inst)                       | muon_sorter_I022_O022_D001_CSN_VHDL_PT1BIT-freq280retfan10000_rev_1 |        120 |        120 |       0 |    0 |  224 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.0.csn_cmp_inst  |                                       csn_cmp_false_false_false_114 |          4 |          4 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.1.csn_cmp_inst  |                                        csn_cmp_false_false_false_19 |          8 |          8 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.10.csn_cmp_inst |                                        csn_cmp_false_false_false_93 |          8 |          8 |       0 |    0 |    7 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.2.csn_cmp_inst  |                                         csn_cmp_false_false_false_9 |         13 |         13 |       0 |    0 |   10 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.3.csn_cmp_inst  |                                        csn_cmp_false_false_false_98 |         13 |         13 |       0 |    0 |   12 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.4.csn_cmp_inst  |                                        csn_cmp_false_false_false_76 |          9 |          9 |       0 |    0 |    8 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.5.csn_cmp_inst  |                                        csn_cmp_false_false_false_79 |         11 |         11 |       0 |    0 |    9 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.6.csn_cmp_inst  |                                        csn_cmp_false_false_false_82 |         11 |         11 |       0 |    0 |    9 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.7.csn_cmp_inst  |                                        csn_cmp_false_false_false_84 |         12 |         12 |       0 |    0 |   11 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.8.csn_cmp_inst  |                                        csn_cmp_false_false_false_87 |          8 |          8 |       0 |    0 |    8 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.9.csn_cmp_inst  |                                        csn_cmp_false_false_false_90 |         14 |         14 |       0 |    0 |   12 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.0.csn_cmp_inst  |                                        csn_cmp_false_false_false_96 |          5 |          5 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.1.csn_cmp_inst  |                                       csn_cmp_false_false_false_107 |         12 |         12 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.10.csn_cmp_inst |                                       csn_cmp_false_false_false_110 |          7 |          7 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.2.csn_cmp_inst  |                                        csn_cmp_false_false_false_68 |         10 |         10 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.3.csn_cmp_inst  |                                        csn_cmp_false_false_false_22 |          9 |          9 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.4.csn_cmp_inst  |                                           csn_cmp_false_false_false |         13 |         13 |       0 |    0 |    7 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.5.csn_cmp_inst  |                                       csn_cmp_false_false_false_106 |          9 |          9 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.6.csn_cmp_inst  |                                         csn_cmp_false_false_false_6 |         13 |         13 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.7.csn_cmp_inst  |                                        csn_cmp_false_false_false_21 |         14 |         14 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.8.csn_cmp_inst  |                                        csn_cmp_false_false_false_11 |         13 |         13 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.9.csn_cmp_inst  |                                        csn_cmp_false_false_false_14 |         13 |         13 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.0.csn_cmp_inst |                                        csn_cmp_false_false_false_39 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.1.csn_cmp_inst |                                        csn_cmp_false_false_false_42 |          4 |          4 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.2.csn_cmp_inst |                                        csn_cmp_false_false_false_45 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.3.csn_cmp_inst |                                        csn_cmp_false_false_false_48 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.4.csn_cmp_inst |                                        csn_cmp_false_false_false_51 |          6 |          6 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.5.csn_cmp_inst |                                        csn_cmp_false_false_false_54 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.6.csn_cmp_inst |                                        csn_cmp_false_false_false_57 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.7.csn_cmp_inst |                                        csn_cmp_false_false_false_59 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.0.csn_cmp_inst |                                        csn_cmp_false_false_false_69 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.1.csn_cmp_inst |                                       csn_cmp_false_false_false_112 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.2.csn_cmp_inst |                                        csn_cmp_false_false_false_86 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.3.csn_cmp_inst |                                        csn_cmp_false_false_false_25 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.4.csn_cmp_inst |                                        csn_cmp_false_false_false_17 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.5.csn_cmp_inst |                                        csn_cmp_false_false_false_70 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.6.csn_cmp_inst |                                        csn_cmp_false_false_false_85 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.0.csn_cmp_inst  |                                         csn_cmp_false_false_false_8 |          9 |          9 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.1.csn_cmp_inst  |                                       csn_cmp_false_false_false_102 |          6 |          6 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.10.csn_cmp_inst |                                        csn_cmp_false_false_false_47 |         14 |         14 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.2.csn_cmp_inst  |                                         csn_cmp_false_false_false_1 |         11 |         11 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.3.csn_cmp_inst  |                                       csn_cmp_false_false_false_109 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.4.csn_cmp_inst  |                                        csn_cmp_false_false_false_31 |          5 |          5 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.5.csn_cmp_inst  |                                        csn_cmp_false_false_false_34 |          8 |          8 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.6.csn_cmp_inst  |                                        csn_cmp_false_false_false_36 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.7.csn_cmp_inst  |                                        csn_cmp_false_false_false_38 |          5 |          5 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.8.csn_cmp_inst  |                                        csn_cmp_false_false_false_41 |         13 |         13 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.9.csn_cmp_inst  |                                        csn_cmp_false_false_false_44 |          7 |          7 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.0.csn_cmp_inst  |                                        csn_cmp_false_false_false_50 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.1.csn_cmp_inst  |                                        csn_cmp_false_false_false_53 |          6 |          6 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.10.csn_cmp_inst |                                        csn_cmp_false_false_false_29 |          7 |          7 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.2.csn_cmp_inst  |                                        csn_cmp_false_false_false_56 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.3.csn_cmp_inst  |                                        csn_cmp_false_false_false_58 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.4.csn_cmp_inst  |                                        csn_cmp_false_false_false_61 |          4 |          4 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.5.csn_cmp_inst  |                                        csn_cmp_false_false_false_63 |         13 |         13 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.6.csn_cmp_inst  |                                        csn_cmp_false_false_false_65 |          4 |          4 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.7.csn_cmp_inst  |                                        csn_cmp_false_false_false_67 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.8.csn_cmp_inst  |                                        csn_cmp_false_false_false_18 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.9.csn_cmp_inst  |                                        csn_cmp_false_false_false_24 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.0.csn_cmp_inst  |                                        csn_cmp_false_false_false_75 |          1 |          1 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.1.csn_cmp_inst  |                                        csn_cmp_false_false_false_78 |          1 |          1 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.7.csn_cmp_inst  |                                        csn_cmp_false_false_false_95 |          3 |          3 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.8.csn_cmp_inst  |                                         csn_cmp_false_false_false_4 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.0.csn_cmp_inst  |                                       csn_cmp_false_false_false_105 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.1.csn_cmp_inst  |                                        csn_cmp_false_false_false_16 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.2.csn_cmp_inst  |                                         csn_cmp_false_false_false_5 |         27 |         27 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.3.csn_cmp_inst  |                                        csn_cmp_false_false_false_20 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.4.csn_cmp_inst  |                                        csn_cmp_false_false_false_28 |         21 |         21 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.5.csn_cmp_inst  |                                        csn_cmp_false_false_false_13 |         24 |         24 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.6.csn_cmp_inst  |                                         csn_cmp_false_false_false_0 |         28 |         28 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.7.csn_cmp_inst  |                                       csn_cmp_false_false_false_113 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.8.csn_cmp_inst  |                                       csn_cmp_false_false_false_101 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.9.csn_cmp_inst  |                                        csn_cmp_false_false_false_73 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.0.csn_cmp_inst  |                                        csn_cmp_false_false_false_30 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.1.csn_cmp_inst  |                                        csn_cmp_false_false_false_33 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.2.csn_cmp_inst  |                                        csn_cmp_false_false_false_35 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.3.csn_cmp_inst  |                                        csn_cmp_false_false_false_37 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.4.csn_cmp_inst  |                                        csn_cmp_false_false_false_40 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.5.csn_cmp_inst  |                                        csn_cmp_false_false_false_43 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.6.csn_cmp_inst  |                                        csn_cmp_false_false_false_46 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.7.csn_cmp_inst  |                                        csn_cmp_false_false_false_49 |          1 |          1 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.8.csn_cmp_inst  |                                        csn_cmp_false_false_false_52 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.9.csn_cmp_inst  |                                        csn_cmp_false_false_false_55 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.0.csn_cmp_inst  |                                        csn_cmp_false_false_false_60 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.1.csn_cmp_inst  |                                        csn_cmp_false_false_false_62 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.2.csn_cmp_inst  |                                        csn_cmp_false_false_false_64 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.3.csn_cmp_inst  |                                        csn_cmp_false_false_false_66 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.4.csn_cmp_inst  |                                        csn_cmp_false_false_false_23 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.5.csn_cmp_inst  |                                         csn_cmp_false_false_false_2 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.6.csn_cmp_inst  |                                         csn_cmp_false_false_false_3 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.7.csn_cmp_inst  |                                        csn_cmp_false_false_false_99 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.8.csn_cmp_inst  |                                        csn_cmp_false_false_false_77 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.9.csn_cmp_inst  |                                        csn_cmp_false_false_false_80 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.0.csn_cmp_inst  |                                        csn_cmp_false_false_false_72 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.1.csn_cmp_inst  |                                        csn_cmp_false_false_false_88 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.2.csn_cmp_inst  |                                        csn_cmp_false_false_false_91 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.3.csn_cmp_inst  |                                        csn_cmp_false_false_false_94 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.4.csn_cmp_inst  |                                        csn_cmp_false_false_false_97 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.5.csn_cmp_inst  |                                       csn_cmp_false_false_false_100 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.6.csn_cmp_inst  |                                       csn_cmp_false_false_false_103 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.7.csn_cmp_inst  |                                       csn_cmp_false_false_false_108 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.0.csn_cmp_inst  |                                       csn_cmp_false_false_false_104 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.1.csn_cmp_inst  |                                        csn_cmp_false_false_false_12 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.2.csn_cmp_inst  |                                        csn_cmp_false_false_false_15 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.3.csn_cmp_inst  |                                        csn_cmp_false_false_false_26 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.4.csn_cmp_inst  |                                        csn_cmp_false_false_false_10 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.5.csn_cmp_inst  |                                        csn_cmp_false_false_false_27 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.6.csn_cmp_inst  |                                         csn_cmp_false_false_false_7 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.7.csn_cmp_inst  |                                        csn_cmp_false_false_false_74 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.8.csn_cmp_inst  |                                        csn_cmp_false_false_false_32 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   lsfr_1                             |                                                                lfsr |          1 |          1 |       0 |    0 |  221 |      0 |      0 |    0 |            0 |
|   reducer_1                          |                                                             reducer |         74 |         74 |       0 |    0 |  293 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_i                    |                                                 shift_reg_tap_220_1 |          0 |          0 |       0 |    0 |  220 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_o                    |                                                 shift_reg_tap_256_1 |          0 |          0 |       0 |    0 |  220 |      0 |      0 |    0 |            0 |
+--------------------------------------+---------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
* Note: The sum of lower-level cells may be larger than their parent cells total, due to cross-hierarchy LUT combining


