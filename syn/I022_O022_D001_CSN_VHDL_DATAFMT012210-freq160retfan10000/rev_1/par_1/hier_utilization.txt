Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Thu Mar  7 03:31:49 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_utilization -hierarchical -file hier_utilization.txt
| Design       : wrapper_csn
| Device       : xcvu9pflgc2104-1
| Design State : Routed
------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+--------------------------------------+----------------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
|               Instance               |                                   Module                                   | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+--------------------------------------+----------------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
| wrapper_csn                          |                                                                      (top) |       1965 |       1965 |       0 |    0 | 2229 |      0 |      0 |    0 |            0 |
|   (wrapper_csn)                      |                                                                      (top) |          0 |          0 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   dut_inst                           | muon_sorter_I022_O022_D001_CSN_VHDL_DATAFMT012210-freq160retfan10000_rev_1 |       1867 |       1867 |       0 |    0 |  987 |      0 |      0 |    0 |            0 |
|     (dut_inst)                       | muon_sorter_I022_O022_D001_CSN_VHDL_DATAFMT012210-freq160retfan10000_rev_1 |          0 |          0 |       0 |    0 |  987 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.0.csn_cmp_inst  |                                              csn_cmp_false_false_false_114 |          5 |          5 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.1.csn_cmp_inst  |                                               csn_cmp_false_false_false_14 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.10.csn_cmp_inst |                                               csn_cmp_false_false_false_31 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.2.csn_cmp_inst  |                                                csn_cmp_false_false_false_8 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.3.csn_cmp_inst  |                                               csn_cmp_false_false_false_11 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.4.csn_cmp_inst  |                                               csn_cmp_false_false_false_15 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.5.csn_cmp_inst  |                                               csn_cmp_false_false_false_21 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.6.csn_cmp_inst  |                                                csn_cmp_false_false_false_0 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.7.csn_cmp_inst  |                                              csn_cmp_false_false_false_108 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.8.csn_cmp_inst  |                                               csn_cmp_false_false_false_25 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.9.csn_cmp_inst  |                                               csn_cmp_false_false_false_28 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.0.csn_cmp_inst  |                                               csn_cmp_false_false_false_34 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.1.csn_cmp_inst  |                                               csn_cmp_false_false_false_37 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.10.csn_cmp_inst |                                               csn_cmp_false_false_false_60 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.2.csn_cmp_inst  |                                               csn_cmp_false_false_false_40 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.3.csn_cmp_inst  |                                               csn_cmp_false_false_false_42 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.4.csn_cmp_inst  |                                               csn_cmp_false_false_false_45 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.5.csn_cmp_inst  |                                               csn_cmp_false_false_false_47 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.6.csn_cmp_inst  |                                               csn_cmp_false_false_false_49 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.7.csn_cmp_inst  |                                               csn_cmp_false_false_false_51 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.8.csn_cmp_inst  |                                               csn_cmp_false_false_false_54 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.9.csn_cmp_inst  |                                               csn_cmp_false_false_false_57 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.0.csn_cmp_inst |                                               csn_cmp_false_false_false_82 |         25 |         25 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.1.csn_cmp_inst |                                               csn_cmp_false_false_false_85 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.2.csn_cmp_inst |                                               csn_cmp_false_false_false_88 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.3.csn_cmp_inst |                                               csn_cmp_false_false_false_91 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.4.csn_cmp_inst |                                               csn_cmp_false_false_false_94 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.5.csn_cmp_inst |                                               csn_cmp_false_false_false_97 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.6.csn_cmp_inst |                                              csn_cmp_false_false_false_100 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.7.csn_cmp_inst |                                              csn_cmp_false_false_false_102 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.0.csn_cmp_inst |                                               csn_cmp_false_false_false_16 |         34 |         34 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.1.csn_cmp_inst |                                              csn_cmp_false_false_false_112 |         32 |         32 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.2.csn_cmp_inst |                                               csn_cmp_false_false_false_12 |         37 |         37 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.3.csn_cmp_inst |                                                csn_cmp_false_false_false_3 |         39 |         39 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.4.csn_cmp_inst |                                              csn_cmp_false_false_false_106 |         28 |         28 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.5.csn_cmp_inst |                                              csn_cmp_false_false_false_103 |         28 |         28 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.6.csn_cmp_inst |                                               csn_cmp_false_false_false_20 |         29 |         29 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.0.csn_cmp_inst  |                                               csn_cmp_false_false_false_63 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.1.csn_cmp_inst  |                                               csn_cmp_false_false_false_66 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.10.csn_cmp_inst |                                               csn_cmp_false_false_false_90 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.2.csn_cmp_inst  |                                               csn_cmp_false_false_false_69 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.3.csn_cmp_inst  |                                               csn_cmp_false_false_false_71 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.4.csn_cmp_inst  |                                               csn_cmp_false_false_false_74 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.5.csn_cmp_inst  |                                               csn_cmp_false_false_false_77 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.6.csn_cmp_inst  |                                               csn_cmp_false_false_false_79 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.7.csn_cmp_inst  |                                               csn_cmp_false_false_false_81 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.8.csn_cmp_inst  |                                               csn_cmp_false_false_false_84 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.9.csn_cmp_inst  |                                               csn_cmp_false_false_false_87 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.0.csn_cmp_inst  |                                               csn_cmp_false_false_false_93 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.1.csn_cmp_inst  |                                               csn_cmp_false_false_false_96 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.10.csn_cmp_inst |                                               csn_cmp_false_false_false_23 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.2.csn_cmp_inst  |                                               csn_cmp_false_false_false_99 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.3.csn_cmp_inst  |                                              csn_cmp_false_false_false_101 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.4.csn_cmp_inst  |                                              csn_cmp_false_false_false_107 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.5.csn_cmp_inst  |                                               csn_cmp_false_false_false_24 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.6.csn_cmp_inst  |                                              csn_cmp_false_false_false_104 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.7.csn_cmp_inst  |                                                csn_cmp_false_false_false_6 |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.8.csn_cmp_inst  |                                               csn_cmp_false_false_false_13 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.9.csn_cmp_inst  |                                               csn_cmp_false_false_false_19 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.0.csn_cmp_inst  |                                              csn_cmp_false_false_false_111 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.1.csn_cmp_inst  |                                                csn_cmp_false_false_false_9 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.2.csn_cmp_inst  |                                               csn_cmp_false_false_false_22 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.3.csn_cmp_inst  |                                                csn_cmp_false_false_false_5 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.4.csn_cmp_inst  |                                              csn_cmp_false_false_false_110 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.5.csn_cmp_inst  |                                               csn_cmp_false_false_false_27 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.6.csn_cmp_inst  |                                               csn_cmp_false_false_false_30 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.7.csn_cmp_inst  |                                               csn_cmp_false_false_false_33 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.8.csn_cmp_inst  |                                               csn_cmp_false_false_false_36 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.9.csn_cmp_inst  |                                               csn_cmp_false_false_false_39 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.0.csn_cmp_inst  |                                               csn_cmp_false_false_false_44 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.1.csn_cmp_inst  |                                               csn_cmp_false_false_false_46 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.2.csn_cmp_inst  |                                               csn_cmp_false_false_false_48 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.3.csn_cmp_inst  |                                               csn_cmp_false_false_false_50 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.4.csn_cmp_inst  |                                               csn_cmp_false_false_false_53 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.5.csn_cmp_inst  |                                               csn_cmp_false_false_false_56 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.6.csn_cmp_inst  |                                               csn_cmp_false_false_false_59 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.7.csn_cmp_inst  |                                               csn_cmp_false_false_false_62 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.8.csn_cmp_inst  |                                               csn_cmp_false_false_false_65 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.9.csn_cmp_inst  |                                               csn_cmp_false_false_false_68 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.0.csn_cmp_inst  |                                               csn_cmp_false_false_false_73 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.1.csn_cmp_inst  |                                               csn_cmp_false_false_false_76 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.2.csn_cmp_inst  |                                               csn_cmp_false_false_false_78 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.3.csn_cmp_inst  |                                               csn_cmp_false_false_false_80 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.4.csn_cmp_inst  |                                               csn_cmp_false_false_false_83 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.5.csn_cmp_inst  |                                               csn_cmp_false_false_false_86 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.6.csn_cmp_inst  |                                               csn_cmp_false_false_false_89 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.7.csn_cmp_inst  |                                               csn_cmp_false_false_false_92 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.8.csn_cmp_inst  |                                               csn_cmp_false_false_false_95 |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.9.csn_cmp_inst  |                                               csn_cmp_false_false_false_98 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.0.csn_cmp_inst  |                                               csn_cmp_false_false_false_17 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.1.csn_cmp_inst  |                                                  csn_cmp_false_false_false |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.2.csn_cmp_inst  |                                              csn_cmp_false_false_false_105 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.3.csn_cmp_inst  |                                                csn_cmp_false_false_false_4 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.4.csn_cmp_inst  |                                               csn_cmp_false_false_false_18 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.5.csn_cmp_inst  |                                                csn_cmp_false_false_false_1 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.6.csn_cmp_inst  |                                                csn_cmp_false_false_false_2 |         24 |         24 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.7.csn_cmp_inst  |                                               csn_cmp_false_false_false_10 |         24 |         24 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.8.csn_cmp_inst  |                                                csn_cmp_false_false_false_7 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.9.csn_cmp_inst  |                                              csn_cmp_false_false_false_113 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.0.csn_cmp_inst  |                                              csn_cmp_false_false_false_109 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.1.csn_cmp_inst  |                                               csn_cmp_false_false_false_26 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.2.csn_cmp_inst  |                                               csn_cmp_false_false_false_29 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.3.csn_cmp_inst  |                                               csn_cmp_false_false_false_32 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.4.csn_cmp_inst  |                                               csn_cmp_false_false_false_35 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.5.csn_cmp_inst  |                                               csn_cmp_false_false_false_38 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.6.csn_cmp_inst  |                                               csn_cmp_false_false_false_41 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.7.csn_cmp_inst  |                                               csn_cmp_false_false_false_43 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.0.csn_cmp_inst  |                                               csn_cmp_false_false_false_52 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.1.csn_cmp_inst  |                                               csn_cmp_false_false_false_55 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.2.csn_cmp_inst  |                                               csn_cmp_false_false_false_58 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.3.csn_cmp_inst  |                                               csn_cmp_false_false_false_61 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.4.csn_cmp_inst  |                                               csn_cmp_false_false_false_64 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.5.csn_cmp_inst  |                                               csn_cmp_false_false_false_67 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.6.csn_cmp_inst  |                                               csn_cmp_false_false_false_70 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.7.csn_cmp_inst  |                                               csn_cmp_false_false_false_72 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.8.csn_cmp_inst  |                                               csn_cmp_false_false_false_75 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   lsfr_1                             |                                                                       lfsr |          1 |          1 |       0 |    0 |  287 |      0 |      0 |    0 |            0 |
|   reducer_1                          |                                                                    reducer |         97 |         97 |       0 |    0 |  383 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_i                    |                                                        shift_reg_tap_286_1 |          0 |          0 |       0 |    0 |  286 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_o                    |                                                       shift_reg_tap_1024_1 |          0 |          0 |       0 |    0 |  286 |      0 |      0 |    0 |            0 |
+--------------------------------------+----------------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
* Note: The sum of lower-level cells may be larger than their parent cells total, due to cross-hierarchy LUT combining


