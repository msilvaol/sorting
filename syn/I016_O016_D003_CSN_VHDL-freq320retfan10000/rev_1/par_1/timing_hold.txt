Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Tue Mar  5 22:15:14 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.021        0.000                      0                  853        0.989        0.000                       0                  1549  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.563}        3.125           320.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.021        0.000                      0                  853        0.989        0.000                       0                  1061  
clk_wrapper                                                                             498.562        0.000                       0                   488  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.021ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        0.989ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.021ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[31]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret_11[1]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.074ns  (logic 0.039ns (52.703%)  route 0.035ns (47.297%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.155ns
    Source Clock Delay      (SCD):    1.774ns
    Clock Pessimism Removal (CPR):    0.375ns
  Clock Net Delay (Source):      1.228ns (routing 0.754ns, distribution 0.474ns)
  Clock Net Delay (Destination): 1.383ns (routing 0.836ns, distribution 0.547ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1060, routed)        1.228     1.774    shift_reg_tap_i/clk_c
    SLICE_X105Y408       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[31]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X105Y408       FDRE (Prop_GFF_SLICEL_C_Q)
                                                      0.039     1.813 r  shift_reg_tap_i/sr_p.sr_1[31]/Q
                         net (fo=1, routed)           0.035     1.848    dut_inst/input_slr[31]
    SLICE_X105Y408       FDRE                                         r  dut_inst/ret_array_1_12.idx_ret_11[1]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1060, routed)        1.383     2.155    dut_inst/clk_c
    SLICE_X105Y408       FDRE                                         r  dut_inst/ret_array_1_12.idx_ret_11[1]/C
                         clock pessimism             -0.375     1.780    
    SLICE_X105Y408       FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.827    dut_inst/ret_array_1_12.idx_ret_11[1]
  -------------------------------------------------------------------
                         required time                         -1.827    
                         arrival time                           1.848    
  -------------------------------------------------------------------
                         slack                                  0.021    

Slack (MET) :             0.021ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[31]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret_11[1]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.074ns  (logic 0.039ns (52.703%)  route 0.035ns (47.297%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.155ns
    Source Clock Delay      (SCD):    1.774ns
    Clock Pessimism Removal (CPR):    0.375ns
  Clock Net Delay (Source):      1.228ns (routing 0.754ns, distribution 0.474ns)
  Clock Net Delay (Destination): 1.383ns (routing 0.836ns, distribution 0.547ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1060, routed)        1.228     1.774    shift_reg_tap_i/clk_c
    SLICE_X105Y408       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[31]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X105Y408       FDRE (Prop_GFF_SLICEL_C_Q)
                                                      0.039     1.813 f  shift_reg_tap_i/sr_p.sr_1[31]/Q
                         net (fo=1, routed)           0.035     1.848    dut_inst/input_slr[31]
    SLICE_X105Y408       FDRE                                         f  dut_inst/ret_array_1_12.idx_ret_11[1]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1060, routed)        1.383     2.155    dut_inst/clk_c
    SLICE_X105Y408       FDRE                                         r  dut_inst/ret_array_1_12.idx_ret_11[1]/C
                         clock pessimism             -0.375     1.780    
    SLICE_X105Y408       FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.827    dut_inst/ret_array_1_12.idx_ret_11[1]
  -------------------------------------------------------------------
                         required time                         -1.827    
                         arrival time                           1.848    
  -------------------------------------------------------------------
                         slack                                  0.021    

Slack (MET) :             0.024ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_2_12.idx_ret_8[1]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            dut_inst/ret_array_3_12.idx_ret_2[10]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.077ns  (logic 0.039ns (50.649%)  route 0.038ns (49.351%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.143ns
    Source Clock Delay      (SCD):    1.764ns
    Clock Pessimism Removal (CPR):    0.373ns
  Clock Net Delay (Source):      1.218ns (routing 0.754ns, distribution 0.464ns)
  Clock Net Delay (Destination): 1.371ns (routing 0.836ns, distribution 0.535ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1060, routed)        1.218     1.764    dut_inst/clk_c
    SLICE_X98Y403        FDRE                                         r  dut_inst/ret_array_2_12.idx_ret_8[1]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y403        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.803 r  dut_inst/ret_array_2_12.idx_ret_8[1]/Q
                         net (fo=2, routed)           0.038     1.841    dut_inst/muon_i_o_o[115]
    SLICE_X98Y403        FDRE                                         r  dut_inst/ret_array_3_12.idx_ret_2[10]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1060, routed)        1.371     2.143    dut_inst/clk_c
    SLICE_X98Y403        FDRE                                         r  dut_inst/ret_array_3_12.idx_ret_2[10]/C
                         clock pessimism             -0.373     1.770    
    SLICE_X98Y403        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.817    dut_inst/ret_array_3_12.idx_ret_2[10]
  -------------------------------------------------------------------
                         required time                         -1.817    
                         arrival time                           1.841    
  -------------------------------------------------------------------
                         slack                                  0.024    

Slack (MET) :             0.024ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_2_12.idx_ret_8[1]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            dut_inst/ret_array_3_12.idx_ret_2[10]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.077ns  (logic 0.039ns (50.649%)  route 0.038ns (49.351%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.143ns
    Source Clock Delay      (SCD):    1.764ns
    Clock Pessimism Removal (CPR):    0.373ns
  Clock Net Delay (Source):      1.218ns (routing 0.754ns, distribution 0.464ns)
  Clock Net Delay (Destination): 1.371ns (routing 0.836ns, distribution 0.535ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1060, routed)        1.218     1.764    dut_inst/clk_c
    SLICE_X98Y403        FDRE                                         r  dut_inst/ret_array_2_12.idx_ret_8[1]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y403        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.803 f  dut_inst/ret_array_2_12.idx_ret_8[1]/Q
                         net (fo=2, routed)           0.038     1.841    dut_inst/muon_i_o_o[115]
    SLICE_X98Y403        FDRE                                         f  dut_inst/ret_array_3_12.idx_ret_2[10]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1060, routed)        1.371     2.143    dut_inst/clk_c
    SLICE_X98Y403        FDRE                                         r  dut_inst/ret_array_3_12.idx_ret_2[10]/C
                         clock pessimism             -0.373     1.770    
    SLICE_X98Y403        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.817    dut_inst/ret_array_3_12.idx_ret_2[10]
  -------------------------------------------------------------------
                         required time                         -1.817    
                         arrival time                           1.841    
  -------------------------------------------------------------------
                         slack                                  0.024    

Slack (MET) :             0.026ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_2_15.pt_ret_3[3]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            dut_inst/ret_array_3_15.pt_ret_3[3]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.078ns  (logic 0.039ns (50.000%)  route 0.039ns (50.000%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.144ns
    Source Clock Delay      (SCD):    1.764ns
    Clock Pessimism Removal (CPR):    0.374ns
  Clock Net Delay (Source):      1.218ns (routing 0.754ns, distribution 0.464ns)
  Clock Net Delay (Destination): 1.372ns (routing 0.836ns, distribution 0.536ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1060, routed)        1.218     1.764    dut_inst/clk_c
    SLICE_X101Y393       FDRE                                         r  dut_inst/ret_array_2_15.pt_ret_3[3]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y393       FDRE (Prop_BFF_SLICEL_C_Q)
                                                      0.039     1.803 r  dut_inst/ret_array_2_15.pt_ret_3[3]/Q
                         net (fo=1, routed)           0.039     1.842    dut_inst/pt_o_o_0[3]
    SLICE_X101Y393       FDRE                                         r  dut_inst/ret_array_3_15.pt_ret_3[3]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=1060, routed)        1.372     2.144    dut_inst/clk_c
    SLICE_X101Y393       FDRE                                         r  dut_inst/ret_array_3_15.pt_ret_3[3]/C
                         clock pessimism             -0.374     1.770    
    SLICE_X101Y393       FDRE (Hold_DFF_SLICEL_C_D)
                                                      0.046     1.816    dut_inst/ret_array_3_15.pt_ret_3[3]
  -------------------------------------------------------------------
                         required time                         -1.816    
                         arrival time                           1.842    
  -------------------------------------------------------------------
                         slack                                  0.026    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.563 }
Period(ns):         3.125
Sources:            { clk }

Check Type        Corner  Lib Pin     Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I    n/a            1.499         3.125       1.626      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.125       1.979      SLICE_X99Y396   dut_inst/ret_array_1_12.idx_ret_0_0_ret_array_1_12.idx_ret_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.125       1.979      SLICE_X104Y409  dut_inst/ret_array_1_12.idx_ret_14_0_ret_array_1_12.idx_ret_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.125       1.979      SLICE_X104Y409  dut_inst/ret_array_1_12.idx_ret_14_10_ret_array_1_12.idx_ret_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.125       1.979      SLICE_X107Y404  dut_inst/ret_array_1_12.idx_ret_14_1_ret_array_1_12.idx_ret_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         1.562       0.989      SLICE_X107Y404  dut_inst/ret_array_1_12.idx_ret_14_1_ret_array_1_12.idx_ret_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         1.562       0.989      SLICE_X107Y407  dut_inst/ret_array_1_12.idx_ret_14_7_ret_array_1_12.idx_ret_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         1.562       0.989      SLICE_X107Y404  dut_inst/ret_array_1_12.idx_ret_14_8_ret_array_1_12.idx_ret_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         1.562       0.989      SLICE_X100Y402  dut_inst/ret_array_1_12.idx_ret_1_10_ret_array_1_12.idx_ret_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         1.562       0.989      SLICE_X100Y406  dut_inst/ret_array_1_12.idx_ret_11_2_ret_array_1_12.idx_ret_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         1.563       0.990      SLICE_X104Y409  dut_inst/ret_array_1_12.idx_ret_14_0_ret_array_1_12.idx_ret_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         1.563       0.990      SLICE_X104Y409  dut_inst/ret_array_1_12.idx_ret_14_10_ret_array_1_12.idx_ret_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         1.563       0.990      SLICE_X104Y407  dut_inst/ret_array_1_12.idx_ret_14_3_ret_array_1_12.idx_ret_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         1.563       0.990      SLICE_X97Y397   dut_inst/ret_array_1_12.idx_ret_0_12_ret_array_1_12.idx_ret_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         1.563       0.990      SLICE_X97Y397   dut_inst/ret_array_1_12.idx_ret_0_14_ret_array_1_12.idx_ret_1/CLK



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X96Y395          lsfr_1/output_vector_1[207]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X106Y402         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X100Y404         lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X97Y396          reducer_1/delay_block[0][156]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X97Y396          reducer_1/delay_block[0][157]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y397          reducer_1/delay_block[0][158]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y395          lsfr_1/output_vector_1[207]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X106Y402         lsfr_1/shiftreg_vector[0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X106Y401         lsfr_1/shiftreg_vector[4]/C



