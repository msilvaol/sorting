Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Sat Feb  9 00:17:47 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.004        0.000                      0                13367       12.225        0.000                       0                 14731  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 12.500}       25.000          40.000          
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.004        0.000                      0                13367       12.225        0.000                       0                 13876  
clk_wrapper                                                                             498.562        0.000                       0                   855  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.004ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack       12.225ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.004ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_7[60]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_o/sr_p.sr_8[60]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.249ns  (logic 0.072ns (28.916%)  route 0.177ns (71.084%))
  Logic Levels:           0  
  Clock Path Skew:        0.192ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.979ns
    Source Clock Delay      (SCD):    2.369ns
    Clock Pessimism Removal (CPR):    0.419ns
  Clock Net Delay (Source):      1.539ns (routing 0.356ns, distribution 1.183ns)
  Clock Net Delay (Destination): 1.838ns (routing 0.389ns, distribution 1.449ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=13875, routed)       1.539     2.369    shift_reg_tap_o/clk_c
    SLICE_X141Y486       FDRE                                         r  shift_reg_tap_o/sr_p.sr_7[60]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X141Y486       FDRE (Prop_FFF2_SLICEM_C_Q)
                                                      0.072     2.441 r  shift_reg_tap_o/sr_p.sr_7[60]/Q
                         net (fo=1, routed)           0.177     2.618    shift_reg_tap_o/sr_7[60]
    SLICE_X145Y485       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[60]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=13875, routed)       1.838     2.979    shift_reg_tap_o/clk_c
    SLICE_X145Y485       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[60]/C
                         clock pessimism             -0.419     2.560    
    SLICE_X145Y485       FDRE (Hold_GFF_SLICEL_C_D)
                                                      0.053     2.613    shift_reg_tap_o/sr_p.sr_8[60]
  -------------------------------------------------------------------
                         required time                         -2.613    
                         arrival time                           2.618    
  -------------------------------------------------------------------
                         slack                                  0.004    

Slack (MET) :             0.004ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_7[60]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_o/sr_p.sr_8[60]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.249ns  (logic 0.072ns (28.916%)  route 0.177ns (71.084%))
  Logic Levels:           0  
  Clock Path Skew:        0.192ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.979ns
    Source Clock Delay      (SCD):    2.369ns
    Clock Pessimism Removal (CPR):    0.419ns
  Clock Net Delay (Source):      1.539ns (routing 0.356ns, distribution 1.183ns)
  Clock Net Delay (Destination): 1.838ns (routing 0.389ns, distribution 1.449ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=13875, routed)       1.539     2.369    shift_reg_tap_o/clk_c
    SLICE_X141Y486       FDRE                                         r  shift_reg_tap_o/sr_p.sr_7[60]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X141Y486       FDRE (Prop_FFF2_SLICEM_C_Q)
                                                      0.072     2.441 f  shift_reg_tap_o/sr_p.sr_7[60]/Q
                         net (fo=1, routed)           0.177     2.618    shift_reg_tap_o/sr_7[60]
    SLICE_X145Y485       FDRE                                         f  shift_reg_tap_o/sr_p.sr_8[60]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=13875, routed)       1.838     2.979    shift_reg_tap_o/clk_c
    SLICE_X145Y485       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[60]/C
                         clock pessimism             -0.419     2.560    
    SLICE_X145Y485       FDRE (Hold_GFF_SLICEL_C_D)
                                                      0.053     2.613    shift_reg_tap_o/sr_p.sr_8[60]
  -------------------------------------------------------------------
                         required time                         -2.613    
                         arrival time                           2.618    
  -------------------------------------------------------------------
                         slack                                  0.004    

Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_7[56]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_o/sr_p.sr_8[56]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.252ns  (logic 0.072ns (28.571%)  route 0.180ns (71.429%))
  Logic Levels:           0  
  Clock Path Skew:        0.187ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.974ns
    Source Clock Delay      (SCD):    2.369ns
    Clock Pessimism Removal (CPR):    0.419ns
  Clock Net Delay (Source):      1.539ns (routing 0.356ns, distribution 1.183ns)
  Clock Net Delay (Destination): 1.833ns (routing 0.389ns, distribution 1.444ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=13875, routed)       1.539     2.369    shift_reg_tap_o/clk_c
    SLICE_X141Y485       FDRE                                         r  shift_reg_tap_o/sr_p.sr_7[56]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X141Y485       FDRE (Prop_GFF2_SLICEM_C_Q)
                                                      0.072     2.441 r  shift_reg_tap_o/sr_p.sr_7[56]/Q
                         net (fo=1, routed)           0.180     2.621    shift_reg_tap_o/sr_7[56]
    SLICE_X145Y485       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[56]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=13875, routed)       1.833     2.974    shift_reg_tap_o/clk_c
    SLICE_X145Y485       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[56]/C
                         clock pessimism             -0.419     2.555    
    SLICE_X145Y485       FDRE (Hold_BFF_SLICEL_C_D)
                                                      0.053     2.608    shift_reg_tap_o/sr_p.sr_8[56]
  -------------------------------------------------------------------
                         required time                         -2.608    
                         arrival time                           2.621    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_7[56]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_o/sr_p.sr_8[56]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.252ns  (logic 0.072ns (28.571%)  route 0.180ns (71.429%))
  Logic Levels:           0  
  Clock Path Skew:        0.187ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.974ns
    Source Clock Delay      (SCD):    2.369ns
    Clock Pessimism Removal (CPR):    0.419ns
  Clock Net Delay (Source):      1.539ns (routing 0.356ns, distribution 1.183ns)
  Clock Net Delay (Destination): 1.833ns (routing 0.389ns, distribution 1.444ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=13875, routed)       1.539     2.369    shift_reg_tap_o/clk_c
    SLICE_X141Y485       FDRE                                         r  shift_reg_tap_o/sr_p.sr_7[56]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X141Y485       FDRE (Prop_GFF2_SLICEM_C_Q)
                                                      0.072     2.441 f  shift_reg_tap_o/sr_p.sr_7[56]/Q
                         net (fo=1, routed)           0.180     2.621    shift_reg_tap_o/sr_7[56]
    SLICE_X145Y485       FDRE                                         f  shift_reg_tap_o/sr_p.sr_8[56]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=13875, routed)       1.833     2.974    shift_reg_tap_o/clk_c
    SLICE_X145Y485       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[56]/C
                         clock pessimism             -0.419     2.555    
    SLICE_X145Y485       FDRE (Hold_BFF_SLICEL_C_D)
                                                      0.053     2.608    shift_reg_tap_o/sr_p.sr_8[56]
  -------------------------------------------------------------------
                         required time                         -2.608    
                         arrival time                           2.621    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.016ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_3[7]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_o/sr_p.sr_4[7]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.272ns  (logic 0.072ns (26.471%)  route 0.200ns (73.529%))
  Logic Levels:           0  
  Clock Path Skew:        0.201ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.981ns
    Source Clock Delay      (SCD):    2.361ns
    Clock Pessimism Removal (CPR):    0.419ns
  Clock Net Delay (Source):      1.531ns (routing 0.356ns, distribution 1.175ns)
  Clock Net Delay (Destination): 1.840ns (routing 0.389ns, distribution 1.451ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=13875, routed)       1.531     2.361    shift_reg_tap_o/clk_c
    SLICE_X141Y534       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[7]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X141Y534       FDRE (Prop_FFF2_SLICEM_C_Q)
                                                      0.072     2.433 r  shift_reg_tap_o/sr_p.sr_3[7]/Q
                         net (fo=1, routed)           0.200     2.633    shift_reg_tap_o/sr_3[7]
    SLICE_X142Y529       FDRE                                         r  shift_reg_tap_o/sr_p.sr_4[7]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=13875, routed)       1.840     2.981    shift_reg_tap_o/clk_c
    SLICE_X142Y529       FDRE                                         r  shift_reg_tap_o/sr_p.sr_4[7]/C
                         clock pessimism             -0.419     2.562    
    SLICE_X142Y529       FDRE (Hold_HFF2_SLICEM_C_D)
                                                      0.055     2.617    shift_reg_tap_o/sr_p.sr_4[7]
  -------------------------------------------------------------------
                         required time                         -2.617    
                         arrival time                           2.633    
  -------------------------------------------------------------------
                         slack                                  0.016    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 12.500 }
Period(ns):         25.000
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         25.000      23.501     BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X141Y486  muon_sorter_1/sr_p.sr_1_15.pt_ret_45/C
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X112Y489  muon_sorter_1/sr_p.sr_1_15.pt_ret_452/C
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X130Y480  muon_sorter_1/sr_p.sr_1_15.pt_ret_457/C
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X131Y492  muon_sorter_1/sr_p.sr_1_15.pt_ret_458/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X148Y500  shift_reg_tap_i/sr_p.sr_5[492]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X150Y525  shift_reg_tap_i/sr_p.sr_5[495]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X156Y543  shift_reg_tap_i/sr_p.sr_9[437]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X153Y531  shift_reg_tap_i/sr_p.sr_9[43]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X117Y480  muon_sorter_1/sr_p.sr_1_15.pt_ret_472/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X141Y486  muon_sorter_1/sr_p.sr_1_15.pt_ret_45/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X112Y489  muon_sorter_1/sr_p.sr_1_15.pt_ret_452/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X131Y492  muon_sorter_1/sr_p.sr_1_15.pt_ret_458/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X141Y486  muon_sorter_1/sr_p.sr_1_15.pt_ret_46/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X129Y478  muon_sorter_1/sr_p.sr_1_15.pt_ret_463/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X103Y536         lsfr_1/output_vector_1[511]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X101Y542         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X139Y538         lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X153Y520         lsfr_1/shiftreg_vector[105]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X149Y504         lsfr_1/shiftreg_vector[114]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X153Y520         lsfr_1/shiftreg_vector[122]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y536         lsfr_1/output_vector_1[511]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X139Y538         lsfr_1/shiftreg_vector[100]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X144Y540         lsfr_1/shiftreg_vector[101]/C



