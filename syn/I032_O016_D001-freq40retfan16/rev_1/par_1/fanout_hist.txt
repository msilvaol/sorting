Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Sat Feb  9 00:22:39 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+-------+--------+
|  Fanout |  Nets |      % |
+---------+-------+--------+
|       1 | 21427 |  57.31 |
|       2 |  3962 |  10.59 |
|       3 |  2741 |   7.33 |
|       4 |  1262 |   3.37 |
|    5-10 |  3786 |  10.12 |
|   11-50 |  4119 |  11.01 |
|  51-100 |    87 |   0.23 |
| 101-500 |     0 |   0.00 |
|    >500 |     0 |   0.00 |
+---------+-------+--------+
|     ALL | 37384 | 100.00 |
+---------+-------+--------+


