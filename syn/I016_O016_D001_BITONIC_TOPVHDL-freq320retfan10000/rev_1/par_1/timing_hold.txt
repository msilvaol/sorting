Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Mon Mar  4 16:10:39 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.032        0.000                      0                  538        1.287        0.000                       0                  1269  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.563}        3.125           320.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.032        0.000                      0                  538        1.287        0.000                       0                   781  
clk_wrapper                                                                             498.562        0.000                       0                   488  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.032ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.287ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.032ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[181]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/qdelay[1][15].idx_ret_1/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.103ns  (logic 0.052ns (50.485%)  route 0.051ns (49.514%))
  Logic Levels:           1  (LUT3=1)
  Clock Path Skew:        0.025ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.897ns
    Source Clock Delay      (SCD):    1.537ns
    Clock Pessimism Removal (CPR):    0.335ns
  Clock Net Delay (Source):      0.991ns (routing 0.522ns, distribution 0.469ns)
  Clock Net Delay (Destination): 1.125ns (routing 0.578ns, distribution 0.547ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=780, routed)         0.991     1.537    shift_reg_tap_i/clk_c
    SLICE_X102Y506       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[181]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X102Y506       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.038     1.575 r  shift_reg_tap_i/sr_p.sr_1[181]/Q
                         net (fo=3, routed)           0.033     1.608    dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/input_slr_25
    SLICE_X102Y507       LUT3 (Prop_A6LUT_SLICEM_I1_O)
                                                      0.014     1.622 r  dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/qdelay[1][15].idx_ret_1_RNO/O
                         net (fo=1, routed)           0.018     1.640    dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/idx_ret_1_RNO_4
    SLICE_X102Y507       FDRE                                         r  dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/qdelay[1][15].idx_ret_1/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=780, routed)         1.125     1.897    dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/clk_c
    SLICE_X102Y507       FDRE                                         r  dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/qdelay[1][15].idx_ret_1/C
                         clock pessimism             -0.335     1.562    
    SLICE_X102Y507       FDRE (Hold_AFF_SLICEM_C_D)
                                                      0.046     1.608    dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/qdelay[1][15].idx_ret_1
  -------------------------------------------------------------------
                         required time                         -1.608    
                         arrival time                           1.640    
  -------------------------------------------------------------------
                         slack                                  0.032    

Slack (MET) :             0.032ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[181]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/qdelay[1][15].idx_ret_1/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.103ns  (logic 0.052ns (50.485%)  route 0.051ns (49.514%))
  Logic Levels:           1  (LUT3=1)
  Clock Path Skew:        0.025ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.897ns
    Source Clock Delay      (SCD):    1.537ns
    Clock Pessimism Removal (CPR):    0.335ns
  Clock Net Delay (Source):      0.991ns (routing 0.522ns, distribution 0.469ns)
  Clock Net Delay (Destination): 1.125ns (routing 0.578ns, distribution 0.547ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=780, routed)         0.991     1.537    shift_reg_tap_i/clk_c
    SLICE_X102Y506       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[181]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X102Y506       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.038     1.575 f  shift_reg_tap_i/sr_p.sr_1[181]/Q
                         net (fo=3, routed)           0.033     1.608    dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/input_slr_25
    SLICE_X102Y507       LUT3 (Prop_A6LUT_SLICEM_I1_O)
                                                      0.014     1.622 f  dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/qdelay[1][15].idx_ret_1_RNO/O
                         net (fo=1, routed)           0.018     1.640    dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/idx_ret_1_RNO_4
    SLICE_X102Y507       FDRE                                         f  dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/qdelay[1][15].idx_ret_1/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=780, routed)         1.125     1.897    dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/clk_c
    SLICE_X102Y507       FDRE                                         r  dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/qdelay[1][15].idx_ret_1/C
                         clock pessimism             -0.335     1.562    
    SLICE_X102Y507       FDRE (Hold_AFF_SLICEM_C_D)
                                                      0.046     1.608    dut_inst/sorter_inst/genblk1.bitonic_sort_high/genblk1.bitonic_sort_high/genblk1.bitonic_sort_low/genblk1.bitonic_merge_i/genblk1.cmp_g[0].compare_i/qdelay[1][15].idx_ret_1
  -------------------------------------------------------------------
                         required time                         -1.608    
                         arrival time                           1.640    
  -------------------------------------------------------------------
                         slack                                  0.032    

Slack (MET) :             0.040ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[165]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            dut_inst/qdelay[1][15].idx_ret_0[5]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.092ns  (logic 0.039ns (42.391%)  route 0.053ns (57.609%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.886ns
    Source Clock Delay      (SCD):    1.531ns
    Clock Pessimism Removal (CPR):    0.349ns
  Clock Net Delay (Source):      0.985ns (routing 0.522ns, distribution 0.463ns)
  Clock Net Delay (Destination): 1.114ns (routing 0.578ns, distribution 0.536ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=780, routed)         0.985     1.531    shift_reg_tap_i/clk_c
    SLICE_X104Y506       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[165]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X104Y506       FDRE (Prop_GFF_SLICEM_C_Q)
                                                      0.039     1.570 r  shift_reg_tap_i/sr_p.sr_1[165]/Q
                         net (fo=1, routed)           0.053     1.623    dut_inst/input_slr[165]
    SLICE_X104Y506       FDRE                                         r  dut_inst/qdelay[1][15].idx_ret_0[5]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=780, routed)         1.114     1.886    dut_inst/clk_c
    SLICE_X104Y506       FDRE                                         r  dut_inst/qdelay[1][15].idx_ret_0[5]/C
                         clock pessimism             -0.349     1.537    
    SLICE_X104Y506       FDRE (Hold_FFF_SLICEM_C_D)
                                                      0.046     1.583    dut_inst/qdelay[1][15].idx_ret_0[5]
  -------------------------------------------------------------------
                         required time                         -1.583    
                         arrival time                           1.623    
  -------------------------------------------------------------------
                         slack                                  0.040    

Slack (MET) :             0.040ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[165]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            dut_inst/qdelay[1][15].idx_ret_0[5]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.092ns  (logic 0.039ns (42.391%)  route 0.053ns (57.609%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.886ns
    Source Clock Delay      (SCD):    1.531ns
    Clock Pessimism Removal (CPR):    0.349ns
  Clock Net Delay (Source):      0.985ns (routing 0.522ns, distribution 0.463ns)
  Clock Net Delay (Destination): 1.114ns (routing 0.578ns, distribution 0.536ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=780, routed)         0.985     1.531    shift_reg_tap_i/clk_c
    SLICE_X104Y506       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[165]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X104Y506       FDRE (Prop_GFF_SLICEM_C_Q)
                                                      0.039     1.570 f  shift_reg_tap_i/sr_p.sr_1[165]/Q
                         net (fo=1, routed)           0.053     1.623    dut_inst/input_slr[165]
    SLICE_X104Y506       FDRE                                         f  dut_inst/qdelay[1][15].idx_ret_0[5]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=780, routed)         1.114     1.886    dut_inst/clk_c
    SLICE_X104Y506       FDRE                                         r  dut_inst/qdelay[1][15].idx_ret_0[5]/C
                         clock pessimism             -0.349     1.537    
    SLICE_X104Y506       FDRE (Hold_FFF_SLICEM_C_D)
                                                      0.046     1.583    dut_inst/qdelay[1][15].idx_ret_0[5]
  -------------------------------------------------------------------
                         required time                         -1.583    
                         arrival time                           1.623    
  -------------------------------------------------------------------
                         slack                                  0.040    

Slack (MET) :             0.042ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[18]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            dut_inst/qdelay[1][15].idx_ret_10[1]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.108ns  (logic 0.039ns (36.111%)  route 0.069ns (63.889%))
  Logic Levels:           0  
  Clock Path Skew:        0.020ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.869ns
    Source Clock Delay      (SCD):    1.515ns
    Clock Pessimism Removal (CPR):    0.334ns
  Clock Net Delay (Source):      0.969ns (routing 0.522ns, distribution 0.447ns)
  Clock Net Delay (Destination): 1.097ns (routing 0.578ns, distribution 0.519ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=780, routed)         0.969     1.515    shift_reg_tap_i/clk_c
    SLICE_X94Y515        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[18]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X94Y515        FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.039     1.554 r  shift_reg_tap_i/sr_p.sr_1[18]/Q
                         net (fo=2, routed)           0.069     1.623    dut_inst/input_slr[18]
    SLICE_X94Y514        FDRE                                         r  dut_inst/qdelay[1][15].idx_ret_10[1]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=780, routed)         1.097     1.869    dut_inst/clk_c
    SLICE_X94Y514        FDRE                                         r  dut_inst/qdelay[1][15].idx_ret_10[1]/C
                         clock pessimism             -0.334     1.535    
    SLICE_X94Y514        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.046     1.581    dut_inst/qdelay[1][15].idx_ret_10[1]
  -------------------------------------------------------------------
                         required time                         -1.581    
                         arrival time                           1.623    
  -------------------------------------------------------------------
                         slack                                  0.042    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.563 }
Period(ns):         3.125
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         3.125       1.626      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X104Y507  dut_inst/qdelay[1][15].idx_ret[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X100Y510  dut_inst/qdelay[1][15].idx_ret[10]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X100Y508  dut_inst/qdelay[1][15].idx_ret[12]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X100Y508  dut_inst/qdelay[1][15].idx_ret[14]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X100Y508  dut_inst/qdelay[1][15].idx_ret[12]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X100Y508  dut_inst/qdelay[1][15].idx_ret[14]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X100Y508  dut_inst/qdelay[1][15].idx_ret[3]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X96Y512   dut_inst/qdelay[1][15].idx_ret_61[3]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X103Y508  dut_inst/qdelay[1][15].idx_ret_622/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X100Y515  dut_inst/qdelay[1][15].idx_ret_62[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X98Y513   dut_inst/qdelay[1][15].idx_ret_63[3]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X102Y513  dut_inst/qdelay[1][15].idx_ret_64[1]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X98Y513   dut_inst/qdelay[1][15].idx_ret_65[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X98Y514   dut_inst/qdelay[1][15].idx_ret_66[0]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X103Y510         lsfr_1/shiftreg_vector[111]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X104Y510         lsfr_1/shiftreg_vector[112]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X104Y510         lsfr_1/shiftreg_vector[113]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y510         lsfr_1/shiftreg_vector[111]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X97Y518          lsfr_1/shiftreg_vector[77]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X93Y504          reducer_1/delay_block[1][4]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y510         lsfr_1/shiftreg_vector[111]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y516          lsfr_1/shiftreg_vector[11]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y516         lsfr_1/shiftreg_vector[122]/C



