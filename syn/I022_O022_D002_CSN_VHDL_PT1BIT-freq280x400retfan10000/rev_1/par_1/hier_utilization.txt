Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Mon Mar 11 18:47:54 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_utilization -hierarchical -file hier_utilization.txt
| Design       : wrapper_csn
| Device       : xcvu9pflgc2104-1
| Design State : Routed
------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+--------------------------------------+-------------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
|               Instance               |                                  Module                                 | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+--------------------------------------+-------------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
| wrapper_csn                          |                                                                   (top) |       3470 |       3470 |       0 |    0 | 1968 |      0 |      0 |    0 |            0 |
|   (wrapper_csn)                      |                                                                   (top) |          0 |          0 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   dut_inst                           | muon_sorter_I022_O022_D002_CSN_VHDL_PT1BIT-freq280x400retfan10000_rev_1 |       3395 |       3395 |       0 |    0 | 1014 |      0 |      0 |    0 |            0 |
|     (dut_inst)                       | muon_sorter_I022_O022_D002_CSN_VHDL_PT1BIT-freq280x400retfan10000_rev_1 |        139 |        139 |       0 |    0 |  425 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.0.csn_cmp_inst  |                                           csn_cmp_false_false_false_113 |          6 |          6 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.1.csn_cmp_inst  |                                            csn_cmp_false_false_false_27 |          8 |          8 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.10.csn_cmp_inst |                                            csn_cmp_false_false_false_54 |         11 |         11 |       0 |    0 |   10 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.2.csn_cmp_inst  |                                            csn_cmp_false_false_false_24 |          7 |          7 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.3.csn_cmp_inst  |                                           csn_cmp_false_false_false_114 |          2 |          2 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.4.csn_cmp_inst  |                                            csn_cmp_false_false_false_37 |          9 |          9 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.5.csn_cmp_inst  |                                            csn_cmp_false_false_false_40 |          4 |          4 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.6.csn_cmp_inst  |                                            csn_cmp_false_false_false_43 |          9 |          9 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.7.csn_cmp_inst  |                                            csn_cmp_false_false_false_45 |          4 |          4 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.8.csn_cmp_inst  |                                            csn_cmp_false_false_false_48 |         11 |         11 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.9.csn_cmp_inst  |                                            csn_cmp_false_false_false_51 |          9 |          9 |       0 |    0 |   10 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.0.csn_cmp_inst  |                                            csn_cmp_false_false_false_57 |         12 |         12 |       0 |    0 |   17 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.1.csn_cmp_inst  |                                            csn_cmp_false_false_false_60 |         13 |         13 |       0 |    0 |   16 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.10.csn_cmp_inst |                                            csn_cmp_false_false_false_83 |         12 |         12 |       0 |    0 |   14 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.2.csn_cmp_inst  |                                            csn_cmp_false_false_false_63 |         13 |         13 |       0 |    0 |   16 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.3.csn_cmp_inst  |                                            csn_cmp_false_false_false_65 |         13 |         13 |       0 |    0 |   17 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.4.csn_cmp_inst  |                                            csn_cmp_false_false_false_68 |         12 |         12 |       0 |    0 |   17 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.5.csn_cmp_inst  |                                            csn_cmp_false_false_false_70 |         10 |         10 |       0 |    0 |   17 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.6.csn_cmp_inst  |                                            csn_cmp_false_false_false_72 |         11 |         11 |       0 |    0 |   13 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.7.csn_cmp_inst  |                                            csn_cmp_false_false_false_74 |         11 |         11 |       0 |    0 |   15 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.8.csn_cmp_inst  |                                            csn_cmp_false_false_false_77 |         11 |         11 |       0 |    0 |   16 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.9.csn_cmp_inst  |                                            csn_cmp_false_false_false_80 |         11 |         11 |       0 |    0 |   14 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.0.csn_cmp_inst |                                           csn_cmp_false_false_false_104 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.1.csn_cmp_inst |                                            csn_cmp_false_false_false_15 |          3 |          3 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.2.csn_cmp_inst |                                             csn_cmp_false_false_false_4 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.3.csn_cmp_inst |                                            csn_cmp_false_false_false_19 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.4.csn_cmp_inst |                                            csn_cmp_false_false_false_26 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.5.csn_cmp_inst |                                            csn_cmp_false_false_false_12 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.6.csn_cmp_inst |                                             csn_cmp_false_false_false_0 |          2 |          2 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.7.csn_cmp_inst |                                           csn_cmp_false_false_false_109 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.0.csn_cmp_inst |                                            csn_cmp_false_false_false_29 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.1.csn_cmp_inst |                                            csn_cmp_false_false_false_34 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.2.csn_cmp_inst |                                            csn_cmp_false_false_false_33 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.3.csn_cmp_inst |                                            csn_cmp_false_false_false_32 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.4.csn_cmp_inst |                                            csn_cmp_false_false_false_35 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.5.csn_cmp_inst |                                            csn_cmp_false_false_false_31 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.6.csn_cmp_inst |                                            csn_cmp_false_false_false_30 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.0.csn_cmp_inst  |                                            csn_cmp_false_false_false_86 |         22 |         22 |       0 |    0 |   10 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.1.csn_cmp_inst  |                                            csn_cmp_false_false_false_89 |         18 |         18 |       0 |    0 |   11 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.10.csn_cmp_inst |                                            csn_cmp_false_false_false_18 |         19 |         19 |       0 |    0 |   11 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.2.csn_cmp_inst  |                                            csn_cmp_false_false_false_92 |         19 |         19 |       0 |    0 |   20 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.3.csn_cmp_inst  |                                            csn_cmp_false_false_false_94 |         21 |         21 |       0 |    0 |   19 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.4.csn_cmp_inst  |                                            csn_cmp_false_false_false_97 |         22 |         22 |       0 |    0 |   12 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.5.csn_cmp_inst  |                                           csn_cmp_false_false_false_100 |         20 |         20 |       0 |    0 |   14 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.6.csn_cmp_inst  |                                           csn_cmp_false_false_false_102 |         20 |         20 |       0 |    0 |   15 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.7.csn_cmp_inst  |                                           csn_cmp_false_false_false_108 |         22 |         22 |       0 |    0 |   18 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.8.csn_cmp_inst  |                                            csn_cmp_false_false_false_28 |         19 |         19 |       0 |    0 |   15 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.9.csn_cmp_inst  |                                           csn_cmp_false_false_false_105 |         19 |         19 |       0 |    0 |   16 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.0.csn_cmp_inst  |                                           csn_cmp_false_false_false_103 |         27 |         27 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.1.csn_cmp_inst  |                                            csn_cmp_false_false_false_11 |         30 |         30 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.10.csn_cmp_inst |                                             csn_cmp_false_false_false_3 |         29 |         29 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.2.csn_cmp_inst  |                                            csn_cmp_false_false_false_14 |         29 |         29 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.3.csn_cmp_inst  |                                             csn_cmp_false_false_false_6 |         45 |         45 |       0 |    0 |   16 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.4.csn_cmp_inst  |                                             csn_cmp_false_false_false_5 |         34 |         34 |       0 |    0 |   10 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.5.csn_cmp_inst  |                                            csn_cmp_false_false_false_25 |         12 |         12 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.6.csn_cmp_inst  |                                            csn_cmp_false_false_false_16 |         25 |         25 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.7.csn_cmp_inst  |                                           csn_cmp_false_false_false_107 |         22 |         22 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.8.csn_cmp_inst  |                                             csn_cmp_false_false_false_2 |         28 |         28 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.9.csn_cmp_inst  |                                           csn_cmp_false_false_false_111 |         27 |         27 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.0.csn_cmp_inst  |                                            csn_cmp_false_false_false_36 |         11 |         11 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.1.csn_cmp_inst  |                                            csn_cmp_false_false_false_39 |         17 |         17 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.2.csn_cmp_inst  |                                            csn_cmp_false_false_false_42 |          3 |          3 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.3.csn_cmp_inst  |                                            csn_cmp_false_false_false_44 |          6 |          6 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.4.csn_cmp_inst  |                                            csn_cmp_false_false_false_47 |          1 |          1 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.5.csn_cmp_inst  |                                            csn_cmp_false_false_false_50 |         12 |         12 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.6.csn_cmp_inst  |                                            csn_cmp_false_false_false_53 |         12 |         12 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.7.csn_cmp_inst  |                                            csn_cmp_false_false_false_56 |          5 |          5 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.8.csn_cmp_inst  |                                            csn_cmp_false_false_false_59 |          6 |          6 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.9.csn_cmp_inst  |                                            csn_cmp_false_false_false_62 |          4 |          4 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.0.csn_cmp_inst  |                                            csn_cmp_false_false_false_67 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.1.csn_cmp_inst  |                                            csn_cmp_false_false_false_69 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.2.csn_cmp_inst  |                                            csn_cmp_false_false_false_71 |          3 |          3 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.4.csn_cmp_inst  |                                            csn_cmp_false_false_false_76 |          2 |          2 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.5.csn_cmp_inst  |                                            csn_cmp_false_false_false_79 |          1 |          1 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.6.csn_cmp_inst  |                                            csn_cmp_false_false_false_82 |          4 |          4 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.7.csn_cmp_inst  |                                            csn_cmp_false_false_false_85 |          1 |          1 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.8.csn_cmp_inst  |                                            csn_cmp_false_false_false_88 |          1 |          1 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.9.csn_cmp_inst  |                                            csn_cmp_false_false_false_91 |          1 |          1 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.0.csn_cmp_inst  |                                            csn_cmp_false_false_false_96 |         24 |         24 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.1.csn_cmp_inst  |                                            csn_cmp_false_false_false_99 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.2.csn_cmp_inst  |                                           csn_cmp_false_false_false_101 |         86 |         86 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.5.csn_cmp_inst  |                                           csn_cmp_false_false_false_106 |          3 |          3 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.7.csn_cmp_inst  |                                            csn_cmp_false_false_false_20 |        100 |        100 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.8.csn_cmp_inst  |                                            csn_cmp_false_false_false_10 |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.9.csn_cmp_inst  |                                            csn_cmp_false_false_false_13 |         26 |         26 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.0.csn_cmp_inst  |                                             csn_cmp_false_false_false_7 |        102 |        102 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.1.csn_cmp_inst  |                                           csn_cmp_false_false_false_110 |        156 |        156 |       0 |    0 |   12 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.2.csn_cmp_inst  |                                             csn_cmp_false_false_false_9 |        182 |        182 |       0 |    0 |   16 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.3.csn_cmp_inst  |                                            csn_cmp_false_false_false_23 |        201 |        201 |       0 |    0 |    9 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.4.csn_cmp_inst  |                                           csn_cmp_false_false_false_112 |        190 |        190 |       0 |    0 |   16 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.5.csn_cmp_inst  |                                             csn_cmp_false_false_false_1 |        193 |        193 |       0 |    0 |    9 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.6.csn_cmp_inst  |                                             csn_cmp_false_false_false_8 |        207 |        207 |       0 |    0 |    8 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.7.csn_cmp_inst  |                                            csn_cmp_false_false_false_22 |        167 |        167 |       0 |    0 |   14 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.8.csn_cmp_inst  |                                            csn_cmp_false_false_false_38 |        165 |        165 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.9.csn_cmp_inst  |                                            csn_cmp_false_false_false_41 |         95 |         95 |       0 |    0 |    9 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.0.csn_cmp_inst  |                                            csn_cmp_false_false_false_46 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.1.csn_cmp_inst  |                                            csn_cmp_false_false_false_49 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.2.csn_cmp_inst  |                                            csn_cmp_false_false_false_52 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.3.csn_cmp_inst  |                                            csn_cmp_false_false_false_55 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.4.csn_cmp_inst  |                                            csn_cmp_false_false_false_58 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.5.csn_cmp_inst  |                                            csn_cmp_false_false_false_61 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.6.csn_cmp_inst  |                                            csn_cmp_false_false_false_64 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.7.csn_cmp_inst  |                                            csn_cmp_false_false_false_66 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.0.csn_cmp_inst  |                                            csn_cmp_false_false_false_75 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.1.csn_cmp_inst  |                                            csn_cmp_false_false_false_78 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.2.csn_cmp_inst  |                                            csn_cmp_false_false_false_81 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.3.csn_cmp_inst  |                                            csn_cmp_false_false_false_84 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.4.csn_cmp_inst  |                                            csn_cmp_false_false_false_87 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.5.csn_cmp_inst  |                                            csn_cmp_false_false_false_90 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.6.csn_cmp_inst  |                                            csn_cmp_false_false_false_93 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.7.csn_cmp_inst  |                                            csn_cmp_false_false_false_95 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.8.csn_cmp_inst  |                                            csn_cmp_false_false_false_98 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   lsfr_1                             |                                                                    lfsr |          1 |          1 |       0 |    0 |  221 |      0 |      0 |    0 |            0 |
|   reducer_1                          |                                                                 reducer |         74 |         74 |       0 |    0 |  293 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_i                    |                                                     shift_reg_tap_220_1 |          0 |          0 |       0 |    0 |  220 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_o                    |                                                     shift_reg_tap_256_1 |          0 |          0 |       0 |    0 |  220 |      0 |      0 |    0 |            0 |
+--------------------------------------+-------------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
* Note: The sum of lower-level cells may be larger than their parent cells total, due to cross-hierarchy LUT combining


