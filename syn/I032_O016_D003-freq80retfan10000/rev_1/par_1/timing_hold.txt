Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Sun Feb 10 02:51:43 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.010        0.000                      0                14953        5.677        0.000                       0                 16255  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 6.250}        12.500          80.000          
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.010        0.000                      0                14953        5.677        0.000                       0                 15400  
clk_wrapper                                                                             498.562        0.000                       0                   855  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.010ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        5.677ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.010ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_14[395]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_15[395]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.193ns  (logic 0.069ns (35.751%)  route 0.124ns (64.249%))
  Logic Levels:           0  
  Clock Path Skew:        0.128ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.022ns
    Source Clock Delay      (SCD):    3.383ns
    Clock Pessimism Removal (CPR):    0.512ns
  Clock Net Delay (Source):      2.553ns (routing 1.566ns, distribution 0.987ns)
  Clock Net Delay (Destination): 2.881ns (routing 1.714ns, distribution 1.167ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15399, routed)       2.553     3.383    shift_reg_tap_i/clk_c
    SLICE_X77Y374        FDRE                                         r  shift_reg_tap_i/sr_p.sr_14[395]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X77Y374        FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     3.452 r  shift_reg_tap_i/sr_p.sr_14[395]/Q
                         net (fo=1, routed)           0.124     3.576    shift_reg_tap_i/sr_14[395]
    SLICE_X78Y376        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[395]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15399, routed)       2.881     4.022    shift_reg_tap_i/clk_c
    SLICE_X78Y376        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[395]/C
                         clock pessimism             -0.512     3.510    
    SLICE_X78Y376        FDRE (Hold_GFF2_SLICEM_C_D)
                                                      0.055     3.565    shift_reg_tap_i/sr_p.sr_15[395]
  -------------------------------------------------------------------
                         required time                         -3.565    
                         arrival time                           3.576    
  -------------------------------------------------------------------
                         slack                                  0.010    

Slack (MET) :             0.010ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_14[395]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_15[395]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.193ns  (logic 0.069ns (35.751%)  route 0.124ns (64.249%))
  Logic Levels:           0  
  Clock Path Skew:        0.128ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.022ns
    Source Clock Delay      (SCD):    3.383ns
    Clock Pessimism Removal (CPR):    0.512ns
  Clock Net Delay (Source):      2.553ns (routing 1.566ns, distribution 0.987ns)
  Clock Net Delay (Destination): 2.881ns (routing 1.714ns, distribution 1.167ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15399, routed)       2.553     3.383    shift_reg_tap_i/clk_c
    SLICE_X77Y374        FDRE                                         r  shift_reg_tap_i/sr_p.sr_14[395]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X77Y374        FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     3.452 f  shift_reg_tap_i/sr_p.sr_14[395]/Q
                         net (fo=1, routed)           0.124     3.576    shift_reg_tap_i/sr_14[395]
    SLICE_X78Y376        FDRE                                         f  shift_reg_tap_i/sr_p.sr_15[395]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15399, routed)       2.881     4.022    shift_reg_tap_i/clk_c
    SLICE_X78Y376        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[395]/C
                         clock pessimism             -0.512     3.510    
    SLICE_X78Y376        FDRE (Hold_GFF2_SLICEM_C_D)
                                                      0.055     3.565    shift_reg_tap_i/sr_p.sr_15[395]
  -------------------------------------------------------------------
                         required time                         -3.565    
                         arrival time                           3.576    
  -------------------------------------------------------------------
                         slack                                  0.010    

Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_6[16]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_7[16]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.181ns  (logic 0.070ns (38.674%)  route 0.111ns (61.326%))
  Logic Levels:           0  
  Clock Path Skew:        0.114ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.010ns
    Source Clock Delay      (SCD):    3.437ns
    Clock Pessimism Removal (CPR):    0.459ns
  Clock Net Delay (Source):      2.607ns (routing 1.566ns, distribution 1.041ns)
  Clock Net Delay (Destination): 2.869ns (routing 1.714ns, distribution 1.155ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15399, routed)       2.607     3.437    shift_reg_tap_i/clk_c
    SLICE_X82Y421        FDRE                                         r  shift_reg_tap_i/sr_p.sr_6[16]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X82Y421        FDRE (Prop_EFF_SLICEL_C_Q)
                                                      0.070     3.507 r  shift_reg_tap_i/sr_p.sr_6[16]/Q
                         net (fo=1, routed)           0.111     3.618    shift_reg_tap_i/sr_6[16]
    SLICE_X82Y418        FDRE                                         r  shift_reg_tap_i/sr_p.sr_7[16]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15399, routed)       2.869     4.010    shift_reg_tap_i/clk_c
    SLICE_X82Y418        FDRE                                         r  shift_reg_tap_i/sr_p.sr_7[16]/C
                         clock pessimism             -0.459     3.551    
    SLICE_X82Y418        FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.055     3.606    shift_reg_tap_i/sr_p.sr_7[16]
  -------------------------------------------------------------------
                         required time                         -3.606    
                         arrival time                           3.618    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_6[16]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_7[16]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.181ns  (logic 0.070ns (38.674%)  route 0.111ns (61.326%))
  Logic Levels:           0  
  Clock Path Skew:        0.114ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.010ns
    Source Clock Delay      (SCD):    3.437ns
    Clock Pessimism Removal (CPR):    0.459ns
  Clock Net Delay (Source):      2.607ns (routing 1.566ns, distribution 1.041ns)
  Clock Net Delay (Destination): 2.869ns (routing 1.714ns, distribution 1.155ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15399, routed)       2.607     3.437    shift_reg_tap_i/clk_c
    SLICE_X82Y421        FDRE                                         r  shift_reg_tap_i/sr_p.sr_6[16]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X82Y421        FDRE (Prop_EFF_SLICEL_C_Q)
                                                      0.070     3.507 f  shift_reg_tap_i/sr_p.sr_6[16]/Q
                         net (fo=1, routed)           0.111     3.618    shift_reg_tap_i/sr_6[16]
    SLICE_X82Y418        FDRE                                         f  shift_reg_tap_i/sr_p.sr_7[16]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15399, routed)       2.869     4.010    shift_reg_tap_i/clk_c
    SLICE_X82Y418        FDRE                                         r  shift_reg_tap_i/sr_p.sr_7[16]/C
                         clock pessimism             -0.459     3.551    
    SLICE_X82Y418        FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.055     3.606    shift_reg_tap_i/sr_p.sr_7[16]
  -------------------------------------------------------------------
                         required time                         -3.606    
                         arrival time                           3.618    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_9[49]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_10[49]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.321ns  (logic 0.070ns (21.807%)  route 0.251ns (78.193%))
  Logic Levels:           0  
  Clock Path Skew:        0.256ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.145ns
    Source Clock Delay      (SCD):    3.374ns
    Clock Pessimism Removal (CPR):    0.515ns
  Clock Net Delay (Source):      2.544ns (routing 1.566ns, distribution 0.978ns)
  Clock Net Delay (Destination): 3.004ns (routing 1.714ns, distribution 1.290ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15399, routed)       2.544     3.374    shift_reg_tap_i/clk_c
    SLICE_X63Y337        FDRE                                         r  shift_reg_tap_i/sr_p.sr_9[49]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X63Y337        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.070     3.444 r  shift_reg_tap_i/sr_p.sr_9[49]/Q
                         net (fo=1, routed)           0.251     3.695    shift_reg_tap_i/sr_9[49]
    SLICE_X64Y344        FDRE                                         r  shift_reg_tap_i/sr_p.sr_10[49]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15399, routed)       3.004     4.145    shift_reg_tap_i/clk_c
    SLICE_X64Y344        FDRE                                         r  shift_reg_tap_i/sr_p.sr_10[49]/C
                         clock pessimism             -0.515     3.630    
    SLICE_X64Y344        FDRE (Hold_AFF_SLICEM_C_D)
                                                      0.053     3.683    shift_reg_tap_i/sr_p.sr_10[49]
  -------------------------------------------------------------------
                         required time                         -3.683    
                         arrival time                           3.695    
  -------------------------------------------------------------------
                         slack                                  0.012    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 6.250 }
Period(ns):         12.500
Sources:            { clk }

Check Type        Corner  Lib Pin     Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location       Pin
Min Period        n/a     BUFGCE/I    n/a            1.499         12.500      11.001     BUFGCE_X1Y224  clk_ibuf/I
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X81Y419  muon_sorter_1/sr_3_0.pt_1_sr_3_0.pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X84Y416  muon_sorter_1/sr_3_0.pt_2_sr_3_0.pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X81Y425  muon_sorter_1/sr_3_0.pt_sr_3_0.pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X87Y421  muon_sorter_1/sr_3_0.roi_0_sr_3_0.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X84Y380  muon_sorter_1/sr_3_3.roi_6_sr_3_0.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X84Y380  muon_sorter_1/sr_3_5.roi_4_sr_3_0.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X80Y395  muon_sorter_1/sr_3_6.roi_3_sr_3_0.pt_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X84Y423  muon_sorter_1/sr_3_0.roi_2_sr_3_0.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X84Y423  muon_sorter_1/sr_3_0.roi_2_sr_3_0.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X88Y404  muon_sorter_1/sr_3_5.roi_5_sr_3_0.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X81Y419  muon_sorter_1/sr_3_0.pt_1_sr_3_0.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X84Y416  muon_sorter_1/sr_3_0.pt_2_sr_3_0.pt_1/CLK
High Pulse Width  Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X84Y416  muon_sorter_1/sr_3_0.pt_2_sr_3_0.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X81Y425  muon_sorter_1/sr_3_0.pt_sr_3_0.pt_1/CLK



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X93Y450          reducer_1/delay_block[0][55]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X90Y446          reducer_1/delay_block[0][56]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X90Y447          reducer_1/delay_block[0][57]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X97Y425          reducer_1/delay_block[0][68]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X97Y425          reducer_1/delay_block[0][69]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X97Y425          reducer_1/delay_block[0][70]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X90Y447          reducer_1/delay_block[0][57]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X90Y447          reducer_1/delay_block[0][58]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X90Y447          reducer_1/delay_block[0][59]/C



