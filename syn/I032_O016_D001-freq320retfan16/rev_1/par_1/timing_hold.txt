Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Fri Feb  8 14:23:59 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.007        0.000                      0                13864        1.287        0.000                       0                 15223  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.563}        3.125           320.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.007        0.000                      0                13864        1.287        0.000                       0                 14368  
clk_wrapper                                                                             498.562        0.000                       0                   855  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.007ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.287ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.007ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_6[52]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_i/sr_p.sr_7[52]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.262ns  (logic 0.072ns (27.481%)  route 0.190ns (72.519%))
  Logic Levels:           0  
  Clock Path Skew:        0.202ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.805ns
    Source Clock Delay      (SCD):    3.163ns
    Clock Pessimism Removal (CPR):    0.440ns
  Clock Net Delay (Source):      2.333ns (routing 1.362ns, distribution 0.971ns)
  Clock Net Delay (Destination): 2.664ns (routing 1.491ns, distribution 1.173ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y7 (CLOCK_ROOT)    net (fo=14367, routed)       2.333     3.163    shift_reg_tap_i/clk_c
    SLICE_X65Y466        FDRE                                         r  shift_reg_tap_i/sr_p.sr_6[52]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X65Y466        FDRE (Prop_GFF2_SLICEL_C_Q)
                                                      0.072     3.235 r  shift_reg_tap_i/sr_p.sr_6[52]/Q
                         net (fo=1, routed)           0.190     3.425    shift_reg_tap_i/sr_6[52]
    SLICE_X65Y481        FDRE                                         r  shift_reg_tap_i/sr_p.sr_7[52]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y7 (CLOCK_ROOT)    net (fo=14367, routed)       2.664     3.805    shift_reg_tap_i/clk_c
    SLICE_X65Y481        FDRE                                         r  shift_reg_tap_i/sr_p.sr_7[52]/C
                         clock pessimism             -0.440     3.365    
    SLICE_X65Y481        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.053     3.418    shift_reg_tap_i/sr_p.sr_7[52]
  -------------------------------------------------------------------
                         required time                         -3.418    
                         arrival time                           3.425    
  -------------------------------------------------------------------
                         slack                                  0.007    

Slack (MET) :             0.007ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_6[52]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_i/sr_p.sr_7[52]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.262ns  (logic 0.072ns (27.481%)  route 0.190ns (72.519%))
  Logic Levels:           0  
  Clock Path Skew:        0.202ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.805ns
    Source Clock Delay      (SCD):    3.163ns
    Clock Pessimism Removal (CPR):    0.440ns
  Clock Net Delay (Source):      2.333ns (routing 1.362ns, distribution 0.971ns)
  Clock Net Delay (Destination): 2.664ns (routing 1.491ns, distribution 1.173ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y7 (CLOCK_ROOT)    net (fo=14367, routed)       2.333     3.163    shift_reg_tap_i/clk_c
    SLICE_X65Y466        FDRE                                         r  shift_reg_tap_i/sr_p.sr_6[52]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X65Y466        FDRE (Prop_GFF2_SLICEL_C_Q)
                                                      0.072     3.235 f  shift_reg_tap_i/sr_p.sr_6[52]/Q
                         net (fo=1, routed)           0.190     3.425    shift_reg_tap_i/sr_6[52]
    SLICE_X65Y481        FDRE                                         f  shift_reg_tap_i/sr_p.sr_7[52]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y7 (CLOCK_ROOT)    net (fo=14367, routed)       2.664     3.805    shift_reg_tap_i/clk_c
    SLICE_X65Y481        FDRE                                         r  shift_reg_tap_i/sr_p.sr_7[52]/C
                         clock pessimism             -0.440     3.365    
    SLICE_X65Y481        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.053     3.418    shift_reg_tap_i/sr_p.sr_7[52]
  -------------------------------------------------------------------
                         required time                         -3.418    
                         arrival time                           3.425    
  -------------------------------------------------------------------
                         slack                                  0.007    

Slack (MET) :             0.011ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_4[15]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_o/sr_p.sr_5[15]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.183ns  (logic 0.072ns (39.344%)  route 0.111ns (60.656%))
  Logic Levels:           0  
  Clock Path Skew:        0.117ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.817ns
    Source Clock Delay      (SCD):    3.203ns
    Clock Pessimism Removal (CPR):    0.497ns
  Clock Net Delay (Source):      2.373ns (routing 1.362ns, distribution 1.011ns)
  Clock Net Delay (Destination): 2.676ns (routing 1.491ns, distribution 1.185ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y7 (CLOCK_ROOT)    net (fo=14367, routed)       2.373     3.203    shift_reg_tap_o/clk_c
    SLICE_X61Y504        FDRE                                         r  shift_reg_tap_o/sr_p.sr_4[15]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X61Y504        FDRE (Prop_EFF2_SLICEL_C_Q)
                                                      0.072     3.275 r  shift_reg_tap_o/sr_p.sr_4[15]/Q
                         net (fo=1, routed)           0.111     3.386    shift_reg_tap_o/sr_4[15]
    SLICE_X63Y503        FDRE                                         r  shift_reg_tap_o/sr_p.sr_5[15]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y7 (CLOCK_ROOT)    net (fo=14367, routed)       2.676     3.817    shift_reg_tap_o/clk_c
    SLICE_X63Y503        FDRE                                         r  shift_reg_tap_o/sr_p.sr_5[15]/C
                         clock pessimism             -0.497     3.320    
    SLICE_X63Y503        FDRE (Hold_GFF2_SLICEL_C_D)
                                                      0.055     3.375    shift_reg_tap_o/sr_p.sr_5[15]
  -------------------------------------------------------------------
                         required time                         -3.375    
                         arrival time                           3.386    
  -------------------------------------------------------------------
                         slack                                  0.011    

Slack (MET) :             0.011ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_4[15]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_o/sr_p.sr_5[15]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.183ns  (logic 0.072ns (39.344%)  route 0.111ns (60.656%))
  Logic Levels:           0  
  Clock Path Skew:        0.117ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.817ns
    Source Clock Delay      (SCD):    3.203ns
    Clock Pessimism Removal (CPR):    0.497ns
  Clock Net Delay (Source):      2.373ns (routing 1.362ns, distribution 1.011ns)
  Clock Net Delay (Destination): 2.676ns (routing 1.491ns, distribution 1.185ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y7 (CLOCK_ROOT)    net (fo=14367, routed)       2.373     3.203    shift_reg_tap_o/clk_c
    SLICE_X61Y504        FDRE                                         r  shift_reg_tap_o/sr_p.sr_4[15]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X61Y504        FDRE (Prop_EFF2_SLICEL_C_Q)
                                                      0.072     3.275 f  shift_reg_tap_o/sr_p.sr_4[15]/Q
                         net (fo=1, routed)           0.111     3.386    shift_reg_tap_o/sr_4[15]
    SLICE_X63Y503        FDRE                                         f  shift_reg_tap_o/sr_p.sr_5[15]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y7 (CLOCK_ROOT)    net (fo=14367, routed)       2.676     3.817    shift_reg_tap_o/clk_c
    SLICE_X63Y503        FDRE                                         r  shift_reg_tap_o/sr_p.sr_5[15]/C
                         clock pessimism             -0.497     3.320    
    SLICE_X63Y503        FDRE (Hold_GFF2_SLICEL_C_D)
                                                      0.055     3.375    shift_reg_tap_o/sr_p.sr_5[15]
  -------------------------------------------------------------------
                         required time                         -3.375    
                         arrival time                           3.386    
  -------------------------------------------------------------------
                         slack                                  0.011    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_14[479]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_i/sr_p.sr_15[479]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.266ns
    Source Clock Delay      (SCD):    1.881ns
    Clock Pessimism Removal (CPR):    0.379ns
  Clock Net Delay (Source):      1.335ns (routing 0.768ns, distribution 0.567ns)
  Clock Net Delay (Destination): 1.494ns (routing 0.847ns, distribution 0.647ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X2Y7 (CLOCK_ROOT)    net (fo=14367, routed)       1.335     1.881    shift_reg_tap_i/clk_c
    SLICE_X63Y470        FDRE                                         r  shift_reg_tap_i/sr_p.sr_14[479]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X63Y470        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.920 r  shift_reg_tap_i/sr_p.sr_14[479]/Q
                         net (fo=1, routed)           0.033     1.953    shift_reg_tap_i/sr_14[479]
    SLICE_X63Y470        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[479]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X2Y7 (CLOCK_ROOT)    net (fo=14367, routed)       1.494     2.266    shift_reg_tap_i/clk_c
    SLICE_X63Y470        FDRE                                         r  shift_reg_tap_i/sr_p.sr_15[479]/C
                         clock pessimism             -0.379     1.887    
    SLICE_X63Y470        FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.934    shift_reg_tap_i/sr_p.sr_15[479]
  -------------------------------------------------------------------
                         required time                         -1.934    
                         arrival time                           1.953    
  -------------------------------------------------------------------
                         slack                                  0.019    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.563 }
Period(ns):         3.125
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         3.125       1.626      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X86Y495   muon_sorter_1/sr_p.sr_1_15.roi_ret_18534/C
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X90Y479   muon_sorter_1/sr_p.sr_1_15.roi_ret_18630/C
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X101Y490  muon_sorter_1/sr_p.sr_1_15.roi_ret_27655/C
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X101Y490  muon_sorter_1/sr_p.sr_1_15.roi_ret_27658/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X61Y466   muon_sorter_1/sr_p.sr_1_8.pt_ret_134/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X62Y532   shift_reg_tap_i/sr_p.sr_12[491]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X64Y536   shift_reg_tap_i/sr_p.sr_12[492]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X61Y536   shift_reg_tap_i/sr_p.sr_12[494]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X60Y537   shift_reg_tap_i/sr_p.sr_12[499]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X102Y521  muon_sorter_1/sr_p.sr_1_15.roi_ret_27661/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X65Y417   shift_reg_tap_i/sr_p.sr_12[490]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X54Y535   shift_reg_tap_i/sr_p.sr_12[495]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X60Y537   shift_reg_tap_i/sr_p.sr_12[499]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X60Y536   shift_reg_tap_i/sr_p.sr_12[49]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X95Y534          lsfr_1/output_vector_1[511]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X110Y533         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X64Y423          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X64Y423          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X64Y423          lsfr_1/shiftreg_vector[101]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X64Y423          lsfr_1/shiftreg_vector[102]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X95Y534          lsfr_1/output_vector_1[511]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X110Y533         lsfr_1/shiftreg_vector[0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X61Y423          lsfr_1/shiftreg_vector[104]/C



