Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Sat Mar  2 20:12:21 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_utilization -hierarchical -file hier_utilization.txt
| Design       : bitonic_sorter_16_top
| Device       : xcvu9pflgc2104-1
| Design State : Routed
------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+------------------------------------------+-------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
|                 Instance                 |                            Module                           | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+------------------------------------------+-------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
| bitonic_sorter_16_top                    |                                                       (top) |       1264 |       1264 |       0 |    0 | 1155 |      0 |      0 |    0 |            0 |
|   (bitonic_sorter_16_top)                |                                                       (top) |          0 |          0 |       0 |    0 |  416 |      0 |      0 |    0 |            0 |
|   dut_inst                               | muon_sorter_I016_O016_D001_BITONIC-freq160retfan10000_rev_1 |       1194 |       1194 |       0 |    0 |  252 |      0 |      0 |    0 |            0 |
|     (dut_inst)                           | muon_sorter_I016_O016_D001_BITONIC-freq160retfan10000_rev_1 |          0 |          0 |       0 |    0 |  248 |      0 |      0 |    0 |            0 |
|     sorter_inst                          |                                      bitonic_sort_16s_1s_8s |       1194 |       1194 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|       genblk1.bitonic_merge_i            |                                     bitonic_merge_16s_1s_8s |        521 |        521 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_merge_high       |                                    bitonic_merge_8s_1s_4s_1 |        198 |        198 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_high     |                                    bitonic_merge_4s_1s_2s_6 |         64 |         64 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                   bitonic_merge_2s_1s_1s_18 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_54 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                   bitonic_merge_2s_1s_1s_17 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_53 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_52 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_51 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_low      |                                    bitonic_merge_4s_1s_2s_5 |         70 |         70 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                   bitonic_merge_2s_1s_1s_16 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_50 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                   bitonic_merge_2s_1s_1s_15 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_49 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_48 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_47 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[0].compare_i     |                                               compare_1s_45 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[1].compare_i     |                                               compare_1s_46 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[2].compare_i     |                                               compare_1s_43 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[3].compare_i     |                                               compare_1s_44 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_merge_low        |                                    bitonic_merge_8s_1s_4s_0 |        207 |        207 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_high     |                                    bitonic_merge_4s_1s_2s_4 |         64 |         64 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                   bitonic_merge_2s_1s_1s_14 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_42 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                   bitonic_merge_2s_1s_1s_13 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_41 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_40 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_39 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_low      |                                    bitonic_merge_4s_1s_2s_3 |         68 |         68 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                   bitonic_merge_2s_1s_1s_12 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_38 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                   bitonic_merge_2s_1s_1s_11 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_37 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_36 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_35 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[0].compare_i     |                                               compare_1s_33 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[1].compare_i     |                                               compare_1s_34 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[2].compare_i     |                                               compare_1s_31 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[3].compare_i     |                                               compare_1s_32 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[0].compare_i       |                                               compare_1s_29 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[1].compare_i       |                                               compare_1s_30 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[2].compare_i       |                                               compare_1s_27 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[3].compare_i       |                                               compare_1s_28 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[4].compare_i       |                                               compare_1s_25 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[5].compare_i       |                                               compare_1s_26 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[6].compare_i       |                                               compare_1s_23 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.cmp_g[7].compare_i       |                                               compare_1s_24 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|       genblk1.bitonic_sort_high          |                                       bitonic_sort_8s_0s_4s |        345 |        345 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_merge_i          |                                      bitonic_merge_8s_0s_4s |        200 |        200 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_high     |                                    bitonic_merge_4s_0s_2s_2 |         72 |         72 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                   bitonic_merge_2s_0s_1s_10 |         22 |         22 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_0s_22 |         22 |         22 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_0s_1s_9 |         18 |         18 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_0s_21 |         18 |         18 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_0s_20 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_0s_19 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_low      |                                    bitonic_merge_4s_0s_2s_1 |         68 |         68 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_0s_1s_8 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_0s_18 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_0s_1s_7 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_0s_17 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_0s_16 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_0s_15 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[0].compare_i     |                                               compare_0s_13 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[1].compare_i     |                                               compare_0s_14 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[2].compare_i     |                                               compare_0s_11 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[3].compare_i     |                                               compare_0s_12 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_sort_high        |                                     bitonic_sort_4s_0s_2s_0 |         73 |         73 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_i        |                                    bitonic_merge_4s_0s_2s_0 |         50 |         50 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_0s_1s_6 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_0s_10 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_0s_1s_5 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_9 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                                compare_0s_8 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                                compare_0s_7 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_high      |                                     bitonic_sort_2s_0s_1s_2 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                    bitonic_merge_2s_0s_1s_4 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_6 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_low       |                                     bitonic_sort_2s_1s_1s_2 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                   bitonic_merge_2s_1s_1s_10 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_22 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_sort_low         |                                     bitonic_sort_4s_1s_2s_0 |         72 |         72 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_i        |                                    bitonic_merge_4s_1s_2s_2 |         52 |         52 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_1s_1s_9 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_21 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_1s_1s_8 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_20 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_19 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_18 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_high      |                                     bitonic_sort_2s_0s_1s_1 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                    bitonic_merge_2s_0s_1s_3 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_5 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_low       |                                     bitonic_sort_2s_1s_1s_1 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                    bitonic_merge_2s_1s_1s_7 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_17 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|       genblk1.bitonic_sort_low           |                                       bitonic_sort_8s_1s_4s |        328 |        328 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_merge_i          |                                      bitonic_merge_8s_1s_4s |        187 |        187 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_high     |                                    bitonic_merge_4s_1s_2s_1 |         71 |         71 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_1s_1s_6 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_16 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_1s_1s_5 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_15 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_14 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                               compare_1s_13 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_low      |                                    bitonic_merge_4s_1s_2s_0 |         69 |         69 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_1s_1s_4 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_12 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_1s_1s_3 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                               compare_1s_11 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                               compare_1s_10 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                                compare_1s_9 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[0].compare_i     |                                                compare_1s_7 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[1].compare_i     |                                                compare_1s_8 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[2].compare_i     |                                                compare_1s_5 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.cmp_g[3].compare_i     |                                                compare_1s_6 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_sort_high        |                                       bitonic_sort_4s_0s_2s |         70 |         70 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_i        |                                      bitonic_merge_4s_0s_2s |         52 |         52 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_0s_1s_2 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_4 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_0s_1s_1 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_3 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                                compare_0s_2 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                                compare_0s_1 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_high      |                                     bitonic_sort_2s_0s_1s_0 |          6 |          6 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                    bitonic_merge_2s_0s_1s_0 |          6 |          6 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_0s_0 |          6 |          6 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_low       |                                     bitonic_sort_2s_1s_1s_0 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                    bitonic_merge_2s_1s_1s_2 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_1s_4 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|         genblk1.bitonic_sort_low         |                                       bitonic_sort_4s_1s_2s |         71 |         71 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_merge_i        |                                      bitonic_merge_4s_1s_2s |         52 |         52 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_high   |                                    bitonic_merge_2s_1s_1s_1 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_1s_3 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_low    |                                    bitonic_merge_2s_1s_1s_0 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                compare_1s_2 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[0].compare_i   |                                                compare_1s_1 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.cmp_g[1].compare_i   |                                                compare_1s_0 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_high      |                                       bitonic_sort_2s_0s_1s |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                      bitonic_merge_2s_0s_1s |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                  compare_0s |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|           genblk1.bitonic_sort_low       |                                       bitonic_sort_2s_1s_1s |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|             genblk1.bitonic_merge_i      |                                      bitonic_merge_2s_1s_1s |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|               genblk1.cmp_g[0].compare_i |                                                  compare_1s |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   input_lfsr                             |                                                   lfsr_208s |          1 |          1 |       0 |    0 |  209 |      0 |      0 |    0 |            0 |
|   output_reducer                         |                                                  reducer_4s |         69 |         69 |       0 |    0 |  278 |      0 |      0 |    0 |            0 |
+------------------------------------------+-------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
* Note: The sum of lower-level cells may be larger than their parent cells total, due to cross-hierarchy LUT combining


