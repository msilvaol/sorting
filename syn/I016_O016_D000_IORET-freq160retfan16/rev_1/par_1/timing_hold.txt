Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Wed Feb 13 16:37:03 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.012        0.000                      0                 7680        2.850        0.000                       0                  8536  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 3.125}        6.250           160.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.012        0.000                      0                 7680        2.850        0.000                       0                  7937  
clk_wrapper                                                                             498.562        0.000                       0                   599  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.012ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        2.850ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_2[21]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_3[21]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.272ns  (logic 0.072ns (26.471%)  route 0.200ns (73.529%))
  Logic Levels:           0  
  Clock Path Skew:        0.205ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.028ns
    Source Clock Delay      (SCD):    3.367ns
    Clock Pessimism Removal (CPR):    0.456ns
  Clock Net Delay (Source):      2.537ns (routing 1.538ns, distribution 0.999ns)
  Clock Net Delay (Destination): 2.887ns (routing 1.683ns, distribution 1.204ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=7936, routed)        2.537     3.367    shift_reg_tap_o/clk_c
    SLICE_X109Y355       FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[21]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X109Y355       FDRE (Prop_FFF2_SLICEL_C_Q)
                                                      0.072     3.439 r  shift_reg_tap_o/sr_p.sr_2[21]/Q
                         net (fo=1, routed)           0.200     3.639    shift_reg_tap_o/sr_2[21]
    SLICE_X109Y361       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[21]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=7936, routed)        2.887     4.028    shift_reg_tap_o/clk_c
    SLICE_X109Y361       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[21]/C
                         clock pessimism             -0.456     3.572    
    SLICE_X109Y361       FDRE (Hold_CFF2_SLICEL_C_D)
                                                      0.055     3.627    shift_reg_tap_o/sr_p.sr_3[21]
  -------------------------------------------------------------------
                         required time                         -3.627    
                         arrival time                           3.639    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_2[21]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_3[21]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.272ns  (logic 0.072ns (26.471%)  route 0.200ns (73.529%))
  Logic Levels:           0  
  Clock Path Skew:        0.205ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.028ns
    Source Clock Delay      (SCD):    3.367ns
    Clock Pessimism Removal (CPR):    0.456ns
  Clock Net Delay (Source):      2.537ns (routing 1.538ns, distribution 0.999ns)
  Clock Net Delay (Destination): 2.887ns (routing 1.683ns, distribution 1.204ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=7936, routed)        2.537     3.367    shift_reg_tap_o/clk_c
    SLICE_X109Y355       FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[21]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X109Y355       FDRE (Prop_FFF2_SLICEL_C_Q)
                                                      0.072     3.439 f  shift_reg_tap_o/sr_p.sr_2[21]/Q
                         net (fo=1, routed)           0.200     3.639    shift_reg_tap_o/sr_2[21]
    SLICE_X109Y361       FDRE                                         f  shift_reg_tap_o/sr_p.sr_3[21]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=7936, routed)        2.887     4.028    shift_reg_tap_o/clk_c
    SLICE_X109Y361       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[21]/C
                         clock pessimism             -0.456     3.572    
    SLICE_X109Y361       FDRE (Hold_CFF2_SLICEL_C_D)
                                                      0.055     3.627    shift_reg_tap_o/sr_p.sr_3[21]
  -------------------------------------------------------------------
                         required time                         -3.627    
                         arrival time                           3.639    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_5[241]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_6[241]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.221ns  (logic 0.069ns (31.222%)  route 0.152ns (68.778%))
  Logic Levels:           0  
  Clock Path Skew:        0.156ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.078ns
    Source Clock Delay      (SCD):    3.409ns
    Clock Pessimism Removal (CPR):    0.513ns
  Clock Net Delay (Source):      2.579ns (routing 1.538ns, distribution 1.041ns)
  Clock Net Delay (Destination): 2.937ns (routing 1.683ns, distribution 1.254ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=7936, routed)        2.579     3.409    shift_reg_tap_i/clk_c
    SLICE_X110Y370       FDRE                                         r  shift_reg_tap_i/sr_p.sr_5[241]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X110Y370       FDRE (Prop_AFF_SLICEM_C_Q)
                                                      0.069     3.478 r  shift_reg_tap_i/sr_p.sr_5[241]/Q
                         net (fo=1, routed)           0.152     3.630    shift_reg_tap_i/sr_5[241]
    SLICE_X107Y370       FDRE                                         r  shift_reg_tap_i/sr_p.sr_6[241]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=7936, routed)        2.937     4.078    shift_reg_tap_i/clk_c
    SLICE_X107Y370       FDRE                                         r  shift_reg_tap_i/sr_p.sr_6[241]/C
                         clock pessimism             -0.513     3.565    
    SLICE_X107Y370       FDRE (Hold_EFF_SLICEM_C_D)
                                                      0.053     3.618    shift_reg_tap_i/sr_p.sr_6[241]
  -------------------------------------------------------------------
                         required time                         -3.618    
                         arrival time                           3.630    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_5[241]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_6[241]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.221ns  (logic 0.069ns (31.222%)  route 0.152ns (68.778%))
  Logic Levels:           0  
  Clock Path Skew:        0.156ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.078ns
    Source Clock Delay      (SCD):    3.409ns
    Clock Pessimism Removal (CPR):    0.513ns
  Clock Net Delay (Source):      2.579ns (routing 1.538ns, distribution 1.041ns)
  Clock Net Delay (Destination): 2.937ns (routing 1.683ns, distribution 1.254ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=7936, routed)        2.579     3.409    shift_reg_tap_i/clk_c
    SLICE_X110Y370       FDRE                                         r  shift_reg_tap_i/sr_p.sr_5[241]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X110Y370       FDRE (Prop_AFF_SLICEM_C_Q)
                                                      0.069     3.478 f  shift_reg_tap_i/sr_p.sr_5[241]/Q
                         net (fo=1, routed)           0.152     3.630    shift_reg_tap_i/sr_5[241]
    SLICE_X107Y370       FDRE                                         f  shift_reg_tap_i/sr_p.sr_6[241]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=7936, routed)        2.937     4.078    shift_reg_tap_i/clk_c
    SLICE_X107Y370       FDRE                                         r  shift_reg_tap_i/sr_p.sr_6[241]/C
                         clock pessimism             -0.513     3.565    
    SLICE_X107Y370       FDRE (Hold_EFF_SLICEM_C_D)
                                                      0.053     3.618    shift_reg_tap_i/sr_p.sr_6[241]
  -------------------------------------------------------------------
                         required time                         -3.618    
                         arrival time                           3.630    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.015ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_9[96]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_10[96]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.206ns  (logic 0.072ns (34.951%)  route 0.134ns (65.049%))
  Logic Levels:           0  
  Clock Path Skew:        0.138ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.020ns
    Source Clock Delay      (SCD):    3.417ns
    Clock Pessimism Removal (CPR):    0.465ns
  Clock Net Delay (Source):      2.587ns (routing 1.538ns, distribution 1.049ns)
  Clock Net Delay (Destination): 2.879ns (routing 1.683ns, distribution 1.196ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=7936, routed)        2.587     3.417    shift_reg_tap_i/clk_c
    SLICE_X102Y421       FDRE                                         r  shift_reg_tap_i/sr_p.sr_9[96]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X102Y421       FDRE (Prop_EFF2_SLICEM_C_Q)
                                                      0.072     3.489 r  shift_reg_tap_i/sr_p.sr_9[96]/Q
                         net (fo=1, routed)           0.134     3.623    shift_reg_tap_i/sr_9[96]
    SLICE_X102Y418       FDRE                                         r  shift_reg_tap_i/sr_p.sr_10[96]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y5 (CLOCK_ROOT)    net (fo=7936, routed)        2.879     4.020    shift_reg_tap_i/clk_c
    SLICE_X102Y418       FDRE                                         r  shift_reg_tap_i/sr_p.sr_10[96]/C
                         clock pessimism             -0.465     3.555    
    SLICE_X102Y418       FDRE (Hold_EFF_SLICEM_C_D)
                                                      0.053     3.608    shift_reg_tap_i/sr_p.sr_10[96]
  -------------------------------------------------------------------
                         required time                         -3.608    
                         arrival time                           3.623    
  -------------------------------------------------------------------
                         slack                                  0.015    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 3.125 }
Period(ns):         6.250
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         6.250       4.751      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X94Y338   shift_reg_tap_i/sr_p.sr_10[145]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X93Y338   shift_reg_tap_i/sr_p.sr_10[146]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X95Y336   shift_reg_tap_i/sr_p.sr_10[147]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X103Y337  shift_reg_tap_i/sr_p.sr_10[148]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X108Y344  shift_reg_tap_i/sr_p.sr_13[228]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X98Y458   shift_reg_tap_i/sr_p.sr_1[72]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X98Y465   shift_reg_tap_i/sr_p.sr_1[74]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X98Y465   shift_reg_tap_i/sr_p.sr_1[75]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X98Y465   shift_reg_tap_i/sr_p.sr_1[76]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X106Y381  shift_reg_tap_o/sr_p.sr_1[242]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X94Y338   shift_reg_tap_i/sr_p.sr_10[145]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X93Y338   shift_reg_tap_i/sr_p.sr_10[146]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X93Y338   shift_reg_tap_i/sr_p.sr_10[146]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X95Y336   shift_reg_tap_i/sr_p.sr_10[147]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X110Y378         lsfr_1/output_vector_1[255]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X111Y390         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X100Y453         lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X100Y453         lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X100Y453         lsfr_1/shiftreg_vector[101]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X100Y453         lsfr_1/shiftreg_vector[102]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X110Y378         lsfr_1/output_vector_1[255]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X100Y453         lsfr_1/shiftreg_vector[100]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X100Y453         lsfr_1/shiftreg_vector[101]/C



