Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Sat Feb  9 09:59:57 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.012        0.000                      0                14750        5.975        0.000                       0                 16104  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 6.250}        12.500          80.000          
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.012        0.000                      0                14750        5.975        0.000                       0                 15249  
clk_wrapper                                                                             498.562        0.000                       0                   855  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.012ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        5.975ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_2[116]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_3[116]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.224ns  (logic 0.070ns (31.250%)  route 0.154ns (68.750%))
  Logic Levels:           0  
  Clock Path Skew:        0.157ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.797ns
    Source Clock Delay      (SCD):    3.195ns
    Clock Pessimism Removal (CPR):    0.445ns
  Clock Net Delay (Source):      2.365ns (routing 1.158ns, distribution 1.207ns)
  Clock Net Delay (Destination): 2.656ns (routing 1.268ns, distribution 1.388ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=15248, routed)       2.365     3.195    shift_reg_tap_i/clk_c
    SLICE_X78Y418        FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[116]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X78Y418        FDRE (Prop_HFF_SLICEM_C_Q)
                                                      0.070     3.265 r  shift_reg_tap_i/sr_p.sr_2[116]/Q
                         net (fo=1, routed)           0.154     3.419    shift_reg_tap_i/sr_2[116]
    SLICE_X78Y422        FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[116]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=15248, routed)       2.656     3.797    shift_reg_tap_i/clk_c
    SLICE_X78Y422        FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[116]/C
                         clock pessimism             -0.445     3.352    
    SLICE_X78Y422        FDRE (Hold_EFF2_SLICEM_C_D)
                                                      0.055     3.407    shift_reg_tap_i/sr_p.sr_3[116]
  -------------------------------------------------------------------
                         required time                         -3.407    
                         arrival time                           3.419    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.012ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_2[116]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_3[116]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.224ns  (logic 0.070ns (31.250%)  route 0.154ns (68.750%))
  Logic Levels:           0  
  Clock Path Skew:        0.157ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.797ns
    Source Clock Delay      (SCD):    3.195ns
    Clock Pessimism Removal (CPR):    0.445ns
  Clock Net Delay (Source):      2.365ns (routing 1.158ns, distribution 1.207ns)
  Clock Net Delay (Destination): 2.656ns (routing 1.268ns, distribution 1.388ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=15248, routed)       2.365     3.195    shift_reg_tap_i/clk_c
    SLICE_X78Y418        FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[116]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X78Y418        FDRE (Prop_HFF_SLICEM_C_Q)
                                                      0.070     3.265 f  shift_reg_tap_i/sr_p.sr_2[116]/Q
                         net (fo=1, routed)           0.154     3.419    shift_reg_tap_i/sr_2[116]
    SLICE_X78Y422        FDRE                                         f  shift_reg_tap_i/sr_p.sr_3[116]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=15248, routed)       2.656     3.797    shift_reg_tap_i/clk_c
    SLICE_X78Y422        FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[116]/C
                         clock pessimism             -0.445     3.352    
    SLICE_X78Y422        FDRE (Hold_EFF2_SLICEM_C_D)
                                                      0.055     3.407    shift_reg_tap_i/sr_p.sr_3[116]
  -------------------------------------------------------------------
                         required time                         -3.407    
                         arrival time                           3.419    
  -------------------------------------------------------------------
                         slack                                  0.012    

Slack (MET) :             0.016ns  (arrival time - required time)
  Source:                 muon_sorter_1/sr_p.sr_2_13.roi[0]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[216]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.183ns  (logic 0.070ns (38.251%)  route 0.113ns (61.749%))
  Logic Levels:           0  
  Clock Path Skew:        0.114ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.765ns
    Source Clock Delay      (SCD):    3.158ns
    Clock Pessimism Removal (CPR):    0.493ns
  Clock Net Delay (Source):      2.328ns (routing 1.158ns, distribution 1.170ns)
  Clock Net Delay (Destination): 2.624ns (routing 1.268ns, distribution 1.356ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=15248, routed)       2.328     3.158    muon_sorter_1/clk_c
    SLICE_X87Y450        FDRE                                         r  muon_sorter_1/sr_p.sr_2_13.roi[0]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X87Y450        FDRE (Prop_DFF_SLICEM_C_Q)
                                                      0.070     3.228 r  muon_sorter_1/sr_p.sr_2_13.roi[0]/Q
                         net (fo=1, routed)           0.113     3.341    shift_reg_tap_o/roi_12[0]
    SLICE_X87Y448        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[216]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=15248, routed)       2.624     3.765    shift_reg_tap_o/clk_c
    SLICE_X87Y448        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[216]/C
                         clock pessimism             -0.493     3.272    
    SLICE_X87Y448        FDRE (Hold_EFF_SLICEM_C_D)
                                                      0.053     3.325    shift_reg_tap_o/sr_p.sr_1[216]
  -------------------------------------------------------------------
                         required time                         -3.325    
                         arrival time                           3.341    
  -------------------------------------------------------------------
                         slack                                  0.016    

Slack (MET) :             0.016ns  (arrival time - required time)
  Source:                 muon_sorter_1/sr_p.sr_2_13.roi[0]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[216]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.183ns  (logic 0.070ns (38.251%)  route 0.113ns (61.749%))
  Logic Levels:           0  
  Clock Path Skew:        0.114ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.765ns
    Source Clock Delay      (SCD):    3.158ns
    Clock Pessimism Removal (CPR):    0.493ns
  Clock Net Delay (Source):      2.328ns (routing 1.158ns, distribution 1.170ns)
  Clock Net Delay (Destination): 2.624ns (routing 1.268ns, distribution 1.356ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=15248, routed)       2.328     3.158    muon_sorter_1/clk_c
    SLICE_X87Y450        FDRE                                         r  muon_sorter_1/sr_p.sr_2_13.roi[0]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X87Y450        FDRE (Prop_DFF_SLICEM_C_Q)
                                                      0.070     3.228 f  muon_sorter_1/sr_p.sr_2_13.roi[0]/Q
                         net (fo=1, routed)           0.113     3.341    shift_reg_tap_o/roi_12[0]
    SLICE_X87Y448        FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[216]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=15248, routed)       2.624     3.765    shift_reg_tap_o/clk_c
    SLICE_X87Y448        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[216]/C
                         clock pessimism             -0.493     3.272    
    SLICE_X87Y448        FDRE (Hold_EFF_SLICEM_C_D)
                                                      0.053     3.325    shift_reg_tap_o/sr_p.sr_1[216]
  -------------------------------------------------------------------
                         required time                         -3.325    
                         arrival time                           3.341    
  -------------------------------------------------------------------
                         slack                                  0.016    

Slack (MET) :             0.017ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_4[57]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_5[57]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.331ns  (logic 0.071ns (21.450%)  route 0.260ns (78.550%))
  Logic Levels:           0  
  Clock Path Skew:        0.259ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.896ns
    Source Clock Delay      (SCD):    3.216ns
    Clock Pessimism Removal (CPR):    0.421ns
  Clock Net Delay (Source):      2.386ns (routing 1.158ns, distribution 1.228ns)
  Clock Net Delay (Destination): 2.755ns (routing 1.268ns, distribution 1.487ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=15248, routed)       2.386     3.216    shift_reg_tap_o/clk_c
    SLICE_X89Y518        FDRE                                         r  shift_reg_tap_o/sr_p.sr_4[57]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X89Y518        FDRE (Prop_BFF2_SLICEL_C_Q)
                                                      0.071     3.287 r  shift_reg_tap_o/sr_p.sr_4[57]/Q
                         net (fo=1, routed)           0.260     3.547    shift_reg_tap_o/sr_4[57]
    SLICE_X89Y541        FDRE                                         r  shift_reg_tap_o/sr_p.sr_5[57]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y8 (CLOCK_ROOT)    net (fo=15248, routed)       2.755     3.896    shift_reg_tap_o/clk_c
    SLICE_X89Y541        FDRE                                         r  shift_reg_tap_o/sr_p.sr_5[57]/C
                         clock pessimism             -0.421     3.475    
    SLICE_X89Y541        FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.055     3.530    shift_reg_tap_o/sr_p.sr_5[57]
  -------------------------------------------------------------------
                         required time                         -3.530    
                         arrival time                           3.547    
  -------------------------------------------------------------------
                         slack                                  0.017    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 6.250 }
Period(ns):         12.500
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location       Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         12.500      11.001     BUFGCE_X1Y224  clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X65Y484  muon_sorter_1/sr_p.sr_1_15.roi_ret_18307/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X65Y485  muon_sorter_1/sr_p.sr_1_15.roi_ret_18310/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X68Y485  muon_sorter_1/sr_p.sr_1_15.roi_ret_18313/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X71Y484  muon_sorter_1/sr_p.sr_1_15.roi_ret_18316/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X68Y486  muon_sorter_1/sr_p.sr_1_15.roi_ret_28748/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X70Y536  shift_reg_tap_i/sr_p.sr_12[470]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X91Y522  shift_reg_tap_i/sr_p.sr_12[476]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X87Y528  shift_reg_tap_i/sr_p.sr_1[449]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X88Y523  shift_reg_tap_i/sr_p.sr_5[420]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X81Y527  shift_reg_tap_i/sr_p.sr_12[466]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X83Y526  shift_reg_tap_i/sr_p.sr_12[473]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X81Y527  shift_reg_tap_i/sr_p.sr_12[475]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X84Y523  shift_reg_tap_i/sr_p.sr_12[479]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X76Y451  shift_reg_tap_i/sr_p.sr_12[47]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X99Y522          lsfr_1/output_vector_1[511]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X99Y522          lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X82Y425          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X99Y522          lsfr_1/output_vector_1[511]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X82Y425          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X82Y425          lsfr_1/shiftreg_vector[101]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X99Y522          lsfr_1/output_vector_1[511]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X82Y425          lsfr_1/shiftreg_vector[100]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X82Y425          lsfr_1/shiftreg_vector[101]/C



