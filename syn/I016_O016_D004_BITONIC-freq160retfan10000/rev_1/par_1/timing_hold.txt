Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Sat Mar  2 22:24:01 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : bitonic_sorter_16_top
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.009        0.000                      0                 1152        2.552        0.000                       0                  1851  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 3.125}        6.250           160.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.009        0.000                      0                 1152        2.552        0.000                       0                  1361  
clk_wrapper                                                                             498.562        0.000                       0                   490  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.009ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        2.552ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.009ns  (arrival time - required time)
  Source:                 dut_inst/qdelay_2__15_.idx_0__ret_10[9]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            dut_inst/qdelay_3__15_.idx_0__ret_9[0]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.170ns  (logic 0.069ns (40.588%)  route 0.101ns (59.412%))
  Logic Levels:           0  
  Clock Path Skew:        0.106ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.130ns
    Source Clock Delay      (SCD):    2.584ns
    Clock Pessimism Removal (CPR):    0.441ns
  Clock Net Delay (Source):      1.735ns (routing 0.930ns, distribution 0.805ns)
  Clock Net Delay (Destination): 1.970ns (routing 1.021ns, distribution 0.949ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.529     0.529 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.529    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.529 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.825    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.849 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1360, routed)        1.735     2.584    dut_inst/clk_c
    SLICE_X105Y520       FDRE                                         r  dut_inst/qdelay_2__15_.idx_0__ret_10[9]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X105Y520       FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     2.653 r  dut_inst/qdelay_2__15_.idx_0__ret_10[9]/Q
                         net (fo=1, routed)           0.101     2.754    dut_inst/m_o_o[25]
    SLICE_X106Y520       FDRE                                         r  dut_inst/qdelay_3__15_.idx_0__ret_9[0]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.798     0.798 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.798    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.798 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.132    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.160 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1360, routed)        1.970     3.130    dut_inst/clk_c
    SLICE_X106Y520       FDRE                                         r  dut_inst/qdelay_3__15_.idx_0__ret_9[0]/C
                         clock pessimism             -0.441     2.689    
    SLICE_X106Y520       FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.055     2.744    dut_inst/qdelay_3__15_.idx_0__ret_9[0]
  -------------------------------------------------------------------
                         required time                         -2.744    
                         arrival time                           2.754    
  -------------------------------------------------------------------
                         slack                                  0.009    

Slack (MET) :             0.009ns  (arrival time - required time)
  Source:                 dut_inst/qdelay_2__15_.idx_0__ret_10[9]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            dut_inst/qdelay_3__15_.idx_0__ret_9[0]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.170ns  (logic 0.069ns (40.588%)  route 0.101ns (59.412%))
  Logic Levels:           0  
  Clock Path Skew:        0.106ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.130ns
    Source Clock Delay      (SCD):    2.584ns
    Clock Pessimism Removal (CPR):    0.441ns
  Clock Net Delay (Source):      1.735ns (routing 0.930ns, distribution 0.805ns)
  Clock Net Delay (Destination): 1.970ns (routing 1.021ns, distribution 0.949ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.529     0.529 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.529    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.529 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.825    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.849 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1360, routed)        1.735     2.584    dut_inst/clk_c
    SLICE_X105Y520       FDRE                                         r  dut_inst/qdelay_2__15_.idx_0__ret_10[9]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X105Y520       FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     2.653 f  dut_inst/qdelay_2__15_.idx_0__ret_10[9]/Q
                         net (fo=1, routed)           0.101     2.754    dut_inst/m_o_o[25]
    SLICE_X106Y520       FDRE                                         f  dut_inst/qdelay_3__15_.idx_0__ret_9[0]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.798     0.798 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.798    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.798 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.132    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.160 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1360, routed)        1.970     3.130    dut_inst/clk_c
    SLICE_X106Y520       FDRE                                         r  dut_inst/qdelay_3__15_.idx_0__ret_9[0]/C
                         clock pessimism             -0.441     2.689    
    SLICE_X106Y520       FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.055     2.744    dut_inst/qdelay_3__15_.idx_0__ret_9[0]
  -------------------------------------------------------------------
                         required time                         -2.744    
                         arrival time                           2.754    
  -------------------------------------------------------------------
                         slack                                  0.009    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 dut_inst/qdelay_2__15_.idx_0__ret[11]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            dut_inst/qdelay_3__15_.idx_0__ret[11]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.929ns
    Source Clock Delay      (SCD):    1.570ns
    Clock Pessimism Removal (CPR):    0.354ns
  Clock Net Delay (Source):      1.005ns (routing 0.524ns, distribution 0.481ns)
  Clock Net Delay (Destination): 1.138ns (routing 0.584ns, distribution 0.554ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.404     0.404 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.404    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.404 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.548    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.565 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1360, routed)        1.005     1.570    dut_inst/clk_c
    SLICE_X101Y497       FDRE                                         r  dut_inst/qdelay_2__15_.idx_0__ret[11]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y497       FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.039     1.609 r  dut_inst/qdelay_2__15_.idx_0__ret[11]/Q
                         net (fo=1, routed)           0.034     1.643    dut_inst/m_o_o[205]
    SLICE_X101Y497       FDRE                                         r  dut_inst/qdelay_3__15_.idx_0__ret[11]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.591     0.591 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.591    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.591 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.772    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.791 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1360, routed)        1.138     1.929    dut_inst/clk_c
    SLICE_X101Y497       FDRE                                         r  dut_inst/qdelay_3__15_.idx_0__ret[11]/C
                         clock pessimism             -0.354     1.576    
    SLICE_X101Y497       FDRE (Hold_BFF2_SLICEL_C_D)
                                                      0.047     1.623    dut_inst/qdelay_3__15_.idx_0__ret[11]
  -------------------------------------------------------------------
                         required time                         -1.623    
                         arrival time                           1.643    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 dut_inst/qdelay_2__15_.idx_0__ret[11]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            dut_inst/qdelay_3__15_.idx_0__ret[11]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.929ns
    Source Clock Delay      (SCD):    1.570ns
    Clock Pessimism Removal (CPR):    0.354ns
  Clock Net Delay (Source):      1.005ns (routing 0.524ns, distribution 0.481ns)
  Clock Net Delay (Destination): 1.138ns (routing 0.584ns, distribution 0.554ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.404     0.404 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.404    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.404 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.548    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.565 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1360, routed)        1.005     1.570    dut_inst/clk_c
    SLICE_X101Y497       FDRE                                         r  dut_inst/qdelay_2__15_.idx_0__ret[11]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y497       FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.039     1.609 f  dut_inst/qdelay_2__15_.idx_0__ret[11]/Q
                         net (fo=1, routed)           0.034     1.643    dut_inst/m_o_o[205]
    SLICE_X101Y497       FDRE                                         f  dut_inst/qdelay_3__15_.idx_0__ret[11]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.591     0.591 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.591    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.591 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.772    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.791 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1360, routed)        1.138     1.929    dut_inst/clk_c
    SLICE_X101Y497       FDRE                                         r  dut_inst/qdelay_3__15_.idx_0__ret[11]/C
                         clock pessimism             -0.354     1.576    
    SLICE_X101Y497       FDRE (Hold_BFF2_SLICEL_C_D)
                                                      0.047     1.623    dut_inst/qdelay_3__15_.idx_0__ret[11]
  -------------------------------------------------------------------
                         required time                         -1.623    
                         arrival time                           1.643    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 dut_inst/qdelay_2__15_.idx_0__ret_1[9]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            dut_inst/qdelay_3__15_.idx_0__ret_1[9]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.914ns
    Source Clock Delay      (SCD):    1.557ns
    Clock Pessimism Removal (CPR):    0.352ns
  Clock Net Delay (Source):      0.992ns (routing 0.524ns, distribution 0.468ns)
  Clock Net Delay (Destination): 1.123ns (routing 0.584ns, distribution 0.539ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.404     0.404 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.404    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.404 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.548    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.565 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1360, routed)        0.992     1.557    dut_inst/clk_c
    SLICE_X103Y499       FDRE                                         r  dut_inst/qdelay_2__15_.idx_0__ret_1[9]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X103Y499       FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.039     1.596 r  dut_inst/qdelay_2__15_.idx_0__ret_1[9]/Q
                         net (fo=1, routed)           0.034     1.630    dut_inst/m_o_o[168]
    SLICE_X103Y499       FDRE                                         r  dut_inst/qdelay_3__15_.idx_0__ret_1[9]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.591     0.591 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.591    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.591 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.772    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.791 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=1360, routed)        1.123     1.914    dut_inst/clk_c
    SLICE_X103Y499       FDRE                                         r  dut_inst/qdelay_3__15_.idx_0__ret_1[9]/C
                         clock pessimism             -0.352     1.563    
    SLICE_X103Y499       FDRE (Hold_BFF2_SLICEL_C_D)
                                                      0.047     1.610    dut_inst/qdelay_3__15_.idx_0__ret_1[9]
  -------------------------------------------------------------------
                         required time                         -1.610    
                         arrival time                           1.630    
  -------------------------------------------------------------------
                         slack                                  0.020    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 3.125 }
Period(ns):         6.250
Sources:            { clk }

Check Type        Corner  Lib Pin     Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I    n/a            1.499         6.250       4.751      BUFGCE_X1Y218   clk_ibuf/I
Min Period        n/a     SRL16E/CLK  n/a            1.146         6.250       5.104      SLICE_X99Y503   dut_inst/qdelay_2__15_.idx_0__ret_70_0_idx_0__ret_78_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         6.250       5.104      SLICE_X102Y517  dut_inst/qdelay_2__15_.idx_0__ret_72_0_idx_0__ret_78_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         6.250       5.104      SLICE_X100Y519  dut_inst/qdelay_2__15_.idx_0__ret_74_0_idx_0__ret_78_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         6.250       5.104      SLICE_X102Y517  dut_inst/qdelay_2__15_.idx_0__ret_76_0_idx_0__ret_78_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X100Y497  dut_inst/qdelay_1__15_.idx_0__ret_316_0_idx_0__ret_78_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X100Y497  dut_inst/qdelay_1__15_.idx_0__ret_98_0_idx_0__ret_78_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X99Y503   dut_inst/qdelay_2__15_.idx_0__ret_70_0_idx_0__ret_78_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X100Y519  dut_inst/qdelay_2__15_.idx_0__ret_74_0_idx_0__ret_78_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X100Y519  dut_inst/qdelay_2__15_.idx_0__ret_78_0_idx_0__ret_78_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X99Y503   dut_inst/qdelay_2__15_.idx_0__ret_70_0_idx_0__ret_78_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X104Y520  dut_inst/qdelay_1__15_.idx_0__ret_496_0_idx_0__ret_78_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X99Y503   dut_inst/qdelay_2__15_.idx_0__ret_64_0_idx_0__ret_78_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X99Y503   dut_inst/qdelay_2__15_.idx_0__ret_66_0_idx_0__ret_78_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         3.125       2.552      SLICE_X99Y503   dut_inst/qdelay_2__15_.idx_0__ret_68_0_idx_0__ret_78_1/CLK



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y518  dout/C
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y517  lfsr_din/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y224          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X102Y498         input_lfsr/output_vector_1[207]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X104Y511         input_lfsr/shiftreg_vector[0]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  dout/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y517  lfsr_din/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  dout/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y517  lfsr_din/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X100Y509         output_reducer/delay_block[1][39]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  dout/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y517  lfsr_din/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  dout/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y517  lfsr_din/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y511         input_lfsr/shiftreg_vector[0]/C



