Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Mon Apr  8 18:57:28 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.167        0.000                      0                  208        1.511        0.000                       0                   935  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.786}        3.572           279.955         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.167        0.000                      0                  208        1.511        0.000                       0                   447  
clk_wrapper                                                                             498.562        0.000                       0                   488  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.167ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.511ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.167ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[115]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[206]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.265ns  (logic 0.115ns (43.396%)  route 0.150ns (56.604%))
  Logic Levels:           2  (LUT5=2)
  Clock Path Skew:        0.052ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.000ns
    Source Clock Delay      (SCD):    1.630ns
    Clock Pessimism Removal (CPR):    0.318ns
  Clock Net Delay (Source):      1.084ns (routing 0.638ns, distribution 0.446ns)
  Clock Net Delay (Destination): 1.228ns (routing 0.707ns, distribution 0.521ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=446, routed)         1.084     1.630    shift_reg_tap_i/clk_c
    SLICE_X96Y452        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[115]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y452        FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.039     1.669 r  shift_reg_tap_i/sr_p.sr_1[115]/Q
                         net (fo=3, routed)           0.028     1.697    dut_inst/stage_g.22.csn_cmp_inst/input_slr_10
    SLICE_X96Y452        LUT5 (Prop_H6LUT_SLICEL_I0_O)
                                                      0.041     1.738 r  dut_inst/stage_g.22.csn_cmp_inst/a_o_comb.idx_lut6_2_o5_lut6_2_o6[7]/O
                         net (fo=4, routed)           0.101     1.839    dut_inst/stage_g.51.csn_cmp_inst/idx_5_0
    SLICE_X97Y452        LUT5 (Prop_B6LUT_SLICEM_I0_O)
                                                      0.035     1.874 r  dut_inst/stage_g.51.csn_cmp_inst/b_o_comb.idx[7]/O
                         net (fo=1, routed)           0.021     1.895    shift_reg_tap_o/idx_7[7]
    SLICE_X97Y452        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[206]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=446, routed)         1.228     2.000    shift_reg_tap_o/clk_c
    SLICE_X97Y452        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[206]/C
                         clock pessimism             -0.318     1.682    
    SLICE_X97Y452        FDRE (Hold_BFF_SLICEM_C_D)
                                                      0.046     1.728    shift_reg_tap_o/sr_p.sr_1[206]
  -------------------------------------------------------------------
                         required time                         -1.728    
                         arrival time                           1.895    
  -------------------------------------------------------------------
                         slack                                  0.167    

Slack (MET) :             0.167ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[115]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[206]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.265ns  (logic 0.115ns (43.396%)  route 0.150ns (56.604%))
  Logic Levels:           2  (LUT5=2)
  Clock Path Skew:        0.052ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.000ns
    Source Clock Delay      (SCD):    1.630ns
    Clock Pessimism Removal (CPR):    0.318ns
  Clock Net Delay (Source):      1.084ns (routing 0.638ns, distribution 0.446ns)
  Clock Net Delay (Destination): 1.228ns (routing 0.707ns, distribution 0.521ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=446, routed)         1.084     1.630    shift_reg_tap_i/clk_c
    SLICE_X96Y452        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[115]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y452        FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.039     1.669 f  shift_reg_tap_i/sr_p.sr_1[115]/Q
                         net (fo=3, routed)           0.028     1.697    dut_inst/stage_g.22.csn_cmp_inst/input_slr_10
    SLICE_X96Y452        LUT5 (Prop_H6LUT_SLICEL_I0_O)
                                                      0.041     1.738 f  dut_inst/stage_g.22.csn_cmp_inst/a_o_comb.idx_lut6_2_o5_lut6_2_o6[7]/O
                         net (fo=4, routed)           0.101     1.839    dut_inst/stage_g.51.csn_cmp_inst/idx_5_0
    SLICE_X97Y452        LUT5 (Prop_B6LUT_SLICEM_I0_O)
                                                      0.035     1.874 f  dut_inst/stage_g.51.csn_cmp_inst/b_o_comb.idx[7]/O
                         net (fo=1, routed)           0.021     1.895    shift_reg_tap_o/idx_7[7]
    SLICE_X97Y452        FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[206]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=446, routed)         1.228     2.000    shift_reg_tap_o/clk_c
    SLICE_X97Y452        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[206]/C
                         clock pessimism             -0.318     1.682    
    SLICE_X97Y452        FDRE (Hold_BFF_SLICEM_C_D)
                                                      0.046     1.728    shift_reg_tap_o/sr_p.sr_1[206]
  -------------------------------------------------------------------
                         required time                         -1.728    
                         arrival time                           1.895    
  -------------------------------------------------------------------
                         slack                                  0.167    

Slack (MET) :             0.167ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[120]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[2]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.261ns  (logic 0.087ns (33.333%)  route 0.174ns (66.667%))
  Logic Levels:           2  (LUT4=1 LUT6=1)
  Clock Path Skew:        0.048ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.996ns
    Source Clock Delay      (SCD):    1.630ns
    Clock Pessimism Removal (CPR):    0.318ns
  Clock Net Delay (Source):      1.084ns (routing 0.638ns, distribution 0.446ns)
  Clock Net Delay (Destination): 1.224ns (routing 0.707ns, distribution 0.517ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=446, routed)         1.084     1.630    shift_reg_tap_i/clk_c
    SLICE_X93Y446        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[120]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X93Y446        FDRE (Prop_AFF_SLICEM_C_Q)
                                                      0.038     1.668 r  shift_reg_tap_i/sr_p.sr_1[120]/Q
                         net (fo=12, routed)          0.093     1.761    dut_inst/stage_g.29.csn_cmp_inst/input_slr_13
    SLICE_X92Y446        LUT6 (Prop_H6LUT_SLICEM_I1_O)
                                                      0.035     1.796 r  dut_inst/stage_g.29.csn_cmp_inst/a_o_comb.pt[3]/O
                         net (fo=5, routed)           0.055     1.851    dut_inst/stage_g.38.csn_cmp_inst/pt_77_0
    SLICE_X92Y445        LUT4 (Prop_D6LUT_SLICEM_I1_O)
                                                      0.014     1.865 r  dut_inst/stage_g.38.csn_cmp_inst/a_o_comb.pt[2]/O
                         net (fo=1, routed)           0.026     1.891    shift_reg_tap_o/pt_6[2]
    SLICE_X92Y445        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[2]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=446, routed)         1.224     1.996    shift_reg_tap_o/clk_c
    SLICE_X92Y445        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[2]/C
                         clock pessimism             -0.318     1.678    
    SLICE_X92Y445        FDRE (Hold_DFF_SLICEM_C_D)
                                                      0.046     1.724    shift_reg_tap_o/sr_p.sr_1[2]
  -------------------------------------------------------------------
                         required time                         -1.724    
                         arrival time                           1.891    
  -------------------------------------------------------------------
                         slack                                  0.167    

Slack (MET) :             0.167ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[120]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[2]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.261ns  (logic 0.087ns (33.333%)  route 0.174ns (66.667%))
  Logic Levels:           2  (LUT4=1 LUT6=1)
  Clock Path Skew:        0.048ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.996ns
    Source Clock Delay      (SCD):    1.630ns
    Clock Pessimism Removal (CPR):    0.318ns
  Clock Net Delay (Source):      1.084ns (routing 0.638ns, distribution 0.446ns)
  Clock Net Delay (Destination): 1.224ns (routing 0.707ns, distribution 0.517ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=446, routed)         1.084     1.630    shift_reg_tap_i/clk_c
    SLICE_X93Y446        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[120]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X93Y446        FDRE (Prop_AFF_SLICEM_C_Q)
                                                      0.038     1.668 f  shift_reg_tap_i/sr_p.sr_1[120]/Q
                         net (fo=12, routed)          0.093     1.761    dut_inst/stage_g.29.csn_cmp_inst/input_slr_13
    SLICE_X92Y446        LUT6 (Prop_H6LUT_SLICEM_I1_O)
                                                      0.035     1.796 f  dut_inst/stage_g.29.csn_cmp_inst/a_o_comb.pt[3]/O
                         net (fo=5, routed)           0.055     1.851    dut_inst/stage_g.38.csn_cmp_inst/pt_77_0
    SLICE_X92Y445        LUT4 (Prop_D6LUT_SLICEM_I1_O)
                                                      0.014     1.865 r  dut_inst/stage_g.38.csn_cmp_inst/a_o_comb.pt[2]/O
                         net (fo=1, routed)           0.026     1.891    shift_reg_tap_o/pt_6[2]
    SLICE_X92Y445        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[2]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=446, routed)         1.224     1.996    shift_reg_tap_o/clk_c
    SLICE_X92Y445        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[2]/C
                         clock pessimism             -0.318     1.678    
    SLICE_X92Y445        FDRE (Hold_DFF_SLICEM_C_D)
                                                      0.046     1.724    shift_reg_tap_o/sr_p.sr_1[2]
  -------------------------------------------------------------------
                         required time                         -1.724    
                         arrival time                           1.891    
  -------------------------------------------------------------------
                         slack                                  0.167    

Slack (MET) :             0.167ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[120]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[2]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.261ns  (logic 0.087ns (33.333%)  route 0.174ns (66.667%))
  Logic Levels:           2  (LUT4=1 LUT6=1)
  Clock Path Skew:        0.048ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.996ns
    Source Clock Delay      (SCD):    1.630ns
    Clock Pessimism Removal (CPR):    0.318ns
  Clock Net Delay (Source):      1.084ns (routing 0.638ns, distribution 0.446ns)
  Clock Net Delay (Destination): 1.224ns (routing 0.707ns, distribution 0.517ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=446, routed)         1.084     1.630    shift_reg_tap_i/clk_c
    SLICE_X93Y446        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[120]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X93Y446        FDRE (Prop_AFF_SLICEM_C_Q)
                                                      0.038     1.668 r  shift_reg_tap_i/sr_p.sr_1[120]/Q
                         net (fo=12, routed)          0.093     1.761    dut_inst/stage_g.29.csn_cmp_inst/input_slr_13
    SLICE_X92Y446        LUT6 (Prop_H6LUT_SLICEM_I1_O)
                                                      0.035     1.796 r  dut_inst/stage_g.29.csn_cmp_inst/a_o_comb.pt[3]/O
                         net (fo=5, routed)           0.055     1.851    dut_inst/stage_g.38.csn_cmp_inst/pt_77_0
    SLICE_X92Y445        LUT4 (Prop_D6LUT_SLICEM_I1_O)
                                                      0.014     1.865 f  dut_inst/stage_g.38.csn_cmp_inst/a_o_comb.pt[2]/O
                         net (fo=1, routed)           0.026     1.891    shift_reg_tap_o/pt_6[2]
    SLICE_X92Y445        FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[2]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=446, routed)         1.224     1.996    shift_reg_tap_o/clk_c
    SLICE_X92Y445        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[2]/C
                         clock pessimism             -0.318     1.678    
    SLICE_X92Y445        FDRE (Hold_DFF_SLICEM_C_D)
                                                      0.046     1.724    shift_reg_tap_o/sr_p.sr_1[2]
  -------------------------------------------------------------------
                         required time                         -1.724    
                         arrival time                           1.891    
  -------------------------------------------------------------------
                         slack                                  0.167    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.786 }
Period(ns):         3.572
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         3.572       2.073      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X97Y453   shift_reg_tap_i/sr_p.sr_1[147]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X97Y453   shift_reg_tap_i/sr_p.sr_1[148]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X95Y456   shift_reg_tap_i/sr_p.sr_1[149]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X101Y449  shift_reg_tap_i/sr_p.sr_1[14]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X94Y453   shift_reg_tap_i/sr_p.sr_1[163]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X100Y438  shift_reg_tap_o/sr_p.sr_1[181]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X102Y439  shift_reg_tap_o/sr_p.sr_1[184]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X94Y453   shift_reg_tap_i/sr_p.sr_1[176]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X100Y438  shift_reg_tap_o/sr_p.sr_1[194]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X97Y453   shift_reg_tap_i/sr_p.sr_1[147]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X97Y453   shift_reg_tap_i/sr_p.sr_1[147]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X97Y453   shift_reg_tap_i/sr_p.sr_1[148]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X97Y453   shift_reg_tap_i/sr_p.sr_1[148]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X95Y456   shift_reg_tap_i/sr_p.sr_1[149]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X92Y454          lsfr_1/shiftreg_vector[138]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X92Y454          lsfr_1/shiftreg_vector[139]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X102Y454         lsfr_1/shiftreg_vector[13]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X102Y454         lsfr_1/shiftreg_vector[13]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X101Y455         lsfr_1/shiftreg_vector[72]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X101Y455         lsfr_1/shiftreg_vector[73]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X92Y454          lsfr_1/shiftreg_vector[138]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X92Y454          lsfr_1/shiftreg_vector[139]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X102Y454         lsfr_1/shiftreg_vector[13]/C



