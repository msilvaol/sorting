Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Mon Mar 11 17:55:23 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper_csn
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+------+--------+
|  Fanout | Nets |      % |
+---------+------+--------+
|       1 | 2465 |  52.23 |
|       2 |  875 |  18.54 |
|       3 |  323 |   6.84 |
|       4 |  233 |   4.93 |
|    5-10 |  457 |   9.68 |
|   11-50 |  359 |   7.60 |
|  51-100 |    7 |   0.14 |
| 101-500 |    0 |   0.00 |
|    >500 |    0 |   0.00 |
+---------+------+--------+
|     ALL | 4719 | 100.00 |
+---------+------+--------+


