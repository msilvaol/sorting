Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Sat Feb  9 11:16:17 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.006        0.000                      0                13758       12.225        0.000                       0                 15126  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 12.500}       25.000          40.000          
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.006        0.000                      0                13758       12.225        0.000                       0                 14271  
clk_wrapper                                                                             498.562        0.000                       0                   855  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.006ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack       12.225ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.006ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_6[343]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_i/sr_p.sr_7[343]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.208ns  (logic 0.070ns (33.654%)  route 0.138ns (66.346%))
  Logic Levels:           0  
  Clock Path Skew:        0.147ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.381ns
    Source Clock Delay      (SCD):    2.824ns
    Clock Pessimism Removal (CPR):    0.410ns
  Clock Net Delay (Source):      1.994ns (routing 0.794ns, distribution 1.200ns)
  Clock Net Delay (Destination): 2.240ns (routing 0.869ns, distribution 1.371ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y9 (CLOCK_ROOT)    net (fo=14270, routed)       1.994     2.824    shift_reg_tap_i/clk_c
    SLICE_X67Y477        FDRE                                         r  shift_reg_tap_i/sr_p.sr_6[343]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X67Y477        FDRE (Prop_DFF_SLICEL_C_Q)
                                                      0.070     2.894 r  shift_reg_tap_i/sr_p.sr_6[343]/Q
                         net (fo=1, routed)           0.138     3.032    shift_reg_tap_i/sr_6[343]
    SLICE_X67Y481        FDRE                                         r  shift_reg_tap_i/sr_p.sr_7[343]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y9 (CLOCK_ROOT)    net (fo=14270, routed)       2.240     3.381    shift_reg_tap_i/clk_c
    SLICE_X67Y481        FDRE                                         r  shift_reg_tap_i/sr_p.sr_7[343]/C
                         clock pessimism             -0.410     2.971    
    SLICE_X67Y481        FDRE (Hold_BFF2_SLICEL_C_D)
                                                      0.055     3.026    shift_reg_tap_i/sr_p.sr_7[343]
  -------------------------------------------------------------------
                         required time                         -3.026    
                         arrival time                           3.032    
  -------------------------------------------------------------------
                         slack                                  0.006    

Slack (MET) :             0.006ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_6[343]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_i/sr_p.sr_7[343]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.208ns  (logic 0.070ns (33.654%)  route 0.138ns (66.346%))
  Logic Levels:           0  
  Clock Path Skew:        0.147ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.381ns
    Source Clock Delay      (SCD):    2.824ns
    Clock Pessimism Removal (CPR):    0.410ns
  Clock Net Delay (Source):      1.994ns (routing 0.794ns, distribution 1.200ns)
  Clock Net Delay (Destination): 2.240ns (routing 0.869ns, distribution 1.371ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y9 (CLOCK_ROOT)    net (fo=14270, routed)       1.994     2.824    shift_reg_tap_i/clk_c
    SLICE_X67Y477        FDRE                                         r  shift_reg_tap_i/sr_p.sr_6[343]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X67Y477        FDRE (Prop_DFF_SLICEL_C_Q)
                                                      0.070     2.894 f  shift_reg_tap_i/sr_p.sr_6[343]/Q
                         net (fo=1, routed)           0.138     3.032    shift_reg_tap_i/sr_6[343]
    SLICE_X67Y481        FDRE                                         f  shift_reg_tap_i/sr_p.sr_7[343]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y9 (CLOCK_ROOT)    net (fo=14270, routed)       2.240     3.381    shift_reg_tap_i/clk_c
    SLICE_X67Y481        FDRE                                         r  shift_reg_tap_i/sr_p.sr_7[343]/C
                         clock pessimism             -0.410     2.971    
    SLICE_X67Y481        FDRE (Hold_BFF2_SLICEL_C_D)
                                                      0.055     3.026    shift_reg_tap_i/sr_p.sr_7[343]
  -------------------------------------------------------------------
                         required time                         -3.026    
                         arrival time                           3.032    
  -------------------------------------------------------------------
                         slack                                  0.006    

Slack (MET) :             0.016ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_11[253]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_i/sr_p.sr_12[253]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.205ns  (logic 0.069ns (33.659%)  route 0.136ns (66.342%))
  Logic Levels:           0  
  Clock Path Skew:        0.136ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.374ns
    Source Clock Delay      (SCD):    2.828ns
    Clock Pessimism Removal (CPR):    0.410ns
  Clock Net Delay (Source):      1.998ns (routing 0.794ns, distribution 1.204ns)
  Clock Net Delay (Destination): 2.233ns (routing 0.869ns, distribution 1.364ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y9 (CLOCK_ROOT)    net (fo=14270, routed)       1.998     2.828    shift_reg_tap_i/clk_c
    SLICE_X67Y478        FDRE                                         r  shift_reg_tap_i/sr_p.sr_11[253]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X67Y478        FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     2.897 r  shift_reg_tap_i/sr_p.sr_11[253]/Q
                         net (fo=1, routed)           0.136     3.033    shift_reg_tap_i/sr_11[253]
    SLICE_X67Y484        FDRE                                         r  shift_reg_tap_i/sr_p.sr_12[253]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y9 (CLOCK_ROOT)    net (fo=14270, routed)       2.233     3.374    shift_reg_tap_i/clk_c
    SLICE_X67Y484        FDRE                                         r  shift_reg_tap_i/sr_p.sr_12[253]/C
                         clock pessimism             -0.410     2.964    
    SLICE_X67Y484        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.053     3.017    shift_reg_tap_i/sr_p.sr_12[253]
  -------------------------------------------------------------------
                         required time                         -3.017    
                         arrival time                           3.033    
  -------------------------------------------------------------------
                         slack                                  0.016    

Slack (MET) :             0.016ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_11[253]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_i/sr_p.sr_12[253]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.205ns  (logic 0.069ns (33.659%)  route 0.136ns (66.342%))
  Logic Levels:           0  
  Clock Path Skew:        0.136ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.374ns
    Source Clock Delay      (SCD):    2.828ns
    Clock Pessimism Removal (CPR):    0.410ns
  Clock Net Delay (Source):      1.998ns (routing 0.794ns, distribution 1.204ns)
  Clock Net Delay (Destination): 2.233ns (routing 0.869ns, distribution 1.364ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y9 (CLOCK_ROOT)    net (fo=14270, routed)       1.998     2.828    shift_reg_tap_i/clk_c
    SLICE_X67Y478        FDRE                                         r  shift_reg_tap_i/sr_p.sr_11[253]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X67Y478        FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     2.897 f  shift_reg_tap_i/sr_p.sr_11[253]/Q
                         net (fo=1, routed)           0.136     3.033    shift_reg_tap_i/sr_11[253]
    SLICE_X67Y484        FDRE                                         f  shift_reg_tap_i/sr_p.sr_12[253]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y9 (CLOCK_ROOT)    net (fo=14270, routed)       2.233     3.374    shift_reg_tap_i/clk_c
    SLICE_X67Y484        FDRE                                         r  shift_reg_tap_i/sr_p.sr_12[253]/C
                         clock pessimism             -0.410     2.964    
    SLICE_X67Y484        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.053     3.017    shift_reg_tap_i/sr_p.sr_12[253]
  -------------------------------------------------------------------
                         required time                         -3.017    
                         arrival time                           3.033    
  -------------------------------------------------------------------
                         slack                                  0.016    

Slack (MET) :             0.017ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_13[20]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Destination:            shift_reg_tap_o/sr_p.sr_14[20]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@12.500ns period=25.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.183ns  (logic 0.069ns (37.705%)  route 0.114ns (62.295%))
  Logic Levels:           0  
  Clock Path Skew:        0.111ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.391ns
    Source Clock Delay      (SCD):    2.823ns
    Clock Pessimism Removal (CPR):    0.458ns
  Clock Net Delay (Source):      1.993ns (routing 0.794ns, distribution 1.199ns)
  Clock Net Delay (Destination): 2.250ns (routing 0.869ns, distribution 1.381ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y9 (CLOCK_ROOT)    net (fo=14270, routed)       1.993     2.823    shift_reg_tap_o/clk_c
    SLICE_X78Y485        FDRE                                         r  shift_reg_tap_o/sr_p.sr_13[20]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X78Y485        FDRE (Prop_FFF_SLICEM_C_Q)
                                                      0.069     2.892 r  shift_reg_tap_o/sr_p.sr_13[20]/Q
                         net (fo=1, routed)           0.114     3.006    shift_reg_tap_o/sr_13[20]
    SLICE_X77Y484        FDRE                                         r  shift_reg_tap_o/sr_p.sr_14[20]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y9 (CLOCK_ROOT)    net (fo=14270, routed)       2.250     3.391    shift_reg_tap_o/clk_c
    SLICE_X77Y484        FDRE                                         r  shift_reg_tap_o/sr_p.sr_14[20]/C
                         clock pessimism             -0.458     2.934    
    SLICE_X77Y484        FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.055     2.989    shift_reg_tap_o/sr_p.sr_14[20]
  -------------------------------------------------------------------
                         required time                         -2.989    
                         arrival time                           3.006    
  -------------------------------------------------------------------
                         slack                                  0.017    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 12.500 }
Period(ns):         25.000
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location       Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         25.000      23.501     BUFGCE_X1Y224  clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X63Y508  muon_sorter_1/sr_p.sr_1_15.sector_ret_1739/C
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X64Y498  muon_sorter_1/sr_p.sr_1_15.sector_ret_174/C
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X62Y512  muon_sorter_1/sr_p.sr_1_15.sector_ret_1744/C
Min Period        n/a     FDRE/C    n/a            0.550         25.000      24.450     SLICE_X67Y507  muon_sorter_1/sr_p.sr_1_15.sector_ret_1750/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X64Y467  shift_reg_tap_i/sr_p.sr_11[224]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X77Y500  shift_reg_tap_o/sr_p.sr_3[116]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X71Y474  shift_reg_tap_o/sr_p.sr_11[114]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X67Y475  shift_reg_tap_i/sr_p.sr_11[270]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X87Y492  shift_reg_tap_i/sr_p.sr_3[316]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X64Y498  muon_sorter_1/sr_p.sr_1_15.sector_ret_174/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X62Y512  muon_sorter_1/sr_p.sr_1_15.sector_ret_1744/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X67Y507  muon_sorter_1/sr_p.sr_1_15.sector_ret_1750/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X86Y551  shift_reg_tap_i/sr_p.sr_11[20]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         12.500      12.225     SLICE_X66Y485  shift_reg_tap_i/sr_p.sr_11[210]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X87Y520          lsfr_1/output_vector_1[511]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X88Y520          lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X60Y483          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X87Y520          lsfr_1/output_vector_1[511]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X60Y483          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X60Y483          lsfr_1/shiftreg_vector[101]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X88Y520          lsfr_1/shiftreg_vector[0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X60Y483          lsfr_1/shiftreg_vector[100]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X60Y483          lsfr_1/shiftreg_vector[101]/C



