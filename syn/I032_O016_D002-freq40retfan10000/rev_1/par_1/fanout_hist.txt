Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Sat Feb  9 11:20:48 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+-------+--------+
|  Fanout |  Nets |      % |
+---------+-------+--------+
|       1 | 20645 |  61.33 |
|       2 |  4205 |  12.49 |
|       3 |  3471 |  10.31 |
|       4 |  1686 |   5.00 |
|    5-10 |  1255 |   3.72 |
|   11-50 |  2262 |   6.72 |
|  51-100 |   133 |   0.39 |
| 101-500 |     0 |   0.00 |
|    >500 |     0 |   0.00 |
+---------+-------+--------+
|     ALL | 33657 | 100.00 |
+---------+-------+--------+


