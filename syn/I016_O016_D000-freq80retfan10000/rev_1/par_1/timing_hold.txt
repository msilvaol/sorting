Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Tue Feb  5 21:25:46 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.016        0.000                      0                 7858        5.975        0.000                       0                  8714  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 6.250}        12.500          80.000          
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.016        0.000                      0                 7858        5.975        0.000                       0                  8115  
clk_wrapper                                                                             498.562        0.000                       0                   599  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.016ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        5.975ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.016ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_10[144]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_11[144]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.188ns  (logic 0.070ns (37.234%)  route 0.118ns (62.766%))
  Logic Levels:           0  
  Clock Path Skew:        0.117ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.988ns
    Source Clock Delay      (SCD):    3.359ns
    Clock Pessimism Removal (CPR):    0.512ns
  Clock Net Delay (Source):      2.529ns (routing 1.566ns, distribution 0.963ns)
  Clock Net Delay (Destination): 2.847ns (routing 1.714ns, distribution 1.133ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=8114, routed)        2.529     3.359    shift_reg_tap_o/clk_c
    SLICE_X82Y391        FDRE                                         r  shift_reg_tap_o/sr_p.sr_10[144]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X82Y391        FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.070     3.429 r  shift_reg_tap_o/sr_p.sr_10[144]/Q
                         net (fo=1, routed)           0.118     3.547    shift_reg_tap_o/sr_10[144]
    SLICE_X82Y388        FDRE                                         r  shift_reg_tap_o/sr_p.sr_11[144]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=8114, routed)        2.847     3.988    shift_reg_tap_o/clk_c
    SLICE_X82Y388        FDRE                                         r  shift_reg_tap_o/sr_p.sr_11[144]/C
                         clock pessimism             -0.512     3.476    
    SLICE_X82Y388        FDRE (Hold_AFF2_SLICEL_C_D)
                                                      0.055     3.531    shift_reg_tap_o/sr_p.sr_11[144]
  -------------------------------------------------------------------
                         required time                         -3.531    
                         arrival time                           3.547    
  -------------------------------------------------------------------
                         slack                                  0.016    

Slack (MET) :             0.016ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_10[144]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_11[144]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.188ns  (logic 0.070ns (37.234%)  route 0.118ns (62.766%))
  Logic Levels:           0  
  Clock Path Skew:        0.117ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.988ns
    Source Clock Delay      (SCD):    3.359ns
    Clock Pessimism Removal (CPR):    0.512ns
  Clock Net Delay (Source):      2.529ns (routing 1.566ns, distribution 0.963ns)
  Clock Net Delay (Destination): 2.847ns (routing 1.714ns, distribution 1.133ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=8114, routed)        2.529     3.359    shift_reg_tap_o/clk_c
    SLICE_X82Y391        FDRE                                         r  shift_reg_tap_o/sr_p.sr_10[144]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X82Y391        FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.070     3.429 f  shift_reg_tap_o/sr_p.sr_10[144]/Q
                         net (fo=1, routed)           0.118     3.547    shift_reg_tap_o/sr_10[144]
    SLICE_X82Y388        FDRE                                         f  shift_reg_tap_o/sr_p.sr_11[144]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=8114, routed)        2.847     3.988    shift_reg_tap_o/clk_c
    SLICE_X82Y388        FDRE                                         r  shift_reg_tap_o/sr_p.sr_11[144]/C
                         clock pessimism             -0.512     3.476    
    SLICE_X82Y388        FDRE (Hold_AFF2_SLICEL_C_D)
                                                      0.055     3.531    shift_reg_tap_o/sr_p.sr_11[144]
  -------------------------------------------------------------------
                         required time                         -3.531    
                         arrival time                           3.547    
  -------------------------------------------------------------------
                         slack                                  0.016    

Slack (MET) :             0.016ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_10[127]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_11[127]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.325ns  (logic 0.072ns (22.154%)  route 0.253ns (77.846%))
  Logic Levels:           0  
  Clock Path Skew:        0.255ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.223ns
    Source Clock Delay      (SCD):    3.509ns
    Clock Pessimism Removal (CPR):    0.459ns
  Clock Net Delay (Source):      2.679ns (routing 1.566ns, distribution 1.113ns)
  Clock Net Delay (Destination): 3.082ns (routing 1.714ns, distribution 1.368ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=8114, routed)        2.679     3.509    shift_reg_tap_o/clk_c
    SLICE_X99Y419        FDRE                                         r  shift_reg_tap_o/sr_p.sr_10[127]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X99Y419        FDRE (Prop_GFF2_SLICEM_C_Q)
                                                      0.072     3.581 r  shift_reg_tap_o/sr_p.sr_10[127]/Q
                         net (fo=1, routed)           0.253     3.834    shift_reg_tap_o/sr_10[127]
    SLICE_X100Y420       FDRE                                         r  shift_reg_tap_o/sr_p.sr_11[127]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=8114, routed)        3.082     4.223    shift_reg_tap_o/clk_c
    SLICE_X100Y420       FDRE                                         r  shift_reg_tap_o/sr_p.sr_11[127]/C
                         clock pessimism             -0.459     3.764    
    SLICE_X100Y420       FDRE (Hold_CFF2_SLICEM_C_D)
                                                      0.054     3.818    shift_reg_tap_o/sr_p.sr_11[127]
  -------------------------------------------------------------------
                         required time                         -3.818    
                         arrival time                           3.834    
  -------------------------------------------------------------------
                         slack                                  0.016    

Slack (MET) :             0.016ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_10[127]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_11[127]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.325ns  (logic 0.072ns (22.154%)  route 0.253ns (77.846%))
  Logic Levels:           0  
  Clock Path Skew:        0.255ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.223ns
    Source Clock Delay      (SCD):    3.509ns
    Clock Pessimism Removal (CPR):    0.459ns
  Clock Net Delay (Source):      2.679ns (routing 1.566ns, distribution 1.113ns)
  Clock Net Delay (Destination): 3.082ns (routing 1.714ns, distribution 1.368ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=8114, routed)        2.679     3.509    shift_reg_tap_o/clk_c
    SLICE_X99Y419        FDRE                                         r  shift_reg_tap_o/sr_p.sr_10[127]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X99Y419        FDRE (Prop_GFF2_SLICEM_C_Q)
                                                      0.072     3.581 f  shift_reg_tap_o/sr_p.sr_10[127]/Q
                         net (fo=1, routed)           0.253     3.834    shift_reg_tap_o/sr_10[127]
    SLICE_X100Y420       FDRE                                         f  shift_reg_tap_o/sr_p.sr_11[127]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=8114, routed)        3.082     4.223    shift_reg_tap_o/clk_c
    SLICE_X100Y420       FDRE                                         r  shift_reg_tap_o/sr_p.sr_11[127]/C
                         clock pessimism             -0.459     3.764    
    SLICE_X100Y420       FDRE (Hold_CFF2_SLICEM_C_D)
                                                      0.054     3.818    shift_reg_tap_o/sr_p.sr_11[127]
  -------------------------------------------------------------------
                         required time                         -3.818    
                         arrival time                           3.834    
  -------------------------------------------------------------------
                         slack                                  0.016    

Slack (MET) :             0.016ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_5[80]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_6[80]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.206ns  (logic 0.071ns (34.466%)  route 0.135ns (65.534%))
  Logic Levels:           0  
  Clock Path Skew:        0.135ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.154ns
    Source Clock Delay      (SCD):    3.496ns
    Clock Pessimism Removal (CPR):    0.524ns
  Clock Net Delay (Source):      2.666ns (routing 1.566ns, distribution 1.100ns)
  Clock Net Delay (Destination): 3.013ns (routing 1.714ns, distribution 1.299ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=8114, routed)        2.666     3.496    shift_reg_tap_o/clk_c
    SLICE_X96Y416        FDRE                                         r  shift_reg_tap_o/sr_p.sr_5[80]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y416        FDRE (Prop_BFF2_SLICEL_C_Q)
                                                      0.071     3.567 r  shift_reg_tap_o/sr_p.sr_5[80]/Q
                         net (fo=1, routed)           0.135     3.702    shift_reg_tap_o/sr_5[80]
    SLICE_X97Y418        FDRE                                         r  shift_reg_tap_o/sr_p.sr_6[80]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=8114, routed)        3.013     4.154    shift_reg_tap_o/clk_c
    SLICE_X97Y418        FDRE                                         r  shift_reg_tap_o/sr_p.sr_6[80]/C
                         clock pessimism             -0.524     3.630    
    SLICE_X97Y418        FDRE (Hold_GFF2_SLICEM_C_D)
                                                      0.055     3.685    shift_reg_tap_o/sr_p.sr_6[80]
  -------------------------------------------------------------------
                         required time                         -3.685    
                         arrival time                           3.702    
  -------------------------------------------------------------------
                         slack                                  0.016    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 6.250 }
Period(ns):         12.500
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location       Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         12.500      11.001     BUFGCE_X1Y224  clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X84Y347  shift_reg_tap_i/sr_p.sr_10[211]/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X88Y361  shift_reg_tap_i/sr_p.sr_10[212]/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X89Y350  shift_reg_tap_i/sr_p.sr_10[213]/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X88Y362  shift_reg_tap_i/sr_p.sr_10[214]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X91Y416  shift_reg_tap_i/sr_p.sr_13[57]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X91Y416  shift_reg_tap_i/sr_p.sr_13[58]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X90Y418  shift_reg_tap_i/sr_p.sr_13[60]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X90Y418  shift_reg_tap_i/sr_p.sr_13[63]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X92Y433  shift_reg_tap_i/sr_p.sr_2[120]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X83Y349  shift_reg_tap_i/sr_p.sr_10[217]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X83Y350  shift_reg_tap_i/sr_p.sr_10[221]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X99Y380  shift_reg_tap_i/sr_p.sr_13[4]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X90Y418  shift_reg_tap_i/sr_p.sr_13[60]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X90Y418  shift_reg_tap_i/sr_p.sr_13[63]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X104Y366         lsfr_1/output_vector_1[255]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X105Y374         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X97Y440          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y371         lsfr_1/shiftreg_vector[10]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X94Y434          lsfr_1/shiftreg_vector[112]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X91Y434          lsfr_1/shiftreg_vector[122]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y366         lsfr_1/output_vector_1[255]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X97Y440          lsfr_1/shiftreg_vector[100]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X97Y438          lsfr_1/shiftreg_vector[101]/C



