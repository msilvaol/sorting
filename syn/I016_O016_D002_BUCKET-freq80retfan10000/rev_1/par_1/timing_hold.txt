Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Thu Feb 21 22:19:22 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_bucket
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 249 combinational loops in the design. (HIGH)


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.019        0.000                      0                 2110        5.975        0.000                       0                  2966  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 6.250}        12.500          80.000          
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.019        0.000                      0                 2110        5.975        0.000                       0                  2367  
clk_wrapper                                                                             498.562        0.000                       0                   599  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.019ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        5.975ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_2[115]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_3[115]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.217ns
    Source Clock Delay      (SCD):    1.831ns
    Clock Pessimism Removal (CPR):    0.380ns
  Clock Net Delay (Source):      1.285ns (routing 0.638ns, distribution 0.647ns)
  Clock Net Delay (Destination): 1.445ns (routing 0.709ns, distribution 0.736ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=2366, routed)        1.285     1.831    shift_reg_tap_i/clk_c
    SLICE_X98Y385        FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[115]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y385        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.870 r  shift_reg_tap_i/sr_p.sr_2[115]/Q
                         net (fo=1, routed)           0.033     1.903    shift_reg_tap_i/sr_2[115]
    SLICE_X98Y385        FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[115]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=2366, routed)        1.445     2.217    shift_reg_tap_i/clk_c
    SLICE_X98Y385        FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[115]/C
                         clock pessimism             -0.380     1.837    
    SLICE_X98Y385        FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.884    shift_reg_tap_i/sr_p.sr_3[115]
  -------------------------------------------------------------------
                         required time                         -1.884    
                         arrival time                           1.903    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_2[115]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_3[115]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.217ns
    Source Clock Delay      (SCD):    1.831ns
    Clock Pessimism Removal (CPR):    0.380ns
  Clock Net Delay (Source):      1.285ns (routing 0.638ns, distribution 0.647ns)
  Clock Net Delay (Destination): 1.445ns (routing 0.709ns, distribution 0.736ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=2366, routed)        1.285     1.831    shift_reg_tap_i/clk_c
    SLICE_X98Y385        FDRE                                         r  shift_reg_tap_i/sr_p.sr_2[115]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y385        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.870 f  shift_reg_tap_i/sr_p.sr_2[115]/Q
                         net (fo=1, routed)           0.033     1.903    shift_reg_tap_i/sr_2[115]
    SLICE_X98Y385        FDRE                                         f  shift_reg_tap_i/sr_p.sr_3[115]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=2366, routed)        1.445     2.217    shift_reg_tap_i/clk_c
    SLICE_X98Y385        FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[115]/C
                         clock pessimism             -0.380     1.837    
    SLICE_X98Y385        FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.884    shift_reg_tap_i/sr_p.sr_3[115]
  -------------------------------------------------------------------
                         required time                         -1.884    
                         arrival time                           1.903    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_3[225]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            muon_cand_14.sector[1]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.220ns
    Source Clock Delay      (SCD):    1.833ns
    Clock Pessimism Removal (CPR):    0.381ns
  Clock Net Delay (Source):      1.287ns (routing 0.638ns, distribution 0.649ns)
  Clock Net Delay (Destination): 1.448ns (routing 0.709ns, distribution 0.739ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=2366, routed)        1.287     1.833    shift_reg_tap_i/clk_c
    SLICE_X98Y374        FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[225]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y374        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.872 r  shift_reg_tap_i/sr_p.sr_3[225]/Q
                         net (fo=1, routed)           0.034     1.906    input_slr[225]
    SLICE_X98Y374        FDRE                                         r  muon_cand_14.sector[1]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=2366, routed)        1.448     2.220    clk_c
    SLICE_X98Y374        FDRE                                         r  muon_cand_14.sector[1]/C
                         clock pessimism             -0.381     1.839    
    SLICE_X98Y374        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.886    muon_cand_14.sector[1]
  -------------------------------------------------------------------
                         required time                         -1.886    
                         arrival time                           1.906    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_3[225]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            muon_cand_14.sector[1]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.220ns
    Source Clock Delay      (SCD):    1.833ns
    Clock Pessimism Removal (CPR):    0.381ns
  Clock Net Delay (Source):      1.287ns (routing 0.638ns, distribution 0.649ns)
  Clock Net Delay (Destination): 1.448ns (routing 0.709ns, distribution 0.739ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=2366, routed)        1.287     1.833    shift_reg_tap_i/clk_c
    SLICE_X98Y374        FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[225]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y374        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.872 f  shift_reg_tap_i/sr_p.sr_3[225]/Q
                         net (fo=1, routed)           0.034     1.906    input_slr[225]
    SLICE_X98Y374        FDRE                                         f  muon_cand_14.sector[1]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=2366, routed)        1.448     2.220    clk_c
    SLICE_X98Y374        FDRE                                         r  muon_cand_14.sector[1]/C
                         clock pessimism             -0.381     1.839    
    SLICE_X98Y374        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.886    muon_cand_14.sector[1]
  -------------------------------------------------------------------
                         required time                         -1.886    
                         arrival time                           1.906    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_3[159]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            muon_cand_9.roi[7]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.242ns
    Source Clock Delay      (SCD):    1.854ns
    Clock Pessimism Removal (CPR):    0.382ns
  Clock Net Delay (Source):      1.308ns (routing 0.638ns, distribution 0.670ns)
  Clock Net Delay (Destination): 1.470ns (routing 0.709ns, distribution 0.761ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=2366, routed)        1.308     1.854    shift_reg_tap_i/clk_c
    SLICE_X96Y456        FDRE                                         r  shift_reg_tap_i/sr_p.sr_3[159]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y456        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.893 r  shift_reg_tap_i/sr_p.sr_3[159]/Q
                         net (fo=1, routed)           0.034     1.927    input_slr[159]
    SLICE_X96Y456        FDRE                                         r  muon_cand_9.roi[7]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X4Y6 (CLOCK_ROOT)    net (fo=2366, routed)        1.470     2.242    clk_c
    SLICE_X96Y456        FDRE                                         r  muon_cand_9.roi[7]/C
                         clock pessimism             -0.382     1.860    
    SLICE_X96Y456        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.907    muon_cand_9.roi[7]
  -------------------------------------------------------------------
                         required time                         -1.907    
                         arrival time                           1.927    
  -------------------------------------------------------------------
                         slack                                  0.020    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 6.250 }
Period(ns):         12.500
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         12.500      11.001     BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X128Y409  muon_cand_0.pt[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X128Y409  muon_cand_0.pt[1]/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X128Y409  muon_cand_0.pt[2]/C
Min Period        n/a     FDRE/C    n/a            0.550         12.500      11.950     SLICE_X128Y410  muon_cand_0.pt[3]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X140Y412  muon_cand_12.sector[0]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X157Y391  muon_cand_15.pt_0_rep1_rep_117/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X98Y370   muon_cand_6.roi[3]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X143Y407  muon_cand_7.sector[0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X142Y402  muon_sorter_1/sr_p.sr_1_1.roi[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X112Y361  muon_cand_0.roi[1]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X112Y361  muon_cand_0.roi_rep_13[1]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X114Y469  muon_cand_0.sector[2]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X131Y403  muon_cand_10.pt[3]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         6.250       5.975      SLICE_X106Y454  muon_cand_10.pt_0_rep1/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X113Y468         lsfr_1/output_vector_1[255]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X113Y468         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X99Y367          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X111Y362         lsfr_1/shiftreg_vector[10]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X99Y457          lsfr_1/shiftreg_vector[154]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X99Y457          lsfr_1/shiftreg_vector[155]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X113Y468         lsfr_1/output_vector_1[255]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X113Y468         lsfr_1/shiftreg_vector[0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X100Y379         lsfr_1/shiftreg_vector[102]/C



