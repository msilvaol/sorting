Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Sat Mar  2 20:27:53 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : bitonic_sorter_16_top
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.028        0.000                      0                  416        4.725        0.000                       0                  1115  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 5.000}        10.000          100.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.028        0.000                      0                  416        4.725        0.000                       0                   625  
clk_wrapper                                                                             498.562        0.000                       0                   490  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.028ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        4.725ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.028ns  (arrival time - required time)
  Source:                 dut_inst/qdelay[1][15].idx[6]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Destination:            sorted_muons[15].idx[6]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.081ns  (logic 0.039ns (48.148%)  route 0.042ns (51.852%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.922ns
    Source Clock Delay      (SCD):    1.565ns
    Clock Pessimism Removal (CPR):    0.352ns
  Clock Net Delay (Source):      1.000ns (routing 0.524ns, distribution 0.476ns)
  Clock Net Delay (Destination): 1.131ns (routing 0.584ns, distribution 0.547ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.404     0.404 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.404    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.404 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.548    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.565 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=624, routed)         1.000     1.565    dut_inst/clk_c
    SLICE_X104Y522       FDRE                                         r  dut_inst/qdelay[1][15].idx[6]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X104Y522       FDRE (Prop_GFF_SLICEM_C_Q)
                                                      0.039     1.604 r  dut_inst/qdelay[1][15].idx[6]/Q
                         net (fo=1, routed)           0.042     1.646    qdelay[1][15].idx[6]
    SLICE_X104Y522       FDRE                                         r  sorted_muons[15].idx[6]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.591     0.591 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.591    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.591 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.772    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.791 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=624, routed)         1.131     1.922    clk_c
    SLICE_X104Y522       FDRE                                         r  sorted_muons[15].idx[6]/C
                         clock pessimism             -0.352     1.571    
    SLICE_X104Y522       FDRE (Hold_EFF2_SLICEM_C_D)
                                                      0.047     1.618    sorted_muons[15].idx[6]
  -------------------------------------------------------------------
                         required time                         -1.618    
                         arrival time                           1.646    
  -------------------------------------------------------------------
                         slack                                  0.028    

Slack (MET) :             0.028ns  (arrival time - required time)
  Source:                 dut_inst/qdelay[1][15].idx[6]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Destination:            sorted_muons[15].idx[6]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.081ns  (logic 0.039ns (48.148%)  route 0.042ns (51.852%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.922ns
    Source Clock Delay      (SCD):    1.565ns
    Clock Pessimism Removal (CPR):    0.352ns
  Clock Net Delay (Source):      1.000ns (routing 0.524ns, distribution 0.476ns)
  Clock Net Delay (Destination): 1.131ns (routing 0.584ns, distribution 0.547ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.404     0.404 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.404    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.404 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.548    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.565 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=624, routed)         1.000     1.565    dut_inst/clk_c
    SLICE_X104Y522       FDRE                                         r  dut_inst/qdelay[1][15].idx[6]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X104Y522       FDRE (Prop_GFF_SLICEM_C_Q)
                                                      0.039     1.604 f  dut_inst/qdelay[1][15].idx[6]/Q
                         net (fo=1, routed)           0.042     1.646    qdelay[1][15].idx[6]
    SLICE_X104Y522       FDRE                                         f  sorted_muons[15].idx[6]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.591     0.591 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.591    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.591 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.772    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.791 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=624, routed)         1.131     1.922    clk_c
    SLICE_X104Y522       FDRE                                         r  sorted_muons[15].idx[6]/C
                         clock pessimism             -0.352     1.571    
    SLICE_X104Y522       FDRE (Hold_EFF2_SLICEM_C_D)
                                                      0.047     1.618    sorted_muons[15].idx[6]
  -------------------------------------------------------------------
                         required time                         -1.618    
                         arrival time                           1.646    
  -------------------------------------------------------------------
                         slack                                  0.028    

Slack (MET) :             0.033ns  (arrival time - required time)
  Source:                 dut_inst/qdelay[1][10].pt[2]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Destination:            sorted_muons[10].pt[2]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.095ns  (logic 0.037ns (38.947%)  route 0.058ns (61.053%))
  Logic Levels:           0  
  Clock Path Skew:        0.015ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.926ns
    Source Clock Delay      (SCD):    1.567ns
    Clock Pessimism Removal (CPR):    0.344ns
  Clock Net Delay (Source):      1.002ns (routing 0.524ns, distribution 0.478ns)
  Clock Net Delay (Destination): 1.135ns (routing 0.584ns, distribution 0.551ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.404     0.404 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.404    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.404 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.548    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.565 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=624, routed)         1.002     1.567    dut_inst/clk_c
    SLICE_X100Y520       FDRE                                         r  dut_inst/qdelay[1][10].pt[2]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X100Y520       FDRE (Prop_FFF_SLICEM_C_Q)
                                                      0.037     1.604 r  dut_inst/qdelay[1][10].pt[2]/Q
                         net (fo=1, routed)           0.058     1.662    qdelay[1][10].pt[2]
    SLICE_X101Y520       FDRE                                         r  sorted_muons[10].pt[2]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.591     0.591 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.591    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.591 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.772    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.791 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=624, routed)         1.135     1.926    clk_c
    SLICE_X101Y520       FDRE                                         r  sorted_muons[10].pt[2]/C
                         clock pessimism             -0.344     1.582    
    SLICE_X101Y520       FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.629    sorted_muons[10].pt[2]
  -------------------------------------------------------------------
                         required time                         -1.629    
                         arrival time                           1.662    
  -------------------------------------------------------------------
                         slack                                  0.033    

Slack (MET) :             0.033ns  (arrival time - required time)
  Source:                 dut_inst/qdelay[1][10].pt[2]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Destination:            sorted_muons[10].pt[2]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.095ns  (logic 0.037ns (38.947%)  route 0.058ns (61.053%))
  Logic Levels:           0  
  Clock Path Skew:        0.015ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.926ns
    Source Clock Delay      (SCD):    1.567ns
    Clock Pessimism Removal (CPR):    0.344ns
  Clock Net Delay (Source):      1.002ns (routing 0.524ns, distribution 0.478ns)
  Clock Net Delay (Destination): 1.135ns (routing 0.584ns, distribution 0.551ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.404     0.404 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.404    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.404 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.548    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.565 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=624, routed)         1.002     1.567    dut_inst/clk_c
    SLICE_X100Y520       FDRE                                         r  dut_inst/qdelay[1][10].pt[2]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X100Y520       FDRE (Prop_FFF_SLICEM_C_Q)
                                                      0.037     1.604 f  dut_inst/qdelay[1][10].pt[2]/Q
                         net (fo=1, routed)           0.058     1.662    qdelay[1][10].pt[2]
    SLICE_X101Y520       FDRE                                         f  sorted_muons[10].pt[2]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.591     0.591 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.591    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.591 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.772    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.791 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=624, routed)         1.135     1.926    clk_c
    SLICE_X101Y520       FDRE                                         r  sorted_muons[10].pt[2]/C
                         clock pessimism             -0.344     1.582    
    SLICE_X101Y520       FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.629    sorted_muons[10].pt[2]
  -------------------------------------------------------------------
                         required time                         -1.629    
                         arrival time                           1.662    
  -------------------------------------------------------------------
                         slack                                  0.033    

Slack (MET) :             0.035ns  (arrival time - required time)
  Source:                 dut_inst/qdelay[1][8].idx[1]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Destination:            sorted_muons[8].idx[1]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@5.000ns period=10.000ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.135ns  (logic 0.039ns (28.889%)  route 0.096ns (71.111%))
  Logic Levels:           0  
  Clock Path Skew:        0.053ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.919ns
    Source Clock Delay      (SCD):    1.558ns
    Clock Pessimism Removal (CPR):    0.308ns
  Clock Net Delay (Source):      0.993ns (routing 0.524ns, distribution 0.469ns)
  Clock Net Delay (Destination): 1.128ns (routing 0.584ns, distribution 0.544ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.404     0.404 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.404    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.404 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.548    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.565 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=624, routed)         0.993     1.558    dut_inst/clk_c
    SLICE_X99Y522        FDRE                                         r  dut_inst/qdelay[1][8].idx[1]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X99Y522        FDRE (Prop_GFF_SLICEM_C_Q)
                                                      0.039     1.597 r  dut_inst/qdelay[1][8].idx[1]/Q
                         net (fo=1, routed)           0.096     1.693    qdelay[1][8].idx[1]
    SLICE_X98Y518        FDRE                                         r  sorted_muons[8].idx[1]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.591     0.591 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.591    clk_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.591 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.772    clk_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.791 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=624, routed)         1.128     1.919    clk_c
    SLICE_X98Y518        FDRE                                         r  sorted_muons[8].idx[1]/C
                         clock pessimism             -0.308     1.611    
    SLICE_X98Y518        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.658    sorted_muons[8].idx[1]
  -------------------------------------------------------------------
                         required time                         -1.658    
                         arrival time                           1.693    
  -------------------------------------------------------------------
                         slack                                  0.035    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 5.000 }
Period(ns):         10.000
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         10.000      8.501      BUFGCE_X1Y218   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         10.000      9.450      SLICE_X101Y524  dut_inst/qdelay[1][0].idx[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         10.000      9.450      SLICE_X94Y522   dut_inst/qdelay[1][0].idx[1]/C
Min Period        n/a     FDRE/C    n/a            0.550         10.000      9.450      SLICE_X96Y523   dut_inst/qdelay[1][0].idx[2]/C
Min Period        n/a     FDRE/C    n/a            0.550         10.000      9.450      SLICE_X98Y522   dut_inst/qdelay[1][0].idx[3]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         5.000       4.725      SLICE_X92Y522   sorted_muons[0].pt[2]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         5.000       4.725      SLICE_X101Y524  dut_inst/qdelay[1][0].idx[0]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         5.000       4.725      SLICE_X94Y522   dut_inst/qdelay[1][0].idx[1]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         5.000       4.725      SLICE_X96Y523   dut_inst/qdelay[1][0].idx[2]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         5.000       4.725      SLICE_X98Y522   dut_inst/qdelay[1][0].idx[3]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         5.000       4.725      SLICE_X96Y520   dut_inst/qdelay[1][5].idx[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         5.000       4.725      SLICE_X93Y521   dut_inst/qdelay[1][5].pt[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         5.000       4.725      SLICE_X96Y520   dut_inst/qdelay[1][6].idx[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         5.000       4.725      SLICE_X96Y520   dut_inst/qdelay[1][7].idx[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         5.000       4.725      SLICE_X93Y521   sorted_muons[4].pt[0]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y518  dout/C
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y517  lfsr_din/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y224          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X103Y531         input_lfsr/shiftreg_vector[117]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X103Y534         input_lfsr/shiftreg_vector[118]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  dout/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y517  lfsr_din/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  dout/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y517  lfsr_din/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X94Y530          input_lfsr/shiftreg_vector[11]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  dout/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y517  lfsr_din/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  dout/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y517  lfsr_din/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X94Y535          input_lfsr/shiftreg_vector[70]/C



