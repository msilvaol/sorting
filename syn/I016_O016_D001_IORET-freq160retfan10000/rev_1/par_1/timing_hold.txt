Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Wed Feb 13 19:40:56 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There are 0 input ports with no input delay specified.

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.019        0.000                      0                 5447        2.850        0.000                       0                  5189  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 3.125}        6.250           160.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.019        0.000                      0                 5447        2.850        0.000                       0                  4852  
clk_wrapper                                                                             498.562        0.000                       0                   337  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.019ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        2.850ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_13[15]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_14[15]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.013ns
    Source Clock Delay      (SCD):    1.647ns
    Clock Pessimism Removal (CPR):    0.360ns
  Clock Net Delay (Source):      1.101ns (routing 0.522ns, distribution 0.579ns)
  Clock Net Delay (Destination): 1.241ns (routing 0.578ns, distribution 0.663ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=4851, routed)        1.101     1.647    shift_reg_tap_o/clk_c
    SLICE_X105Y449       FDRE                                         r  shift_reg_tap_o/sr_p.sr_13[15]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X105Y449       FDRE (Prop_BFF_SLICEL_C_Q)
                                                      0.039     1.686 r  shift_reg_tap_o/sr_p.sr_13[15]/Q
                         net (fo=1, routed)           0.033     1.719    shift_reg_tap_o/sr_13[15]
    SLICE_X105Y449       FDRE                                         r  shift_reg_tap_o/sr_p.sr_14[15]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=4851, routed)        1.241     2.013    shift_reg_tap_o/clk_c
    SLICE_X105Y449       FDRE                                         r  shift_reg_tap_o/sr_p.sr_14[15]/C
                         clock pessimism             -0.360     1.653    
    SLICE_X105Y449       FDRE (Hold_CFF2_SLICEL_C_D)
                                                      0.047     1.700    shift_reg_tap_o/sr_p.sr_14[15]
  -------------------------------------------------------------------
                         required time                         -1.700    
                         arrival time                           1.719    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_13[15]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_14[15]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.013ns
    Source Clock Delay      (SCD):    1.647ns
    Clock Pessimism Removal (CPR):    0.360ns
  Clock Net Delay (Source):      1.101ns (routing 0.522ns, distribution 0.579ns)
  Clock Net Delay (Destination): 1.241ns (routing 0.578ns, distribution 0.663ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=4851, routed)        1.101     1.647    shift_reg_tap_o/clk_c
    SLICE_X105Y449       FDRE                                         r  shift_reg_tap_o/sr_p.sr_13[15]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X105Y449       FDRE (Prop_BFF_SLICEL_C_Q)
                                                      0.039     1.686 f  shift_reg_tap_o/sr_p.sr_13[15]/Q
                         net (fo=1, routed)           0.033     1.719    shift_reg_tap_o/sr_13[15]
    SLICE_X105Y449       FDRE                                         f  shift_reg_tap_o/sr_p.sr_14[15]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=4851, routed)        1.241     2.013    shift_reg_tap_o/clk_c
    SLICE_X105Y449       FDRE                                         r  shift_reg_tap_o/sr_p.sr_14[15]/C
                         clock pessimism             -0.360     1.653    
    SLICE_X105Y449       FDRE (Hold_CFF2_SLICEL_C_D)
                                                      0.047     1.700    shift_reg_tap_o/sr_p.sr_14[15]
  -------------------------------------------------------------------
                         required time                         -1.700    
                         arrival time                           1.719    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_8[120]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_9[120]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.005ns
    Source Clock Delay      (SCD):    1.643ns
    Clock Pessimism Removal (CPR):    0.356ns
  Clock Net Delay (Source):      1.097ns (routing 0.522ns, distribution 0.575ns)
  Clock Net Delay (Destination): 1.233ns (routing 0.578ns, distribution 0.655ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=4851, routed)        1.097     1.643    shift_reg_tap_o/clk_c
    SLICE_X111Y496       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[120]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X111Y496       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.682 r  shift_reg_tap_o/sr_p.sr_8[120]/Q
                         net (fo=1, routed)           0.033     1.715    shift_reg_tap_o/sr_8[120]
    SLICE_X111Y496       FDRE                                         r  shift_reg_tap_o/sr_p.sr_9[120]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=4851, routed)        1.233     2.005    shift_reg_tap_o/clk_c
    SLICE_X111Y496       FDRE                                         r  shift_reg_tap_o/sr_p.sr_9[120]/C
                         clock pessimism             -0.356     1.649    
    SLICE_X111Y496       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.696    shift_reg_tap_o/sr_p.sr_9[120]
  -------------------------------------------------------------------
                         required time                         -1.696    
                         arrival time                           1.715    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_8[120]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_9[120]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.005ns
    Source Clock Delay      (SCD):    1.643ns
    Clock Pessimism Removal (CPR):    0.356ns
  Clock Net Delay (Source):      1.097ns (routing 0.522ns, distribution 0.575ns)
  Clock Net Delay (Destination): 1.233ns (routing 0.578ns, distribution 0.655ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=4851, routed)        1.097     1.643    shift_reg_tap_o/clk_c
    SLICE_X111Y496       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[120]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X111Y496       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.682 f  shift_reg_tap_o/sr_p.sr_8[120]/Q
                         net (fo=1, routed)           0.033     1.715    shift_reg_tap_o/sr_8[120]
    SLICE_X111Y496       FDRE                                         f  shift_reg_tap_o/sr_p.sr_9[120]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=4851, routed)        1.233     2.005    shift_reg_tap_o/clk_c
    SLICE_X111Y496       FDRE                                         r  shift_reg_tap_o/sr_p.sr_9[120]/C
                         clock pessimism             -0.356     1.649    
    SLICE_X111Y496       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.696    shift_reg_tap_o/sr_p.sr_9[120]
  -------------------------------------------------------------------
                         required time                         -1.696    
                         arrival time                           1.715    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_8[251]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_9[251]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.996ns
    Source Clock Delay      (SCD):    1.634ns
    Clock Pessimism Removal (CPR):    0.356ns
  Clock Net Delay (Source):      1.088ns (routing 0.522ns, distribution 0.566ns)
  Clock Net Delay (Destination): 1.224ns (routing 0.578ns, distribution 0.646ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=4851, routed)        1.088     1.634    shift_reg_tap_o/clk_c
    SLICE_X101Y484       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[251]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y484       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.673 r  shift_reg_tap_o/sr_p.sr_8[251]/Q
                         net (fo=1, routed)           0.033     1.706    shift_reg_tap_o/sr_8[251]
    SLICE_X101Y484       FDRE                                         r  shift_reg_tap_o/sr_p.sr_9[251]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y8 (CLOCK_ROOT)    net (fo=4851, routed)        1.224     1.996    shift_reg_tap_o/clk_c
    SLICE_X101Y484       FDRE                                         r  shift_reg_tap_o/sr_p.sr_9[251]/C
                         clock pessimism             -0.356     1.640    
    SLICE_X101Y484       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.687    shift_reg_tap_o/sr_p.sr_9[251]
  -------------------------------------------------------------------
                         required time                         -1.687    
                         arrival time                           1.706    
  -------------------------------------------------------------------
                         slack                                  0.019    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 3.125 }
Period(ns):         6.250
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         6.250       4.751      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X104Y471  muon_sorter_1/sr_p.sr_1_0.roi_ret/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X104Y471  muon_sorter_1/sr_p.sr_1_0.roi_ret_0/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X103Y471  muon_sorter_1/sr_p.sr_1_0.roi_ret_1/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X103Y475  muon_sorter_1/sr_p.sr_1_0.roi_ret_10/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X106Y443  shift_reg_tap_o/sr_p.sr_11[51]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X104Y505  shift_reg_tap_o/sr_p.sr_4[98]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X106Y459  shift_reg_tap_o/sr_p.sr_11[72]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X102Y572  muon_sorter_1/sr_p.sr_1_3.pt_ret_6/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X109Y494  shift_reg_tap_o/sr_p.sr_5[159]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X102Y474  muon_sorter_1/sr_p.sr_1_1.roi_ret_11/C
High Pulse Width  Slow    FDSE/C    n/a            0.275         3.125       2.850      SLICE_X111Y562  muon_sorter_1/sr_p.sr_1_11.pt[1]/C
High Pulse Width  Slow    FDSE/C    n/a            0.275         3.125       2.850      SLICE_X111Y565  muon_sorter_1/sr_p.sr_1_11.pt[2]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X105Y481  muon_sorter_1/sr_p.sr_1_15.roi_ret_206/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X102Y473  muon_sorter_1/sr_p.sr_1_15.roi_ret_208/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X102Y477         reducer_1/delay_block[0][226]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X102Y477         reducer_1/delay_block[0][227]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X101Y487         reducer_1/delay_block[0][228]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X92Y586          reducer_1/delay_block[0][97]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X92Y586          reducer_1/delay_block[0][98]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X91Y585          reducer_1/delay_block[0][99]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X102Y477         reducer_1/delay_block[0][226]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X102Y477         reducer_1/delay_block[0][227]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X105Y440         reducer_1/delay_block[0][22]/C



