Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Tue Mar 12 16:11:30 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+------+--------+
|  Fanout | Nets |      % |
+---------+------+--------+
|       1 | 1754 |  22.48 |
|       2 | 2286 |  29.30 |
|       3 | 1433 |  18.36 |
|       4 |  904 |  11.58 |
|    5-10 |  924 |  11.84 |
|   11-50 |  501 |   6.42 |
|  51-100 |    0 |   0.00 |
| 101-500 |    0 |   0.00 |
|    >500 |    0 |   0.00 |
+---------+------+--------+
|     ALL | 7802 | 100.00 |
+---------+------+--------+


