Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Thu Feb 14 13:46:31 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+------+--------+
|  Fanout | Nets |      % |
+---------+------+--------+
|       1 | 4090 |  53.81 |
|       2 |  479 |   6.30 |
|       3 |  917 |  12.06 |
|       4 |  299 |   3.93 |
|    5-10 |  793 |  10.43 |
|   11-50 | 1022 |  13.44 |
|  51-100 |    0 |   0.00 |
| 101-500 |    0 |   0.00 |
|    >500 |    0 |   0.00 |
+---------+------+--------+
|     ALL | 7600 | 100.00 |
+---------+------+--------+


