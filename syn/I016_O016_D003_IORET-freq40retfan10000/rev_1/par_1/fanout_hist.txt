Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Thu Feb 14 03:03:27 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+------+--------+
|  Fanout | Nets |      % |
+---------+------+--------+
|       1 | 7056 |  75.12 |
|       2 |  301 |   3.20 |
|       3 | 1114 |  11.86 |
|       4 |  285 |   3.03 |
|    5-10 |  102 |   1.08 |
|   11-50 |  531 |   5.65 |
|  51-100 |    3 |   0.03 |
| 101-500 |    0 |   0.00 |
|    >500 |    0 |   0.00 |
+---------+------+--------+
|     ALL | 9392 | 100.00 |
+---------+------+--------+


