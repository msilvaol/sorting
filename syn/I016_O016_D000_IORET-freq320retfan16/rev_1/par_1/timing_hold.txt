Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Wed Feb 13 15:42:19 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.010        0.000                      0                 7680        1.287        0.000                       0                  8536  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.563}        3.125           320.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.010        0.000                      0                 7680        1.287        0.000                       0                  7937  
clk_wrapper                                                                             498.562        0.000                       0                   599  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.010ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.287ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.010ns  (arrival time - required time)
  Source:                 muon_cand_0.roi[5]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[253]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.569ns  (logic 0.156ns (27.417%)  route 0.413ns (72.583%))
  Logic Levels:           2  (LUT5=1 LUT6=1)
  Clock Path Skew:        0.506ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.997ns
    Source Clock Delay      (SCD):    3.008ns
    Clock Pessimism Removal (CPR):    0.483ns
  Clock Net Delay (Source):      2.178ns (routing 1.334ns, distribution 0.844ns)
  Clock Net Delay (Destination): 2.856ns (routing 1.460ns, distribution 1.396ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=7936, routed)        2.178     3.008    clk_c
    SLICE_X106Y468       FDRE                                         r  muon_cand_0.roi[5]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X106Y468       FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.070     3.078 r  muon_cand_0.roi[5]/Q
                         net (fo=16, routed)          0.331     3.409    muon_sorter_1/roi_0[5]
    SLICE_X98Y453        LUT6 (Prop_F6LUT_SLICEL_I0_O)
                                                      0.043     3.452 r  muon_sorter_1/top_cand_comb_15.roi_0_1[5]/O
                         net (fo=1, routed)           0.059     3.511    muon_sorter_1/roi_0_1_0[5]
    SLICE_X98Y451        LUT5 (Prop_E6LUT_SLICEL_I2_O)
                                                      0.043     3.554 r  muon_sorter_1/top_cand_comb_15.roi[5]/O
                         net (fo=1, routed)           0.023     3.577    shift_reg_tap_o/roi[5]
    SLICE_X98Y451        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[253]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=7936, routed)        2.856     3.997    shift_reg_tap_o/clk_c
    SLICE_X98Y451        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[253]/C
                         clock pessimism             -0.483     3.514    
    SLICE_X98Y451        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.053     3.567    shift_reg_tap_o/sr_p.sr_1[253]
  -------------------------------------------------------------------
                         required time                         -3.567    
                         arrival time                           3.577    
  -------------------------------------------------------------------
                         slack                                  0.010    

Slack (MET) :             0.010ns  (arrival time - required time)
  Source:                 muon_cand_0.roi[5]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[253]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.569ns  (logic 0.156ns (27.417%)  route 0.413ns (72.583%))
  Logic Levels:           2  (LUT5=1 LUT6=1)
  Clock Path Skew:        0.506ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.997ns
    Source Clock Delay      (SCD):    3.008ns
    Clock Pessimism Removal (CPR):    0.483ns
  Clock Net Delay (Source):      2.178ns (routing 1.334ns, distribution 0.844ns)
  Clock Net Delay (Destination): 2.856ns (routing 1.460ns, distribution 1.396ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=7936, routed)        2.178     3.008    clk_c
    SLICE_X106Y468       FDRE                                         r  muon_cand_0.roi[5]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X106Y468       FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.070     3.078 f  muon_cand_0.roi[5]/Q
                         net (fo=16, routed)          0.331     3.409    muon_sorter_1/roi_0[5]
    SLICE_X98Y453        LUT6 (Prop_F6LUT_SLICEL_I0_O)
                                                      0.043     3.452 f  muon_sorter_1/top_cand_comb_15.roi_0_1[5]/O
                         net (fo=1, routed)           0.059     3.511    muon_sorter_1/roi_0_1_0[5]
    SLICE_X98Y451        LUT5 (Prop_E6LUT_SLICEL_I2_O)
                                                      0.043     3.554 f  muon_sorter_1/top_cand_comb_15.roi[5]/O
                         net (fo=1, routed)           0.023     3.577    shift_reg_tap_o/roi[5]
    SLICE_X98Y451        FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[253]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=7936, routed)        2.856     3.997    shift_reg_tap_o/clk_c
    SLICE_X98Y451        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[253]/C
                         clock pessimism             -0.483     3.514    
    SLICE_X98Y451        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.053     3.567    shift_reg_tap_o/sr_p.sr_1[253]
  -------------------------------------------------------------------
                         required time                         -3.567    
                         arrival time                           3.577    
  -------------------------------------------------------------------
                         slack                                  0.010    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_8[74]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_i/sr_p.sr_9[74]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.186ns
    Source Clock Delay      (SCD):    1.802ns
    Clock Pessimism Removal (CPR):    0.378ns
  Clock Net Delay (Source):      1.256ns (routing 0.754ns, distribution 0.502ns)
  Clock Net Delay (Destination): 1.414ns (routing 0.836ns, distribution 0.578ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=7936, routed)        1.256     1.802    shift_reg_tap_i/clk_c
    SLICE_X101Y473       FDRE                                         r  shift_reg_tap_i/sr_p.sr_8[74]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y473       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.841 r  shift_reg_tap_i/sr_p.sr_8[74]/Q
                         net (fo=1, routed)           0.033     1.874    shift_reg_tap_i/sr_8[74]
    SLICE_X101Y473       FDRE                                         r  shift_reg_tap_i/sr_p.sr_9[74]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=7936, routed)        1.414     2.186    shift_reg_tap_i/clk_c
    SLICE_X101Y473       FDRE                                         r  shift_reg_tap_i/sr_p.sr_9[74]/C
                         clock pessimism             -0.378     1.808    
    SLICE_X101Y473       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.855    shift_reg_tap_i/sr_p.sr_9[74]
  -------------------------------------------------------------------
                         required time                         -1.855    
                         arrival time                           1.874    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_8[74]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_i/sr_p.sr_9[74]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.186ns
    Source Clock Delay      (SCD):    1.802ns
    Clock Pessimism Removal (CPR):    0.378ns
  Clock Net Delay (Source):      1.256ns (routing 0.754ns, distribution 0.502ns)
  Clock Net Delay (Destination): 1.414ns (routing 0.836ns, distribution 0.578ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=7936, routed)        1.256     1.802    shift_reg_tap_i/clk_c
    SLICE_X101Y473       FDRE                                         r  shift_reg_tap_i/sr_p.sr_8[74]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y473       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.841 f  shift_reg_tap_i/sr_p.sr_8[74]/Q
                         net (fo=1, routed)           0.033     1.874    shift_reg_tap_i/sr_8[74]
    SLICE_X101Y473       FDRE                                         f  shift_reg_tap_i/sr_p.sr_9[74]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=7936, routed)        1.414     2.186    shift_reg_tap_i/clk_c
    SLICE_X101Y473       FDRE                                         r  shift_reg_tap_i/sr_p.sr_9[74]/C
                         clock pessimism             -0.378     1.808    
    SLICE_X101Y473       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.855    shift_reg_tap_i/sr_p.sr_9[74]
  -------------------------------------------------------------------
                         required time                         -1.855    
                         arrival time                           1.874    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_2[228]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_o/sr_p.sr_3[228]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.175ns
    Source Clock Delay      (SCD):    1.793ns
    Clock Pessimism Removal (CPR):    0.376ns
  Clock Net Delay (Source):      1.247ns (routing 0.754ns, distribution 0.493ns)
  Clock Net Delay (Destination): 1.403ns (routing 0.836ns, distribution 0.567ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=7936, routed)        1.247     1.793    shift_reg_tap_o/clk_c
    SLICE_X105Y454       FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[228]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X105Y454       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.832 r  shift_reg_tap_o/sr_p.sr_2[228]/Q
                         net (fo=1, routed)           0.033     1.865    shift_reg_tap_o/sr_2[228]
    SLICE_X105Y454       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[228]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y6 (CLOCK_ROOT)    net (fo=7936, routed)        1.403     2.175    shift_reg_tap_o/clk_c
    SLICE_X105Y454       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[228]/C
                         clock pessimism             -0.376     1.799    
    SLICE_X105Y454       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.846    shift_reg_tap_o/sr_p.sr_3[228]
  -------------------------------------------------------------------
                         required time                         -1.846    
                         arrival time                           1.865    
  -------------------------------------------------------------------
                         slack                                  0.019    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.563 }
Period(ns):         3.125
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         3.125       1.626      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X93Y465   shift_reg_tap_i/sr_p.sr_13[125]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X93Y463   shift_reg_tap_i/sr_p.sr_13[126]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X95Y406   shift_reg_tap_i/sr_p.sr_13[127]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X94Y404   shift_reg_tap_i/sr_p.sr_13[128]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X87Y428   shift_reg_tap_o/sr_p.sr_6[46]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X87Y428   shift_reg_tap_o/sr_p.sr_3[46]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X98Y467   shift_reg_tap_i/sr_p.sr_13[73]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X86Y423   shift_reg_tap_o/sr_p.sr_15[67]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X87Y428   shift_reg_tap_o/sr_p.sr_7[46]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X99Y453   shift_reg_tap_o/sr_p.sr_1[244]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X101Y452  shift_reg_tap_o/sr_p.sr_1[246]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X97Y452   shift_reg_tap_o/sr_p.sr_1[247]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X99Y452   shift_reg_tap_o/sr_p.sr_1[248]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X97Y452   shift_reg_tap_o/sr_p.sr_1[251]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X108Y402         lsfr_1/output_vector_1[255]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X108Y397         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X95Y475          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X95Y475          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X95Y475          lsfr_1/shiftreg_vector[101]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X93Y454          reducer_1/delay_block[1][30]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X93Y473          lsfr_1/shiftreg_vector[102]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X93Y473          lsfr_1/shiftreg_vector[103]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X93Y473          lsfr_1/shiftreg_vector[104]/C



