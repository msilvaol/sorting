Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Wed Feb 13 15:45:16 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+-------+--------+
|  Fanout |  Nets |      % |
+---------+-------+--------+
|       1 | 10975 |  71.16 |
|       2 |   726 |   4.70 |
|       3 |   357 |   2.31 |
|       4 |   356 |   2.30 |
|    5-10 |  1944 |  12.60 |
|   11-50 |  1064 |   6.89 |
|  51-100 |     0 |   0.00 |
| 101-500 |     0 |   0.00 |
|    >500 |     0 |   0.00 |
+---------+-------+--------+
|     ALL | 15422 | 100.00 |
+---------+-------+--------+


