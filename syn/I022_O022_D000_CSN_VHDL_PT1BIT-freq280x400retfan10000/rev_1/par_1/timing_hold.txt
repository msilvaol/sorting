Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Mon Mar 11 16:50:38 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.127        0.000                      0                  220        1.511        0.000                       0                  1005  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.786}        3.572           279.955         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.127        0.000                      0                  220        1.511        0.000                       0                   489  
clk_wrapper                                                                             498.562        0.000                       0                   516  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.127ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.511ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.127ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1_0_rep2/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[210]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.197ns  (logic 0.061ns (30.964%)  route 0.136ns (69.036%))
  Logic Levels:           1  (LUT6=1)
  Clock Path Skew:        0.024ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.011ns
    Source Clock Delay      (SCD):    1.640ns
    Clock Pessimism Removal (CPR):    0.347ns
  Clock Net Delay (Source):      1.094ns (routing 0.638ns, distribution 0.456ns)
  Clock Net Delay (Destination): 1.239ns (routing 0.707ns, distribution 0.532ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=488, routed)         1.094     1.640    shift_reg_tap_i/clk_c
    SLICE_X98Y443        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1_0_rep2/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y443        FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.039     1.679 r  shift_reg_tap_i/sr_p.sr_1_0_rep2/Q
                         net (fo=17, routed)          0.120     1.799    dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/input_slr_0
    SLICE_X98Y438        LUT6 (Prop_D6LUT_SLICEL_I2_O)
                                                      0.022     1.821 r  dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/b_o_comb.pt_0[0]/O
                         net (fo=1, routed)           0.016     1.837    shift_reg_tap_o/un1_dut_inst_0_210
    SLICE_X98Y438        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[210]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=488, routed)         1.239     2.011    shift_reg_tap_o/clk_c
    SLICE_X98Y438        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[210]/C
                         clock pessimism             -0.347     1.664    
    SLICE_X98Y438        FDRE (Hold_DFF_SLICEL_C_D)
                                                      0.046     1.710    shift_reg_tap_o/sr_p.sr_1[210]
  -------------------------------------------------------------------
                         required time                         -1.710    
                         arrival time                           1.837    
  -------------------------------------------------------------------
                         slack                                  0.127    

Slack (MET) :             0.127ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1_0_rep2/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[210]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.197ns  (logic 0.061ns (30.964%)  route 0.136ns (69.036%))
  Logic Levels:           1  (LUT6=1)
  Clock Path Skew:        0.024ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.011ns
    Source Clock Delay      (SCD):    1.640ns
    Clock Pessimism Removal (CPR):    0.347ns
  Clock Net Delay (Source):      1.094ns (routing 0.638ns, distribution 0.456ns)
  Clock Net Delay (Destination): 1.239ns (routing 0.707ns, distribution 0.532ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=488, routed)         1.094     1.640    shift_reg_tap_i/clk_c
    SLICE_X98Y443        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1_0_rep2/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y443        FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.039     1.679 f  shift_reg_tap_i/sr_p.sr_1_0_rep2/Q
                         net (fo=17, routed)          0.120     1.799    dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/input_slr_0
    SLICE_X98Y438        LUT6 (Prop_D6LUT_SLICEL_I2_O)
                                                      0.022     1.821 f  dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/b_o_comb.pt_0[0]/O
                         net (fo=1, routed)           0.016     1.837    shift_reg_tap_o/un1_dut_inst_0_210
    SLICE_X98Y438        FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[210]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=488, routed)         1.239     2.011    shift_reg_tap_o/clk_c
    SLICE_X98Y438        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[210]/C
                         clock pessimism             -0.347     1.664    
    SLICE_X98Y438        FDRE (Hold_DFF_SLICEL_C_D)
                                                      0.046     1.710    shift_reg_tap_o/sr_p.sr_1[210]
  -------------------------------------------------------------------
                         required time                         -1.710    
                         arrival time                           1.837    
  -------------------------------------------------------------------
                         slack                                  0.127    

Slack (MET) :             0.181ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1_0_rep2/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[210]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.290ns  (logic 0.113ns (38.966%)  route 0.177ns (61.034%))
  Logic Levels:           1  (LUT6=1)
  Clock Path Skew:        0.055ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.301ns
    Source Clock Delay      (SCD):    2.727ns
    Clock Pessimism Removal (CPR):    0.519ns
  Clock Net Delay (Source):      1.897ns (routing 1.130ns, distribution 0.767ns)
  Clock Net Delay (Destination): 2.160ns (routing 1.237ns, distribution 0.923ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=488, routed)         1.897     2.727    shift_reg_tap_i/clk_c
    SLICE_X98Y443        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1_0_rep2/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y443        FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.070     2.797 r  shift_reg_tap_i/sr_p.sr_1_0_rep2/Q
                         net (fo=17, routed)          0.152     2.949    dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/input_slr_0
    SLICE_X98Y438        LUT6 (Prop_D6LUT_SLICEL_I2_O)
                                                      0.043     2.992 r  dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/b_o_comb.pt_0[0]/O
                         net (fo=1, routed)           0.025     3.017    shift_reg_tap_o/un1_dut_inst_0_210
    SLICE_X98Y438        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[210]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=488, routed)         2.160     3.301    shift_reg_tap_o/clk_c
    SLICE_X98Y438        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[210]/C
                         clock pessimism             -0.519     2.782    
    SLICE_X98Y438        FDRE (Hold_DFF_SLICEL_C_D)
                                                      0.053     2.835    shift_reg_tap_o/sr_p.sr_1[210]
  -------------------------------------------------------------------
                         required time                         -2.835    
                         arrival time                           3.017    
  -------------------------------------------------------------------
                         slack                                  0.181    

Slack (MET) :             0.181ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1_0_rep2/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[210]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.290ns  (logic 0.113ns (38.966%)  route 0.177ns (61.034%))
  Logic Levels:           1  (LUT6=1)
  Clock Path Skew:        0.055ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.301ns
    Source Clock Delay      (SCD):    2.727ns
    Clock Pessimism Removal (CPR):    0.519ns
  Clock Net Delay (Source):      1.897ns (routing 1.130ns, distribution 0.767ns)
  Clock Net Delay (Destination): 2.160ns (routing 1.237ns, distribution 0.923ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=488, routed)         1.897     2.727    shift_reg_tap_i/clk_c
    SLICE_X98Y443        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1_0_rep2/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y443        FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.070     2.797 f  shift_reg_tap_i/sr_p.sr_1_0_rep2/Q
                         net (fo=17, routed)          0.152     2.949    dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/input_slr_0
    SLICE_X98Y438        LUT6 (Prop_D6LUT_SLICEL_I2_O)
                                                      0.043     2.992 f  dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/b_o_comb.pt_0[0]/O
                         net (fo=1, routed)           0.025     3.017    shift_reg_tap_o/un1_dut_inst_0_210
    SLICE_X98Y438        FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[210]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=488, routed)         2.160     3.301    shift_reg_tap_o/clk_c
    SLICE_X98Y438        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[210]/C
                         clock pessimism             -0.519     2.782    
    SLICE_X98Y438        FDRE (Hold_DFF_SLICEL_C_D)
                                                      0.053     2.835    shift_reg_tap_o/sr_p.sr_1[210]
  -------------------------------------------------------------------
                         required time                         -2.835    
                         arrival time                           3.017    
  -------------------------------------------------------------------
                         slack                                  0.181    

Slack (MET) :             0.185ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1_fast[20]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[210]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.246ns  (logic 0.089ns (36.179%)  route 0.157ns (63.821%))
  Logic Levels:           1  (LUT6=1)
  Clock Path Skew:        0.015ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.011ns
    Source Clock Delay      (SCD):    1.649ns
    Clock Pessimism Removal (CPR):    0.347ns
  Clock Net Delay (Source):      1.103ns (routing 0.638ns, distribution 0.465ns)
  Clock Net Delay (Destination): 1.239ns (routing 0.707ns, distribution 0.532ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=488, routed)         1.103     1.649    shift_reg_tap_i/clk_c
    SLICE_X97Y442        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1_fast[20]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X97Y442        FDRE (Prop_HFF_SLICEM_C_Q)
                                                      0.039     1.688 r  shift_reg_tap_i/sr_p.sr_1_fast[20]/Q
                         net (fo=18, routed)          0.141     1.829    dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/input_slr_fast_10
    SLICE_X98Y438        LUT6 (Prop_D6LUT_SLICEL_I1_O)
                                                      0.050     1.879 r  dut_inst/stage_g.5.pair_g.0.csn_cmp_inst/b_o_comb.pt_0[0]/O
                         net (fo=1, routed)           0.016     1.895    shift_reg_tap_o/un1_dut_inst_0_210
    SLICE_X98Y438        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[210]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=488, routed)         1.239     2.011    shift_reg_tap_o/clk_c
    SLICE_X98Y438        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[210]/C
                         clock pessimism             -0.347     1.664    
    SLICE_X98Y438        FDRE (Hold_DFF_SLICEL_C_D)
                                                      0.046     1.710    shift_reg_tap_o/sr_p.sr_1[210]
  -------------------------------------------------------------------
                         required time                         -1.710    
                         arrival time                           1.895    
  -------------------------------------------------------------------
                         slack                                  0.185    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.786 }
Period(ns):         3.572
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         3.572       2.073      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X93Y440   shift_reg_tap_i/sr_p.sr_1[138]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X94Y443   shift_reg_tap_i/sr_p.sr_1[139]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X95Y438   shift_reg_tap_i/sr_p.sr_1[13]/C
Min Period        n/a     FDRE/C    n/a            0.550         3.572       3.022      SLICE_X100Y441  shift_reg_tap_i/sr_p.sr_1[140]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X96Y439   shift_reg_tap_i/sr_p.sr_1[14]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X92Y451   shift_reg_tap_o/sr_p.sr_1[176]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X95Y440   shift_reg_tap_i/sr_p.sr_1[16]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X100Y457  shift_reg_tap_o/sr_p.sr_1[188]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X96Y439   shift_reg_tap_i/sr_p.sr_1_20_rep1/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X95Y438   shift_reg_tap_i/sr_p.sr_1[13]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X95Y438   shift_reg_tap_i/sr_p.sr_1[13]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X100Y441  shift_reg_tap_i/sr_p.sr_1[140]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X102Y441  shift_reg_tap_i/sr_p.sr_1[141]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         1.786       1.511      SLICE_X102Y442  shift_reg_tap_i/sr_p.sr_1[142]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X102Y447         lsfr_1/output_vector_1[219]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X104Y459         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X93Y442          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X93Y442          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X93Y442          lsfr_1/shiftreg_vector[105]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y441          lsfr_1/shiftreg_vector[47]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X102Y447         lsfr_1/output_vector_1[219]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y459         lsfr_1/shiftreg_vector[0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X93Y442          lsfr_1/shiftreg_vector[100]/C



