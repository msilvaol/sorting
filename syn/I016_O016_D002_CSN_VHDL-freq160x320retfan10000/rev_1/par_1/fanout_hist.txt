Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Tue Mar  5 23:18:01 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper_csn
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+------+--------+
|  Fanout | Nets |      % |
+---------+------+--------+
|       1 | 1351 |  45.56 |
|       2 |  714 |  24.08 |
|       3 |  342 |  11.53 |
|       4 |  243 |   8.19 |
|    5-10 |  198 |   6.67 |
|   11-50 |  117 |   3.94 |
|  51-100 |    0 |   0.00 |
| 101-500 |    0 |   0.00 |
|    >500 |    0 |   0.00 |
+---------+------+--------+
|     ALL | 2965 | 100.00 |
+---------+------+--------+


