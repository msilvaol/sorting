Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Wed Mar  6 14:42:19 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.035        0.000                      0                  898        1.213        0.000                       0                  1595  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.786}        3.572           279.955         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.035        0.000                      0                  898        1.213        0.000                       0                  1107  
clk_wrapper                                                                             498.562        0.000                       0                   488  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.035ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.213ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.035ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_5_7.idx_3_DOUT[0]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[103]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.119ns  (logic 0.038ns (31.933%)  route 0.081ns (68.067%))
  Logic Levels:           0  
  Clock Path Skew:        0.038ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.629ns
    Source Clock Delay      (SCD):    1.310ns
    Clock Pessimism Removal (CPR):    0.281ns
  Clock Net Delay (Source):      0.764ns (routing 0.316ns, distribution 0.448ns)
  Clock Net Delay (Destination): 0.857ns (routing 0.348ns, distribution 0.509ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1106, routed)        0.764     1.310    dut_inst/clk_c
    SLICE_X93Y573        FDRE                                         r  dut_inst/ret_array_5_7.idx_3_DOUT[0]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X93Y573        FDRE (Prop_BFF_SLICEM_C_Q)
                                                      0.038     1.348 r  dut_inst/ret_array_5_7.idx_3_DOUT[0]/Q
                         net (fo=1, routed)           0.081     1.429    shift_reg_tap_o/idx_3_DOUT_0_0
    SLICE_X95Y573        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[103]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1106, routed)        0.857     1.629    shift_reg_tap_o/clk_c
    SLICE_X95Y573        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[103]/C
                         clock pessimism             -0.281     1.348    
    SLICE_X95Y573        FDRE (Hold_EFF_SLICEM_C_D)
                                                      0.046     1.394    shift_reg_tap_o/sr_p.sr_1[103]
  -------------------------------------------------------------------
                         required time                         -1.394    
                         arrival time                           1.429    
  -------------------------------------------------------------------
                         slack                                  0.035    

Slack (MET) :             0.035ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_5_7.idx_3_DOUT[0]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[103]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.119ns  (logic 0.038ns (31.933%)  route 0.081ns (68.067%))
  Logic Levels:           0  
  Clock Path Skew:        0.038ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.629ns
    Source Clock Delay      (SCD):    1.310ns
    Clock Pessimism Removal (CPR):    0.281ns
  Clock Net Delay (Source):      0.764ns (routing 0.316ns, distribution 0.448ns)
  Clock Net Delay (Destination): 0.857ns (routing 0.348ns, distribution 0.509ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1106, routed)        0.764     1.310    dut_inst/clk_c
    SLICE_X93Y573        FDRE                                         r  dut_inst/ret_array_5_7.idx_3_DOUT[0]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X93Y573        FDRE (Prop_BFF_SLICEM_C_Q)
                                                      0.038     1.348 f  dut_inst/ret_array_5_7.idx_3_DOUT[0]/Q
                         net (fo=1, routed)           0.081     1.429    shift_reg_tap_o/idx_3_DOUT_0_0
    SLICE_X95Y573        FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[103]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1106, routed)        0.857     1.629    shift_reg_tap_o/clk_c
    SLICE_X95Y573        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[103]/C
                         clock pessimism             -0.281     1.348    
    SLICE_X95Y573        FDRE (Hold_EFF_SLICEM_C_D)
                                                      0.046     1.394    shift_reg_tap_o/sr_p.sr_1[103]
  -------------------------------------------------------------------
                         required time                         -1.394    
                         arrival time                           1.429    
  -------------------------------------------------------------------
                         slack                                  0.035    

Slack (MET) :             0.036ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_3_4.pt[0]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[52]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.106ns  (logic 0.039ns (36.792%)  route 0.067ns (63.208%))
  Logic Levels:           0  
  Clock Path Skew:        0.024ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.646ns
    Source Clock Delay      (SCD):    1.312ns
    Clock Pessimism Removal (CPR):    0.310ns
  Clock Net Delay (Source):      0.766ns (routing 0.316ns, distribution 0.450ns)
  Clock Net Delay (Destination): 0.874ns (routing 0.348ns, distribution 0.526ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1106, routed)        0.766     1.312    dut_inst/clk_c
    SLICE_X96Y579        FDRE                                         r  dut_inst/ret_array_3_4.pt[0]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y579        FDRE (Prop_DFF_SLICEL_C_Q)
                                                      0.039     1.351 r  dut_inst/ret_array_3_4.pt[0]/Q
                         net (fo=1, routed)           0.067     1.418    shift_reg_tap_o/pt_1[0]
    SLICE_X96Y578        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[52]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1106, routed)        0.874     1.646    shift_reg_tap_o/clk_c
    SLICE_X96Y578        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[52]/C
                         clock pessimism             -0.310     1.336    
    SLICE_X96Y578        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.046     1.382    shift_reg_tap_o/sr_p.sr_1[52]
  -------------------------------------------------------------------
                         required time                         -1.382    
                         arrival time                           1.418    
  -------------------------------------------------------------------
                         slack                                  0.036    

Slack (MET) :             0.036ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_3_4.pt[0]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[52]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.106ns  (logic 0.039ns (36.792%)  route 0.067ns (63.208%))
  Logic Levels:           0  
  Clock Path Skew:        0.024ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.646ns
    Source Clock Delay      (SCD):    1.312ns
    Clock Pessimism Removal (CPR):    0.310ns
  Clock Net Delay (Source):      0.766ns (routing 0.316ns, distribution 0.450ns)
  Clock Net Delay (Destination): 0.874ns (routing 0.348ns, distribution 0.526ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1106, routed)        0.766     1.312    dut_inst/clk_c
    SLICE_X96Y579        FDRE                                         r  dut_inst/ret_array_3_4.pt[0]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y579        FDRE (Prop_DFF_SLICEL_C_Q)
                                                      0.039     1.351 f  dut_inst/ret_array_3_4.pt[0]/Q
                         net (fo=1, routed)           0.067     1.418    shift_reg_tap_o/pt_1[0]
    SLICE_X96Y578        FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[52]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1106, routed)        0.874     1.646    shift_reg_tap_o/clk_c
    SLICE_X96Y578        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[52]/C
                         clock pessimism             -0.310     1.336    
    SLICE_X96Y578        FDRE (Hold_EFF_SLICEL_C_D)
                                                      0.046     1.382    shift_reg_tap_o/sr_p.sr_1[52]
  -------------------------------------------------------------------
                         required time                         -1.382    
                         arrival time                           1.418    
  -------------------------------------------------------------------
                         slack                                  0.036    

Slack (MET) :             0.037ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_3_10.idx[5]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[139]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.120ns  (logic 0.039ns (32.500%)  route 0.081ns (67.500%))
  Logic Levels:           0  
  Clock Path Skew:        0.037ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.648ns
    Source Clock Delay      (SCD):    1.330ns
    Clock Pessimism Removal (CPR):    0.281ns
  Clock Net Delay (Source):      0.784ns (routing 0.316ns, distribution 0.468ns)
  Clock Net Delay (Destination): 0.876ns (routing 0.348ns, distribution 0.528ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1106, routed)        0.784     1.330    dut_inst/clk_c
    SLICE_X100Y574       FDRE                                         r  dut_inst/ret_array_3_10.idx[5]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X100Y574       FDRE (Prop_DFF_SLICEM_C_Q)
                                                      0.039     1.369 r  dut_inst/ret_array_3_10.idx[5]/Q
                         net (fo=1, routed)           0.081     1.450    shift_reg_tap_o/idx_5[5]
    SLICE_X102Y574       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[139]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=1106, routed)        0.876     1.648    shift_reg_tap_o/clk_c
    SLICE_X102Y574       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[139]/C
                         clock pessimism             -0.281     1.367    
    SLICE_X102Y574       FDRE (Hold_EFF_SLICEM_C_D)
                                                      0.046     1.413    shift_reg_tap_o/sr_p.sr_1[139]
  -------------------------------------------------------------------
                         required time                         -1.413    
                         arrival time                           1.450    
  -------------------------------------------------------------------
                         slack                                  0.037    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.786 }
Period(ns):         3.572
Sources:            { clk }

Check Type        Corner  Lib Pin     Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I    n/a            1.499         3.572       2.073      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.572       2.426      SLICE_X95Y572   dut_inst/ret_array_4_6.pt_pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.572       2.426      SLICE_X102Y583  dut_inst/ret_array_4_9.pt_0_pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.572       2.426      SLICE_X99Y571   dut_inst/ret_array_4_9.pt_pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.572       2.426      SLICE_X97Y569   dut_inst/ret_array_5_7.idx_0_pt_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X95Y572   dut_inst/ret_array_4_6.pt_pt_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X102Y583  dut_inst/ret_array_4_9.pt_0_pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X99Y571   dut_inst/ret_array_4_9.pt_pt_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X97Y569   dut_inst/ret_array_5_7.idx_0_pt_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X97Y569   dut_inst/ret_array_5_7.idx_1_pt_1/CLK
High Pulse Width  Fast    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X95Y572   dut_inst/ret_array_4_6.pt_pt_1/CLK
High Pulse Width  Fast    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X102Y583  dut_inst/ret_array_4_9.pt_0_pt_1/CLK
High Pulse Width  Fast    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X97Y569   dut_inst/ret_array_5_7.idx_0_pt_1/CLK
High Pulse Width  Fast    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X97Y569   dut_inst/ret_array_5_7.idx_1_pt_1/CLK
High Pulse Width  Fast    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X97Y569   dut_inst/ret_array_5_7.idx_4_pt_1/CLK



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X101Y594         lsfr_1/shiftreg_vector[191]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X100Y595         lsfr_1/shiftreg_vector[192]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X102Y595         lsfr_1/shiftreg_vector[193]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y594         lsfr_1/shiftreg_vector[201]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X100Y571         reducer_1/delay_block[0][120]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X102Y575         reducer_1/delay_block[0][128]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X101Y594         lsfr_1/shiftreg_vector[191]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X102Y595         lsfr_1/shiftreg_vector[193]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X102Y595         lsfr_1/shiftreg_vector[194]/C



