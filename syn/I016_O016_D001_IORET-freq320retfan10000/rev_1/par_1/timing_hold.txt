Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Wed Feb 13 18:42:49 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There are 0 input ports with no input delay specified.

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.019        0.000                      0                 5440        1.287        0.000                       0                  5189  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.563}        3.125           320.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.019        0.000                      0                 5440        1.287        0.000                       0                  4852  
clk_wrapper                                                                             498.562        0.000                       0                   337  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.019ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.287ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_7[126]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_o/sr_p.sr_8[126]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.603ns
    Source Clock Delay      (SCD):    1.273ns
    Clock Pessimism Removal (CPR):    0.324ns
  Clock Net Delay (Source):      0.727ns (routing 0.096ns, distribution 0.631ns)
  Clock Net Delay (Destination): 0.831ns (routing 0.108ns, distribution 0.723ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=4851, routed)        0.727     1.273    shift_reg_tap_o/clk_c
    SLICE_X105Y517       FDRE                                         r  shift_reg_tap_o/sr_p.sr_7[126]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X105Y517       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.312 r  shift_reg_tap_o/sr_p.sr_7[126]/Q
                         net (fo=1, routed)           0.033     1.345    shift_reg_tap_o/sr_7[126]
    SLICE_X105Y517       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[126]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=4851, routed)        0.831     1.603    shift_reg_tap_o/clk_c
    SLICE_X105Y517       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[126]/C
                         clock pessimism             -0.324     1.279    
    SLICE_X105Y517       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.326    shift_reg_tap_o/sr_p.sr_8[126]
  -------------------------------------------------------------------
                         required time                         -1.326    
                         arrival time                           1.345    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_7[126]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_o/sr_p.sr_8[126]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.603ns
    Source Clock Delay      (SCD):    1.273ns
    Clock Pessimism Removal (CPR):    0.324ns
  Clock Net Delay (Source):      0.727ns (routing 0.096ns, distribution 0.631ns)
  Clock Net Delay (Destination): 0.831ns (routing 0.108ns, distribution 0.723ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=4851, routed)        0.727     1.273    shift_reg_tap_o/clk_c
    SLICE_X105Y517       FDRE                                         r  shift_reg_tap_o/sr_p.sr_7[126]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X105Y517       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.312 f  shift_reg_tap_o/sr_p.sr_7[126]/Q
                         net (fo=1, routed)           0.033     1.345    shift_reg_tap_o/sr_7[126]
    SLICE_X105Y517       FDRE                                         f  shift_reg_tap_o/sr_p.sr_8[126]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=4851, routed)        0.831     1.603    shift_reg_tap_o/clk_c
    SLICE_X105Y517       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[126]/C
                         clock pessimism             -0.324     1.279    
    SLICE_X105Y517       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.326    shift_reg_tap_o/sr_p.sr_8[126]
  -------------------------------------------------------------------
                         required time                         -1.326    
                         arrival time                           1.345    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_7[151]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_o/sr_p.sr_8[151]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.534ns
    Source Clock Delay      (SCD):    1.213ns
    Clock Pessimism Removal (CPR):    0.315ns
  Clock Net Delay (Source):      0.667ns (routing 0.096ns, distribution 0.571ns)
  Clock Net Delay (Destination): 0.762ns (routing 0.108ns, distribution 0.654ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=4851, routed)        0.667     1.213    shift_reg_tap_o/clk_c
    SLICE_X118Y566       FDRE                                         r  shift_reg_tap_o/sr_p.sr_7[151]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X118Y566       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.252 r  shift_reg_tap_o/sr_p.sr_7[151]/Q
                         net (fo=1, routed)           0.033     1.285    shift_reg_tap_o/sr_7[151]
    SLICE_X118Y566       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[151]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=4851, routed)        0.762     1.534    shift_reg_tap_o/clk_c
    SLICE_X118Y566       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[151]/C
                         clock pessimism             -0.315     1.219    
    SLICE_X118Y566       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.266    shift_reg_tap_o/sr_p.sr_8[151]
  -------------------------------------------------------------------
                         required time                         -1.266    
                         arrival time                           1.285    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_7[151]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_o/sr_p.sr_8[151]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.534ns
    Source Clock Delay      (SCD):    1.213ns
    Clock Pessimism Removal (CPR):    0.315ns
  Clock Net Delay (Source):      0.667ns (routing 0.096ns, distribution 0.571ns)
  Clock Net Delay (Destination): 0.762ns (routing 0.108ns, distribution 0.654ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=4851, routed)        0.667     1.213    shift_reg_tap_o/clk_c
    SLICE_X118Y566       FDRE                                         r  shift_reg_tap_o/sr_p.sr_7[151]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X118Y566       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.252 f  shift_reg_tap_o/sr_p.sr_7[151]/Q
                         net (fo=1, routed)           0.033     1.285    shift_reg_tap_o/sr_7[151]
    SLICE_X118Y566       FDRE                                         f  shift_reg_tap_o/sr_p.sr_8[151]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=4851, routed)        0.762     1.534    shift_reg_tap_o/clk_c
    SLICE_X118Y566       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[151]/C
                         clock pessimism             -0.315     1.219    
    SLICE_X118Y566       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.266    shift_reg_tap_o/sr_p.sr_8[151]
  -------------------------------------------------------------------
                         required time                         -1.266    
                         arrival time                           1.285    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_7[89]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Destination:            shift_reg_tap_o/sr_p.sr_8[89]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.563ns period=3.125ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.588ns
    Source Clock Delay      (SCD):    1.261ns
    Clock Pessimism Removal (CPR):    0.321ns
  Clock Net Delay (Source):      0.715ns (routing 0.096ns, distribution 0.619ns)
  Clock Net Delay (Destination): 0.816ns (routing 0.108ns, distribution 0.708ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=4851, routed)        0.715     1.261    shift_reg_tap_o/clk_c
    SLICE_X106Y519       FDRE                                         r  shift_reg_tap_o/sr_p.sr_7[89]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X106Y519       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.300 r  shift_reg_tap_o/sr_p.sr_7[89]/Q
                         net (fo=1, routed)           0.033     1.333    shift_reg_tap_o/sr_7[89]
    SLICE_X106Y519       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[89]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X4Y9 (CLOCK_ROOT)    net (fo=4851, routed)        0.816     1.588    shift_reg_tap_o/clk_c
    SLICE_X106Y519       FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[89]/C
                         clock pessimism             -0.321     1.267    
    SLICE_X106Y519       FDRE (Hold_HFF2_SLICEL_C_D)
                                                      0.047     1.314    shift_reg_tap_o/sr_p.sr_8[89]
  -------------------------------------------------------------------
                         required time                         -1.314    
                         arrival time                           1.333    
  -------------------------------------------------------------------
                         slack                                  0.019    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.563 }
Period(ns):         3.125
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         3.125       1.626      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X109Y524  muon_sorter_1/sr_p.sr_1_0.roi_ret/C
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X109Y527  muon_sorter_1/sr_p.sr_1_0.roi_ret_0/C
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X110Y525  muon_sorter_1/sr_p.sr_1_0.roi_ret_1/C
Min Period        n/a     FDRE/C    n/a            0.550         3.125       2.575      SLICE_X109Y526  muon_sorter_1/sr_p.sr_1_0.roi_ret_10/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X105Y518  muon_sorter_1/sr_p.sr_1_0.roi_ret_100/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X123Y564  muon_sorter_1/sr_p.sr_1_15.roi_ret_12/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X106Y525  muon_sorter_1/sr_p.sr_1_2.roi_ret_80/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X103Y522  muon_sorter_1/sr_p.sr_1_2.roi_ret_84/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         1.562       1.287      SLICE_X105Y524  muon_sorter_1/sr_p.sr_1_2.roi_ret_90/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X102Y519  muon_sorter_1/sr_p.sr_1_0.roi_ret_103/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X108Y527  muon_sorter_1/sr_p.sr_1_0.roi_ret_104/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X111Y534  muon_sorter_1/sr_p.sr_1_12.roi[5]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X111Y533  muon_sorter_1/sr_p.sr_1_12.roi[6]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         1.563       1.288      SLICE_X103Y525  muon_sorter_1/sr_p.sr_1_2.roi_ret_8/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X103Y510         reducer_1/delay_block[0][224]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X103Y510         reducer_1/delay_block[0][225]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X103Y510         reducer_1/delay_block[0][226]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y510         reducer_1/delay_block[0][228]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y510         reducer_1/delay_block[0][229]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y510         reducer_1/delay_block[0][230]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y510         reducer_1/delay_block[0][228]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X103Y510         reducer_1/delay_block[0][229]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y492         reducer_1/delay_block[0][22]/C



