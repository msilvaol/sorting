Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Tue Mar  5 22:39:06 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.116        0.000                      0                  208        2.850        0.000                       0                   905  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 3.125}        6.250           160.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.116        0.000                      0                  208        2.850        0.000                       0                   417  
clk_wrapper                                                                             498.562        0.000                       0                   488  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.116ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        2.850ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.116ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[94]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[198]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.269ns  (logic 0.067ns (24.907%)  route 0.202ns (75.093%))
  Logic Levels:           2  (LUT4=2)
  Clock Path Skew:        0.107ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.052ns
    Source Clock Delay      (SCD):    1.650ns
    Clock Pessimism Removal (CPR):    0.295ns
  Clock Net Delay (Source):      1.104ns (routing 0.638ns, distribution 0.466ns)
  Clock Net Delay (Destination): 1.280ns (routing 0.707ns, distribution 0.573ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=416, routed)         1.104     1.650    shift_reg_tap_i/clk_c
    SLICE_X99Y477        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[94]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X99Y477        FDRE (Prop_GFF_SLICEM_C_Q)
                                                      0.039     1.689 r  shift_reg_tap_i/sr_p.sr_1[94]/Q
                         net (fo=23, routed)          0.137     1.826    dut_inst/stage_g.1.pair_g.3.csn_cmp_inst/input_slr_92
    SLICE_X102Y480       LUT4 (Prop_E6LUT_SLICEM_I3_O)
                                                      0.014     1.840 r  dut_inst/stage_g.1.pair_g.3.csn_cmp_inst/b_o_comb.pt[3]/O
                         net (fo=13, routed)          0.041     1.881    dut_inst/stage_g.3.pair_g.7.csn_cmp_inst/pt_48_0
    SLICE_X102Y480       LUT4 (Prop_C6LUT_SLICEM_I3_O)
                                                      0.014     1.895 r  dut_inst/stage_g.3.pair_g.7.csn_cmp_inst/b_o_comb.pt[3]/O
                         net (fo=1, routed)           0.024     1.919    shift_reg_tap_o/pt[3]
    SLICE_X102Y480       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[198]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=416, routed)         1.280     2.052    shift_reg_tap_o/clk_c
    SLICE_X102Y480       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[198]/C
                         clock pessimism             -0.295     1.757    
    SLICE_X102Y480       FDRE (Hold_CFF_SLICEM_C_D)
                                                      0.046     1.803    shift_reg_tap_o/sr_p.sr_1[198]
  -------------------------------------------------------------------
                         required time                         -1.803    
                         arrival time                           1.919    
  -------------------------------------------------------------------
                         slack                                  0.116    

Slack (MET) :             0.116ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[94]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[198]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.269ns  (logic 0.067ns (24.907%)  route 0.202ns (75.093%))
  Logic Levels:           2  (LUT4=2)
  Clock Path Skew:        0.107ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.052ns
    Source Clock Delay      (SCD):    1.650ns
    Clock Pessimism Removal (CPR):    0.295ns
  Clock Net Delay (Source):      1.104ns (routing 0.638ns, distribution 0.466ns)
  Clock Net Delay (Destination): 1.280ns (routing 0.707ns, distribution 0.573ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=416, routed)         1.104     1.650    shift_reg_tap_i/clk_c
    SLICE_X99Y477        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[94]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X99Y477        FDRE (Prop_GFF_SLICEM_C_Q)
                                                      0.039     1.689 f  shift_reg_tap_i/sr_p.sr_1[94]/Q
                         net (fo=23, routed)          0.137     1.826    dut_inst/stage_g.1.pair_g.3.csn_cmp_inst/input_slr_92
    SLICE_X102Y480       LUT4 (Prop_E6LUT_SLICEM_I3_O)
                                                      0.014     1.840 f  dut_inst/stage_g.1.pair_g.3.csn_cmp_inst/b_o_comb.pt[3]/O
                         net (fo=13, routed)          0.041     1.881    dut_inst/stage_g.3.pair_g.7.csn_cmp_inst/pt_48_0
    SLICE_X102Y480       LUT4 (Prop_C6LUT_SLICEM_I3_O)
                                                      0.014     1.895 f  dut_inst/stage_g.3.pair_g.7.csn_cmp_inst/b_o_comb.pt[3]/O
                         net (fo=1, routed)           0.024     1.919    shift_reg_tap_o/pt[3]
    SLICE_X102Y480       FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[198]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=416, routed)         1.280     2.052    shift_reg_tap_o/clk_c
    SLICE_X102Y480       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[198]/C
                         clock pessimism             -0.295     1.757    
    SLICE_X102Y480       FDRE (Hold_CFF_SLICEM_C_D)
                                                      0.046     1.803    shift_reg_tap_o/sr_p.sr_1[198]
  -------------------------------------------------------------------
                         required time                         -1.803    
                         arrival time                           1.919    
  -------------------------------------------------------------------
                         slack                                  0.116    

Slack (MET) :             0.136ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[81]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[198]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.286ns  (logic 0.074ns (25.874%)  route 0.212ns (74.126%))
  Logic Levels:           2  (LUT4=2)
  Clock Path Skew:        0.104ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.052ns
    Source Clock Delay      (SCD):    1.653ns
    Clock Pessimism Removal (CPR):    0.295ns
  Clock Net Delay (Source):      1.107ns (routing 0.638ns, distribution 0.469ns)
  Clock Net Delay (Destination): 1.280ns (routing 0.707ns, distribution 0.573ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=416, routed)         1.107     1.653    shift_reg_tap_i/clk_c
    SLICE_X100Y477       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[81]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X100Y477       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.038     1.691 r  shift_reg_tap_i/sr_p.sr_1[81]/Q
                         net (fo=23, routed)          0.147     1.838    dut_inst/stage_g.1.pair_g.3.csn_cmp_inst/input_slr_79
    SLICE_X102Y480       LUT4 (Prop_E6LUT_SLICEM_I2_O)
                                                      0.022     1.860 r  dut_inst/stage_g.1.pair_g.3.csn_cmp_inst/b_o_comb.pt[3]/O
                         net (fo=13, routed)          0.041     1.901    dut_inst/stage_g.3.pair_g.7.csn_cmp_inst/pt_48_0
    SLICE_X102Y480       LUT4 (Prop_C6LUT_SLICEM_I3_O)
                                                      0.014     1.915 r  dut_inst/stage_g.3.pair_g.7.csn_cmp_inst/b_o_comb.pt[3]/O
                         net (fo=1, routed)           0.024     1.939    shift_reg_tap_o/pt[3]
    SLICE_X102Y480       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[198]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=416, routed)         1.280     2.052    shift_reg_tap_o/clk_c
    SLICE_X102Y480       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[198]/C
                         clock pessimism             -0.295     1.757    
    SLICE_X102Y480       FDRE (Hold_CFF_SLICEM_C_D)
                                                      0.046     1.803    shift_reg_tap_o/sr_p.sr_1[198]
  -------------------------------------------------------------------
                         required time                         -1.803    
                         arrival time                           1.939    
  -------------------------------------------------------------------
                         slack                                  0.136    

Slack (MET) :             0.136ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[81]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[198]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.286ns  (logic 0.074ns (25.874%)  route 0.212ns (74.126%))
  Logic Levels:           2  (LUT4=2)
  Clock Path Skew:        0.104ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.052ns
    Source Clock Delay      (SCD):    1.653ns
    Clock Pessimism Removal (CPR):    0.295ns
  Clock Net Delay (Source):      1.107ns (routing 0.638ns, distribution 0.469ns)
  Clock Net Delay (Destination): 1.280ns (routing 0.707ns, distribution 0.573ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=416, routed)         1.107     1.653    shift_reg_tap_i/clk_c
    SLICE_X100Y477       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[81]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X100Y477       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.038     1.691 r  shift_reg_tap_i/sr_p.sr_1[81]/Q
                         net (fo=23, routed)          0.147     1.838    dut_inst/stage_g.1.pair_g.3.csn_cmp_inst/input_slr_79
    SLICE_X102Y480       LUT4 (Prop_E6LUT_SLICEM_I2_O)
                                                      0.022     1.860 r  dut_inst/stage_g.1.pair_g.3.csn_cmp_inst/b_o_comb.pt[3]/O
                         net (fo=13, routed)          0.041     1.901    dut_inst/stage_g.3.pair_g.7.csn_cmp_inst/pt_48_0
    SLICE_X102Y480       LUT4 (Prop_C6LUT_SLICEM_I3_O)
                                                      0.014     1.915 r  dut_inst/stage_g.3.pair_g.7.csn_cmp_inst/b_o_comb.pt[3]/O
                         net (fo=1, routed)           0.024     1.939    shift_reg_tap_o/pt[3]
    SLICE_X102Y480       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[198]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=416, routed)         1.280     2.052    shift_reg_tap_o/clk_c
    SLICE_X102Y480       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[198]/C
                         clock pessimism             -0.295     1.757    
    SLICE_X102Y480       FDRE (Hold_CFF_SLICEM_C_D)
                                                      0.046     1.803    shift_reg_tap_o/sr_p.sr_1[198]
  -------------------------------------------------------------------
                         required time                         -1.803    
                         arrival time                           1.939    
  -------------------------------------------------------------------
                         slack                                  0.136    

Slack (MET) :             0.136ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[81]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[198]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.286ns  (logic 0.074ns (25.874%)  route 0.212ns (74.126%))
  Logic Levels:           2  (LUT4=2)
  Clock Path Skew:        0.104ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.052ns
    Source Clock Delay      (SCD):    1.653ns
    Clock Pessimism Removal (CPR):    0.295ns
  Clock Net Delay (Source):      1.107ns (routing 0.638ns, distribution 0.469ns)
  Clock Net Delay (Destination): 1.280ns (routing 0.707ns, distribution 0.573ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=416, routed)         1.107     1.653    shift_reg_tap_i/clk_c
    SLICE_X100Y477       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[81]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X100Y477       FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.038     1.691 f  shift_reg_tap_i/sr_p.sr_1[81]/Q
                         net (fo=23, routed)          0.147     1.838    dut_inst/stage_g.1.pair_g.3.csn_cmp_inst/input_slr_79
    SLICE_X102Y480       LUT4 (Prop_E6LUT_SLICEM_I2_O)
                                                      0.022     1.860 f  dut_inst/stage_g.1.pair_g.3.csn_cmp_inst/b_o_comb.pt[3]/O
                         net (fo=13, routed)          0.041     1.901    dut_inst/stage_g.3.pair_g.7.csn_cmp_inst/pt_48_0
    SLICE_X102Y480       LUT4 (Prop_C6LUT_SLICEM_I3_O)
                                                      0.014     1.915 f  dut_inst/stage_g.3.pair_g.7.csn_cmp_inst/b_o_comb.pt[3]/O
                         net (fo=1, routed)           0.024     1.939    shift_reg_tap_o/pt[3]
    SLICE_X102Y480       FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[198]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=416, routed)         1.280     2.052    shift_reg_tap_o/clk_c
    SLICE_X102Y480       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[198]/C
                         clock pessimism             -0.295     1.757    
    SLICE_X102Y480       FDRE (Hold_CFF_SLICEM_C_D)
                                                      0.046     1.803    shift_reg_tap_o/sr_p.sr_1[198]
  -------------------------------------------------------------------
                         required time                         -1.803    
                         arrival time                           1.939    
  -------------------------------------------------------------------
                         slack                                  0.136    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 3.125 }
Period(ns):         6.250
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         6.250       4.751      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X96Y478   shift_reg_tap_i/sr_p.sr_1[146]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X95Y469   shift_reg_tap_i/sr_p.sr_1[147]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X95Y469   shift_reg_tap_i/sr_p.sr_1[148]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X95Y469   shift_reg_tap_i/sr_p.sr_1[149]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X103Y475  shift_reg_tap_o/sr_p.sr_1[118]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X103Y475  shift_reg_tap_o/sr_p.sr_1[123]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X103Y475  shift_reg_tap_o/sr_p.sr_1[131]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X103Y475  shift_reg_tap_o/sr_p.sr_1[136]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X95Y469   shift_reg_tap_i/sr_p.sr_1[147]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X97Y468   shift_reg_tap_i/sr_p.sr_1[151]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X96Y474   shift_reg_tap_i/sr_p.sr_1[156]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X96Y474   shift_reg_tap_i/sr_p.sr_1[157]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X96Y474   shift_reg_tap_i/sr_p.sr_1[158]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X96Y474   shift_reg_tap_i/sr_p.sr_1[159]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X99Y468          lsfr_1/shiftreg_vector[177]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X99Y468          lsfr_1/shiftreg_vector[178]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X99Y468          lsfr_1/shiftreg_vector[179]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X98Y468          lsfr_1/shiftreg_vector[180]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X98Y468          lsfr_1/shiftreg_vector[181]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X98Y468          lsfr_1/shiftreg_vector[182]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X95Y468          lsfr_1/shiftreg_vector[183]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y469          lsfr_1/shiftreg_vector[188]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y469          lsfr_1/shiftreg_vector[189]/C



