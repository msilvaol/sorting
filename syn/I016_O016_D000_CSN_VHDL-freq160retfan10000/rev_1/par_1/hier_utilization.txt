Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Tue Mar  5 22:38:50 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_utilization -hierarchical -file hier_utilization.txt
| Design       : wrapper_csn
| Device       : xcvu9pflgc2104-1
| Design State : Routed
------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+-------------------------------------+--------------------------------------------------------------+------------+------------+---------+------+-----+--------+--------+------+--------------+
|               Instance              |                            Module                            | Total LUTs | Logic LUTs | LUTRAMs | SRLs | FFs | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+-------------------------------------+--------------------------------------------------------------+------------+------------+---------+------+-----+--------+--------+------+--------------+
| wrapper_csn                         |                                                        (top) |       1054 |       1054 |       0 |    0 | 902 |      0 |      0 |    0 |            0 |
|   (wrapper_csn)                     |                                                        (top) |          0 |          0 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|   dut_inst                          | muon_sorter_I016_O016_D000_CSN_VHDL-freq160retfan10000_rev_1 |        984 |        984 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.0.csn_cmp_inst |                                  csn_cmp_false_false_false_0 |         13 |         13 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.1.csn_cmp_inst |                                 csn_cmp_false_false_false_42 |         11 |         11 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_39 |         15 |         15 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.3.csn_cmp_inst |                                 csn_cmp_false_false_false_36 |         10 |         10 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.4.csn_cmp_inst |                                 csn_cmp_false_false_false_33 |         17 |         17 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.5.csn_cmp_inst |                                 csn_cmp_false_false_false_43 |         14 |         14 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.6.csn_cmp_inst |                                 csn_cmp_false_false_false_27 |         15 |         15 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.7.csn_cmp_inst |                                 csn_cmp_false_false_false_14 |         13 |         13 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.0.csn_cmp_inst |                                 csn_cmp_false_false_false_57 |         16 |         16 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.1.csn_cmp_inst |                                 csn_cmp_false_false_false_54 |         11 |         11 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_51 |         15 |         15 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.3.csn_cmp_inst |                                 csn_cmp_false_false_false_25 |        112 |        112 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.4.csn_cmp_inst |                                 csn_cmp_false_false_false_47 |         14 |         14 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.5.csn_cmp_inst |                                    csn_cmp_false_false_false |         20 |         20 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.6.csn_cmp_inst |                                 csn_cmp_false_false_false_46 |         13 |         13 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.7.csn_cmp_inst |                                 csn_cmp_false_false_false_40 |         16 |         16 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.0.csn_cmp_inst |                                 csn_cmp_false_false_false_37 |         12 |         12 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.1.csn_cmp_inst |                                 csn_cmp_false_false_false_34 |         16 |         16 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_45 |          9 |          9 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.3.csn_cmp_inst |                                 csn_cmp_false_false_false_28 |         13 |         13 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.4.csn_cmp_inst |                                 csn_cmp_false_false_false_15 |         16 |         16 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.5.csn_cmp_inst |                                 csn_cmp_false_false_false_58 |         12 |         12 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.6.csn_cmp_inst |                                 csn_cmp_false_false_false_55 |          7 |          7 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.7.csn_cmp_inst |                                 csn_cmp_false_false_false_52 |         10 |         10 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.0.csn_cmp_inst |                                 csn_cmp_false_false_false_26 |         20 |         20 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.1.csn_cmp_inst |                                 csn_cmp_false_false_false_48 |         16 |         16 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_10 |         18 |         18 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.3.csn_cmp_inst |                                  csn_cmp_false_false_false_9 |         15 |         15 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.4.csn_cmp_inst |                                 csn_cmp_false_false_false_41 |         12 |         12 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.5.csn_cmp_inst |                                 csn_cmp_false_false_false_38 |         18 |         18 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.6.csn_cmp_inst |                                 csn_cmp_false_false_false_35 |         14 |         14 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.7.csn_cmp_inst |                                 csn_cmp_false_false_false_32 |         19 |         19 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.0.csn_cmp_inst |                                 csn_cmp_false_false_false_29 |         10 |         10 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.1.csn_cmp_inst |                                 csn_cmp_false_false_false_16 |         15 |         15 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_59 |         12 |         12 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.3.csn_cmp_inst |                                 csn_cmp_false_false_false_56 |         12 |         12 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.4.csn_cmp_inst |                                 csn_cmp_false_false_false_53 |         15 |         15 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.5.csn_cmp_inst |                                 csn_cmp_false_false_false_50 |          9 |          9 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.6.csn_cmp_inst |                                 csn_cmp_false_false_false_24 |         13 |         13 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.0.csn_cmp_inst |                                  csn_cmp_false_false_false_8 |         14 |         14 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.1.csn_cmp_inst |                                  csn_cmp_false_false_false_6 |         18 |         18 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.2.csn_cmp_inst |                                  csn_cmp_false_false_false_4 |         16 |         16 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.3.csn_cmp_inst |                                  csn_cmp_false_false_false_2 |         15 |         15 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.4.csn_cmp_inst |                                 csn_cmp_false_false_false_31 |         18 |         18 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.5.csn_cmp_inst |                                 csn_cmp_false_false_false_30 |         15 |         15 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.6.csn_cmp_inst |                                 csn_cmp_false_false_false_17 |         15 |         15 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.0.csn_cmp_inst |                                 csn_cmp_false_false_false_22 |         18 |         18 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.1.csn_cmp_inst |                                 csn_cmp_false_false_false_20 |         10 |         10 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_13 |         16 |         16 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.3.csn_cmp_inst |                                 csn_cmp_false_false_false_11 |         22 |         22 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.4.csn_cmp_inst |                                 csn_cmp_false_false_false_19 |         17 |         17 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.5.csn_cmp_inst |                                 csn_cmp_false_false_false_12 |         16 |         16 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.0.csn_cmp_inst |                                  csn_cmp_false_false_false_3 |         13 |         13 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.1.csn_cmp_inst |                                  csn_cmp_false_false_false_1 |         19 |         19 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_44 |         14 |         14 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.3.csn_cmp_inst |                                 csn_cmp_false_false_false_18 |         11 |         11 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.0.csn_cmp_inst |                                 csn_cmp_false_false_false_49 |         16 |         16 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.1.csn_cmp_inst |                                 csn_cmp_false_false_false_23 |         16 |         16 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_21 |         14 |         14 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.3.csn_cmp_inst |                                  csn_cmp_false_false_false_7 |         16 |         16 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.4.csn_cmp_inst |                                  csn_cmp_false_false_false_5 |         17 |         17 |       0 |    0 |   0 |      0 |      0 |    0 |            0 |
|   lsfr_1                            |                                                         lfsr |          1 |          1 |       0 |    0 | 209 |      0 |      0 |    0 |            0 |
|   reducer_1                         |                                                      reducer |         69 |         69 |       0 |    0 | 277 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_i                   |                                          shift_reg_tap_208_1 |          0 |          0 |       0 |    0 | 208 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_o                   |                                          shift_reg_tap_256_1 |          0 |          0 |       0 |    0 | 208 |      0 |      0 |    0 |            0 |
+-------------------------------------+--------------------------------------------------------------+------------+------------+---------+------+-----+--------+--------+------+--------------+
* Note: The sum of lower-level cells may be larger than their parent cells total, due to cross-hierarchy LUT combining


