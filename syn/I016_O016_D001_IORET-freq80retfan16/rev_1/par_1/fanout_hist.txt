Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Wed Feb 13 21:22:02 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+-------+--------+
|  Fanout |  Nets |      % |
+---------+-------+--------+
|       1 |  6883 |  55.17 |
|       2 |  1065 |   8.53 |
|       3 |   760 |   6.09 |
|       4 |   496 |   3.97 |
|    5-10 |  2223 |  17.82 |
|   11-50 |  1047 |   8.39 |
|  51-100 |     0 |   0.00 |
| 101-500 |     0 |   0.00 |
|    >500 |     0 |   0.00 |
+---------+-------+--------+
|     ALL | 12474 | 100.00 |
+---------+-------+--------+


