Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Sat Mar  2 21:05:00 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_drc -file post_route_drc.txt
| Design       : bitonic_sorter_16_top
| Device       : xcvu9p-flgc2104-1-e
| Speed File   : -1
| Design State : Routed
------------------------------------------------------------------------------------

Report DRC

Table of Contents
-----------------
1. REPORT SUMMARY
2. REPORT DETAILS

1. REPORT SUMMARY
-----------------
            Netlist: netlist
          Floorplan: design_1
      Design limits: <entire design considered>
           Ruledeck: default
             Max violations: <unlimited>
             Violations found: 2
+--------+------------------+----------------------------+------------+
| Rule   | Severity         | Description                | Violations |
+--------+------------------+----------------------------+------------+
| NSTD-1 | Critical Warning | Unspecified I/O Standard   | 1          |
| UCIO-1 | Critical Warning | Unconstrained Logical Port | 1          |
+--------+------------------+----------------------------+------------+

2. REPORT DETAILS
-----------------
NSTD-1#1 Critical Warning
Unspecified I/O Standard  
4 out of 4 logical ports use I/O standard (IOSTANDARD) value 'DEFAULT', instead of a user assigned specific value. This may cause I/O contention or incompatibility with the board power or connectivity affecting performance, signal integrity or in extreme cases cause damage to the device or the components to which it is connected. To correct this violation, specify all I/O standards. This design will fail to generate a bitstream unless all logical ports have a user specified I/O standard value defined. To allow bitstream creation with unspecified I/O standard values (not recommended), use this command: set_property SEVERITY {Warning} [get_drc_checks NSTD-1].  NOTE: When using the Vivado Runs infrastructure (e.g. launch_runs Tcl command), add this command to a .tcl file and add that file as a pre-hook for write_bitstream step for the implementation run. Problem ports: clk, clk_wrapper, din, dout.
Related violations: <none>

UCIO-1#1 Critical Warning
Unconstrained Logical Port  
4 out of 4 logical ports have no user assigned specific location constraint (LOC). This may cause I/O contention or incompatibility with the board power or connectivity affecting performance, signal integrity or in extreme cases cause damage to the device or the components to which it is connected. To correct this violation, specify all pin locations. This design will fail to generate a bitstream unless all logical ports have a user specified site LOC constraint defined.  To allow bitstream creation with unspecified pin locations (not recommended), use this command: set_property SEVERITY {Warning} [get_drc_checks UCIO-1].  NOTE: When using the Vivado Runs infrastructure (e.g. launch_runs Tcl command), add this command to a .tcl file and add that file as a pre-hook for write_bitstream step for the implementation run.  Problem ports: clk, clk_wrapper, din, dout.
Related violations: <none>


