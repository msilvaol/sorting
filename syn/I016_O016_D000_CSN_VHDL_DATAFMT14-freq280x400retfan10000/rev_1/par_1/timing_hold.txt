Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Wed Mar  6 17:16:47 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.019        0.000                      0                  711        1.213        0.000                       0                  1429  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 1.786}        3.572           279.955         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.019        0.000                      0                  711        1.213        0.000                       0                   941  
clk_wrapper                                                                             498.562        0.000                       0                   488  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.019ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        1.213ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_3_10.idx[6]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[140]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.642ns
    Source Clock Delay      (SCD):    1.316ns
    Clock Pessimism Removal (CPR):    0.320ns
  Clock Net Delay (Source):      0.770ns (routing 0.316ns, distribution 0.454ns)
  Clock Net Delay (Destination): 0.870ns (routing 0.348ns, distribution 0.522ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=940, routed)         0.770     1.316    dut_inst/clk_c
    SLICE_X96Y578        FDRE                                         r  dut_inst/ret_array_3_10.idx[6]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y578        FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.039     1.355 r  dut_inst/ret_array_3_10.idx[6]/Q
                         net (fo=1, routed)           0.033     1.388    shift_reg_tap_o/idx_4[6]
    SLICE_X96Y578        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[140]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=940, routed)         0.870     1.642    shift_reg_tap_o/clk_c
    SLICE_X96Y578        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[140]/C
                         clock pessimism             -0.320     1.322    
    SLICE_X96Y578        FDRE (Hold_CFF2_SLICEL_C_D)
                                                      0.047     1.369    shift_reg_tap_o/sr_p.sr_1[140]
  -------------------------------------------------------------------
                         required time                         -1.369    
                         arrival time                           1.388    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.019ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_3_10.idx[6]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[140]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.072ns  (logic 0.039ns (54.167%)  route 0.033ns (45.833%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.642ns
    Source Clock Delay      (SCD):    1.316ns
    Clock Pessimism Removal (CPR):    0.320ns
  Clock Net Delay (Source):      0.770ns (routing 0.316ns, distribution 0.454ns)
  Clock Net Delay (Destination): 0.870ns (routing 0.348ns, distribution 0.522ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=940, routed)         0.770     1.316    dut_inst/clk_c
    SLICE_X96Y578        FDRE                                         r  dut_inst/ret_array_3_10.idx[6]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y578        FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.039     1.355 f  dut_inst/ret_array_3_10.idx[6]/Q
                         net (fo=1, routed)           0.033     1.388    shift_reg_tap_o/idx_4[6]
    SLICE_X96Y578        FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[140]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=940, routed)         0.870     1.642    shift_reg_tap_o/clk_c
    SLICE_X96Y578        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[140]/C
                         clock pessimism             -0.320     1.322    
    SLICE_X96Y578        FDRE (Hold_CFF2_SLICEL_C_D)
                                                      0.047     1.369    shift_reg_tap_o/sr_p.sr_1[140]
  -------------------------------------------------------------------
                         required time                         -1.369    
                         arrival time                           1.388    
  -------------------------------------------------------------------
                         slack                                  0.019    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_3_11.idx[2]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[149]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.648ns
    Source Clock Delay      (SCD):    1.320ns
    Clock Pessimism Removal (CPR):    0.322ns
  Clock Net Delay (Source):      0.774ns (routing 0.316ns, distribution 0.458ns)
  Clock Net Delay (Destination): 0.876ns (routing 0.348ns, distribution 0.528ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=940, routed)         0.774     1.320    dut_inst/clk_c
    SLICE_X96Y580        FDRE                                         r  dut_inst/ret_array_3_11.idx[2]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y580        FDRE (Prop_EFF_SLICEL_C_Q)
                                                      0.039     1.359 r  dut_inst/ret_array_3_11.idx[2]/Q
                         net (fo=1, routed)           0.034     1.393    shift_reg_tap_o/idx_6[2]
    SLICE_X96Y580        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[149]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=940, routed)         0.876     1.648    shift_reg_tap_o/clk_c
    SLICE_X96Y580        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[149]/C
                         clock pessimism             -0.322     1.326    
    SLICE_X96Y580        FDRE (Hold_GFF2_SLICEL_C_D)
                                                      0.047     1.373    shift_reg_tap_o/sr_p.sr_1[149]
  -------------------------------------------------------------------
                         required time                         -1.373    
                         arrival time                           1.393    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_3_11.idx[2]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[149]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.648ns
    Source Clock Delay      (SCD):    1.320ns
    Clock Pessimism Removal (CPR):    0.322ns
  Clock Net Delay (Source):      0.774ns (routing 0.316ns, distribution 0.458ns)
  Clock Net Delay (Destination): 0.876ns (routing 0.348ns, distribution 0.528ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=940, routed)         0.774     1.320    dut_inst/clk_c
    SLICE_X96Y580        FDRE                                         r  dut_inst/ret_array_3_11.idx[2]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y580        FDRE (Prop_EFF_SLICEL_C_Q)
                                                      0.039     1.359 f  dut_inst/ret_array_3_11.idx[2]/Q
                         net (fo=1, routed)           0.034     1.393    shift_reg_tap_o/idx_6[2]
    SLICE_X96Y580        FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[149]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=940, routed)         0.876     1.648    shift_reg_tap_o/clk_c
    SLICE_X96Y580        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[149]/C
                         clock pessimism             -0.322     1.326    
    SLICE_X96Y580        FDRE (Hold_GFF2_SLICEL_C_D)
                                                      0.047     1.373    shift_reg_tap_o/sr_p.sr_1[149]
  -------------------------------------------------------------------
                         required time                         -1.373    
                         arrival time                           1.393    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.025ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_2_10.idx[4]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Destination:            dut_inst/ret_array_3_10.idx[4]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@1.786ns period=3.572ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.078ns  (logic 0.039ns (50.000%)  route 0.039ns (50.000%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.657ns
    Source Clock Delay      (SCD):    1.327ns
    Clock Pessimism Removal (CPR):    0.324ns
  Clock Net Delay (Source):      0.781ns (routing 0.316ns, distribution 0.465ns)
  Clock Net Delay (Destination): 0.885ns (routing 0.348ns, distribution 0.537ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=940, routed)         0.781     1.327    dut_inst/clk_c
    SLICE_X97Y579        FDRE                                         r  dut_inst/ret_array_2_10.idx[4]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X97Y579        FDRE (Prop_HFF_SLICEM_C_Q)
                                                      0.039     1.366 r  dut_inst/ret_array_2_10.idx[4]/Q
                         net (fo=1, routed)           0.039     1.405    dut_inst/idx_8[4]
    SLICE_X97Y579        FDRE                                         r  dut_inst/ret_array_3_10.idx[4]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=940, routed)         0.885     1.657    dut_inst/clk_c
    SLICE_X97Y579        FDRE                                         r  dut_inst/ret_array_3_10.idx[4]/C
                         clock pessimism             -0.324     1.333    
    SLICE_X97Y579        FDRE (Hold_EFF2_SLICEM_C_D)
                                                      0.047     1.380    dut_inst/ret_array_3_10.idx[4]
  -------------------------------------------------------------------
                         required time                         -1.380    
                         arrival time                           1.405    
  -------------------------------------------------------------------
                         slack                                  0.025    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 1.786 }
Period(ns):         3.572
Sources:            { clk }

Check Type        Corner  Lib Pin     Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location       Pin
Min Period        n/a     BUFGCE/I    n/a            1.499         3.572       2.073      BUFGCE_X1Y224  clk_ibuf/I
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.572       2.426      SLICE_X93Y573  dut_inst/ret_array_4_12.idx_5_pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.572       2.426      SLICE_X93Y573  dut_inst/ret_array_4_12.idx_6_pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.572       2.426      SLICE_X95Y572  dut_inst/ret_array_4_12.idx_7_pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         3.572       2.426      SLICE_X93Y573  dut_inst/ret_array_4_12.idx_pt_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X93Y580  dut_inst/ret_array_5_14.idx_1_idx_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X93Y580  dut_inst/ret_array_5_14.idx_7_idx_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X93Y580  dut_inst/ret_array_5_15.idx_5_idx_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X93Y580  dut_inst/ret_array_5_15.idx_idx_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X93Y573  dut_inst/ret_array_4_12.idx_5_pt_1/CLK
High Pulse Width  Fast    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X95Y572  dut_inst/ret_array_4_12.idx_7_pt_1/CLK
High Pulse Width  Fast    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X95Y572  dut_inst/ret_array_4_12.pt_0_pt_1/CLK
High Pulse Width  Fast    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X95Y579  dut_inst/ret_array_4_12.pt_2_pt_1/CLK
High Pulse Width  Fast    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X95Y572  dut_inst/ret_array_4_12.pt_pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         1.786       1.213      SLICE_X92Y580  dut_inst/ret_array_4_13.idx_0_pt_1/CLK



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X95Y599          lsfr_1/output_vector_1[207]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X96Y597          lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X100Y598         lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X102Y594         lsfr_1/shiftreg_vector[10]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y573          reducer_1/delay_block[1][36]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y573          reducer_1/delay_block[1][37]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X100Y598         lsfr_1/shiftreg_vector[100]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X100Y598         lsfr_1/shiftreg_vector[101]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X100Y598         lsfr_1/shiftreg_vector[102]/C



