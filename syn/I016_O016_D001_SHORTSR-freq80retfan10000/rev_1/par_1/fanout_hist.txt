Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Thu Feb 14 14:48:13 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+------+--------+
|  Fanout | Nets |      % |
+---------+------+--------+
|       1 | 4621 |  54.60 |
|       2 |  957 |  11.30 |
|       3 |  656 |   7.75 |
|       4 |  532 |   6.28 |
|    5-10 |  970 |  11.46 |
|   11-50 |  724 |   8.55 |
|  51-100 |    3 |   0.03 |
| 101-500 |    0 |   0.00 |
|    >500 |    0 |   0.00 |
+---------+------+--------+
|     ALL | 8463 | 100.00 |
+---------+------+--------+


