Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Tue Mar  5 22:59:03 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.020        0.000                      0                  463        2.850        0.000                       0                  1160  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 3.125}        6.250           160.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.020        0.000                      0                  463        2.850        0.000                       0                   672  
clk_wrapper                                                                             498.562        0.000                       0                   488  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.020ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        2.850ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[97]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret_13[11]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.004ns
    Source Clock Delay      (SCD):    1.639ns
    Clock Pessimism Removal (CPR):    0.359ns
  Clock Net Delay (Source):      1.093ns (routing 0.638ns, distribution 0.455ns)
  Clock Net Delay (Destination): 1.232ns (routing 0.707ns, distribution 0.525ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=671, routed)         1.093     1.639    shift_reg_tap_i/clk_c
    SLICE_X96Y463        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[97]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y463        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.678 r  shift_reg_tap_i/sr_p.sr_1[97]/Q
                         net (fo=1, routed)           0.034     1.712    dut_inst/input_slr[97]
    SLICE_X96Y463        FDRE                                         r  dut_inst/ret_array_1_12.idx_ret_13[11]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=671, routed)         1.232     2.004    dut_inst/clk_c
    SLICE_X96Y463        FDRE                                         r  dut_inst/ret_array_1_12.idx_ret_13[11]/C
                         clock pessimism             -0.359     1.645    
    SLICE_X96Y463        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.692    dut_inst/ret_array_1_12.idx_ret_13[11]
  -------------------------------------------------------------------
                         required time                         -1.692    
                         arrival time                           1.712    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[97]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret_13[11]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.004ns
    Source Clock Delay      (SCD):    1.639ns
    Clock Pessimism Removal (CPR):    0.359ns
  Clock Net Delay (Source):      1.093ns (routing 0.638ns, distribution 0.455ns)
  Clock Net Delay (Destination): 1.232ns (routing 0.707ns, distribution 0.525ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=671, routed)         1.093     1.639    shift_reg_tap_i/clk_c
    SLICE_X96Y463        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[97]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y463        FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.678 f  shift_reg_tap_i/sr_p.sr_1[97]/Q
                         net (fo=1, routed)           0.034     1.712    dut_inst/input_slr[97]
    SLICE_X96Y463        FDRE                                         f  dut_inst/ret_array_1_12.idx_ret_13[11]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=671, routed)         1.232     2.004    dut_inst/clk_c
    SLICE_X96Y463        FDRE                                         r  dut_inst/ret_array_1_12.idx_ret_13[11]/C
                         clock pessimism             -0.359     1.645    
    SLICE_X96Y463        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.692    dut_inst/ret_array_1_12.idx_ret_13[11]
  -------------------------------------------------------------------
                         required time                         -1.692    
                         arrival time                           1.712    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.021ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[103]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret_13[17]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.074ns  (logic 0.039ns (52.703%)  route 0.035ns (47.297%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.001ns
    Source Clock Delay      (SCD):    1.636ns
    Clock Pessimism Removal (CPR):    0.359ns
  Clock Net Delay (Source):      1.090ns (routing 0.638ns, distribution 0.452ns)
  Clock Net Delay (Destination): 1.229ns (routing 0.707ns, distribution 0.522ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=671, routed)         1.090     1.636    shift_reg_tap_i/clk_c
    SLICE_X94Y464        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[103]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X94Y464        FDRE (Prop_GFF_SLICEL_C_Q)
                                                      0.039     1.675 r  shift_reg_tap_i/sr_p.sr_1[103]/Q
                         net (fo=1, routed)           0.035     1.710    dut_inst/input_slr[103]
    SLICE_X94Y464        FDRE                                         r  dut_inst/ret_array_1_12.idx_ret_13[17]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=671, routed)         1.229     2.001    dut_inst/clk_c
    SLICE_X94Y464        FDRE                                         r  dut_inst/ret_array_1_12.idx_ret_13[17]/C
                         clock pessimism             -0.359     1.642    
    SLICE_X94Y464        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.689    dut_inst/ret_array_1_12.idx_ret_13[17]
  -------------------------------------------------------------------
                         required time                         -1.689    
                         arrival time                           1.710    
  -------------------------------------------------------------------
                         slack                                  0.021    

Slack (MET) :             0.021ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[103]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret_13[17]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.074ns  (logic 0.039ns (52.703%)  route 0.035ns (47.297%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.001ns
    Source Clock Delay      (SCD):    1.636ns
    Clock Pessimism Removal (CPR):    0.359ns
  Clock Net Delay (Source):      1.090ns (routing 0.638ns, distribution 0.452ns)
  Clock Net Delay (Destination): 1.229ns (routing 0.707ns, distribution 0.522ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=671, routed)         1.090     1.636    shift_reg_tap_i/clk_c
    SLICE_X94Y464        FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[103]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X94Y464        FDRE (Prop_GFF_SLICEL_C_Q)
                                                      0.039     1.675 f  shift_reg_tap_i/sr_p.sr_1[103]/Q
                         net (fo=1, routed)           0.035     1.710    dut_inst/input_slr[103]
    SLICE_X94Y464        FDRE                                         f  dut_inst/ret_array_1_12.idx_ret_13[17]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=671, routed)         1.229     2.001    dut_inst/clk_c
    SLICE_X94Y464        FDRE                                         r  dut_inst/ret_array_1_12.idx_ret_13[17]/C
                         clock pessimism             -0.359     1.642    
    SLICE_X94Y464        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.689    dut_inst/ret_array_1_12.idx_ret_13[17]
  -------------------------------------------------------------------
                         required time                         -1.689    
                         arrival time                           1.710    
  -------------------------------------------------------------------
                         slack                                  0.021    

Slack (MET) :             0.025ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_1[202]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            dut_inst/ret_array_1_12.idx_ret[3]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.078ns  (logic 0.039ns (50.000%)  route 0.039ns (50.000%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.022ns
    Source Clock Delay      (SCD):    1.655ns
    Clock Pessimism Removal (CPR):    0.361ns
  Clock Net Delay (Source):      1.109ns (routing 0.638ns, distribution 0.471ns)
  Clock Net Delay (Destination): 1.250ns (routing 0.707ns, distribution 0.543ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=671, routed)         1.109     1.655    shift_reg_tap_i/clk_c
    SLICE_X100Y471       FDRE                                         r  shift_reg_tap_i/sr_p.sr_1[202]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X100Y471       FDRE (Prop_HFF_SLICEM_C_Q)
                                                      0.039     1.694 r  shift_reg_tap_i/sr_p.sr_1[202]/Q
                         net (fo=1, routed)           0.039     1.733    dut_inst/input_slr[202]
    SLICE_X100Y471       FDRE                                         r  dut_inst/ret_array_1_12.idx_ret[3]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=671, routed)         1.250     2.022    dut_inst/clk_c
    SLICE_X100Y471       FDRE                                         r  dut_inst/ret_array_1_12.idx_ret[3]/C
                         clock pessimism             -0.361     1.661    
    SLICE_X100Y471       FDRE (Hold_EFF2_SLICEM_C_D)
                                                      0.047     1.708    dut_inst/ret_array_1_12.idx_ret[3]
  -------------------------------------------------------------------
                         required time                         -1.708    
                         arrival time                           1.733    
  -------------------------------------------------------------------
                         slack                                  0.025    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 3.125 }
Period(ns):         6.250
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location       Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         6.250       4.751      BUFGCE_X1Y224  clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X97Y459  dut_inst/ret_array_1_0.idx_ret_17/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X95Y459  dut_inst/ret_array_1_0.pt_ret[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X95Y459  dut_inst/ret_array_1_0.pt_ret[1]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X96Y460  dut_inst/ret_array_1_0.pt_ret[2]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X94Y468  dut_inst/ret_array_1_15.pt_ret[1]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X95Y459  dut_inst/ret_array_1_0.pt_ret_3[0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X96Y473  dut_inst/ret_array_1_12.idx_ret_1[9]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X95Y472  dut_inst/ret_array_1_12.idx_ret_2[0]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X98Y471  dut_inst/ret_array_1_12.idx_ret_2[14]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X95Y459  dut_inst/ret_array_1_0.pt_ret[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X95Y459  dut_inst/ret_array_1_0.pt_ret[1]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X95Y459  dut_inst/ret_array_1_0.pt_ret_3[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X95Y459  dut_inst/ret_array_1_0.pt_ret_3[1]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X94Y466  dut_inst/ret_array_1_12.idx_ret_20/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X95Y463          lsfr_1/shiftreg_vector[102]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X95Y463          lsfr_1/shiftreg_vector[103]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X95Y463          lsfr_1/shiftreg_vector[104]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X97Y469          lsfr_1/shiftreg_vector[111]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X97Y469          lsfr_1/shiftreg_vector[112]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X97Y469          lsfr_1/shiftreg_vector[113]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X95Y463          lsfr_1/shiftreg_vector[102]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X95Y463          lsfr_1/shiftreg_vector[103]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X95Y463          lsfr_1/shiftreg_vector[104]/C



