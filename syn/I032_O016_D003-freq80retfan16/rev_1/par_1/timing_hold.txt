Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Sun Feb 10 04:18:09 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.008        0.000                      0                15080        5.677        0.000                       0                 16358  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 6.250}        12.500          80.000          
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.008        0.000                      0                15080        5.677        0.000                       0                 15503  
clk_wrapper                                                                             498.562        0.000                       0                   855  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.008ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        5.677ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.008ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_9[245]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_10[245]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.279ns  (logic 0.069ns (24.731%)  route 0.210ns (75.269%))
  Logic Levels:           0  
  Clock Path Skew:        0.216ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.168ns
    Source Clock Delay      (SCD):    3.493ns
    Clock Pessimism Removal (CPR):    0.459ns
  Clock Net Delay (Source):      2.663ns (routing 1.566ns, distribution 1.097ns)
  Clock Net Delay (Destination): 3.027ns (routing 1.714ns, distribution 1.313ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15502, routed)       2.663     3.493    shift_reg_tap_o/clk_c
    SLICE_X96Y410        FDRE                                         r  shift_reg_tap_o/sr_p.sr_9[245]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y410        FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     3.562 r  shift_reg_tap_o/sr_p.sr_9[245]/Q
                         net (fo=1, routed)           0.210     3.772    shift_reg_tap_o/sr_9[245]
    SLICE_X96Y426        FDRE                                         r  shift_reg_tap_o/sr_p.sr_10[245]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15502, routed)       3.027     4.168    shift_reg_tap_o/clk_c
    SLICE_X96Y426        FDRE                                         r  shift_reg_tap_o/sr_p.sr_10[245]/C
                         clock pessimism             -0.459     3.709    
    SLICE_X96Y426        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.055     3.764    shift_reg_tap_o/sr_p.sr_10[245]
  -------------------------------------------------------------------
                         required time                         -3.764    
                         arrival time                           3.772    
  -------------------------------------------------------------------
                         slack                                  0.008    

Slack (MET) :             0.008ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_9[245]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_10[245]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.279ns  (logic 0.069ns (24.731%)  route 0.210ns (75.269%))
  Logic Levels:           0  
  Clock Path Skew:        0.216ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.168ns
    Source Clock Delay      (SCD):    3.493ns
    Clock Pessimism Removal (CPR):    0.459ns
  Clock Net Delay (Source):      2.663ns (routing 1.566ns, distribution 1.097ns)
  Clock Net Delay (Destination): 3.027ns (routing 1.714ns, distribution 1.313ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15502, routed)       2.663     3.493    shift_reg_tap_o/clk_c
    SLICE_X96Y410        FDRE                                         r  shift_reg_tap_o/sr_p.sr_9[245]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y410        FDRE (Prop_FFF_SLICEL_C_Q)
                                                      0.069     3.562 f  shift_reg_tap_o/sr_p.sr_9[245]/Q
                         net (fo=1, routed)           0.210     3.772    shift_reg_tap_o/sr_9[245]
    SLICE_X96Y426        FDRE                                         f  shift_reg_tap_o/sr_p.sr_10[245]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15502, routed)       3.027     4.168    shift_reg_tap_o/clk_c
    SLICE_X96Y426        FDRE                                         r  shift_reg_tap_o/sr_p.sr_10[245]/C
                         clock pessimism             -0.459     3.709    
    SLICE_X96Y426        FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.055     3.764    shift_reg_tap_o/sr_p.sr_10[245]
  -------------------------------------------------------------------
                         required time                         -3.764    
                         arrival time                           3.772    
  -------------------------------------------------------------------
                         slack                                  0.008    

Slack (MET) :             0.008ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_8[211]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_9[211]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.299ns  (logic 0.070ns (23.411%)  route 0.229ns (76.589%))
  Logic Levels:           0  
  Clock Path Skew:        0.236ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.189ns
    Source Clock Delay      (SCD):    3.494ns
    Clock Pessimism Removal (CPR):    0.459ns
  Clock Net Delay (Source):      2.664ns (routing 1.566ns, distribution 1.098ns)
  Clock Net Delay (Destination): 3.048ns (routing 1.714ns, distribution 1.334ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15502, routed)       2.664     3.494    shift_reg_tap_o/clk_c
    SLICE_X96Y413        FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[211]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y413        FDRE (Prop_EFF_SLICEL_C_Q)
                                                      0.070     3.564 r  shift_reg_tap_o/sr_p.sr_8[211]/Q
                         net (fo=1, routed)           0.229     3.793    shift_reg_tap_o/sr_8[211]
    SLICE_X97Y428        FDRE                                         r  shift_reg_tap_o/sr_p.sr_9[211]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15502, routed)       3.048     4.189    shift_reg_tap_o/clk_c
    SLICE_X97Y428        FDRE                                         r  shift_reg_tap_o/sr_p.sr_9[211]/C
                         clock pessimism             -0.459     3.730    
    SLICE_X97Y428        FDRE (Hold_FFF2_SLICEM_C_D)
                                                      0.055     3.785    shift_reg_tap_o/sr_p.sr_9[211]
  -------------------------------------------------------------------
                         required time                         -3.785    
                         arrival time                           3.793    
  -------------------------------------------------------------------
                         slack                                  0.008    

Slack (MET) :             0.008ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_8[211]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_9[211]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.299ns  (logic 0.070ns (23.411%)  route 0.229ns (76.589%))
  Logic Levels:           0  
  Clock Path Skew:        0.236ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.189ns
    Source Clock Delay      (SCD):    3.494ns
    Clock Pessimism Removal (CPR):    0.459ns
  Clock Net Delay (Source):      2.664ns (routing 1.566ns, distribution 1.098ns)
  Clock Net Delay (Destination): 3.048ns (routing 1.714ns, distribution 1.334ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15502, routed)       2.664     3.494    shift_reg_tap_o/clk_c
    SLICE_X96Y413        FDRE                                         r  shift_reg_tap_o/sr_p.sr_8[211]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y413        FDRE (Prop_EFF_SLICEL_C_Q)
                                                      0.070     3.564 f  shift_reg_tap_o/sr_p.sr_8[211]/Q
                         net (fo=1, routed)           0.229     3.793    shift_reg_tap_o/sr_8[211]
    SLICE_X97Y428        FDRE                                         f  shift_reg_tap_o/sr_p.sr_9[211]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15502, routed)       3.048     4.189    shift_reg_tap_o/clk_c
    SLICE_X97Y428        FDRE                                         r  shift_reg_tap_o/sr_p.sr_9[211]/C
                         clock pessimism             -0.459     3.730    
    SLICE_X97Y428        FDRE (Hold_FFF2_SLICEM_C_D)
                                                      0.055     3.785    shift_reg_tap_o/sr_p.sr_9[211]
  -------------------------------------------------------------------
                         required time                         -3.785    
                         arrival time                           3.793    
  -------------------------------------------------------------------
                         slack                                  0.008    

Slack (MET) :             0.009ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_9[87]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_i/sr_p.sr_10[87]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.247ns  (logic 0.069ns (27.935%)  route 0.178ns (72.065%))
  Logic Levels:           0  
  Clock Path Skew:        0.183ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    4.064ns
    Source Clock Delay      (SCD):    3.370ns
    Clock Pessimism Removal (CPR):    0.512ns
  Clock Net Delay (Source):      2.540ns (routing 1.566ns, distribution 0.974ns)
  Clock Net Delay (Destination): 2.923ns (routing 1.714ns, distribution 1.209ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.510     0.510 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.510    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.510 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.806    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.830 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15502, routed)       2.540     3.370    shift_reg_tap_i/clk_c
    SLICE_X78Y387        FDRE                                         r  shift_reg_tap_i/sr_p.sr_9[87]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X78Y387        FDRE (Prop_EFF_SLICEM_C_Q)
                                                      0.069     3.439 r  shift_reg_tap_i/sr_p.sr_9[87]/Q
                         net (fo=1, routed)           0.178     3.617    shift_reg_tap_i/sr_9[87]
    SLICE_X78Y393        FDRE                                         r  shift_reg_tap_i/sr_p.sr_10[87]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.779     0.779 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.779    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.779 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.113    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.141 r  clk_ibuf/O
    X2Y6 (CLOCK_ROOT)    net (fo=15502, routed)       2.923     4.064    shift_reg_tap_i/clk_c
    SLICE_X78Y393        FDRE                                         r  shift_reg_tap_i/sr_p.sr_10[87]/C
                         clock pessimism             -0.512     3.553    
    SLICE_X78Y393        FDRE (Hold_GFF2_SLICEM_C_D)
                                                      0.055     3.608    shift_reg_tap_i/sr_p.sr_10[87]
  -------------------------------------------------------------------
                         required time                         -3.608    
                         arrival time                           3.617    
  -------------------------------------------------------------------
                         slack                                  0.009    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 6.250 }
Period(ns):         12.500
Sources:            { clk }

Check Type        Corner  Lib Pin     Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I    n/a            1.499         12.500      11.001     BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X104Y459  muon_sorter_1/sr_3_4.pt_1_sr_3_0.pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X97Y455   muon_sorter_1/sr_3_4.pt_2_sr_3_0.pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X100Y459  muon_sorter_1/sr_3_4.pt_sr_3_0.pt_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X99Y453   muon_sorter_1/sr_3_4.roi_0_sr_3_0.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X104Y455  muon_sorter_1/sr_3_5.pt_0_sr_3_0.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X104Y455  muon_sorter_1/sr_3_5.roi_6_sr_3_0.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X99Y469   muon_sorter_1/sr_3_0.pt_0_sr_3_0.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X99Y469   muon_sorter_1/sr_3_1.pt_0_sr_3_0.pt_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X99Y472   muon_sorter_1/sr_3_1.pt_1_sr_3_0.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X97Y459   muon_sorter_1/sr_3_4.roi_1_sr_3_0.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X97Y459   muon_sorter_1/sr_3_4.roi_4_sr_3_0.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X97Y459   muon_sorter_1/sr_3_4.roi_5_sr_3_0.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X97Y459   muon_sorter_1/sr_3_0.sector_1_sr_3_0.pt_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X95Y471   muon_sorter_1/sr_3_1.roi_6_sr_3_0.pt_1/CLK



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X96Y475          lsfr_1/output_vector_1[511]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X96Y475          lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X83Y327          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y475          lsfr_1/output_vector_1[511]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X77Y326          lsfr_1/shiftreg_vector[103]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X77Y326          lsfr_1/shiftreg_vector[104]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y475          lsfr_1/shiftreg_vector[0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X83Y327          lsfr_1/shiftreg_vector[100]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X77Y326          lsfr_1/shiftreg_vector[103]/C



