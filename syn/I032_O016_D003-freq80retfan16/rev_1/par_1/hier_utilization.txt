Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Sun Feb 10 04:18:01 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_utilization -hierarchical -file hier_utilization.txt
| Design       : wrapper
| Device       : xcvu9pflgc2104-1
| Design State : Routed
------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+-------------------+-------------------------------------------------+------------+------------+---------+------+-------+--------+--------+------+--------------+
|      Instance     |                      Module                     | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs  | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+-------------------+-------------------------------------------------+------------+------------+---------+------+-------+--------+--------+------+--------------+
| wrapper           |                                           (top) |      22083 |      21979 |       0 |  104 | 16251 |      0 |      0 |    0 |            0 |
|   (wrapper)       |                                           (top) |         20 |         20 |       0 |    0 |   513 |      0 |      0 |    0 |            0 |
|   lsfr_1          |                                            lfsr |          1 |          1 |       0 |    0 |   513 |      0 |      0 |    0 |            0 |
|   dut_inst        | muon_sorter_I032_O016_D003-freq80retfan16_rev_1 |      21977 |      21873 |       0 |  104 |  3365 |      0 |      0 |    0 |            0 |
|   reducer_1       |                                         reducer |         85 |         85 |       0 |    0 |   340 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_i |                             shift_reg_tap_512_4 |          0 |          0 |       0 |    0 |  7680 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_o |                             shift_reg_tap_256_4 |          0 |          0 |       0 |    0 |  3840 |      0 |      0 |    0 |            0 |
+-------------------+-------------------------------------------------+------------+------------+---------+------+-------+--------+--------+------+--------------+
* Note: The sum of lower-level cells may be larger than their parent cells total, due to cross-hierarchy LUT combining


