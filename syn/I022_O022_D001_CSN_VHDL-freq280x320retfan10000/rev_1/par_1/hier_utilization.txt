Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Mon Mar 11 23:36:23 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_utilization -hierarchical -file hier_utilization.txt
| Design       : wrapper_csn
| Device       : xcvu9pflgc2104-1
| Design State : Routed
------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+--------------------------------------+------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
|               Instance               |                              Module                              | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+--------------------------------------+------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
| wrapper_csn                          |                                                            (top) |       3629 |       3629 |       0 |    0 | 1818 |      0 |      0 |    0 |            0 |
|   (wrapper_csn)                      |                                                            (top) |          0 |          0 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   dut_inst                           | muon_sorter_I022_O022_D001_CSN_VHDL-freq280x320retfan10000_rev_1 |       3531 |       3531 |       0 |    0 |  526 |      0 |      0 |    0 |            0 |
|     (dut_inst)                       | muon_sorter_I022_O022_D001_CSN_VHDL-freq280x320retfan10000_rev_1 |          6 |          6 |       0 |    0 |  309 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.0.csn_cmp_inst  |                                    csn_cmp_false_false_false_114 |         14 |         14 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_19 |         18 |         18 |       0 |    0 |    9 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.10.csn_cmp_inst |                                     csn_cmp_false_false_false_93 |         23 |         23 |       0 |    0 |   13 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.2.csn_cmp_inst  |                                      csn_cmp_false_false_false_9 |         22 |         22 |       0 |    0 |   14 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_98 |         12 |         12 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_76 |         24 |         24 |       0 |    0 |   13 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.5.csn_cmp_inst  |                                     csn_cmp_false_false_false_79 |         17 |         17 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.6.csn_cmp_inst  |                                     csn_cmp_false_false_false_82 |         21 |         21 |       0 |    0 |    8 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_84 |         12 |         12 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.8.csn_cmp_inst  |                                     csn_cmp_false_false_false_87 |         22 |         22 |       0 |    0 |   14 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.9.csn_cmp_inst  |                                     csn_cmp_false_false_false_90 |         11 |         11 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.0.csn_cmp_inst  |                                     csn_cmp_false_false_false_96 |         26 |         26 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.1.csn_cmp_inst  |                                    csn_cmp_false_false_false_107 |         27 |         27 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.10.csn_cmp_inst |                                    csn_cmp_false_false_false_110 |         25 |         25 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.2.csn_cmp_inst  |                                     csn_cmp_false_false_false_68 |         27 |         27 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_22 |         27 |         27 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.4.csn_cmp_inst  |                                        csn_cmp_false_false_false |         26 |         26 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.5.csn_cmp_inst  |                                    csn_cmp_false_false_false_106 |         26 |         26 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.6.csn_cmp_inst  |                                      csn_cmp_false_false_false_6 |         27 |         27 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_21 |         26 |         26 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.8.csn_cmp_inst  |                                     csn_cmp_false_false_false_11 |         26 |         26 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.9.csn_cmp_inst  |                                     csn_cmp_false_false_false_14 |         28 |         28 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.0.csn_cmp_inst |                                     csn_cmp_false_false_false_39 |         26 |         26 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.1.csn_cmp_inst |                                     csn_cmp_false_false_false_42 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.2.csn_cmp_inst |                                     csn_cmp_false_false_false_45 |         24 |         24 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.3.csn_cmp_inst |                                     csn_cmp_false_false_false_48 |         25 |         25 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.4.csn_cmp_inst |                                     csn_cmp_false_false_false_51 |         27 |         27 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.5.csn_cmp_inst |                                     csn_cmp_false_false_false_54 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.6.csn_cmp_inst |                                     csn_cmp_false_false_false_57 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.10.pair_g.7.csn_cmp_inst |                                     csn_cmp_false_false_false_59 |         28 |         28 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.0.csn_cmp_inst |                                     csn_cmp_false_false_false_69 |         25 |         25 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.1.csn_cmp_inst |                                    csn_cmp_false_false_false_112 |         32 |         32 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.2.csn_cmp_inst |                                     csn_cmp_false_false_false_86 |         37 |         37 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.3.csn_cmp_inst |                                     csn_cmp_false_false_false_25 |         26 |         26 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.4.csn_cmp_inst |                                     csn_cmp_false_false_false_17 |         26 |         26 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.5.csn_cmp_inst |                                     csn_cmp_false_false_false_70 |         31 |         31 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.11.pair_g.6.csn_cmp_inst |                                     csn_cmp_false_false_false_85 |         28 |         28 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.0.csn_cmp_inst  |                                      csn_cmp_false_false_false_8 |         39 |         39 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.1.csn_cmp_inst  |                                    csn_cmp_false_false_false_102 |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.10.csn_cmp_inst |                                     csn_cmp_false_false_false_47 |         30 |         30 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.2.csn_cmp_inst  |                                      csn_cmp_false_false_false_1 |         26 |         26 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.3.csn_cmp_inst  |                                    csn_cmp_false_false_false_109 |         29 |         29 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_31 |         30 |         30 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.5.csn_cmp_inst  |                                     csn_cmp_false_false_false_34 |         38 |         38 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.6.csn_cmp_inst  |                                     csn_cmp_false_false_false_36 |         32 |         32 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_38 |         28 |         28 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.8.csn_cmp_inst  |                                     csn_cmp_false_false_false_41 |         27 |         27 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.9.csn_cmp_inst  |                                     csn_cmp_false_false_false_44 |         45 |         45 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.0.csn_cmp_inst  |                                     csn_cmp_false_false_false_50 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_53 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.10.csn_cmp_inst |                                     csn_cmp_false_false_false_29 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.2.csn_cmp_inst  |                                     csn_cmp_false_false_false_56 |         15 |         15 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_58 |         25 |         25 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_61 |         24 |         24 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.5.csn_cmp_inst  |                                     csn_cmp_false_false_false_63 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.6.csn_cmp_inst  |                                     csn_cmp_false_false_false_65 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_67 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.8.csn_cmp_inst  |                                     csn_cmp_false_false_false_18 |         24 |         24 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.9.csn_cmp_inst  |                                     csn_cmp_false_false_false_24 |         18 |         18 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.0.csn_cmp_inst  |                                     csn_cmp_false_false_false_75 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_78 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.2.csn_cmp_inst  |                                     csn_cmp_false_false_false_81 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_83 |          9 |          9 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_71 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.5.csn_cmp_inst  |                                     csn_cmp_false_false_false_89 |          8 |          8 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.6.csn_cmp_inst  |                                     csn_cmp_false_false_false_92 |          6 |          6 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_95 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.8.csn_cmp_inst  |                                      csn_cmp_false_false_false_4 |          7 |          7 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.9.csn_cmp_inst  |                                    csn_cmp_false_false_false_111 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.0.csn_cmp_inst  |                                    csn_cmp_false_false_false_105 |         45 |         45 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_16 |         65 |         65 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.2.csn_cmp_inst  |                                      csn_cmp_false_false_false_5 |        131 |        131 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_20 |        124 |        124 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_28 |        134 |        134 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.5.csn_cmp_inst  |                                     csn_cmp_false_false_false_13 |        131 |        131 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.6.csn_cmp_inst  |                                      csn_cmp_false_false_false_0 |        132 |        132 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.7.csn_cmp_inst  |                                    csn_cmp_false_false_false_113 |        127 |        127 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.8.csn_cmp_inst  |                                    csn_cmp_false_false_false_101 |        120 |        120 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.9.csn_cmp_inst  |                                     csn_cmp_false_false_false_73 |        127 |        127 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.0.csn_cmp_inst  |                                     csn_cmp_false_false_false_30 |         19 |         19 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_33 |         34 |         34 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.2.csn_cmp_inst  |                                     csn_cmp_false_false_false_35 |         27 |         27 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_37 |         27 |         27 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_40 |         28 |         28 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.5.csn_cmp_inst  |                                     csn_cmp_false_false_false_43 |         21 |         21 |       0 |    0 |    4 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.6.csn_cmp_inst  |                                     csn_cmp_false_false_false_46 |         24 |         24 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_49 |         24 |         24 |       0 |    0 |    9 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.8.csn_cmp_inst  |                                     csn_cmp_false_false_false_52 |         33 |         33 |       0 |    0 |    5 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.9.csn_cmp_inst  |                                     csn_cmp_false_false_false_55 |         24 |         24 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.0.csn_cmp_inst  |                                     csn_cmp_false_false_false_60 |         21 |         21 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_62 |         23 |         23 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.2.csn_cmp_inst  |                                     csn_cmp_false_false_false_64 |         15 |         15 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_66 |         24 |         24 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_23 |         28 |         28 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.5.csn_cmp_inst  |                                      csn_cmp_false_false_false_2 |         25 |         25 |       0 |    0 |    3 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.6.csn_cmp_inst  |                                      csn_cmp_false_false_false_3 |         26 |         26 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_99 |         23 |         23 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.8.csn_cmp_inst  |                                     csn_cmp_false_false_false_77 |         25 |         25 |       0 |    0 |    6 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.9.csn_cmp_inst  |                                     csn_cmp_false_false_false_80 |         25 |         25 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.0.csn_cmp_inst  |                                     csn_cmp_false_false_false_72 |         19 |         19 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_88 |         23 |         23 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.2.csn_cmp_inst  |                                     csn_cmp_false_false_false_91 |         24 |         24 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_94 |         26 |         26 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_97 |         19 |         19 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.5.csn_cmp_inst  |                                    csn_cmp_false_false_false_100 |         23 |         23 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.6.csn_cmp_inst  |                                    csn_cmp_false_false_false_103 |         21 |         21 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.7.csn_cmp_inst  |                                    csn_cmp_false_false_false_108 |         22 |         22 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.0.csn_cmp_inst  |                                    csn_cmp_false_false_false_104 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.1.csn_cmp_inst  |                                     csn_cmp_false_false_false_12 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.2.csn_cmp_inst  |                                     csn_cmp_false_false_false_15 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.3.csn_cmp_inst  |                                     csn_cmp_false_false_false_26 |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.4.csn_cmp_inst  |                                     csn_cmp_false_false_false_10 |         25 |         25 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.5.csn_cmp_inst  |                                     csn_cmp_false_false_false_27 |         26 |         26 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.6.csn_cmp_inst  |                                      csn_cmp_false_false_false_7 |         27 |         27 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.7.csn_cmp_inst  |                                     csn_cmp_false_false_false_74 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.9.pair_g.8.csn_cmp_inst  |                                     csn_cmp_false_false_false_32 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   lsfr_1                             |                                                             lfsr |          1 |          1 |       0 |    0 |  287 |      0 |      0 |    0 |            0 |
|   reducer_1                          |                                                          reducer |         97 |         97 |       0 |    0 |  383 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_i                    |                                              shift_reg_tap_286_1 |          0 |          0 |       0 |    0 |  336 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_o                    |                                             shift_reg_tap_1024_1 |          0 |          0 |       0 |    0 |  286 |      0 |      0 |    0 |            0 |
+--------------------------------------+------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
* Note: The sum of lower-level cells may be larger than their parent cells total, due to cross-hierarchy LUT combining


