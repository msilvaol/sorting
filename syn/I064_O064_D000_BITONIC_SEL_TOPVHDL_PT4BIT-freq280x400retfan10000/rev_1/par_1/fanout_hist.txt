Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Thu Mar 21 14:29:25 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+-------+--------+
|  Fanout |  Nets |      % |
+---------+-------+--------+
|       1 |  2750 |  20.82 |
|       2 |  2459 |  18.61 |
|       3 |  4166 |  31.54 |
|       4 |  1127 |   8.53 |
|    5-10 |  1293 |   9.79 |
|   11-50 |  1407 |  10.65 |
|  51-100 |     5 |   0.03 |
| 101-500 |     0 |   0.00 |
|    >500 |     0 |   0.00 |
+---------+-------+--------+
|     ALL | 13207 | 100.00 |
+---------+-------+--------+


