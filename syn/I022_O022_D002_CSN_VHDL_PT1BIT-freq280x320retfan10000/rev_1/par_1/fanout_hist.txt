Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Mon Mar 11 19:07:13 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper_csn
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+------+--------+
|  Fanout | Nets |      % |
+---------+------+--------+
|       1 | 2608 |  54.38 |
|       2 |  822 |  17.14 |
|       3 |  339 |   7.06 |
|       4 |  138 |   2.87 |
|    5-10 |  552 |  11.51 |
|   11-50 |  314 |   6.54 |
|  51-100 |   18 |   0.37 |
| 101-500 |    4 |   0.08 |
|    >500 |    0 |   0.00 |
+---------+------+--------+
|     ALL | 4795 | 100.00 |
+---------+------+--------+


