Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Wed Mar  6 18:54:29 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper_csn
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.032        0.000                      0                  683        2.850        0.000                       0                  1380  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 3.125}        6.250           160.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.032        0.000                      0                  683        2.850        0.000                       0                   892  
clk_wrapper                                                                             498.562        0.000                       0                   488  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.032ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        2.850ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.032ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_2_11.idx[3]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[150]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.099ns  (logic 0.039ns (39.394%)  route 0.060ns (60.606%))
  Logic Levels:           0  
  Clock Path Skew:        0.020ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.657ns
    Source Clock Delay      (SCD):    1.326ns
    Clock Pessimism Removal (CPR):    0.311ns
  Clock Net Delay (Source):      0.780ns (routing 0.316ns, distribution 0.464ns)
  Clock Net Delay (Destination): 0.885ns (routing 0.348ns, distribution 0.537ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=891, routed)         0.780     1.326    dut_inst/clk_c
    SLICE_X103Y576       FDRE                                         r  dut_inst/ret_array_2_11.idx[3]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X103Y576       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.365 r  dut_inst/ret_array_2_11.idx[3]/Q
                         net (fo=1, routed)           0.060     1.425    shift_reg_tap_o/idx_4[3]
    SLICE_X103Y574       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[150]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=891, routed)         0.885     1.657    shift_reg_tap_o/clk_c
    SLICE_X103Y574       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[150]/C
                         clock pessimism             -0.311     1.346    
    SLICE_X103Y574       FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.393    shift_reg_tap_o/sr_p.sr_1[150]
  -------------------------------------------------------------------
                         required time                         -1.393    
                         arrival time                           1.425    
  -------------------------------------------------------------------
                         slack                                  0.032    

Slack (MET) :             0.032ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_2_11.idx[3]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[150]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.099ns  (logic 0.039ns (39.394%)  route 0.060ns (60.606%))
  Logic Levels:           0  
  Clock Path Skew:        0.020ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.657ns
    Source Clock Delay      (SCD):    1.326ns
    Clock Pessimism Removal (CPR):    0.311ns
  Clock Net Delay (Source):      0.780ns (routing 0.316ns, distribution 0.464ns)
  Clock Net Delay (Destination): 0.885ns (routing 0.348ns, distribution 0.537ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=891, routed)         0.780     1.326    dut_inst/clk_c
    SLICE_X103Y576       FDRE                                         r  dut_inst/ret_array_2_11.idx[3]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X103Y576       FDRE (Prop_HFF_SLICEL_C_Q)
                                                      0.039     1.365 f  dut_inst/ret_array_2_11.idx[3]/Q
                         net (fo=1, routed)           0.060     1.425    shift_reg_tap_o/idx_4[3]
    SLICE_X103Y574       FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[150]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=891, routed)         0.885     1.657    shift_reg_tap_o/clk_c
    SLICE_X103Y574       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[150]/C
                         clock pessimism             -0.311     1.346    
    SLICE_X103Y574       FDRE (Hold_EFF2_SLICEL_C_D)
                                                      0.047     1.393    shift_reg_tap_o/sr_p.sr_1[150]
  -------------------------------------------------------------------
                         required time                         -1.393    
                         arrival time                           1.425    
  -------------------------------------------------------------------
                         slack                                  0.032    

Slack (MET) :             0.038ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_2_6.idx[7]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[89]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.102ns  (logic 0.039ns (38.235%)  route 0.063ns (61.765%))
  Logic Levels:           0  
  Clock Path Skew:        0.017ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.643ns
    Source Clock Delay      (SCD):    1.316ns
    Clock Pessimism Removal (CPR):    0.310ns
  Clock Net Delay (Source):      0.770ns (routing 0.316ns, distribution 0.454ns)
  Clock Net Delay (Destination): 0.871ns (routing 0.348ns, distribution 0.523ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=891, routed)         0.770     1.316    dut_inst/clk_c
    SLICE_X96Y574        FDRE                                         r  dut_inst/ret_array_2_6.idx[7]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y574        FDRE (Prop_EFF_SLICEL_C_Q)
                                                      0.039     1.355 r  dut_inst/ret_array_2_6.idx[7]/Q
                         net (fo=1, routed)           0.063     1.418    shift_reg_tap_o/idx_3[7]
    SLICE_X96Y573        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[89]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=891, routed)         0.871     1.643    shift_reg_tap_o/clk_c
    SLICE_X96Y573        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[89]/C
                         clock pessimism             -0.310     1.333    
    SLICE_X96Y573        FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.047     1.380    shift_reg_tap_o/sr_p.sr_1[89]
  -------------------------------------------------------------------
                         required time                         -1.380    
                         arrival time                           1.418    
  -------------------------------------------------------------------
                         slack                                  0.038    

Slack (MET) :             0.038ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_2_6.idx[7]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[89]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.102ns  (logic 0.039ns (38.235%)  route 0.063ns (61.765%))
  Logic Levels:           0  
  Clock Path Skew:        0.017ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.643ns
    Source Clock Delay      (SCD):    1.316ns
    Clock Pessimism Removal (CPR):    0.310ns
  Clock Net Delay (Source):      0.770ns (routing 0.316ns, distribution 0.454ns)
  Clock Net Delay (Destination): 0.871ns (routing 0.348ns, distribution 0.523ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=891, routed)         0.770     1.316    dut_inst/clk_c
    SLICE_X96Y574        FDRE                                         r  dut_inst/ret_array_2_6.idx[7]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X96Y574        FDRE (Prop_EFF_SLICEL_C_Q)
                                                      0.039     1.355 f  dut_inst/ret_array_2_6.idx[7]/Q
                         net (fo=1, routed)           0.063     1.418    shift_reg_tap_o/idx_3[7]
    SLICE_X96Y573        FDRE                                         f  shift_reg_tap_o/sr_p.sr_1[89]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=891, routed)         0.871     1.643    shift_reg_tap_o/clk_c
    SLICE_X96Y573        FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[89]/C
                         clock pessimism             -0.310     1.333    
    SLICE_X96Y573        FDRE (Hold_FFF2_SLICEL_C_D)
                                                      0.047     1.380    shift_reg_tap_o/sr_p.sr_1[89]
  -------------------------------------------------------------------
                         required time                         -1.380    
                         arrival time                           1.418    
  -------------------------------------------------------------------
                         slack                                  0.038    

Slack (MET) :             0.040ns  (arrival time - required time)
  Source:                 dut_inst/ret_array_2_12.pt[1]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_o/sr_p.sr_1[157]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.125ns  (logic 0.039ns (31.200%)  route 0.086ns (68.800%))
  Logic Levels:           0  
  Clock Path Skew:        0.038ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    1.646ns
    Source Clock Delay      (SCD):    1.327ns
    Clock Pessimism Removal (CPR):    0.281ns
  Clock Net Delay (Source):      0.781ns (routing 0.316ns, distribution 0.465ns)
  Clock Net Delay (Destination): 0.874ns (routing 0.348ns, distribution 0.526ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=891, routed)         0.781     1.327    dut_inst/clk_c
    SLICE_X102Y574       FDRE                                         r  dut_inst/ret_array_2_12.pt[1]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X102Y574       FDRE (Prop_HFF_SLICEM_C_Q)
                                                      0.039     1.366 r  dut_inst/ret_array_2_12.pt[1]/Q
                         net (fo=1, routed)           0.086     1.452    shift_reg_tap_o/pt_8[1]
    SLICE_X101Y572       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[157]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X3Y9 (CLOCK_ROOT)    net (fo=891, routed)         0.874     1.646    shift_reg_tap_o/clk_c
    SLICE_X101Y572       FDRE                                         r  shift_reg_tap_o/sr_p.sr_1[157]/C
                         clock pessimism             -0.281     1.365    
    SLICE_X101Y572       FDRE (Hold_GFF2_SLICEL_C_D)
                                                      0.047     1.412    shift_reg_tap_o/sr_p.sr_1[157]
  -------------------------------------------------------------------
                         required time                         -1.412    
                         arrival time                           1.452    
  -------------------------------------------------------------------
                         slack                                  0.040    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 3.125 }
Period(ns):         6.250
Sources:            { clk }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         6.250       4.751      BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X102Y578  dut_inst/ret_array_1_12.idx_ret_11[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X101Y577  dut_inst/ret_array_1_12.idx_ret_11[1]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X102Y578  dut_inst/ret_array_1_12.idx_ret_11[2]/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X103Y578  dut_inst/ret_array_1_12.idx_ret_11[3]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X94Y584   shift_reg_tap_i/sr_p.sr_1[202]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X94Y584   shift_reg_tap_i/sr_p.sr_1[203]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X94Y584   shift_reg_tap_i/sr_p.sr_1[204]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X92Y584   shift_reg_tap_i/sr_p.sr_1[207]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X92Y579   dut_inst/ret_array_1_3.pt_ret_15[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X100Y578  dut_inst/ret_array_1_12.idx_ret_12[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X101Y579  dut_inst/ret_array_1_4.idx_ret_45[3]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X102Y576  dut_inst/ret_array_1_8.pt_ret_65[0]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X98Y576   dut_inst/stage_g.7.pair_g.2.csn_cmp_inst/ret_array_2_9.idx_ret_7/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X102Y584  shift_reg_tap_i/sr_p.sr_1[23]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X95Y584          lsfr_1/shiftreg_vector[114]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X95Y584          lsfr_1/shiftreg_vector[115]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X95Y584          lsfr_1/shiftreg_vector[116]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X102Y584         lsfr_1/shiftreg_vector[11]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y585          lsfr_1/shiftreg_vector[125]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X96Y585          lsfr_1/shiftreg_vector[126]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X94Y588          lsfr_1/shiftreg_vector[119]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X102Y584         lsfr_1/shiftreg_vector[11]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X94Y587          lsfr_1/shiftreg_vector[120]/C



