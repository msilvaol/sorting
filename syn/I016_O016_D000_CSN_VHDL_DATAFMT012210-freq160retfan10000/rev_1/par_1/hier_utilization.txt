Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Wed Mar  6 18:54:14 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_utilization -hierarchical -file hier_utilization.txt
| Design       : wrapper_csn
| Device       : xcvu9pflgc2104-1
| Design State : Routed
------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+-------------------------------------+----------------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
|               Instance              |                                   Module                                   | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+-------------------------------------+----------------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
| wrapper_csn                         |                                                                      (top) |       1239 |       1239 |       0 |    0 | 1377 |      0 |      0 |    0 |            0 |
|   (wrapper_csn)                     |                                                                      (top) |          0 |          0 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   dut_inst                          | muon_sorter_I016_O016_D000_CSN_VHDL_DATAFMT012210-freq160retfan10000_rev_1 |       1169 |       1169 |       0 |    0 |  475 |      0 |      0 |    0 |            0 |
|     (dut_inst)                      | muon_sorter_I016_O016_D000_CSN_VHDL_DATAFMT012210-freq160retfan10000_rev_1 |          0 |          0 |       0 |    0 |  410 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.0.csn_cmp_inst |                                               csn_cmp_false_false_false_13 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.1.csn_cmp_inst |                                               csn_cmp_false_false_false_35 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.2.csn_cmp_inst |                                               csn_cmp_false_false_false_40 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.3.csn_cmp_inst |                                                csn_cmp_false_false_false_4 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.4.csn_cmp_inst |                                                csn_cmp_false_false_false_7 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.5.csn_cmp_inst |                                               csn_cmp_false_false_false_10 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.6.csn_cmp_inst |                                               csn_cmp_false_false_false_41 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.7.csn_cmp_inst |                                               csn_cmp_false_false_false_27 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.0.csn_cmp_inst |                                               csn_cmp_false_false_false_51 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.1.csn_cmp_inst |                                               csn_cmp_false_false_false_57 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.2.csn_cmp_inst |                                               csn_cmp_false_false_false_23 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.3.csn_cmp_inst |                                               csn_cmp_false_false_false_33 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.4.csn_cmp_inst |                                               csn_cmp_false_false_false_17 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.5.csn_cmp_inst |                                                csn_cmp_false_false_false_0 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.6.csn_cmp_inst |                                               csn_cmp_false_false_false_14 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.7.csn_cmp_inst |                                               csn_cmp_false_false_false_45 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.0.csn_cmp_inst |                                                csn_cmp_false_false_false_3 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.1.csn_cmp_inst |                                                csn_cmp_false_false_false_6 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.2.csn_cmp_inst |                                                csn_cmp_false_false_false_9 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.3.csn_cmp_inst |                                               csn_cmp_false_false_false_12 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.4.csn_cmp_inst |                                               csn_cmp_false_false_false_26 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.5.csn_cmp_inst |                                               csn_cmp_false_false_false_50 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.6.csn_cmp_inst |                                               csn_cmp_false_false_false_58 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.7.csn_cmp_inst |                                               csn_cmp_false_false_false_22 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.0.csn_cmp_inst |                                               csn_cmp_false_false_false_55 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.1.csn_cmp_inst |                                               csn_cmp_false_false_false_32 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.2.csn_cmp_inst |                                                csn_cmp_false_false_false_1 |         21 |         21 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.3.csn_cmp_inst |                                               csn_cmp_false_false_false_46 |         23 |         23 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.4.csn_cmp_inst |                                                  csn_cmp_false_false_false |         21 |         21 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.5.csn_cmp_inst |                                                csn_cmp_false_false_false_2 |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.6.csn_cmp_inst |                                                csn_cmp_false_false_false_5 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.7.csn_cmp_inst |                                                csn_cmp_false_false_false_8 |         22 |         22 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.0.csn_cmp_inst |                                               csn_cmp_false_false_false_11 |         24 |         24 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.1.csn_cmp_inst |                                               csn_cmp_false_false_false_18 |         24 |         24 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.2.csn_cmp_inst |                                               csn_cmp_false_false_false_28 |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.3.csn_cmp_inst |                                               csn_cmp_false_false_false_25 |         20 |         20 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.4.csn_cmp_inst |                                               csn_cmp_false_false_false_56 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.5.csn_cmp_inst |                                               csn_cmp_false_false_false_24 |         18 |         18 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.6.csn_cmp_inst |                                               csn_cmp_false_false_false_31 |         25 |         25 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.0.csn_cmp_inst |                                               csn_cmp_false_false_false_20 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.1.csn_cmp_inst |                                               csn_cmp_false_false_false_44 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.2.csn_cmp_inst |                                               csn_cmp_false_false_false_37 |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.3.csn_cmp_inst |                                               csn_cmp_false_false_false_39 |         19 |         19 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.4.csn_cmp_inst |                                               csn_cmp_false_false_false_15 |         28 |         28 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.5.csn_cmp_inst |                                               csn_cmp_false_false_false_43 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.6.csn_cmp_inst |                                               csn_cmp_false_false_false_19 |         23 |         23 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.0.csn_cmp_inst |                                               csn_cmp_false_false_false_29 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.1.csn_cmp_inst |                                               csn_cmp_false_false_false_53 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.2.csn_cmp_inst |                                               csn_cmp_false_false_false_48 |         17 |         17 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.3.csn_cmp_inst |                                               csn_cmp_false_false_false_49 |         25 |         25 |       0 |    0 |   11 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.4.csn_cmp_inst |                                               csn_cmp_false_false_false_54 |         18 |         18 |       0 |    0 |   11 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.5.csn_cmp_inst |                                               csn_cmp_false_false_false_30 |         16 |         16 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.0.csn_cmp_inst |                                               csn_cmp_false_false_false_38 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.1.csn_cmp_inst |                                               csn_cmp_false_false_false_34 |         27 |         27 |       0 |    0 |   13 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.2.csn_cmp_inst |                                               csn_cmp_false_false_false_42 |         32 |         32 |       0 |    0 |   23 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.3.csn_cmp_inst |                                               csn_cmp_false_false_false_16 |          9 |          9 |       0 |    0 |    2 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.0.csn_cmp_inst |                                               csn_cmp_false_false_false_52 |         30 |         30 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.1.csn_cmp_inst |                                               csn_cmp_false_false_false_59 |         28 |         28 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.2.csn_cmp_inst |                                               csn_cmp_false_false_false_47 |         18 |         18 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.3.csn_cmp_inst |                                               csn_cmp_false_false_false_21 |         26 |         26 |       0 |    0 |    1 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.4.csn_cmp_inst |                                               csn_cmp_false_false_false_36 |         30 |         30 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   lsfr_1                            |                                                                       lfsr |          1 |          1 |       0 |    0 |  209 |      0 |      0 |    0 |            0 |
|   reducer_1                         |                                                                    reducer |         69 |         69 |       0 |    0 |  277 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_i                   |                                                        shift_reg_tap_208_1 |          0 |          0 |       0 |    0 |  208 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_o                   |                                                        shift_reg_tap_256_1 |          0 |          0 |       0 |    0 |  208 |      0 |      0 |    0 |            0 |
+-------------------------------------+----------------------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
* Note: The sum of lower-level cells may be larger than their parent cells total, due to cross-hierarchy LUT combining


