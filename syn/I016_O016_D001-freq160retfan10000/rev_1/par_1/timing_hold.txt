Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Tue Feb  5 17:01:57 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.002        0.000                      0                 8433        2.850        0.000                       0                  9271  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk_user     {0.000 3.125}        6.250           160.000         
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk_user            0.002        0.000                      0                 8433        2.850        0.000                       0                  8672  
clk_wrapper                                                                             498.562        0.000                       0                   599  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk_user
  To Clock:  clk_user

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.002ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        2.850ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.002ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_10[117]/C
                            (rising edge-triggered cell FDRE clocked by clk_user  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_11[117]/D
                            (rising edge-triggered cell FDRE clocked by clk_user  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk_user
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk_user rise@0.000ns - clk_user rise@0.000ns)
  Data Path Delay:        0.314ns  (logic 0.072ns (22.930%)  route 0.242ns (77.070%))
  Logic Levels:           0  
  Clock Path Skew:        0.259ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.700ns
    Source Clock Delay      (SCD):    2.966ns
    Clock Pessimism Removal (CPR):    0.475ns
  Clock Net Delay (Source):      2.117ns (routing 1.133ns, distribution 0.984ns)
  Clock Net Delay (Destination): 2.540ns (routing 1.244ns, distribution 1.296ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk_user rise edge)
                                                      0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk_user (IN)
                         net (fo=0)                   0.000     0.000    clk_user_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.529     0.529 r  clk_user_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.529    clk_user_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.529 r  clk_user_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.825    clk_user_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.849 r  clk_user_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=8671, routed)        2.117     2.966    shift_reg_tap_i/clk_user_c
    SLICE_X106Y438       FDRE                                         r  shift_reg_tap_i/sr_p.sr_10[117]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X106Y438       FDRE (Prop_FFF2_SLICEL_C_Q)
                                                      0.072     3.038 r  shift_reg_tap_i/sr_p.sr_10[117]/Q
                         net (fo=1, routed)           0.242     3.280    shift_reg_tap_i/sr_10[117]
    SLICE_X107Y436       FDRE                                         r  shift_reg_tap_i/sr_p.sr_11[117]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk_user rise edge)
                                                      0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk_user (IN)
                         net (fo=0)                   0.000     0.000    clk_user_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.798     0.798 r  clk_user_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.798    clk_user_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.798 r  clk_user_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.132    clk_user_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.160 r  clk_user_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=8671, routed)        2.540     3.700    shift_reg_tap_i/clk_user_c
    SLICE_X107Y436       FDRE                                         r  shift_reg_tap_i/sr_p.sr_11[117]/C
                         clock pessimism             -0.475     3.225    
    SLICE_X107Y436       FDRE (Hold_EFF_SLICEM_C_D)
                                                      0.053     3.278    shift_reg_tap_i/sr_p.sr_11[117]
  -------------------------------------------------------------------
                         required time                         -3.278    
                         arrival time                           3.280    
  -------------------------------------------------------------------
                         slack                                  0.002    

Slack (MET) :             0.002ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_10[117]/C
                            (rising edge-triggered cell FDRE clocked by clk_user  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_11[117]/D
                            (rising edge-triggered cell FDRE clocked by clk_user  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk_user
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk_user rise@0.000ns - clk_user rise@0.000ns)
  Data Path Delay:        0.314ns  (logic 0.072ns (22.930%)  route 0.242ns (77.070%))
  Logic Levels:           0  
  Clock Path Skew:        0.259ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.700ns
    Source Clock Delay      (SCD):    2.966ns
    Clock Pessimism Removal (CPR):    0.475ns
  Clock Net Delay (Source):      2.117ns (routing 1.133ns, distribution 0.984ns)
  Clock Net Delay (Destination): 2.540ns (routing 1.244ns, distribution 1.296ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk_user rise edge)
                                                      0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk_user (IN)
                         net (fo=0)                   0.000     0.000    clk_user_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.529     0.529 r  clk_user_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.529    clk_user_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.529 r  clk_user_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.825    clk_user_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.849 r  clk_user_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=8671, routed)        2.117     2.966    shift_reg_tap_i/clk_user_c
    SLICE_X106Y438       FDRE                                         r  shift_reg_tap_i/sr_p.sr_10[117]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X106Y438       FDRE (Prop_FFF2_SLICEL_C_Q)
                                                      0.072     3.038 f  shift_reg_tap_i/sr_p.sr_10[117]/Q
                         net (fo=1, routed)           0.242     3.280    shift_reg_tap_i/sr_10[117]
    SLICE_X107Y436       FDRE                                         f  shift_reg_tap_i/sr_p.sr_11[117]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk_user rise edge)
                                                      0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk_user (IN)
                         net (fo=0)                   0.000     0.000    clk_user_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.798     0.798 r  clk_user_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.798    clk_user_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.798 r  clk_user_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.132    clk_user_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.160 r  clk_user_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=8671, routed)        2.540     3.700    shift_reg_tap_i/clk_user_c
    SLICE_X107Y436       FDRE                                         r  shift_reg_tap_i/sr_p.sr_11[117]/C
                         clock pessimism             -0.475     3.225    
    SLICE_X107Y436       FDRE (Hold_EFF_SLICEM_C_D)
                                                      0.053     3.278    shift_reg_tap_i/sr_p.sr_11[117]
  -------------------------------------------------------------------
                         required time                         -3.278    
                         arrival time                           3.280    
  -------------------------------------------------------------------
                         slack                                  0.002    

Slack (MET) :             0.003ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_8[233]/C
                            (rising edge-triggered cell FDRE clocked by clk_user  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_9[233]/D
                            (rising edge-triggered cell FDRE clocked by clk_user  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk_user
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk_user rise@0.000ns - clk_user rise@0.000ns)
  Data Path Delay:        0.216ns  (logic 0.071ns (32.870%)  route 0.145ns (67.130%))
  Logic Levels:           0  
  Clock Path Skew:        0.160ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.619ns
    Source Clock Delay      (SCD):    2.984ns
    Clock Pessimism Removal (CPR):    0.476ns
  Clock Net Delay (Source):      2.135ns (routing 1.133ns, distribution 1.002ns)
  Clock Net Delay (Destination): 2.459ns (routing 1.244ns, distribution 1.215ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk_user rise edge)
                                                      0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk_user (IN)
                         net (fo=0)                   0.000     0.000    clk_user_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.529     0.529 r  clk_user_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.529    clk_user_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.529 r  clk_user_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.825    clk_user_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.849 r  clk_user_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=8671, routed)        2.135     2.984    shift_reg_tap_i/clk_user_c
    SLICE_X108Y434       FDRE                                         r  shift_reg_tap_i/sr_p.sr_8[233]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X108Y434       FDRE (Prop_HFF2_SLICEM_C_Q)
                                                      0.071     3.055 r  shift_reg_tap_i/sr_p.sr_8[233]/Q
                         net (fo=1, routed)           0.145     3.200    shift_reg_tap_i/sr_8[233]
    SLICE_X107Y433       FDRE                                         r  shift_reg_tap_i/sr_p.sr_9[233]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk_user rise edge)
                                                      0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk_user (IN)
                         net (fo=0)                   0.000     0.000    clk_user_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.798     0.798 r  clk_user_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.798    clk_user_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.798 r  clk_user_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.132    clk_user_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.160 r  clk_user_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=8671, routed)        2.459     3.619    shift_reg_tap_i/clk_user_c
    SLICE_X107Y433       FDRE                                         r  shift_reg_tap_i/sr_p.sr_9[233]/C
                         clock pessimism             -0.476     3.144    
    SLICE_X107Y433       FDRE (Hold_FFF_SLICEM_C_D)
                                                      0.053     3.197    shift_reg_tap_i/sr_p.sr_9[233]
  -------------------------------------------------------------------
                         required time                         -3.197    
                         arrival time                           3.200    
  -------------------------------------------------------------------
                         slack                                  0.003    

Slack (MET) :             0.003ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_8[233]/C
                            (rising edge-triggered cell FDRE clocked by clk_user  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_9[233]/D
                            (rising edge-triggered cell FDRE clocked by clk_user  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk_user
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk_user rise@0.000ns - clk_user rise@0.000ns)
  Data Path Delay:        0.216ns  (logic 0.071ns (32.870%)  route 0.145ns (67.130%))
  Logic Levels:           0  
  Clock Path Skew:        0.160ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.619ns
    Source Clock Delay      (SCD):    2.984ns
    Clock Pessimism Removal (CPR):    0.476ns
  Clock Net Delay (Source):      2.135ns (routing 1.133ns, distribution 1.002ns)
  Clock Net Delay (Destination): 2.459ns (routing 1.244ns, distribution 1.215ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk_user rise edge)
                                                      0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk_user (IN)
                         net (fo=0)                   0.000     0.000    clk_user_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.529     0.529 r  clk_user_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.529    clk_user_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.529 r  clk_user_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.825    clk_user_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.849 r  clk_user_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=8671, routed)        2.135     2.984    shift_reg_tap_i/clk_user_c
    SLICE_X108Y434       FDRE                                         r  shift_reg_tap_i/sr_p.sr_8[233]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X108Y434       FDRE (Prop_HFF2_SLICEM_C_Q)
                                                      0.071     3.055 f  shift_reg_tap_i/sr_p.sr_8[233]/Q
                         net (fo=1, routed)           0.145     3.200    shift_reg_tap_i/sr_8[233]
    SLICE_X107Y433       FDRE                                         f  shift_reg_tap_i/sr_p.sr_9[233]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk_user rise edge)
                                                      0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk_user (IN)
                         net (fo=0)                   0.000     0.000    clk_user_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.798     0.798 r  clk_user_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.798    clk_user_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.798 r  clk_user_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.132    clk_user_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.160 r  clk_user_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=8671, routed)        2.459     3.619    shift_reg_tap_i/clk_user_c
    SLICE_X107Y433       FDRE                                         r  shift_reg_tap_i/sr_p.sr_9[233]/C
                         clock pessimism             -0.476     3.144    
    SLICE_X107Y433       FDRE (Hold_FFF_SLICEM_C_D)
                                                      0.053     3.197    shift_reg_tap_i/sr_p.sr_9[233]
  -------------------------------------------------------------------
                         required time                         -3.197    
                         arrival time                           3.200    
  -------------------------------------------------------------------
                         slack                                  0.003    

Slack (MET) :             0.011ns  (arrival time - required time)
  Source:                 shift_reg_tap_i/sr_p.sr_6[108]/C
                            (rising edge-triggered cell FDRE clocked by clk_user  {rise@0.000ns fall@3.125ns period=6.250ns})
  Destination:            shift_reg_tap_i/sr_p.sr_7[108]/D
                            (rising edge-triggered cell FDRE clocked by clk_user  {rise@0.000ns fall@3.125ns period=6.250ns})
  Path Group:             clk_user
  Path Type:              Hold (Min at Slow Process Corner)
  Requirement:            0.000ns  (clk_user rise@0.000ns - clk_user rise@0.000ns)
  Data Path Delay:        0.181ns  (logic 0.072ns (39.779%)  route 0.109ns (60.221%))
  Logic Levels:           0  
  Clock Path Skew:        0.115ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    3.545ns
    Source Clock Delay      (SCD):    3.008ns
    Clock Pessimism Removal (CPR):    0.423ns
  Clock Net Delay (Source):      2.159ns (routing 1.133ns, distribution 1.026ns)
  Clock Net Delay (Destination): 2.385ns (routing 1.244ns, distribution 1.141ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk_user rise edge)
                                                      0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk_user (IN)
                         net (fo=0)                   0.000     0.000    clk_user_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.529     0.529 r  clk_user_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.529    clk_user_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.529 r  clk_user_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.296     0.825    clk_user_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.024     0.849 r  clk_user_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=8671, routed)        2.159     3.008    shift_reg_tap_i/clk_user_c
    SLICE_X98Y481        FDRE                                         r  shift_reg_tap_i/sr_p.sr_6[108]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y481        FDRE (Prop_GFF2_SLICEL_C_Q)
                                                      0.072     3.080 r  shift_reg_tap_i/sr_p.sr_6[108]/Q
                         net (fo=1, routed)           0.109     3.189    shift_reg_tap_i/sr_6[108]
    SLICE_X98Y478        FDRE                                         r  shift_reg_tap_i/sr_p.sr_7[108]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk_user rise edge)
                                                      0.000     0.000 r  
    AW33                                              0.000     0.000 r  clk_user (IN)
                         net (fo=0)                   0.000     0.000    clk_user_ibuf_iso/I
    AW33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.798     0.798 r  clk_user_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.798    clk_user_ibuf_iso/OUT
    AW33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.798 r  clk_user_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.334     1.132    clk_user_ibuf_iso
    BUFGCE_X1Y218        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.028     1.160 r  clk_user_ibuf/O
    X3Y7 (CLOCK_ROOT)    net (fo=8671, routed)        2.385     3.545    shift_reg_tap_i/clk_user_c
    SLICE_X98Y478        FDRE                                         r  shift_reg_tap_i/sr_p.sr_7[108]/C
                         clock pessimism             -0.423     3.123    
    SLICE_X98Y478        FDRE (Hold_BFF2_SLICEL_C_D)
                                                      0.055     3.178    shift_reg_tap_i/sr_p.sr_7[108]
  -------------------------------------------------------------------
                         required time                         -3.178    
                         arrival time                           3.189    
  -------------------------------------------------------------------
                         slack                                  0.011    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_user
Waveform(ns):       { 0.000 3.125 }
Period(ns):         6.250
Sources:            { clk_user }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I  n/a            1.499         6.250       4.751      BUFGCE_X1Y218   clk_user_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X108Y439  muon_sorter_1/sr_p.sr_1_10.roi_ret/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X108Y444  muon_sorter_1/sr_p.sr_1_15.sector_ret_3200/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X105Y445  muon_sorter_1/sr_p.sr_1_15.sector_ret_3201/C
Min Period        n/a     FDRE/C    n/a            0.550         6.250       5.700      SLICE_X105Y434  muon_sorter_1/sr_p.sr_1_7.pt_ret_88/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X104Y492  shift_reg_tap_i/sr_p.sr_4[28]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X104Y499  shift_reg_tap_i/sr_p.sr_4[2]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X104Y492  shift_reg_tap_i/sr_p.sr_4[30]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X104Y490  shift_reg_tap_i/sr_p.sr_4[31]/C
Low Pulse Width   Fast    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X100Y487  shift_reg_tap_i/sr_p.sr_8[10]/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X108Y439  muon_sorter_1/sr_p.sr_1_10.roi_ret/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X108Y444  muon_sorter_1/sr_p.sr_1_15.sector_ret_3200/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X95Y464   muon_sorter_1/sr_p.sr_1_7.roi_ret/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X108Y444  muon_sorter_1/sr_p.sr_1_7.roi_ret_315/C
High Pulse Width  Slow    FDRE/C    n/a            0.275         3.125       2.850      SLICE_X104Y470  shift_reg_tap_i/sr_p.sr_12[64]/C



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y518  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y224          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X106Y504         lsfr_1/output_vector_1[255]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X107Y504         lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X101Y500         lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y488         lsfr_1/shiftreg_vector[131]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y488         lsfr_1/shiftreg_vector[132]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X104Y488         lsfr_1/shiftreg_vector[133]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y518  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X101Y500         lsfr_1/shiftreg_vector[100]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X101Y488         lsfr_1/shiftreg_vector[107]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X101Y488         lsfr_1/shiftreg_vector[108]/C



