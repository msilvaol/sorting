Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------
| Tool Version      : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date              : Thu Feb 14 18:08:16 2019
| Host              : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command           : report_timing_summary -hold -nworst 5 -max_paths 5 -file timing_hold.txt
| Design            : wrapper
| Device            : xcvu9p-flgc2104
| Speed File        : -1  PRODUCTION 1.20 05-21-2018
| Temperature Grade : E
-----------------------------------------------------------------------------------------------

Timing Summary Report

------------------------------------------------------------------------------------------------
| Timer Settings
| --------------
------------------------------------------------------------------------------------------------

  Enable Multi Corner Analysis               :  Yes
  Enable Pessimism Removal                   :  Yes
  Pessimism Removal Resolution               :  Nearest Common Node
  Enable Input Delay Default Clock           :  No
  Enable Preset / Clear Arcs                 :  No
  Disable Flight Delays                      :  No
  Ignore I/O Paths                           :  No
  Timing Early Launch at Borrowing Latches   :  false

  Corner  Analyze    Analyze    
  Name    Max Paths  Min Paths  
  ------  ---------  ---------  
  Slow    Yes        Yes        
  Fast    Yes        Yes        



check_timing report

Table of Contents
-----------------
1. checking no_clock
2. checking constant_clock
3. checking pulse_width_clock
4. checking unconstrained_internal_endpoints
5. checking no_input_delay
6. checking no_output_delay
7. checking multiple_clock
8. checking generated_clocks
9. checking loops
10. checking partial_input_delay
11. checking partial_output_delay
12. checking latch_loops

1. checking no_clock
--------------------
 There are 0 register/latch pins with no clock.


2. checking constant_clock
--------------------------
 There are 0 register/latch pins with constant_clock.


3. checking pulse_width_clock
-----------------------------
 There are 0 register/latch pins which need pulse_width check


4. checking unconstrained_internal_endpoints
--------------------------------------------
 There are 0 pins that are not constrained for maximum delay.

 There are 0 pins that are not constrained for maximum delay due to constant clock.


5. checking no_input_delay
--------------------------
 There is 1 input port with no input delay specified. (HIGH)

 There are 0 input ports with no input delay but user has a false path constraint.


6. checking no_output_delay
---------------------------
 There is 1 port with no output delay specified. (HIGH)

 There are 0 ports with no output delay but user has a false path constraint

 There are 0 ports with no output delay but with a timing clock defined on it or propagating through it


7. checking multiple_clock
--------------------------
 There are 0 register/latch pins with multiple clocks.


8. checking generated_clocks
----------------------------
 There are 0 generated clocks that are not connected to a clock source.


9. checking loops
-----------------
 There are 0 combinational loops in the design.


10. checking partial_input_delay
--------------------------------
 There are 0 input ports with partial input delay specified.


11. checking partial_output_delay
---------------------------------
 There are 0 ports with partial output delay specified.


12. checking latch_loops
------------------------
 There are 0 combinational latch loops in the design through latch input



------------------------------------------------------------------------------------------------
| Design Timing Summary
| ---------------------
------------------------------------------------------------------------------------------------

    WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
    -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
      0.020        0.000                      0                 2366        5.677        0.000                       0                  3214  


All user specified timing constraints are met.


------------------------------------------------------------------------------------------------
| Clock Summary
| -------------
------------------------------------------------------------------------------------------------

Clock        Waveform(ns)         Period(ns)      Frequency(MHz)
-----        ------------         ----------      --------------
clk          {0.000 6.250}        12.500          80.000          
clk_wrapper  {0.000 500.000}      1000.000        1.000           


------------------------------------------------------------------------------------------------
| Intra Clock Table
| -----------------
------------------------------------------------------------------------------------------------

Clock             WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints     WPWS(ns)     TPWS(ns)  TPWS Failing Endpoints  TPWS Total Endpoints  
-----             -------      -------  ---------------------  -------------------     --------     --------  ----------------------  --------------------  
clk                 0.020        0.000                      0                 2366        5.677        0.000                       0                  2615  
clk_wrapper                                                                             498.562        0.000                       0                   599  


------------------------------------------------------------------------------------------------
| Inter Clock Table
| -----------------
------------------------------------------------------------------------------------------------

From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Other Path Groups Table
| -----------------------
------------------------------------------------------------------------------------------------

Path Group    From Clock    To Clock          WHS(ns)      THS(ns)  THS Failing Endpoints  THS Total Endpoints  
----------    ----------    --------          -------      -------  ---------------------  -------------------  


------------------------------------------------------------------------------------------------
| Timing Details
| --------------
------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
From Clock:  clk
  To Clock:  clk

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :            0  Failing Endpoints,  Worst Slack        0.020ns,  Total Violation        0.000ns
PW    :            0  Failing Endpoints,  Worst Slack        5.677ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Min Delay Paths
--------------------------------------------------------------------------------------
Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_2[121]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_3[121]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.495ns
    Source Clock Delay      (SCD):    2.085ns
    Clock Pessimism Removal (CPR):    0.404ns
  Clock Net Delay (Source):      1.539ns (routing 1.000ns, distribution 0.539ns)
  Clock Net Delay (Destination): 1.723ns (routing 1.105ns, distribution 0.618ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=2614, routed)        1.539     2.085    shift_reg_tap_o/clk_c
    SLICE_X98Y327        FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[121]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y327        FDRE (Prop_EFF_SLICEL_C_Q)
                                                      0.039     2.124 r  shift_reg_tap_o/sr_p.sr_2[121]/Q
                         net (fo=1, routed)           0.034     2.158    shift_reg_tap_o/sr_2[121]
    SLICE_X98Y327        FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[121]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=2614, routed)        1.723     2.495    shift_reg_tap_o/clk_c
    SLICE_X98Y327        FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[121]/C
                         clock pessimism             -0.404     2.091    
    SLICE_X98Y327        FDRE (Hold_GFF2_SLICEL_C_D)
                                                      0.047     2.138    shift_reg_tap_o/sr_p.sr_3[121]
  -------------------------------------------------------------------
                         required time                         -2.138    
                         arrival time                           2.158    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_2[121]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_3[121]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.495ns
    Source Clock Delay      (SCD):    2.085ns
    Clock Pessimism Removal (CPR):    0.404ns
  Clock Net Delay (Source):      1.539ns (routing 1.000ns, distribution 0.539ns)
  Clock Net Delay (Destination): 1.723ns (routing 1.105ns, distribution 0.618ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=2614, routed)        1.539     2.085    shift_reg_tap_o/clk_c
    SLICE_X98Y327        FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[121]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X98Y327        FDRE (Prop_EFF_SLICEL_C_Q)
                                                      0.039     2.124 f  shift_reg_tap_o/sr_p.sr_2[121]/Q
                         net (fo=1, routed)           0.034     2.158    shift_reg_tap_o/sr_2[121]
    SLICE_X98Y327        FDRE                                         f  shift_reg_tap_o/sr_p.sr_3[121]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=2614, routed)        1.723     2.495    shift_reg_tap_o/clk_c
    SLICE_X98Y327        FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[121]/C
                         clock pessimism             -0.404     2.091    
    SLICE_X98Y327        FDRE (Hold_GFF2_SLICEL_C_D)
                                                      0.047     2.138    shift_reg_tap_o/sr_p.sr_3[121]
  -------------------------------------------------------------------
                         required time                         -2.138    
                         arrival time                           2.158    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_2[124]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_3[124]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.506ns
    Source Clock Delay      (SCD):    2.095ns
    Clock Pessimism Removal (CPR):    0.405ns
  Clock Net Delay (Source):      1.549ns (routing 1.000ns, distribution 0.549ns)
  Clock Net Delay (Destination): 1.734ns (routing 1.105ns, distribution 0.629ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=2614, routed)        1.549     2.095    shift_reg_tap_o/clk_c
    SLICE_X101Y331       FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[124]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y331       FDRE (Prop_EFF_SLICEL_C_Q)
                                                      0.039     2.134 r  shift_reg_tap_o/sr_p.sr_2[124]/Q
                         net (fo=1, routed)           0.034     2.168    shift_reg_tap_o/sr_2[124]
    SLICE_X101Y331       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[124]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=2614, routed)        1.734     2.506    shift_reg_tap_o/clk_c
    SLICE_X101Y331       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[124]/C
                         clock pessimism             -0.405     2.101    
    SLICE_X101Y331       FDRE (Hold_GFF2_SLICEL_C_D)
                                                      0.047     2.148    shift_reg_tap_o/sr_p.sr_3[124]
  -------------------------------------------------------------------
                         required time                         -2.148    
                         arrival time                           2.168    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_2[124]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_3[124]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.506ns
    Source Clock Delay      (SCD):    2.095ns
    Clock Pessimism Removal (CPR):    0.405ns
  Clock Net Delay (Source):      1.549ns (routing 1.000ns, distribution 0.549ns)
  Clock Net Delay (Destination): 1.734ns (routing 1.105ns, distribution 0.629ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=2614, routed)        1.549     2.095    shift_reg_tap_o/clk_c
    SLICE_X101Y331       FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[124]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y331       FDRE (Prop_EFF_SLICEL_C_Q)
                                                      0.039     2.134 f  shift_reg_tap_o/sr_p.sr_2[124]/Q
                         net (fo=1, routed)           0.034     2.168    shift_reg_tap_o/sr_2[124]
    SLICE_X101Y331       FDRE                                         f  shift_reg_tap_o/sr_p.sr_3[124]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=2614, routed)        1.734     2.506    shift_reg_tap_o/clk_c
    SLICE_X101Y331       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[124]/C
                         clock pessimism             -0.405     2.101    
    SLICE_X101Y331       FDRE (Hold_GFF2_SLICEL_C_D)
                                                      0.047     2.148    shift_reg_tap_o/sr_p.sr_3[124]
  -------------------------------------------------------------------
                         required time                         -2.148    
                         arrival time                           2.168    
  -------------------------------------------------------------------
                         slack                                  0.020    

Slack (MET) :             0.020ns  (arrival time - required time)
  Source:                 shift_reg_tap_o/sr_p.sr_2[84]/C
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Destination:            shift_reg_tap_o/sr_p.sr_3[84]/D
                            (rising edge-triggered cell FDRE clocked by clk  {rise@0.000ns fall@6.250ns period=12.500ns})
  Path Group:             clk
  Path Type:              Hold (Min at Fast Process Corner)
  Requirement:            0.000ns  (clk rise@0.000ns - clk rise@0.000ns)
  Data Path Delay:        0.073ns  (logic 0.039ns (53.425%)  route 0.034ns (46.575%))
  Logic Levels:           0  
  Clock Path Skew:        0.006ns (DCD - SCD - CPR)
    Destination Clock Delay (DCD):    2.495ns
    Source Clock Delay      (SCD):    2.086ns
    Clock Pessimism Removal (CPR):    0.403ns
  Clock Net Delay (Source):      1.540ns (routing 1.000ns, distribution 0.540ns)
  Clock Net Delay (Destination): 1.723ns (routing 1.105ns, distribution 0.618ns)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.385     0.385 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.385    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.385 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.144     0.529    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.017     0.546 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=2614, routed)        1.540     2.086    shift_reg_tap_o/clk_c
    SLICE_X101Y328       FDRE                                         r  shift_reg_tap_o/sr_p.sr_2[84]/C
  -------------------------------------------------------------------    -------------------
    SLICE_X101Y328       FDRE (Prop_AFF_SLICEL_C_Q)
                                                      0.039     2.125 r  shift_reg_tap_o/sr_p.sr_2[84]/Q
                         net (fo=1, routed)           0.034     2.159    shift_reg_tap_o/sr_2[84]
    SLICE_X101Y328       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[84]/D
  -------------------------------------------------------------------    -------------------

                         (clock clk rise edge)        0.000     0.000 r  
    AV33                                              0.000     0.000 r  clk (IN)
                         net (fo=0)                   0.000     0.000    clk_ibuf_iso/I
    AV33                 INBUF (Prop_INBUF_HPIOB_M_PAD_O)
                                                      0.572     0.572 r  clk_ibuf_iso/INBUF_INST/O
                         net (fo=1, routed)           0.000     0.572    clk_ibuf_iso/OUT
    AV33                 IBUFCTRL (Prop_IBUFCTRL_HPIOB_M_I_O)
                                                      0.000     0.572 r  clk_ibuf_iso/IBUFCTRL_INST/O
                         net (fo=1, routed)           0.181     0.753    clk_ibuf_iso
    BUFGCE_X1Y224        BUFGCE (Prop_BUFCE_BUFGCE_I_O)
                                                      0.019     0.772 r  clk_ibuf/O
    X2Y5 (CLOCK_ROOT)    net (fo=2614, routed)        1.723     2.495    shift_reg_tap_o/clk_c
    SLICE_X101Y328       FDRE                                         r  shift_reg_tap_o/sr_p.sr_3[84]/C
                         clock pessimism             -0.403     2.092    
    SLICE_X101Y328       FDRE (Hold_BFF2_SLICEL_C_D)
                                                      0.047     2.139    shift_reg_tap_o/sr_p.sr_3[84]
  -------------------------------------------------------------------
                         required time                         -2.139    
                         arrival time                           2.159    
  -------------------------------------------------------------------
                         slack                                  0.020    





Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk
Waveform(ns):       { 0.000 6.250 }
Period(ns):         12.500
Sources:            { clk }

Check Type        Corner  Lib Pin     Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location        Pin
Min Period        n/a     BUFGCE/I    n/a            1.499         12.500      11.001     BUFGCE_X1Y224   clk_ibuf/I
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X80Y338   muon_sorter_1/sr_3_1.roi_2_sr_3_0.sector_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X80Y338   muon_sorter_1/sr_3_1.roi_3_sr_3_0.sector_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X99Y336   muon_sorter_1/sr_3_4.sector_0_sr_3_0.sector_1/CLK
Min Period        n/a     SRL16E/CLK  n/a            1.146         12.500      11.354     SLICE_X99Y336   muon_sorter_1/sr_3_4.sector_1_sr_3_0.sector_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X76Y343   muon_sorter_1/sr_3_1.roi_5_sr_3_0.sector_1/CLK
Low Pulse Width   Fast    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X88Y336   muon_sorter_1/sr_3_0.roi_sr_3_0.sector_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X74Y344   muon_sorter_1/sr_3_1.pt_0_sr_3_0.sector_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X74Y344   muon_sorter_1/sr_3_1.pt_1_sr_3_0.sector_1/CLK
Low Pulse Width   Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X74Y344   muon_sorter_1/sr_3_1.pt_2_sr_3_0.sector_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X100Y331  muon_sorter_1/sr_3_4.sector_2_sr_3_0.sector_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X99Y333   muon_sorter_1/sr_3_4.sector_sr_3_0.sector_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X100Y327  muon_sorter_1/sr_3_5.pt_1_sr_3_0.sector_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X100Y327  muon_sorter_1/sr_3_5.roi_2_sr_3_0.sector_1/CLK
High Pulse Width  Slow    SRL16E/CLK  n/a            0.573         6.250       5.677      SLICE_X100Y327  muon_sorter_1/sr_3_5.roi_sr_3_0.sector_1/CLK



---------------------------------------------------------------------------------------------------
From Clock:  clk_wrapper
  To Clock:  clk_wrapper

Setup :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
Hold  :           NA  Failing Endpoints,  Worst Slack           NA  ,  Total Violation           NA
PW    :            0  Failing Endpoints,  Worst Slack      498.562ns,  Total Violation        0.000ns
---------------------------------------------------------------------------------------------------


Pulse Width Checks
--------------------------------------------------------------------------------------
Clock Name:         clk_wrapper
Waveform(ns):       { 0.000 500.000 }
Period(ns):         1000.000
Sources:            { clk_wrapper }

Check Type        Corner  Lib Pin   Reference Pin  Required(ns)  Actual(ns)  Slack(ns)  Location               Pin
Min Period        n/a     FDRE/C    n/a            3.195         1000.000    996.805    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Min Period        n/a     BUFGCE/I  n/a            1.499         1000.000    998.501    BUFGCE_X1Y230          clk_wrapper_ibuf/I
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X83Y321          lsfr_1/output_vector_1[255]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X86Y325          lsfr_1/shiftreg_vector[0]/C
Min Period        n/a     FDRE/C    n/a            0.550         1000.000    999.450    SLICE_X72Y319          lsfr_1/shiftreg_vector[100]/C
Low Pulse Width   Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X83Y321          lsfr_1/output_vector_1[255]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X88Y323          reducer_1/delay_block[0][132]/C
Low Pulse Width   Slow    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X88Y323          reducer_1/delay_block[0][133]/C
High Pulse Width  Fast    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Slow    FDRE/C    n/a            1.438         500.000     498.562    BITSLICE_RX_TX_X1Y483  reducer_1/delay_block[4][0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X86Y325          lsfr_1/shiftreg_vector[0]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X72Y319          lsfr_1/shiftreg_vector[100]/C
High Pulse Width  Fast    FDRE/C    n/a            0.275         500.000     499.725    SLICE_X67Y315          lsfr_1/shiftreg_vector[101]/C



