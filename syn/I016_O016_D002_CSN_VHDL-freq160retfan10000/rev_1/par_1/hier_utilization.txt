Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Tue Mar  5 23:15:17 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_utilization -hierarchical -file hier_utilization.txt
| Design       : wrapper_csn
| Device       : xcvu9pflgc2104-1
| Design State : Routed
------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+-------------------------------------+--------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
|               Instance              |                            Module                            | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs | RAMB36 | RAMB18 | URAM | DSP48 Blocks |
+-------------------------------------+--------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
| wrapper_csn                         |                                                        (top) |        931 |        931 |       0 |    0 | 1402 |      0 |      0 |    0 |            0 |
|   (wrapper_csn)                     |                                                        (top) |          0 |          0 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   dut_inst                          | muon_sorter_I016_O016_D002_CSN_VHDL-freq160retfan10000_rev_1 |        861 |        861 |       0 |    0 |  500 |      0 |      0 |    0 |            0 |
|     (dut_inst)                      | muon_sorter_I016_O016_D002_CSN_VHDL-freq160retfan10000_rev_1 |          0 |          0 |       0 |    0 |  500 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.0.csn_cmp_inst |                                 csn_cmp_false_false_false_40 |          4 |          4 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.1.csn_cmp_inst |                                 csn_cmp_false_false_false_45 |          5 |          5 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_36 |          4 |          4 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.3.csn_cmp_inst |                                 csn_cmp_false_false_false_35 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.4.csn_cmp_inst |                                 csn_cmp_false_false_false_21 |          6 |          6 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.5.csn_cmp_inst |                                 csn_cmp_false_false_false_24 |          4 |          4 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.6.csn_cmp_inst |                                 csn_cmp_false_false_false_51 |          4 |          4 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.0.pair_g.7.csn_cmp_inst |                                 csn_cmp_false_false_false_29 |          5 |          5 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.0.csn_cmp_inst |                                 csn_cmp_false_false_false_16 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.1.csn_cmp_inst |                                  csn_cmp_false_false_false_1 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.2.csn_cmp_inst |                                  csn_cmp_false_false_false_4 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.3.csn_cmp_inst |                                 csn_cmp_false_false_false_25 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.4.csn_cmp_inst |                                 csn_cmp_false_false_false_26 |          6 |          6 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.5.csn_cmp_inst |                                  csn_cmp_false_false_false_0 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.6.csn_cmp_inst |                                 csn_cmp_false_false_false_44 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.1.pair_g.7.csn_cmp_inst |                                 csn_cmp_false_false_false_11 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.0.csn_cmp_inst |                                 csn_cmp_false_false_false_34 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.1.csn_cmp_inst |                                 csn_cmp_false_false_false_39 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_23 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.3.csn_cmp_inst |                                 csn_cmp_false_false_false_50 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.4.csn_cmp_inst |                                 csn_cmp_false_false_false_14 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.5.csn_cmp_inst |                                 csn_cmp_false_false_false_15 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.6.csn_cmp_inst |                                 csn_cmp_false_false_false_30 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.2.pair_g.7.csn_cmp_inst |                                  csn_cmp_false_false_false_3 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.0.csn_cmp_inst |                                 csn_cmp_false_false_false_59 |          6 |          6 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.1.csn_cmp_inst |                                 csn_cmp_false_false_false_58 |          4 |          4 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_10 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.3.csn_cmp_inst |                                 csn_cmp_false_false_false_41 |          3 |          3 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.4.csn_cmp_inst |                                 csn_cmp_false_false_false_42 |          5 |          5 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.5.csn_cmp_inst |                                 csn_cmp_false_false_false_37 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.6.csn_cmp_inst |                                 csn_cmp_false_false_false_38 |          6 |          6 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.3.pair_g.7.csn_cmp_inst |                                 csn_cmp_false_false_false_22 |          6 |          6 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.0.csn_cmp_inst |                                 csn_cmp_false_false_false_49 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.1.csn_cmp_inst |                                 csn_cmp_false_false_false_28 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_27 |        311 |        311 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.3.csn_cmp_inst |                                    csn_cmp_false_false_false |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.4.csn_cmp_inst |                                  csn_cmp_false_false_false_2 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.5.csn_cmp_inst |                                  csn_cmp_false_false_false_5 |         11 |         11 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.4.pair_g.6.csn_cmp_inst |                                 csn_cmp_false_false_false_57 |          6 |          6 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.0.csn_cmp_inst |                                  csn_cmp_false_false_false_6 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.1.csn_cmp_inst |                                  csn_cmp_false_false_false_9 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_52 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.3.csn_cmp_inst |                                 csn_cmp_false_false_false_54 |         13 |         13 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.4.csn_cmp_inst |                                 csn_cmp_false_false_false_56 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.5.csn_cmp_inst |                                 csn_cmp_false_false_false_31 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.5.pair_g.6.csn_cmp_inst |                                 csn_cmp_false_false_false_43 |          3 |          3 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.0.csn_cmp_inst |                                 csn_cmp_false_false_false_32 |         12 |         12 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.1.csn_cmp_inst |                                 csn_cmp_false_false_false_48 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_47 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.3.csn_cmp_inst |                                 csn_cmp_false_false_false_20 |          7 |          7 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.4.csn_cmp_inst |                                 csn_cmp_false_false_false_19 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.6.pair_g.5.csn_cmp_inst |                                 csn_cmp_false_false_false_18 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.0.csn_cmp_inst |                                 csn_cmp_false_false_false_53 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.1.csn_cmp_inst |                                 csn_cmp_false_false_false_55 |         10 |         10 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_13 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.7.pair_g.3.csn_cmp_inst |                                 csn_cmp_false_false_false_12 |          9 |          9 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.0.csn_cmp_inst |                                 csn_cmp_false_false_false_17 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.1.csn_cmp_inst |                                 csn_cmp_false_false_false_46 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.2.csn_cmp_inst |                                 csn_cmp_false_false_false_33 |         15 |         15 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.3.csn_cmp_inst |                                  csn_cmp_false_false_false_7 |         14 |         14 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|     stage_g.8.pair_g.4.csn_cmp_inst |                                  csn_cmp_false_false_false_8 |          8 |          8 |       0 |    0 |    0 |      0 |      0 |    0 |            0 |
|   lsfr_1                            |                                                         lfsr |          1 |          1 |       0 |    0 |  209 |      0 |      0 |    0 |            0 |
|   reducer_1                         |                                                      reducer |         69 |         69 |       0 |    0 |  277 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_i                   |                                          shift_reg_tap_208_1 |          0 |          0 |       0 |    0 |  208 |      0 |      0 |    0 |            0 |
|   shift_reg_tap_o                   |                                          shift_reg_tap_256_1 |          0 |          0 |       0 |    0 |  208 |      0 |      0 |    0 |            0 |
+-------------------------------------+--------------------------------------------------------------+------------+------------+---------+------+------+--------+--------+------+--------------+
* Note: The sum of lower-level cells may be larger than their parent cells total, due to cross-hierarchy LUT combining


