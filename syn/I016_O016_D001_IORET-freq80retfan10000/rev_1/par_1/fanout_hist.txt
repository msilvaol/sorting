Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
------------------------------------------------------------------------------------
| Tool Version : Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
| Date         : Wed Feb 13 20:53:02 2019
| Host         : PCPHESEBE02 running 64-bit Service Pack 1  (build 7601)
| Command      : report_high_fanout_nets -histogram -file fanout_hist.txt
| Design       : wrapper
| Device       : xcvu9p
------------------------------------------------------------------------------------

High Fan-out Nets Information

1. Histogram
------------

+---------+-------+--------+
|  Fanout |  Nets |      % |
+---------+-------+--------+
|       1 |  7222 |  61.50 |
|       2 |   959 |   8.16 |
|       3 |   844 |   7.18 |
|       4 |   471 |   4.01 |
|    5-10 |  1136 |   9.67 |
|   11-50 |  1107 |   9.42 |
|  51-100 |     4 |   0.03 |
| 101-500 |     0 |   0.00 |
|    >500 |     0 |   0.00 |
+---------+-------+--------+
|     ALL | 11743 | 100.00 |
+---------+-------+--------+


