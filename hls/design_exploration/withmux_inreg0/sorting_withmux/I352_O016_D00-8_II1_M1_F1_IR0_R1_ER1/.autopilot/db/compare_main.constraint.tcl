set clock_constraint { \
    name clk \
    module compare_main \
    port ap_clk \
    period 6.25 \
    uncertainty 0.8 \
}

set all_path {}

set false_path {}

