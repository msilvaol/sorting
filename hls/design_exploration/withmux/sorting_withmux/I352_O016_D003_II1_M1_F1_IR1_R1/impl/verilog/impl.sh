#!/bin/sh
# ==============================================================
# Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.2.1 (64-bit)
# Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
# ==============================================================
/softs/xilinx/vivado/2019.2_lin64/bin/vivado  -notrace -mode batch -source run_vivado.tcl || exit $?


