#ifndef __PAIRS_5__
#define __PAIRS_5__

#define N 4

const int np = 5;
const int pairs[np][2] =
{
		{0,1},
		{2,3},
		{0,2},
		{1,3},
		{1,2}
};

#endif