library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std_unsigned.all;

package SortingHierarchical is

	-- 2D matrix of std_logic	
	type std_logic_matrix is array (natural range <>) of std_logic_vector;
	type pt_compare_matrix_t is array (integer range <>, natural range <>) of std_logic_vector;


end package SortingHierarchical;

package body SortingHierarchical is

end package body SortingHierarchical;
